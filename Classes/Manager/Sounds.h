#ifndef PuppyMadness__Sounds
#define PuppyMadness__Sounds

#include "cocos2d.h"


typedef enum _sound_name
{
    SOUND_PUPPY_SPAWN,
    SOUND_PUPPY_JUMP,
    SOUND_PUPPY_BOUNCE,
    SOUND_PUPPY_CATCH,
    SOUND_PUPPY_GROUNDED,
    SOUND_COINS,
    SOUND_GLORIFY_1,
    SOUND_GLORIFY_2,
    SOUND_GLORIFY_3,
    SOUND_GLORIFY_4,
    SOUND_GLORIFY_5,
    SOUND_RANDOM_LIVES,
    SOUND_RANDOM_SPEED,
    SOUND_POWERUP_READY,
    SOUND_POWERUP_VORTEX_ACTIVE,
    SOUND_POWERUP_MAGNET_ACTIVE,
    SOUND_POWERUP_HELIUM_ACTIVE,
    SOUND_POWERUP_CLEAN_ACTIVE,
    SOUND_POWERUP_SLOWMO_ACTIVE,
    SOUND_POWERUP_ENDED,
    SOUND_BASKET_TELEPORTED,
    SOUND_BASKET_FENCED,
    SOUND_BASKET_BOMBED,
    SOUND_BOBO_DROP,
    SOUND_BOBO_BULLET,
    SOUND_SNAKE_ATTACK,
    SOUND_STORE_BUY_COINS,
    SOUND_STORE_BUY_DIAMONDS,
    SOUND_STORE_BUY_IAP,
    SOUND_STORE_EXCHANGE,
    SOUND_STORE_POPUP,
    SOUND_TAP_FORWARD,
    SOUND_TAP_BACK,
    SOUND_TAP_TAB,
    SOUND_BUY_PRODUCT,
    SOUND_BUY_IAP,
    SOUND_BUY_NO_COINS,
    SOUND_QUEST_PROGRESS,
    SOUND_QUEST_DONE,
    SOUND_TACTICS_EQUIP,
    SOUND_TACTICS_UNEQUIP,
    SOUND_TACTICS_CLEAR
    
} SoundName;

typedef struct _sound_file{
    std::string file;
    int count;
} SoundFile;

//
//typedef std::map<SoundName, std::pair<std::string, int> > SoundFile;

typedef enum _music_name
{
    MUSIC_TITLE
} MusicName;



class Sounds{
    
public:
    Sounds();
    ~Sounds();
    
    static Sounds* create();
    
    void init();
    
    void initSound(SoundName name, std::string file, int random);
    
    void preloadSounds();
    
    void playSound(SoundName sound);
    
    void playMusic(MusicName music);
    
    void toogleSound();
    
    bool isSoundOn();
    
    void toogleMusic();
    
    bool isMusicOn();
    
    void playBackgroundMusic(bool allowMusic);
    
    
    
    
private:
    
    bool _allowSound;
    bool _allowMusic;
    
    MusicName _currentMusic;
    bool _didStartPlayMusic = false;
    
    std::map<SoundName, SoundFile> _files;
    
};


#endif
