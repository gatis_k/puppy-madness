#ifndef PuppyMadness__QuestHandler
#define PuppyMadness__QuestHandler

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM || CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM)
#include "Manager/Quest.h"
#include "Utils/Jzon.h"

#else
#include "Quest.h"
#include "Jzon.h"

#endif

#include "cocos2d.h"




const int kMaxActiveQuests = 3;
const int kMaxActiveDailyQuests = 1;

class QuestHandler{
    
public:
    
    QuestHandler();
    ~QuestHandler();
    
    static QuestHandler* create();
    
    void init();
    
    std::vector<Quest*> getActiveGeneral(bool show = false);
    
    bool hasDailyQuest();
    
    void setDailyQuestTime();
    
    std::vector<Quest*> getActiveDaily(bool show = false);
    
    std::vector<Quest*> getGeneralQuestToClaim();
    
    std::vector<Quest*> getDailyQuestToClaim();
    
    
    int getTotalQuestCompleted(bool daily = false);
    
    Quest*  getGeneral(std::string tag);
    
    Quest*  getDaily(std::string tag);
    
    bool newQuestToShow();
    
    void setSeen();
    
    void reloadRoundVariables();
    
    void reset();
    
private:
    
    void parseQuest(std::string tag, bool daily = false);
    
    void recheckDailyQuestCounter();
    
    std::map<std::string, Quest*> _daily;
    
    std::map<std::string, Quest*> _general;
    
    int _questsCompleted;
    int _dailyQuestsCompleted;
    
    std::string _dailyCompletedTime;
    
    std::string getTimeString();
    
    std::vector<int> parseTimeString(std::string timeStr);
    
    std::string intToString(int nr);
    
    // just for idea sake: valentines day, easter special
    //std::map<std::string, Quest*> _special;
    
    
};



#endif /* PuppyMadness__QuestHandler */
