#ifndef PuppyMadness__Store
#define PuppyMadness__Store

#include "cocos2d.h"
#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Product.h"
#include "Manager/IAP.h"

#else
#include "Product.h"
#include "IAP.h"

#endif




class Store{
    
public:
    
    Store();
    
    ~Store();
    
    static Store* create();
    
    void init();
    
    void updateLocks(int userLevel = 1);
    
    std::vector<Product*> getProductsByGroup(ProductGroup group);
    
    std::vector<Product*> getProductsByLevel(int userLevel);
    
    Product* getProductByKey(std::string key);
    
    std::vector<IAP*> getAllIAP();
    
    IAP* getIAPByKey(std::string key);
    
    IAP* getExchangeByKey(std::string key);
    
    std::vector<IAP*> getAllExchange();
    
    void reset();
    
private:
    
    void initIAP(IAPDetails iap);
    
    void initOutfits();
    
    void initTalismans();

    void initPowerUps();
    
    void initStaticProducts();
    
    void initIAP();
    
    void initExchange();
    
    std::map<std::string, Product*> _products;
    
    std::map<std::string, IAP*> _iap;
    
    std::map<std::string, IAP*> _exchange;
    
    
};





#endif

