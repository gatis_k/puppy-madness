#ifndef PuppyMadness__User
#define PuppyMadness__User


#include "cocos2d.h"

class User{
public:
    
    User();
    ~User();
    
    static User* create();
    
    void init();
    
    bool isLevelUp();
    
    void levelUp();

    void depositeXP(int xp);
    
    int getUserLevel();
    
    std::string getUserLevelStr();
    
    bool isEndGame();
    
    int getUserXP(bool reduce = false);
    
    std::string getUserXPStr(bool reduce = false);
    
    int getUserXPNextLevel(bool reduce = false, int level = 0);
    
    std::string getUserXPNextLevelStr(bool reduce = false, int level = 0);
    
    std::string getTitle();
    
    void becomePuppyWhisperer();
    
    void reset();
    
private:
    
    std::vector<int> _levelBreakPoints;
    
    char _charText[100];
    
    int _level;
    
    int _levelMax;
    
    int _xp;
    
    int getLevelByXP();
    
};



#endif /* User_hpp */
