#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM || CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM)
#include "Manager/Sounds.h"

#else
#include "Sounds.h"

#endif
////////////////////////////


USING_NS_CC;

#include "SimpleAudioEngine.h"

using namespace CocosDenshion;


Sounds::Sounds()
{
   
}

Sounds::~Sounds()
{
    
}

Sounds* Sounds::create()
{
    Sounds* pointer = new Sounds;
    return pointer;
}

void Sounds::init()
{
    _allowSound = UserDefault::getInstance()->getIntegerForKey("sound", 1) == 1 ? true : false;
    _allowMusic = ( UserDefault::getInstance()->getIntegerForKey("music", 1) == 1 && _allowSound ) ? true : false;
    CCLOG("Sound: %d, Music: %d", _allowSound, _allowMusic);
    
    initSound(SOUND_PUPPY_JUMP, "puppy_jump_%i_ue.wav", 2);
    initSound(SOUND_PUPPY_BOUNCE, "puppy_bounce_%i_ue.wav", 0);
    initSound(SOUND_PUPPY_CATCH, "puppy_catch_%i_ue.wav", 3);
    initSound(SOUND_PUPPY_GROUNDED, "puppy_grounded_%i_hvz.wav", 0);
    initSound(SOUND_COINS, "coins_%i_hvz.wav", 0);
    initSound(SOUND_BOBO_BULLET, "bobo_bullet_%i_hvz.wav", 1);
    initSound(SOUND_BOBO_DROP, "bobo_drop_%i_ue.wav", 0);
    initSound(SOUND_TAP_FORWARD, "tap_settings_%i_ue.wav", 0);
    initSound(SOUND_TAP_BACK, "tap_simple_%i_ue.wav", 0);
    initSound(SOUND_TAP_TAB, "tap_tab_%i_ue.wav", 0);
    initSound(SOUND_STORE_EXCHANGE, "store_exchange.wav", 0);
    initSound(SOUND_STORE_BUY_COINS, "store_buy_coins.wav", 0);
    initSound(SOUND_STORE_BUY_DIAMONDS, "store_buy_diamonds.wav", 0);
    initSound(SOUND_STORE_BUY_IAP, "store_buy_iap.wav", 0);
    initSound(SOUND_STORE_POPUP, "store_popup.wav", 0);
}

void Sounds::initSound(SoundName name, std::string file, int count)
{
    SoundFile newFile{file, count};
    _files.insert(std::pair<SoundName,SoundFile>(name, newFile));
}

void Sounds::preloadSounds()
{
    CCLOG("PRELOADING SOUND SHOULD BE IMPROVED");
    char str[100] = { 0 };
    std::string soundStr = "";
    for (std::map<SoundName, SoundFile>::iterator it = _files.begin(); it != _files.end(); ++it)
    {
        for(int i = 0; i <= it->second.count; i++)
        {
            soundStr = it->second.file;
            sprintf(str, soundStr.c_str(), i);
            CCLOG("PRELOAD SOUND FILE %s", str);
            SimpleAudioEngine::getInstance()->preloadEffect(str);
        }
    }
}



void Sounds::playSound(SoundName sound)
{
    if(_allowSound)
    {
        char str[100] = { 0 };
        int random = 0;
        int randomCount = 0;
        std::string soundStr = "";
        
        
        if(_files.find(sound) ==  _files.end())
        {
            CCLOG("Sound not found ID: %d", sound);
            return;
        }
        else
        {
            soundStr = _files.at(sound).file;
            random = _files.at(sound).count;
        }
        
        if(randomCount > 0)
            random = (rand() % randomCount);
            
            //CCLOG("random %d", random);
        sprintf(str, soundStr.c_str(), random);
            
        SimpleAudioEngine::getInstance()->playEffect(str);
    }
}


void Sounds::playMusic(MusicName music)
{
    _currentMusic = music;
    
    if(_allowMusic)
    {
        std::string musicStr = "";
        
        switch(music)
        {
            case MUSIC_TITLE:
                musicStr = "title.mp3";
                break;
            default:
                CCLOG("Music not found");
                _didStartPlayMusic = false;
                return;
                break;
                
        }
        
        
        SimpleAudioEngine::getInstance()->playBackgroundMusic(musicStr.c_str(), true);
        SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(.25f);
        _didStartPlayMusic = true;
    }
    else
        _didStartPlayMusic = false;
    
}



void Sounds::toogleSound()
{
    _allowSound = _allowSound ? false : true;
    UserDefault::getInstance()->setIntegerForKey("sound", (_allowSound ? 1 : 0));
    
    if(!_allowSound)
        SimpleAudioEngine::getInstance()->pauseAllEffects();
    else
        SimpleAudioEngine::getInstance()->resumeAllEffects();
    
    
    CCLOG("Changed sound to: %d", _allowSound);
    
    if(_allowSound && _allowMusic)
        playBackgroundMusic(true);
    else
        playBackgroundMusic(false);
}

bool Sounds::isSoundOn()
{
    return _allowSound;
}

void Sounds::toogleMusic()
{
    _allowMusic = _allowMusic ? false : true;
    UserDefault::getInstance()->setIntegerForKey("music", (_allowMusic ? 1 : 0));
    
    CCLOG("Changed music to: %d", _allowMusic);
    
    playBackgroundMusic(_allowMusic);
}

void Sounds::playBackgroundMusic(bool allowMusic)
{
    if(allowMusic)
    {
        if(_didStartPlayMusic)
            SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        else
            playMusic(_currentMusic);
    }
    else{
        SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    }
}


bool Sounds::isMusicOn()
{
    return _allowMusic;
}



