#ifndef PuppyMadness__Slots
#define PuppyMadness__Slots

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Product.h"

#else
#include "Product.h"

#endif

#include "cocos2d.h"


class Slot{
    
public:
    
    Slot();
    
    ~Slot();
    
    static Slot* create();
    
    void init(ProductGroup group, std::string tag, std::string productKey = "");
    
    bool isEmpty();
    
    bool isBought();
    
    std::string getProductEquiped();
    
    CC_SYNTHESIZE(int, _uses, Uses);
    
    std::string getStoreKey();
    
    void setProductEquiped(std::string product);
    
    ProductGroup getProductGroup();
    
    void clear();
    
private:
    
    ProductGroup _group;
    
    std::string _tag;
    
    std::string _productKey;
    
    std::string _product;
    
};





#endif

