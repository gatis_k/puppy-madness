#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Store.h"
#include "Manager/Manager.h"

#else
#include "Store.h"
#include "Manager.h"

#endif
////////////////////////////


USING_NS_CC;


Store::Store()
{
}

Store::~Store()
{
    
    
}

Store* Store::create()
{
    Store* pointer = new Store;
    return pointer;
}

void Store::init()
{
    
    this->initPowerUps();
 
    this->initTalismans();
    
    this->initOutfits();
 
    this->initStaticProducts();
    
    this->initExchange();
    
    this->initIAP();
    
    
    
}

void Store::initOutfits()
{
    std::vector<ProductStatic> outfits;
    
    // min level, price, key, title, description, sprite normal, sprite locked
    
    outfits.push_back({1, 0, "p_elena_0_basic", "Casual", "Classic", "basic", "basic", "basic", true});
    outfits.push_back({1, 240, "p_elena_1_angel", "Angel", "+2 PU", "basic", "basic", "angel", true});
    outfits.push_back({1, 240, "p_elena_2_medic", "Medic", "+10 Lives", "medic", "medic", "medic", true});
    outfits.push_back({1, 240, "p_elena_3_evil", "Devil", "+20% Coins", "basic", "basic", "devil", true});
    outfits.push_back({1, 240, "p_elena_4_control", "Control", "Borders", "basic", "basic", "control", true});
    
    for(auto product: outfits)
    {
        Product *pPointer = Product::create();
        pPointer->init(product, GROUP_OUTFIT);
        _products.insert(std::pair<std::string, Product*>(product.key, pPointer));
    }

}



void Store::initTalismans()
{
    
    std::vector<ProductConsumable> talismans;
    
    // min level, price, max ammount, key, title, description, sprite normal, sprite locked
    
    talismans.push_back({3, 160, 1, "p_talisman_1_drop_lives", "Lives", "Extra Live, Slow Down drop", "store-product-dropchance", "store-product-dropchance-none", "tactics-icon-talisman-dropchance", false});
    talismans.push_back({4, 160, 1, "p_talisman_2_drop_speed", "Speed", "Extra Live, Slow Down drop", "store-product-dropchance", "store-product-dropchance-none", "tactics-icon-talisman-dropchance", false});
    talismans.push_back({5, 160, 1, "p_talisman_3_putimer", "Timer", "Increases Power Up duration", "store-product-putimer", "store-product-putimer-none",  "tactics-icon-talisman-putimer", false});
    talismans.push_back({6, 160, 1, "p_talisman_4_discipline", "Discipline", "More Control", "store-product-discipline", "store-product-discipline-none",  "tactics-icon-talisman-putimer", false});
    talismans.push_back({7, 160, 1, "p_talisman_5_faststack", "Glory", "Stacks faster", "store-product-dropchance", "store-product-dropchance-none", "tactics-icon-talisman-dropchance", false});
    
    for(auto product: talismans)
    {
        Product *pPointer = Product::create();
        pPointer->init(product, GROUP_TALISMAN);
        _products.insert(std::pair<std::string, Product*>(product.key, pPointer));
    }
    
}

void Store::initPowerUps()
{
    std::vector<ProductConsumable> powerUps;
    
    // min level, price, max ammount, key, title, description, sprite normal, sprite locked
    powerUps.push_back({2, 100, 5, "p_pu_1_helium", "Helium", "Helium Description", "store-product-helium", "store-product-helium-none", "tactics-icon-powerup-helium", false});
    powerUps.push_back({4, 120, 5, "p_pu_2_vortex", "Vortex", "Vortex Description", "store-product-vortex", "store-product-vortex-none", "tactics-icon-powerup-vortex", false});
    powerUps.push_back({6, 140, 5, "p_pu_3_magnet", "Magnet", "Magnet Description", "store-product-magnet", "store-product-magnet-none", "tactics-icon-powerup-magnet", false});
    powerUps.push_back({7, 200, 5, "p_pu_4_clear", "Clear", "Clear Description", "store-product-clear", "store-product-clear-none", "tactics-icon-powerup-clear", false});
    powerUps.push_back({8, 220, 5, "p_pu_5_slowmo", "SlowMo", "SlowMo Description", "store-product-slowmo", "store-product-slowmo-none", "tactics-icon-powerup-slowmo", false});
    
    
    
    for(auto product: powerUps)
    {
        Product *pPointer = Product::create();
        pPointer->init(product, GROUP_POWERUP);
        _products.insert(std::pair<std::string, Product*>(product.key, pPointer));
    }
    
}

void Store::initStaticProducts()
{
    
    std::vector<ProductStatic> staticProducts;
    
    // min level, price, key, title, description, sprite locked, sprite normal
    
    staticProducts.push_back({2, 0, "p_extra_slot_1", "Slot I", "PowerUp Slot", "store-product-s1", "store-product-s1-none", "----", false});
    
    staticProducts.push_back({5, 10000, "p_extra_slot_2", "Slot II", "+1 Additional PowerUp Slot", "store-product-s2", "store-product-s2-none", "----", false});
    
    staticProducts.push_back({10, 50000, "p_extra_slot_3", "Slot III", "+2 Additional PowerUp Slot", "store-product-s3", "store-product-s3-none", "----", false});
    
    staticProducts.push_back({3, 0, "p_extra_talisman_1", "Talisman", "Talisman Slot", "store-product-t1", "store-product-t1-none", "----", false});
    
    staticProducts.push_back({9, 40000, "p_extra_talisman_2", "Dual", "+1 Additional Talisman Slot", "store-product-t2", "store-product-t2-none", "----", false});
    
    for(auto product: staticProducts)
    {
        Product *pPointer = Product::create();
        pPointer->init(product, GROUP_STATIC);
        _products.insert(std::pair<std::string, Product*>(product.key, pPointer));
    }
    
}

void Store::initIAP()
{
    std::vector<IAPDetails> iaps;
    
    iaps.push_back({"iap_puppylove_1_80", "Pouch", "store-iap-diamond", 99, 80});
    
    iaps.push_back({"iap_puppylove_2_200", "Bucket", "store-iap-diamond", 199, 200});
    
    iaps.push_back({"iap_puppylove_3_600", "Barrel", "store-iap-diamond", 499, 600});
    
    iaps.push_back({"iap_puppylove_4_1350", "Wagon", "store-iap-diamond", 999, 1350});
    
    iaps.push_back({"iap_puppylove_5_3000", "Mountain", "store-iap-diamond", 1999, 3000});
    
    for(auto product: iaps)
    {
        IAP *IAPPointer = IAP::create();
        IAPPointer->init(product);
        _iap.insert(std::pair<std::string, IAP*>(product.key, IAPPointer));
    }
}


void Store::initExchange()
{
    std::vector<IAPDetails> iaps;
    
    iaps.push_back({"exchange_1", "Smuggler", "store-exchange-coins", 50, 4000});
    
    iaps.push_back({"exchange_2", "Trader", "store-exchange-coins", 80, 6700});
    
    iaps.push_back({"exchange_3", "Banker", "store-exchange-coins", 200, 18000});
    
    iaps.push_back({"exchange_4", "King", "store-exchange-coins", 600, 59000});
    
    
    for(auto product: iaps)
    {
        IAP *IAPPointer = IAP::create();
        IAPPointer->init(product);
        _exchange.insert(std::pair<std::string, IAP*>(product.key, IAPPointer));
    }
}

void Store::updateLocks(int userLevel)
{
    for (std::map<std::string, Product*>::iterator it = _products.begin(); it != _products.end(); ++it)
        it->second->updateLock(userLevel);
}

std::vector<Product*> Store::getProductsByGroup(ProductGroup group)
{
    std::vector<Product*> products;
    for (std::map<std::string, Product*>::iterator it = _products.begin(); it != _products.end(); ++it)
        if(it->second->getProductGroup() == group)
            products.push_back(it->second);
    
    return products;
}

std::vector<Product*> Store::getProductsByLevel(int userLevel)
{
    std::vector<Product*> products;
    for (std::map<std::string, Product*>::iterator it = _products.begin(); it != _products.end(); ++it)
        if(it->second->getLevelRequired() == userLevel)
           products.push_back(it->second);
    return products;
}


Product* Store::getProductByKey(std::string key)
{
    if(_products.at(key) == NULL)
        return nullptr;
    else
        return _products.at(key);
}

std::vector<IAP*> Store::getAllIAP()
{
    std::vector<IAP*> iaps;
    for (std::map<std::string, IAP*>::iterator it = _iap.begin(); it != _iap.end(); ++it)
        iaps.push_back(it->second);
        
    return iaps;
}

std::vector<IAP*> Store::getAllExchange()
{
    std::vector<IAP*> exchange;
    for (std::map<std::string, IAP*>::iterator it = _exchange.begin(); it != _exchange.end(); ++it)
        exchange.push_back(it->second);
        
        return exchange;
}

IAP* Store::getIAPByKey(std::string key)
{
    if(_iap.at(key) == NULL)
        return nullptr;
    else
        return _iap.at(key);
}



IAP* Store::getExchangeByKey(std::string key)
{
    if(_exchange.at(key) == NULL)
        return nullptr;
    else
        return _exchange.at(key);
}


void Store::reset()
{
    for (std::map<std::string, Product*>::iterator it = _products.begin(); it != _products.end(); ++it)
        it->second->reset();
    
}

