#ifndef PuppyMadness_Manager_h
#define PuppyMadness_Manager_h

#include "cocos2d.h"

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM || CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM)
#include "Manager/Stages.h"
#include "Manager/Store.h"
#include "Manager/SlotHandler.h"
#include "Manager/Sounds.h"
#include "Manager/User.h"
#include "Manager/QuestHandler.h"
#include "Game/PowerUp.h"

const std::string kFontPath = "fonts/";
const std::string kLevelPath = "stages/";
const std::string kSoundsPath = "sounds/";
const std::string kImagesPath = "images/";
const std::string kShaderPath = "shaders/";
const std::string kQuestsPath = "quests/";

#else
#include "Stages.h"
#include "Store.h"
#include "SlotHandler.h"
#include "Sounds.h"
#include "User.h"
#include "QuestHandler.h"
#include "PowerUp.h"


const std::string kFontPath = "";
const std::string kLevelPath = "";
const std::string kSoundsPath = "";
const std::string kImagesPath = "";
const std::string kShaderPath = "";
const std::string kQuestsPath = "";

#endif

const std::string kFontName = "helvetica_bold.otf";
const std::string kFontNameBold = "arial_black.ttf";
const std::string kFontNameHUD = "helvetica_rounded.otf";
const std::string kFontNameStyle = "softplain.ttf";

const float kTileSize = 20;

const int kTAG_QUEST = 882200;
const int kTAG_QUEST_DAILY = 882201;

const int kTAG_GAMELOOP_CLAIM = 884400;
const int kTAG_GAMELOOP_LEVELUP = 885500;

const int kTAG_USER_COINS = 990011;
const int kTAG_USER_COINS_BTN = 990012;
const int kTAG_USER_COINS_BTN2 = 990013;
const int kTAG_USER_DIAMONDS = 990022;
const int kTAG_USER_DIAMONDS_BTN = 990021;
const int kTAG_USER_DIAMONDS_BTN2= 990022;
const int kTAG_USER_INFO = 990033;
const int kTAG_USER_INFO_TOOLTIP = 990044;

const int kTAG_TACTICS = 883300;
/*
const int kTAG_TACTICS_OUTFIT = 883301;
const int kTAG_TACTICS_TALISMAN = 883302;
const int kTAG_TACTICS_POWERUP = 883303;
const int kTAG_TACTICS_STATIC = 883304;
*/

const cocos2d::Color4B kCOLOR_BACK(255,255,255,180);
const cocos2d::Color4B kCOLOR_FRAME(66,35,19,255);

typedef enum _sceen_name
{
    SCENE_SPLASH,
    SCENE_LOADING,
    SCENE_MAIN,
    SCENE_GAME,
    SCENE_REWARDS,
    SCENE_STORE,
    SCENE_SETTINGS,
    SCENE_FACEBOOK,
	SCENE_PRELOAD
    
} SceenName;

typedef struct _game_data
{
    std::string stage;
    float time;
    int coins;
    int puppies;
    int speed;
} GameData;

class Manager{
    
public:
    
    Manager();
    ~Manager();
    
    void initDebugTools();
    bool getDebugTools();
    void setDebugTools(bool tool);
    bool _debugTools;
    
    bool init();
    
    static Manager* getInstance();
    
    void runScene(SceenName sceneName, std::string str = "");
   
    CC_SYNTHESIZE(std::string, _activePopUp, ActivePopUp);
    CC_SYNTHESIZE(std::string, _activeTacticsSlot, ActiveTacticsSlot);
    CC_SYNTHESIZE(GameData, _lastGame, LastGame);
    

    float getGameSpeedModifier();

    Sounds* getSoundsLibrary();
    
    Stage* getStage(std::string str);
    void updateStageLocks();
    
    std::vector<std::string> getAllStageNames();
     
    Store* getStore();
    
    SlotHandler* getSlotHandler();
    
    User* getUser();
    
    QuestHandler* getQuests();
    
    int getBasketType();
    
    std::vector<std::string> getGameLoopList();
    
    cocos2d::Node* getUserXPnWaller();
    cocos2d::Node* getTactics(bool resetBtn = false);
  
    int getCoins();
    std::string getCoinsString();
    void depositeCoins(int coins);
    void removeCoins(int coins);
    
    int getDiamonds();
    std::string getDiamondsString();
    void depositeDiamonds(int diamonds);
    void removeDiamonds(int diamonds);
    
    void depositePuppies(int puppies);
    
    // DEV STUFF
    void resetCoins();
    void resetAll();
    void resetAllStore();    
    void completeAll();
    
private:
    
    void initStages(bool reset = false);
    
    User* _user;
    QuestHandler* _questHandler;
    Store* _store;
    Sounds* _sounds;
    SlotHandler* _slotHandler;

    std::map<std::string, Stage*> _stages;
    
    
    int _coins;
    int _coins_lifetime;
    int _diamonds;
    int _diamonds_lifetime;
    int _puppies_lifetime;
    int _paid;
    

    
};

#endif
