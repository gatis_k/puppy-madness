#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/User.h"

#else
#include "User.h"


#endif
////////////////////////////


USING_NS_CC;


User::User()
{
    
}

User::~User()
{
    
}


User* User::create()
{
    User* pointer = new User;
    return pointer;
}

void User::init()
{
    _levelBreakPoints.push_back(100); // > level 2 250
    _levelBreakPoints.push_back(700); // > level 3
    _levelBreakPoints.push_back(1500); // > level 4
    _levelBreakPoints.push_back(3000); // > level 5
    _levelBreakPoints.push_back(5000); // > level 6
    _levelBreakPoints.push_back(12000); // > level 7
    _levelBreakPoints.push_back(16000); // > level 8
    _levelBreakPoints.push_back(20000); // > level 9
    _levelBreakPoints.push_back(50000); // > level 10
    
    _levelMax = (int)_levelBreakPoints.size() + 1;
    
    _xp = UserDefault::getInstance()->getIntegerForKey("user_xp", 0);
    _level = UserDefault::getInstance()->getIntegerForKey("user_lvl", getLevelByXP());

    
    /* for testing
    _xp = 90;
         */
//    CCLOG("LEVEL 1 %d", getLevelByXP());

}

bool User::isLevelUp()
{
    if(getLevelByXP() > _level)
        return true;
    else
        return false;
}

void User::levelUp()
{
    _level = getLevelByXP();
    UserDefault::getInstance()->setIntegerForKey("user_lvl", _level);
}

void User::depositeXP(int xp)
{
    _xp = _xp + xp;
    UserDefault::getInstance()->setIntegerForKey("user_xp", _xp);
}

int User::getLevelByXP()
{
    int level = 1;
    bool flag = true;
    int i;
    for(i = 0; i<_levelBreakPoints.size(); i++)
    {
        if(_levelBreakPoints.at(i) > _xp)
        {
            level = i + 1;
            flag = false;
            break;
        }
    }
    if(flag && _xp >= _levelBreakPoints.at(_levelBreakPoints.size()-1) && _xp > 0)
    {
        level = _levelMax;
    }
    
    return level;
}


int User::getUserLevel()
{
    return _level;
}

std::string User::getUserLevelStr()
{
    sprintf(_charText, "%d", _level);
    std::string userLevel(_charText);
    return userLevel;
}

bool User::isEndGame()
{
    if(_level == _levelMax)
        return true;
    else
        return false;
}

int User::getUserXP(bool reduce)
{
    if(reduce && _level > 1)
    {
        return (_xp - _levelBreakPoints.at(_level-2));
    }
    else
        return _xp;
}


std::string User::getUserXPStr(bool reduce)
{
    sprintf(_charText, "%d", getUserXP(reduce));
    std::string userXP(_charText);
    return userXP;
}

int User::getUserXPNextLevel(bool reduce, int level)
{
    level = level == 0 ? _level : level;
    if(level < _levelMax)
        if(reduce && level > 1)
            return _levelBreakPoints.at((level - 1)) - _levelBreakPoints.at((level - 2));
        else
            return _levelBreakPoints.at((level - 1));
    else
        return 0;
}

std::string User::getUserXPNextLevelStr(bool reduce, int level)
{
    sprintf(_charText, "%d", getUserXPNextLevel(reduce, level));
    std::string userXPNextLevel(_charText);
    return userXPNextLevel;
}

std::string User::getTitle()
{
    std::string title;
    switch (_level) {
        case 1: title = "Litter Box Cleaner";   break;
        case 2: title = "Scooby Snack";         break;
        case 3: title = "Cat Chaser";           break;
        case 4: title = "Litter Box Cleaner";   break;
        case 5: title = "Litter Box Cleaner";   break;
        case 6: title = "Litter Box Cleaner";   break;
        case 7: title = "Litter Box Cleaner";   break;
        case 8: title = "Litter Box Cleaner";   break;
        case 9: title = "Litter Box Cleaner";   break;
        case 10:title = "Puppy Whisperer";   break;
    }
    
    return title;
}

void User::becomePuppyWhisperer()
{
    int xp = _levelBreakPoints.at(_levelBreakPoints.size()-1) + 100;
    this->depositeXP(xp);
    this->levelUp();
}

void User::reset()
{
    _xp = 0;
    UserDefault::getInstance()->setIntegerForKey("user_xp", _xp);
    
    _level = getLevelByXP();
    UserDefault::getInstance()->setIntegerForKey("user_lvl", _level);
}