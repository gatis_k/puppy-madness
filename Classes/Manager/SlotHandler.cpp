#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/SlotHandler.h"
#include "Manager/Slots.h"
#include "Manager/Manager.h"

#else
#include "SlotHandler.h"
#include "Slots.h"
#include "Manager.h"

#endif


#include <math.h>
#include <algorithm>


SlotHandler::SlotHandler()
{

}


SlotHandler::~SlotHandler()
{
    
}

SlotHandler* SlotHandler::create()
{
    SlotHandler* pointer = new SlotHandler;
    return pointer;
}

void SlotHandler::init()
{
    // init slots
    auto slotGirl = Slot::create();
    slotGirl->init(GROUP_OUTFIT, "slot_a_outfit");
    _slots.insert(std::pair<std::string, Slot*>("slot_a_outfit", slotGirl));
    
    auto slotTalisman1 = Slot::create();
    slotTalisman1->init(GROUP_TALISMAN, "slot_b_talisman_1", "p_extra_talisman_1");
    _slots.insert(std::pair<std::string, Slot*>("slot_b_talisman_1", slotTalisman1));
    
    auto slotTalisman2 = Slot::create();
    slotTalisman2->init(GROUP_TALISMAN, "slot_b_talisman_2", "p_extra_talisman_2");
    _slots.insert(std::pair<std::string, Slot*>("slot_b_talisman_2", slotTalisman2));
    
    auto slotPowerUp1 = Slot::create();
    slotPowerUp1->init(GROUP_POWERUP, "slot_c_powerup_1", "p_extra_slot_1");
    _slots.insert(std::pair<std::string, Slot*>("slot_c_powerup_1", slotPowerUp1));
    
    auto slotPowerUp2 = Slot::create();
    slotPowerUp2->init(GROUP_POWERUP, "slot_c_powerup_2", "p_extra_slot_2");
    _slots.insert(std::pair<std::string, Slot*>("slot_c_powerup_2", slotPowerUp2));
    
    auto slotPowerUp3 = Slot::create();
    slotPowerUp3->init(GROUP_POWERUP, "slot_c_powerup_3", "p_extra_slot_3");
    _slots.insert(std::pair<std::string, Slot*>("slot_c_powerup_3", slotPowerUp3));
}



std::vector<Slot*> SlotHandler::getAllSlots()
{
    std::vector<Slot*> slots;
    for (std::map<std::string, Slot*>::iterator it = _slots.begin(); it != _slots.end(); ++it)
        slots.push_back(it->second);
    
    return slots;
}

std::vector<std::string> SlotHandler::getAllProductsEquiped()
{
    std::vector<std::string> data;
    for(auto slot: getAllSlots())
        if(!slot->isEmpty())
            data.push_back(slot->getProductEquiped());
    
    return data;
}

bool SlotHandler::isProductEquiped(std::string key)
{
    auto data = this->getAllProductsEquiped();
    if (std::find(data.begin(), data.end(), key) != data.end())
        return true;
    else
        return false;
}

Slot* SlotHandler::getSlotByName(std::string name)
{
    return _slots.at(name);
}

std::vector<int> SlotHandler::getAvailablePowerUps()
{
    std::vector<int> powerUps;
    
    for (std::map<std::string, Slot*>::iterator it = _slots.begin(); it != _slots.end(); ++it)
    {
        if(!it->second->isEmpty() && it->second->getProductGroup() == GROUP_POWERUP)
        {
            auto powerUpStr = it->second->getProductEquiped();
            if(powerUpStr == "p_pu_2_vortex"){
                powerUps.push_back(POWERUP_VORTEX);
            }
            if(powerUpStr == "p_pu_4_slowmo"){
                powerUps.push_back(POWERUP_SLOWMO);
            }
            if(powerUpStr == "p_pu_5_clear"){
                powerUps.push_back(POWERUP_CLEAR);
            }
            if(powerUpStr == "p_pu_3_magnet"){
                powerUps.push_back(POWERUP_MAGNET);
            }
            if(powerUpStr == "p_pu_1_helium"){
                powerUps.push_back(POWERUP_HELIUM);
            }
        }
    }    
    return powerUps;
}

bool SlotHandler::anyFreeSlots(ProductGroup group, std::string key)
{
    for (std::map<std::string, Slot*>::iterator it = _slots.begin(); it != _slots.end(); ++it){
        
        if(it->second->getProductGroup() == group)
        {
            if(it->second->isEmpty() && it->second->isBought() && it->second->getProductEquiped() != key)
                return true;
        }
    }
    return false;
}

void SlotHandler::equipProduct(ProductGroup group, std::string key)
{
    if(group == GROUP_OUTFIT)
    {
        _slots.at("slot_a_outfit")->setProductEquiped(key);
    }
    else
    {
        for (std::map<std::string, Slot*>::iterator it = _slots.begin(); it != _slots.end(); ++it){
        
            if(it->second->getProductGroup() == group && it->second->isEmpty() && it->second->isBought())
            {
                it->second->setProductEquiped(key);
                break;
            }
        }
    }
}

void SlotHandler::useTalismans()
{
    for (std::map<std::string, Slot*>::iterator it = _slots.begin(); it != _slots.end(); ++it){
        
        if(it->second->getProductGroup() == GROUP_TALISMAN && !it->second->isEmpty())
        {
            Product* product = Manager::getInstance()->getStore()->getProductByKey(it->second->getProductEquiped());
            
            product->removeAmount(1);
            if(product->getAmount()<=0)
            {
                it->second->clear();
            }
        }
    }
}

int SlotHandler::getPowerUpUses(int powerUp)
{
    Slot* slot = this->findSlotByProduct(parsePowerUp(powerUp));
    if(slot != nullptr)
        return slot->getUses();
    else
        
        return 0;
}

bool SlotHandler::hasCharges(int powerUp)
{
    Slot* slot = this->findSlotByProduct(parsePowerUp(powerUp));
    if(slot != nullptr)
    {
        if(slot->getUses()>0)
            return true;
        else
            return false;
    }
    return false;
}

void SlotHandler::usePowerUp(int powerUp)
{
    Slot* slot = this->findSlotByProduct(parsePowerUp(powerUp));
    if(slot != nullptr)
    {
        int uses = slot->getUses();
        if(uses > 0)
        {
            slot->setUses(--uses);
            Product* product =  Manager::getInstance()->getStore()->getProductByKey(slot->getProductEquiped());
            product->removeAmount(1);

            if(!product->hasAmount()) //burn after reading
                slot->clear();
        }
    }
}

void SlotHandler::clearSlots()
{
    CCLOG("CLEAR SLOTS");
    for (std::map<std::string, Slot*>::iterator it = _slots.begin(); it != _slots.end(); ++it)
    {
        if(!it->second->isEmpty())
            it->second->clear();
    }
    _slots.at("slot_a_outfit")->setProductEquiped("p_elena_0_basic");
}


void SlotHandler::reset()
{
    for (std::map<std::string, Slot*>::iterator it = _slots.begin(); it != _slots.end(); ++it)
    {
        it->second->clear();
        
    }
}


std::string SlotHandler::parsePowerUp(int powerUp)
{
    std::string str = "";
    switch(powerUp)
    {
        case POWERUP_HELIUM:
            str = "p_pu_1_helium";
            break;
        case POWERUP_CLEAR:
            str = "p_pu_4_clear";
            break;
        case POWERUP_VORTEX:
            str = "p_pu_2_vortex";
            break;
        case POWERUP_MAGNET:
            str = "p_pu_3_magnet";
            break;
        case POWERUP_SLOWMO:
            str = "p_pu_5_slowmo";
            break;
    }
    return str;
}

Slot* SlotHandler::findSlotByProduct(std::string key)
{
    Slot* slot = nullptr;
    for (std::map<std::string, Slot*>::iterator it = _slots.begin(); it != _slots.end(); ++it){
        
        if(!it->second->isEmpty() && it->second->getProductEquiped() == key)
        {
            slot = it->second;
        }
    }
    return slot;
}


