#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Product.h"

#else
#include "Product.h"

#endif
////////////////////////////


USING_NS_CC;


Product::Product()
{
    
}

Product::~Product()
{
    
}

Product* Product::create()
{
    Product* pointer = new Product;
    return pointer;
}


void Product::init(ProductConsumable product, ProductGroup group)
{
    _consumable = true;
    _locked = true;
    _premium = product.premium;
    
    _minLevel = product.level;
    _price = product.price;
    _uses = product.uses;
    _key = product.key;
    _title = product.title;
    _description = product.description;
    _spriteNormal = product.spriteNormal;
    _spriteLocked = product.spriteLocked;
    _spriteUI = product.spriteUI;
    _group = group;
    
    _amount = UserDefault::getInstance()->getIntegerForKey(_key.c_str(), 0);
    
    std::string key_seen = _key + "_seen";
    _seen = UserDefault::getInstance()->getIntegerForKey(key_seen.c_str(), 0);
}

void Product::init(ProductStatic product, ProductGroup group)
{
    _consumable = false;
    _locked = true;
    _premium = product.premium;
    
    _minLevel = product.level;
    _price = product.price;
    _key = product.key;
    _title = product.title;
    _description = product.description;
    _spriteNormal = product.spriteNormal;
    _spriteLocked = product.spriteLocked;
    _group = group;
    
    _amount = UserDefault::getInstance()->getIntegerForKey(_key.c_str(), 0);
    
    std::string key_seen = _key + "_seen";
    _seen = UserDefault::getInstance()->getIntegerForKey(key_seen.c_str(), 0);
}

void Product::updateLock(int userLevel)
{
    if(_locked && userLevel >= _minLevel )
        _locked = false;
}

ProductGroup Product::getProductGroup()
{
    return _group;
}

std::string Product::getTitle()
{
    return _title;
}

std::string Product::getDescription()
{
    return _description;
}

std::string Product::getSpriteName()
{
    if(_locked)
        return _spriteLocked;
    else
        return _spriteNormal;
}

std::string Product::getKey()
{
    return _key;
}


std::string Product::getProductGroupSprite()
{
    std::string spriteGroup = "";
    if(_group == GROUP_OUTFIT)
        spriteGroup = "outfit";
    else if(_group == GROUP_POWERUP)
        spriteGroup = "powerup";
    else if(_group == GROUP_TALISMAN)
        spriteGroup = "talisman";
    else if(_group == GROUP_STATIC)
        spriteGroup = "static";
    
    return spriteGroup;
}

std::string Product::getSpriteUI()
{
    return _spriteUI;
}


bool Product::isPremium()
{
    return _premium;
}

int Product::getPrice()
{
    return _price;
}

int Product::getLevelRequired()
{
    return _minLevel;
}

int Product::getAmount()
{
    return _amount;
}

int Product::getUses()
{
    return _uses;
}

std::string Product::getAmountString()
{
    char charText[100];
    sprintf(charText, "%d", _amount);
    std::string amountStr(charText);
    return amountStr;
    
}

std::string Product::getLevelRequiredStr()
{
    char charText[100];
    sprintf(charText, "%d", _minLevel);
    std::string amountStr(charText);
    return amountStr;
}

bool Product::canAddAmount(int amount)
{
    if(_amount + amount < _uses) // error here uses <> maxAmount
        return true;
    else
        return false;
}

void Product::addAmount(int amount)
{
    _amount += amount;
    UserDefault::getInstance()->setIntegerForKey(_key.c_str(), _amount);
}

bool Product::canRemoveAmount(int amount)
{
    if(_amount - amount >= 0)
        return true;
    else
        return false;
}

void Product::removeAmount(int amount)
{
    _amount -= amount;
    UserDefault::getInstance()->setIntegerForKey(_key.c_str(), _amount);
}

bool Product::isConsumbale()
{
    return _consumable;
}

bool Product::isLocked()
{
    return _locked;
}

void Product::setSeen(bool seen)
{
    _seen = (seen) ? 1 : 0;
    std::string key_seen = _key + "_seen";
    UserDefault::getInstance()->setIntegerForKey(key_seen.c_str(), _seen);
}

bool Product::isSeen()
{
    if(_seen == 1)
        return true;
    else
        return false;
}

bool Product::hasAmount()
{
    if(_amount > 0)
        return true;
    else
        return false;
}

void Product::reset()
{
    _amount = 0;
    UserDefault::getInstance()->setIntegerForKey(_key.c_str(), 0);
    std::string key_seen = _key + "_seen";
    UserDefault::getInstance()->setIntegerForKey(key_seen.c_str(), 0);
}



