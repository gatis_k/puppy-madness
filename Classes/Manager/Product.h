#ifndef PuppyMadness__Product
#define PuppyMadness__Product

#include "cocos2d.h"




typedef enum _product_group{
    GROUP_POWERUP,
    GROUP_TALISMAN,
    GROUP_OUTFIT,
    GROUP_STATIC
} ProductGroup;



typedef struct _product_consumable{
    
    int level;
    int price;
    int uses;
    std::string key;
    std::string title;
    std::string description;
    std::string spriteNormal;
    std::string spriteLocked;
    std::string spriteUI;
    bool premium;

    
} ProductConsumable;

typedef struct _product_static{
    
    int level;
    int price;
    std::string key;
    std::string title;
    std::string description;
    std::string spriteNormal;
    std::string spriteLocked;
    std::string spriteUI;
    bool premium;
    
} ProductStatic;




class Product{
    
public:
    
    Product();
    
    ~Product();
    
    static Product* create();
    
    void init(ProductConsumable product, ProductGroup group);
    
    void init(ProductStatic product, ProductGroup group);
    
    void updateLock(int userLevel);
    
    ProductGroup getProductGroup();
    
    std::string getTitle();
    
    std::string getDescription();
    
    std::string getSpriteName();
    
    std::string getKey();
    
    std::string getProductGroupSprite();
    
    std::string getSpriteUI();
    
    bool isPremium();
    
    int getPrice();
    
    int getLevelRequired();
    
    std::string getLevelRequiredStr();
    
    int getAmount();
    
    int getUses();
    
    std::string getAmountString();
    
    bool canAddAmount(int amount = 1);
    
    void addAmount(int amount = 1);
    
    bool canRemoveAmount(int amount = 1);
    
    void removeAmount(int amount = 1);
    
    bool isConsumbale();
    
    bool isLocked();
    
    void setSeen(bool seen);
    
    bool isSeen();
    
    bool hasAmount();
    
    void reset(); // for dev purpose
    
private:
    
    bool _consumable;
    
    bool _locked;
    
    int _seen;
    
    int _minLevel;
    
    int _price;
    
    int _amount;
    
    int _uses;
    
    bool _premium;
    
    std::string _key;
    
    std::string _title;
    
    std::string _description;
    
    std::string _spriteLocked;
    
    std::string _spriteNormal;
    
    std::string _spriteUI;
    
    ProductGroup _group;
    
};





#endif

