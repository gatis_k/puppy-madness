#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/QuestHandler.h"
#include "Manager/Manager.h"

#else
#include "QuestHandler.h"
#include "Manager.h"

#endif

#include <sstream>
#include <time.h>
////////////////////////////


USING_NS_CC;


QuestHandler::QuestHandler()
{
    
}

QuestHandler::~QuestHandler()
{
    
}

QuestHandler* QuestHandler::create()
{
    QuestHandler* pointer = new QuestHandler;
    return pointer;
}

void QuestHandler::init()
{
    _questsCompleted = 0;
    _dailyQuestsCompleted = 0;
    
    _dailyCompletedTime = UserDefault::getInstance()->getStringForKey("daily_completed_time","");
    
    // init General Quests
    
    char charText[100];
    std::string questPath = "";
    
    for(int i=1; i<=70; i++)
    {
        if(i<10)
            sprintf(charText, "00%d_quest", i);
        else
            sprintf(charText, "0%d_quest", i);
        questPath = (std::string)charText;
        parseQuest(questPath);
//        CCLOG("Quest name: %s", questPath.c_str());
    }
    
    
    // init Daily Quests
    std::vector<std::string> dailyPaths{
        "001_daily",
        "002_daily"};
    
    for(auto questPath: dailyPaths)
        parseQuest(questPath, true);
    
    
    CCLOG("QUEST STATUS: GENERAL (%d/%d), DAILY (%d/%d)", _questsCompleted, (int)_general.size(), _dailyQuestsCompleted, (int)_daily.size());
    
}

void QuestHandler::parseQuest(std::string tag, bool daily)
{
    
    std::string questPath = kQuestsPath + ((daily) ? "daily/" : "general/") + tag + ".txt";
    std::string json_str = FileUtils::getInstance()->getStringFromFile(questPath);
    
    Jzon::Object rootNode;
    Jzon::Parser parser(rootNode, json_str);
    
    if (parser.Parse())
    {
        
        Quest* questP = new Quest();
        
        std::string title = rootNode.Get("title").ToString();
        std::string stage = rootNode.Get("stage").ToString();
        int amount = rootNode.Get("amount").ToInt();
        bool singleRound = (rootNode.Get("distance").ToString() == "overall") ? false : true;
        int coins = rootNode.Get("coins").ToInt();
        int xp = rootNode.Get("xp").ToInt();
        
        if(daily)
            questP->setMinLevel(rootNode.Get("min_lvl").ToInt());
        
        std::string event = rootNode.Get("event").ToString();
        std::map<std::string, int> param;

        
        if (rootNode.Get("param").IsArray())
        {
            const Jzon::Array &tasks = rootNode.Get("param").AsArray();
            for (Jzon::Array::const_iterator it = tasks.begin(); it != tasks.end(); ++it)
            {
                
                if((*it).Has("puppy")) // puppy type 1 -mopsis, 2 mailo, 3 husky
                    param.insert(std::pair<std::string, int>("puppy", (*it).Get("puppy").ToInt()));
                
                if((*it).Has("powerup")) // with powerup active (0-x stands for powerup code)
                    param.insert(std::pair<std::string, int>("powerup", (*it).Get("powerup").ToInt()));
                
                if((*it).Has("jump")) // puppy is in jump state for 0, puppy is in fall state for 1
                    param.insert(std::pair<std::string, int>("jump", (*it).Get("jump").ToInt()));
                
                if((*it).Has("height")) // at height (0 for ceilling, 1 for ground (just before water)
                    param.insert(std::pair<std::string, int>("height", (*it).Get("height").ToInt()));
                
                if((*it).Has("teleport")) // basket was teleported before (0 without teleport, 1 with teleport)
                    param.insert(std::pair<std::string, int>("teleport", (*it).Get("teleport").ToInt()));
                
                if((*it).Has("fenced")) // basket was fenced before (0 without basket beeing fenced, 1 with)
                    param.insert(std::pair<std::string, int>("fenced", (*it).Get("fenced").ToInt()));
                
                if((*it).Has("bombed")) // basket recieved bullet before (0 without basket beeing bombed, 1 with)
                    param.insert(std::pair<std::string, int>("bombed", (*it).Get("bombed").ToInt()));
                
                if((*it).Has("challenge")) // challenge mode type 0-n
                    param.insert(std::pair<std::string, int>("challenge", (*it).Get("challenge").ToInt()));
            }
        }
        
        questP->init(tag, title, amount, coins, xp, daily);
        if(!questP->isCompleted())
            questP->initGoal(event, stage, param, singleRound);
        
        
        if(questP->isCompleted())
        {
            if(daily)
                _dailyQuestsCompleted++;
            else
                _questsCompleted ++;
        }

        
        if(daily)
        {
           // questP->setMinLevel(int var)
            _daily.insert(std::pair<std::string, Quest*>(tag, questP));
            
        }
        else
            _general.insert(std::pair<std::string, Quest*>(tag, questP));
       
        
    }
    else{
        CCLOG("JSON ERROR ON LEVEL PROGRESS LOAD");
    }
    
    
}

std::vector<Quest*> QuestHandler::getActiveGeneral(bool show)
{
    std::vector<Quest*> quests;
    int count = 0;
    for (std::map<std::string, Quest*>::iterator it = _general.begin(); it != _general.end(); ++it)
    {
        
        if(!it->second->isClaimend())
        {
            if(show)
                quests.push_back(it->second);
            else if(!show && !it->second->isCompleted())
                quests.push_back(it->second);
            
            count++;
            //CCLOG("Quests active: %s (%d/%d)", it->first.c_str(), it->second->getAmountNow(), it->second->getAmountGoal());
            
        }


        if(count>=kMaxActiveQuests)
            break;

    }
    return quests;
}


void QuestHandler::setDailyQuestTime()
{
    _dailyCompletedTime = getTimeString();
    UserDefault::getInstance()->setStringForKey("daily_completed_time",_dailyCompletedTime);
}

bool QuestHandler::hasDailyQuest()
{
//    _dailyCompletedTime = "2016-7-5-23-41-00";
    
    std::vector<int> timeNow = parseTimeString(getTimeString());
    std::vector<int> timeDaily = parseTimeString(_dailyCompletedTime);
    
    CCLOG("TIME NOW %s DAILY %s", getTimeString().c_str(), _dailyCompletedTime.c_str());
    
    if(_dailyCompletedTime == "")
    {
//        return true;
    }
    
    
    if(timeNow.at(0) == timeDaily.at(0) && timeNow.at(1) == timeDaily.at(1) && timeNow.at(2) == timeDaily.at(2))
    {
        return false;
    }
    
    recheckDailyQuestCounter();
    
    
    return true;
    
    
}

std::vector<Quest*> QuestHandler::getActiveDaily(bool show)
{
    int userLvl = Manager::getInstance()->getUser()->getUserLevel();
    std::vector<Quest*> quests;
    int count = 0;
    for (std::map<std::string, Quest*>::iterator it = _daily.begin(); it != _daily.end(); ++it)
    {
        //quests.push_back(it->second);
        //count++;
        //break;
        
        
        if(!it->second->isClaimend() && it->second->getMinLevel() <= userLvl)
        {
            if(show)
                quests.push_back(it->second);
            else if(!show && !it->second->isCompleted())
                quests.push_back(it->second);
            count++;
            
        }
        
        if(count>=kMaxActiveDailyQuests)
            break;
    }
    return quests;
}


void QuestHandler::recheckDailyQuestCounter()
{
    
    int qCompleted = 0;
    int qAvailable = 0;
    
    int userLvl = Manager::getInstance()->getUser()->getUserLevel();
    for (std::map<std::string, Quest*>::iterator it = _daily.begin(); it != _daily.end(); ++it)
    {
        if(it->second->isClaimend())
            qCompleted++;
        if(it->second->getMinLevel() <= userLvl)
            qAvailable++;
    }
    
    if(qCompleted == qAvailable)
    {
        for (std::map<std::string, Quest*>::iterator it = _daily.begin(); it != _daily.end(); ++it)
        {
            it->second->reset();
            it->second->resetVariables();
        }
        CCLOG("MUST RELOAD DAILY %d %d", qCompleted, qAvailable);
    }

    
}

std::vector<Quest*> QuestHandler::getGeneralQuestToClaim()
{
    std::vector<Quest*> quests;
    for(auto quest: this->getActiveGeneral(true))
    {
        if(quest->isCompleted() && !quest->isClaimend())
            quests.push_back(quest);
    }
    CCLOG("General Completed %d", (int)quests.size());
    /*
    for(auto quest: this->getActiveDaily(true))
    {
        if(quest->isCompleted() && !quest->isClaimend())
            quests.push_back(quest);
    }*/
    return quests;
}

std::vector<Quest*> QuestHandler::getDailyQuestToClaim()
{
    std::vector<Quest*> quests;
    for(auto quest: this->getActiveDaily(true))
    {
        if(quest->isCompleted() && !quest->isClaimend())
            quests.push_back(quest);
    }
//    CCLOG("Daily Completed %d", (int)quests.size());
    return quests;
}



bool QuestHandler::newQuestToShow()
{
    bool newQuests = false;
    for(auto quest: getActiveGeneral())
    {
        if(!quest->isSeen())
        {
            newQuests = true;
            break;
        }
    }
    for(auto quest: getActiveDaily())
    {
        if(!quest->isSeen())
        {
            newQuests = true;
            break;
        }
    }
    return newQuests;
}

void QuestHandler::setSeen()
{
    for(auto quest: getActiveGeneral())
    {
        quest->setSeen(true);
    }
    for(auto quest: getActiveDaily())
    {
        quest->setSeen(true);
    }
}


int QuestHandler::getTotalQuestCompleted(bool daily)
{
    if(daily)
        return _dailyQuestsCompleted;
    else
        return _questsCompleted;
}


Quest*  QuestHandler::getGeneral(std::string tag)
{
    return _general.at(tag);
}

Quest*  QuestHandler::getDaily(std::string tag)
{
    return _daily.at(tag);
}

void QuestHandler::reloadRoundVariables()
{
    for (std::map<std::string, Quest*>::iterator it = _daily.begin(); it != _daily.end(); ++it)
    {
        it->second->reload();
    }
    for (std::map<std::string, Quest*>::iterator it = _general.begin(); it != _general.end(); ++it)
    {
        it->second->reload();
    }
}


void QuestHandler::reset()
{
    for (std::map<std::string, Quest*>::iterator it = _daily.begin(); it != _daily.end(); ++it)
    {
        it->second->reset();
    }
    
    for (std::map<std::string, Quest*>::iterator it = _general.begin(); it != _general.end(); ++it)
    {
        it->second->reset();
    }
    
    _daily.clear();
    _general.clear();
    _questsCompleted = 0;
    _dailyQuestsCompleted = 0;
    this->init();
    
}

std::string QuestHandler::getTimeString()
{
    std::string timeStr = "";
    
    time_t now;
    struct tm * timeInfo;
    time(&now);
    timeInfo = localtime(&now);
    
    timeStr = intToString(timeInfo->tm_year + 1900) + "-" + intToString(timeInfo->tm_mon + 1) + "-" + intToString(timeInfo->tm_mday) + "-" + intToString(timeInfo->tm_hour) + "-" + intToString(timeInfo->tm_min) + "-" + intToString(timeInfo->tm_sec);
    
    return timeStr;
}

std::vector<int> QuestHandler::parseTimeString(std::string timeStr)
{
    std::vector<int> vect;
    std::stringstream ss(timeStr);
    int i;
    while (ss >> i)
    {
        vect.push_back(i);
        if (ss.peek() == '-')
            ss.ignore();
    }
    
    return vect;
}



std::string QuestHandler::intToString(int nr)
{
    char charText[100];
    sprintf(charText, "%d", nr);
    std::string str(charText);
    return str;
}

