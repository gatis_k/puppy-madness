#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Manager.h"
#include "Scenes/SplashScene.h"
#include "Scenes/MainScene.h"
#include "Scenes/RewardsScene.h"
#include "Scenes/StoreScene.h"
#include "Scenes/SettingsScene.h"
#include "Scenes/GameScene.h"
#include "Scenes/PreloadScene.h"
#include "Utils/Jzon.h"

#else
#include "Manager.h"
#include "Sounds.h"
#include "SplashScene.h"
#include "MainScene.h"
#include "RewardsScene.h"
#include "StoreScene.h"
#include "SettingsScene.h"
#include "GameScene.h"
#include "PreloadScene.h"
#include "Jzon.h"

#endif


////////////////////////////


USING_NS_CC;



Manager::Manager()
{
    init();
}
Manager::~Manager()
{
    
}




Manager* Manager::getInstance()
{
    static Manager sIstance;
    return &sIstance;
}

bool Manager::init()
{
    initDebugTools();
    
    _user = User::create();
    _user->init();
    
    _questHandler = QuestHandler::create();
    _questHandler->init();

    
    // init stages
    this->initStages();
    
    if(getStage("stage_a")->isLocked())
    {
        CCLOG("First run. Clean setup.");
    //   getStage("stage_forest")->unlock();
    }

    
    // init store
    _store = Store::create();
    _store->init();
    _store->updateLocks(_user->getUserLevel());
    
    _sounds = Sounds::create();
    _sounds->init();
    
    _slotHandler = SlotHandler::create();
    _slotHandler->init();
    
    /// BUYS AND ADDS ELENE BASIC
    if(!_store->getProductByKey("p_elena_0_basic")->hasAmount())
    {
        _store->getProductByKey("p_elena_0_basic")->addAmount();
        CCLOG("ELENT BASIC BOUGHT");
    }
    if(_slotHandler->getSlotByName("slot_a_outfit")->isEmpty())
    {
        _slotHandler->getSlotByName("slot_a_outfit")->setProductEquiped("p_elena_0_basic");
        CCLOG("ELENT BASIC MOUNTED");
    }
    
    
    
    _coins = UserDefault::getInstance()->getIntegerForKey("coins", 0);
    _coins_lifetime = UserDefault::getInstance()->getIntegerForKey("coins_lifetime", 0);
    _diamonds = UserDefault::getInstance()->getIntegerForKey("diamonds", 0);
    _diamonds_lifetime = UserDefault::getInstance()->getIntegerForKey("diamonds_lifetime", 0);
    _puppies_lifetime = UserDefault::getInstance()->getIntegerForKey("puppies_lifetime", 0);
    _paid = UserDefault::getInstance()->getIntegerForKey("_paid", 1);

    return true;
}

void Manager::runScene(SceenName sceneName, std::string str)
{
    Scene* scene = NULL;
    switch (sceneName) {
        default:
        case SCENE_SPLASH:      scene = SplashScene::createScene();     break;
		case SCENE_MAIN:        scene = MainScene::createScene(str);	break;
        case SCENE_STORE:       scene = StoreScene::createScene(str);   break;
        case SCENE_SETTINGS:    scene = SettingsScene::createScene();   break;
        case SCENE_GAME:        scene = GameScene::create(str);         break;
        case SCENE_REWARDS:     scene = RewardsScene::createScene(str); break;
		case SCENE_PRELOAD:		scene = PreloadScene::createScene();	break;
    }
    
    if (Director::getInstance()->getRunningScene() == NULL)
        Director::getInstance()->runWithScene(scene);
    else
        Director::getInstance()->replaceScene(scene);
    
}

Sounds* Manager::getSoundsLibrary()
{
    return _sounds;
}

void Manager::initStages(bool reset)
{
    if(reset)
        _stages.clear();
    
    std::vector<std::string> stages {
        "stage_a.txt",
        "stage_b.txt",
        "stage_c.txt",
        "stage_d.txt"};
    
    for(auto stageString: stages)
    {
        Stage* stageP = Stage::create();
        stageP->initFromFile(kLevelPath + stageString);
        stageP->validateUserLevel(_user->getUserLevel());
        _stages.insert(std::pair<std::string, Stage*>(stageP->getTag(), stageP));
    }
}

Stage* Manager::getStage(std::string str)
{
    return _stages.at(str);
}

void Manager::updateStageLocks()
{
    for (std::map<std::string, Stage*>::iterator it = _stages.begin(); it != _stages.end(); ++it)
    {
        it->second->validateUserLevel(_user->getUserLevel());
    }
    
}

std::vector<std::string> Manager::getAllStageNames()
{
    std::vector<std::string> stageNames;
    for (std::map<std::string, Stage*>::iterator it = _stages.begin(); it != _stages.end(); ++it)
    {
        stageNames.push_back(it->first);
    }
    return stageNames;
    
}

float Manager::getGameSpeedModifier()
{
    // for inteligent rasomaha
    return 1.0f;
}


Store* Manager::getStore()
{
    return _store;
}

SlotHandler* Manager::getSlotHandler()
{
    return _slotHandler;
}

User* Manager::getUser()
{
    return _user;
}

QuestHandler* Manager::getQuests()
{
    return _questHandler;
}



int Manager::getBasketType()
{
    if(getSlotHandler()->isProductEquiped("p_elena_2_medic"))
        return 1;
    
    // [GATIS] nomaini return 0 uz attiecīgu enum int
    
    if(getSlotHandler()->isProductEquiped("p_elena_1_angle"))
        return 0;
    
    if(getSlotHandler()->isProductEquiped("p_elena_3_evil"))
        return 0;
    
    if(getSlotHandler()->isProductEquiped("p_elena_4_control"))
        return 0;
      
    return 0;
}

int Manager::getCoins()
{
    return _coins;
}

std::string Manager::getCoinsString()
{
    char charText[100];
    sprintf(charText, "%d", _coins);
    std::string str(charText);
    return str;
}

void Manager::depositeCoins(int coins)
{
    _coins = _coins + coins;
    _coins_lifetime = _coins_lifetime + coins;
    UserDefault::getInstance()->setIntegerForKey("coins", _coins);
    UserDefault::getInstance()->setIntegerForKey("coins_lifetime", _coins_lifetime);
}

void Manager::removeCoins(int coins)
{
    _coins = _coins - coins;
    UserDefault::getInstance()->setIntegerForKey("coins", _coins);
}

int Manager::getDiamonds()
{
    return _diamonds;
}

std::string Manager::getDiamondsString()
{
    char charText[100];
    sprintf(charText, "%d", _diamonds);
    std::string str(charText);
    return str;
}

void Manager::depositeDiamonds(int diamonds)
{
    _diamonds = _diamonds + diamonds;
    _diamonds_lifetime = _diamonds_lifetime + diamonds;
    UserDefault::getInstance()->setIntegerForKey("diamonds", _diamonds);
    UserDefault::getInstance()->setIntegerForKey("diamonds_lifetime", _diamonds_lifetime);
}

void Manager::removeDiamonds(int diamonds)
{
    _diamonds = _diamonds - diamonds;
    UserDefault::getInstance()->setIntegerForKey("diamonds", _diamonds);
}

void Manager::depositePuppies(int puppies)
{
    _puppies_lifetime = _puppies_lifetime + puppies;
    UserDefault::getInstance()->setIntegerForKey("puppies_lifetime", _puppies_lifetime);
}

std::vector<std::string> Manager::getGameLoopList()
{
    std::vector<std::string> list;
    
    if(getQuests()->getDailyQuestToClaim().size()>0)
        list.push_back("daily");
    
    if(getQuests()->getGeneralQuestToClaim().size()>0)
        list.push_back("quests");
    
    if(getUser()->isLevelUp())
        list.push_back("levelup");
    
    // if user level up list. ("level up")
    // if new item available for unlock
    
    
    return list;
}


cocos2d::Node* Manager::getUserXPnWaller()
{
    Node* node = Node::create();
    Size size(200, 50);

    
    Manager* manager = Manager::getInstance();
    
    
    Sprite* pUserLevelBack = Sprite::createWithSpriteFrameName("ui-hud-user-level-back.png");
    pUserLevelBack->setAnchorPoint(Vec2(.0f, .0f));
    pUserLevelBack->setPosition(0, 0);
    pUserLevelBack->setTag(kTAG_USER_INFO);
    node->addChild(pUserLevelBack);
    
    float percWidth;
    if(!manager->getUser()->isEndGame())
    {
        percWidth = manager->getUser()->getUserXP(true) * 100 / manager->getUser()->getUserXPNextLevel(true);
        
        Sprite* pUserLevelBar = Sprite::createWithSpriteFrameName("ui-hud-user-level.png");
        ProgressTimer* pXP = ProgressTimer::create(pUserLevelBar);
        pXP->setType(ProgressTimer::Type::RADIAL); // kCCProgressTimerTypeRadial
        pXP->setPercentage(percWidth);
        pXP->setAnchorPoint(Vec2(.5f, .5f));
        pXP->setPosition(28.0f, 28.0f);
        node->addChild(pXP);
    }
    else{
        Sprite* pUserLevelBack = Sprite::createWithSpriteFrameName("ui-hud-user-level-end.png");
        pUserLevelBack->setAnchorPoint(Vec2(.5f, .5f));
        pUserLevelBack->setPosition(28.0f, 28.0f);
        node->addChild(pUserLevelBack);
    }
    
    
    
    Label* userLevel = Label::createWithTTF(manager->getUser()->getUserLevelStr(), kFontPath + kFontNameStyle, 44);
    userLevel->setAnchorPoint(Vec2(.5f, .0f));
    userLevel->setColor(Color3B{252, 249, 206});
    userLevel->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    userLevel->setPosition(28.0f, 4.0f);
    node->addChild(userLevel);
    
    
    Vec2 pRBack[4];
    //*
    Label* name = Label::createWithTTF(manager->getUser()->getTitle(), kFontPath + kFontNameStyle, 14);
    name->setAnchorPoint(Vec2(.0f, .5f));
    name->setColor(Color3B::WHITE);
    name->enableShadow(Color4B(0,0,0,55),Size(0.5,-0.5),0);
    name->setPosition(63, 14);
    
    Size titleSize(name->getContentSize().width - 14, 12.5);
    
    auto nameBack= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(titleSize.width, 0);
    pRBack[2] = Vec2(titleSize.width, titleSize.height);
    pRBack[3] = Vec2(0, titleSize.height);
    nameBack->drawPolygon(pRBack, 4, Color4F(0,0,0,.2f), 0, Color4F::BLACK);
    nameBack->setPosition(87.5, 8.5);
    node->addChild(nameBack);
    node->addChild(name);
    //*/
    

    Sprite* coinsIcon = Sprite::createWithSpriteFrameName("ui-hud-coins.png");
    coinsIcon->setAnchorPoint(Vec2(.5f, .5f));
    coinsIcon->setTag(kTAG_USER_COINS_BTN2);
    coinsIcon->setPosition(70, 37);
    
    Label* coins = Label::createWithTTF(manager->getCoinsString(), kFontPath + kFontNameStyle, 24);
    coins->setAnchorPoint(Vec2(.0f, .5f));
    coins->setColor(Color3B::WHITE);
    coins->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    coins->setPosition(88, 36);
    
    Size coinsBackSize(coins->getContentSize().width + 30, 24);
    
    
    float dPos;
    
    auto coinsBarBack= DrawNode::create();
    //Vec2 pRBack[4];
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(coinsBackSize.width, 0);
    pRBack[2] = Vec2(coinsBackSize.width, coinsBackSize.height);
    pRBack[3] = Vec2(0, coinsBackSize.height);
    coinsBarBack->drawPolygon(pRBack, 4, Color4F(0,0,0,.2f), 0, Color4F::BLACK);
    coinsBarBack->setPosition(87.5, 25);
    coinsBarBack->setContentSize(coinsBackSize);
    coinsBarBack->setTag(kTAG_USER_COINS);
    node->addChild(coinsBarBack);
    
    node->addChild(coinsIcon);
    node->addChild(coins);
    
    
    Sprite* coinsBuy = Sprite::createWithSpriteFrameName("ui-main-buy.png");
    coinsBuy->setAnchorPoint(Vec2(.0f, .5f));
    coinsBuy->setPosition(coinsBackSize.width + 64, 37);
    coinsBuy->setTag(kTAG_USER_COINS_BTN);
    node->addChild(coinsBuy);
    
    dPos = coinsBuy->getPositionX() + 44;
    
    auto diamondsIconBack = Sprite::createWithSpriteFrameName("ui-hud-diamonds-back.png");
    diamondsIconBack->setAnchorPoint(Vec2(.5f, .5f));
    diamondsIconBack->setPosition(dPos, 38.5);
    node->addChild(diamondsIconBack);
    
    
    Sprite* diamondsIcon = Sprite::createWithSpriteFrameName("ui-hud-diamonds.png");
    diamondsIcon->setAnchorPoint(Vec2(.5f, .5f));
    diamondsIcon->setTag(kTAG_USER_DIAMONDS_BTN);
    diamondsIcon->setPosition(dPos, 38.5);

    
    Label* diamonds = Label::createWithTTF(manager->getDiamondsString(), kFontPath + kFontNameStyle, 24);
    diamonds->setAnchorPoint(Vec2(.0f, .5f));
    diamonds->setColor(Color3B::WHITE);
    diamonds->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    diamonds->setPosition(dPos + 20, 36);
    
    Size diamondsBackSize(diamonds->getContentSize().width + 34, 24);
    
    //dPos = dPos;
    
    auto diamondsBarBack= DrawNode::create();
    //Vec2 pRBack[4];
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(diamondsBackSize.width, 0);
    pRBack[2] = Vec2(diamondsBackSize.width, diamondsBackSize.height);
    pRBack[3] = Vec2(0, diamondsBackSize.height);
    diamondsBarBack->drawPolygon(pRBack, 4, Color4F(0,0,0,.2f), 0, Color4F::BLACK);
    diamondsBarBack->setPosition(dPos + 16.5, 25);
    diamondsBarBack->setContentSize(diamondsBackSize);
    diamondsBarBack->setTag(kTAG_USER_DIAMONDS);
    node->addChild(diamondsBarBack);
    
    node->addChild(diamondsIcon);
    node->addChild(diamonds);
    
    dPos = dPos + diamondsBackSize.width;
    
    Sprite* diamondsBuy = Sprite::createWithSpriteFrameName("ui-main-buy-iap.png");
    diamondsBuy->setAnchorPoint(Vec2(.0f, .5f));
    diamondsBuy->setPosition(dPos - 7, 37);
    diamondsBuy->setTag(kTAG_USER_DIAMONDS_BTN2);
    node->addChild(diamondsBuy);
    
    size.width = dPos + diamondsBuy->getContentSize().width;
    
    if(Manager::getInstance()->getDebugTools())
    {
        auto dNode = DrawNode::create();
        Vec2 rectangle[4];
        rectangle[0] = Vec2(0, 0);
        rectangle[1] = Vec2(size.width, 0);
        rectangle[2] = Vec2(size.width, size.height);
        rectangle[3] = Vec2(0, size.height);
        dNode->drawPolygon(rectangle, 4, Color4F(0.25f, 0.65f, 0.78f, 0.2f), 1, Color4F::RED);
        node->addChild(dNode);
    }
    
    
//    node->setContentSize(pUserLevelBack->getContentSize());
    node->setAnchorPoint(Vec2(0.0f, 1.0f));
    
    node->setContentSize(size);

    //DRAW TOOLTIP
    auto user = Manager::getInstance()->getUser();
    Node* nodeToolTip = Node::create(); //_toolTip
    
    Size toolTipSize(140, 65);
    if(user->isEndGame())
        toolTipSize = Size(160, 55);
    
    auto toolTipBack= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(toolTipSize.width, 0);
    pRBack[2] = Vec2(toolTipSize.width, toolTipSize.height);
    pRBack[3] = Vec2(0, toolTipSize.height);
    toolTipBack->drawPolygon(pRBack, 4, Color4F(1.0f,1.0f,1.0f,1.0f), 0, Color4F::BLACK);
    nodeToolTip->addChild(toolTipBack);
    
    nodeToolTip->setAnchorPoint(Vec2(0.0f, 1.0f));
    nodeToolTip->setContentSize(toolTipSize);
        
        
    Label* exptext = Label::createWithTTF("Experience Level " + user->getUserLevelStr(), kFontPath + kFontNameHUD, 10);
    exptext->setAnchorPoint(Vec2(.0f, .5f));
    exptext->setColor(Color3B::BLACK);
    exptext->setPosition(5, toolTipSize.height - 10);
    nodeToolTip->addChild(exptext);
    
    
        
    if(user->isEndGame())
    {
        std::string descriptionTxt = "You are the ultimate puppy whisperer! Complete daily quests to not lose the grip!";
        
        Label* description = Label::createWithTTF(descriptionTxt, kFontPath + kFontNameHUD, 8);
        description->setAnchorPoint(Vec2(.0f, .5f));
        description->setColor(Color3B::BLACK);
        description->setPosition(5, toolTipSize.height - 40);
        description->setDimensions(toolTipSize.width - 10, 40);
        nodeToolTip->addChild(description);
        
    }
    else{
        
        Label* text = Label::createWithTTF("Experience Points to next level " + user->getUserXPStr(true) + "/" + user->getUserXPNextLevelStr(true), kFontPath + kFontNameHUD, 8);
        text->setAnchorPoint(Vec2(.0f, .5f));
        text->setColor(Color3B::BLACK);
        text->setPosition(5, toolTipSize.height - 40);
        text->setDimensions(toolTipSize.width - 10, 40);
        nodeToolTip->addChild(text);
        
        std::string descriptionTxt = "Earn Experience Points by completed the quests";
        
        Label* description = Label::createWithTTF(descriptionTxt, kFontPath + kFontNameHUD, 8);
        description->setAnchorPoint(Vec2(.0f, .5f));
        description->setColor(Color3B::BLACK);
        description->setPosition(5, toolTipSize.height - 65);
        description->setDimensions(toolTipSize.width - 10, 40);
        nodeToolTip->addChild(description);
        
    }
    nodeToolTip->setVisible(false);
    nodeToolTip->setTag(kTAG_USER_INFO_TOOLTIP);
    node->addChild(nodeToolTip);
    
    return node;
}

cocos2d::Node* Manager::getTactics(bool resetBtn)
{
    Node* node = Node::create();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    float height = 10.0f;
    
    Vec2 pRBack[4];
    auto blackLine= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(visibleSize.width, 0);
    pRBack[2] = Vec2(visibleSize.width, height);
    pRBack[3] = Vec2(0, height);
    blackLine->drawPolygon(pRBack, 4, Color4F(0.0f,0.0f,0.0f,1.0f), 0, Color4F::BLACK);
    node->addChild(blackLine);
    
    float shadowHeight = 26.0f;
    
    auto shadowLine = DrawNode::create();
    pRBack[0] = Vec2(0, height);
    pRBack[1] = Vec2(visibleSize.width, height);
    pRBack[2] = Vec2(visibleSize.width, height + shadowHeight);
    pRBack[3] = Vec2(0, height + shadowHeight);
    shadowLine->drawPolygon(pRBack, 4, Color4F(Color4B(0,0,0,60)), 0, Color4F::BLACK);
    node->addChild(shadowLine);
    
    Label* title = Label::createWithTTF("TACTICS", kFontPath + kFontNameStyle, 18);
    title->setAnchorPoint(Vec2(.5f, .5f));
    title->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
//    title->setColor(Color3B{252, 208, 74});
    title->setColor(Color3B::WHITE);
    title->setPosition(visibleSize.width / 2 - 167, height + shadowHeight / 2 - 1);
    node->addChild(title);
    
    
    int i = 0;
    float frameMargin = 2.0f;
    std::string slotSpriteStr, iconSpriteStr;
    for(auto slot: Manager::getInstance()->getSlotHandler()->getAllSlots())
    {
        ProductGroup group = slot->getProductGroup();
        
        if(group == GROUP_OUTFIT)
        {
            slotSpriteStr = "tactics-slot-outfit.png";
        }
        else if(group == GROUP_TALISMAN)
        {
            slotSpriteStr = "tactics-slot-talisman.png";
            iconSpriteStr = "store-tab-talisman-0.png";
        }
        else if(group == GROUP_POWERUP)
        {
            slotSpriteStr = "tactics-slot-powerup.png";
            iconSpriteStr = "store-tab-powerup-0.png";
        }
        
        Product* product = nullptr;
        if(group != GROUP_OUTFIT)
        {
            product = Manager::getStore()->getProductByKey(slot->getStoreKey());
            
            bool bought = slot->isBought();
            if(!bought || slot->isEmpty())
            {
                slotSpriteStr = "tactics-slot-empty.png";
                
            }
            
        }
        
        Sprite* slotSprite = Sprite::createWithSpriteFrameName(slotSpriteStr);
        
        std::string nodeName = "";
        if(group == GROUP_OUTFIT)
        {
            nodeName = "1_outfits";
        }
        else{
            if(product->isLocked() || !product->hasAmount())
                nodeName = "4_unlocks";
            else
            {
                if(group == GROUP_POWERUP)
                    nodeName = "3_powerups";
                else if(group == GROUP_TALISMAN)
                    nodeName = "2_talismans";
            }
        }
        slotSprite->setName(nodeName);
        slotSprite->setTag(kTAG_TACTICS);
        
        Vec2 pos(visibleSize.width / 2 - ((slotSprite->getContentSize().width + frameMargin) * 3) + slotSprite->getContentSize().width / 2 + (i * (slotSprite->getContentSize().width + frameMargin)), slotSprite->getContentSize().height / 2 + frameMargin);
        slotSprite->setPosition(pos);
        node->addChild(slotSprite);
        
        if(product != nullptr)
        {
            if(slot->isEmpty())
            {
                Sprite* spriteIcon = Sprite::createWithSpriteFrameName(iconSpriteStr);
                spriteIcon->setOpacity(155);
                spriteIcon->setPosition(pos);
                node->addChild(spriteIcon);
            }
            
            Size consSize(slotSprite->getContentSize().width - 5, 20);
            
            Vec2 rectangle[4];
            auto productRequired = DrawNode::create();
            rectangle[0] = Vec2(0, 0);
            rectangle[1] = Vec2(consSize.width, 0);
            rectangle[2] = Vec2(consSize.width, consSize.height);
            rectangle[3] = Vec2(0, consSize.height);
            productRequired->drawPolygon(rectangle, 4, Color4F(Color4B(0,0,0,100)), 0, Color4F::RED);
            productRequired->setContentSize(consSize);
            productRequired->setAnchorPoint(Vec2(0.5f, 0.5f));
            productRequired->setPosition(pos.x, pos.y - 1);
            
            
            
            if(product->isLocked())
            {
                node->addChild(productRequired);
                
                auto productRequiredTxt = Label::createWithTTF("Level "+ product->getLevelRequiredStr() +" required", kFontPath + kFontNameBold, 6.5f);
                productRequiredTxt->setColor(Color3B::WHITE);
                productRequiredTxt->enableShadow(Color4B(0,0,0,75),Size(1,-1),0);
                productRequiredTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
                productRequiredTxt->setContentSize(consSize);
                productRequiredTxt->setLineHeight(12.0f);
                productRequiredTxt->setPosition(pos.x, pos.y - 3);
                productRequiredTxt->setDimensions(consSize.width - 5,consSize.height);
                productRequiredTxt->setAlignment(TextHAlignment::CENTER);
                node->addChild(productRequiredTxt);
            }
            else if(!product->hasAmount())
            {
                node->addChild(productRequired);
                
                auto btnUnlock = Sprite::createWithSpriteFrameName("tactics-button-unlock.png");
                btnUnlock->setPosition(pos.x, pos.y - 1);
                node->addChild(btnUnlock);
            }
            
        }
        
        
        if(!slot->isEmpty())
        {
            Product* productInSlot = Manager::getInstance()->getStore()->getProductByKey(slot->getProductEquiped());
            
            std::string productIcon;
            if(group == GROUP_OUTFIT)
            {
                productIcon = "elena-"+productInSlot->getSpriteName()+"-IDLE-0.png";
            }
            else
            {
                productIcon = productInSlot->getSpriteName() + ".png";
            }
                
            Sprite* spriteIcon = Sprite::createWithSpriteFrameName(productIcon);
            if(group == GROUP_OUTFIT)
                spriteIcon->setScale(0.45f);
            else
                spriteIcon->setScale(0.55f);
            spriteIcon->setPosition(pos);
            node->addChild(spriteIcon);
            
            if(group == GROUP_POWERUP || group == GROUP_TALISMAN)
            {
                int uses = productInSlot->getUses();
                if(Manager::getInstance()->getSlotHandler()->isProductEquiped("p_elena_1_angel") && group == GROUP_POWERUP) uses += 2;
                int amount = (productInSlot->getAmount() > uses)? uses : productInSlot->getAmount();
                slot->setUses(amount);
                
                //CCLOG("init uses %d / %d", uses, amount);
                
                
                if(group == GROUP_POWERUP)
                {
                    char charText[100];
                    sprintf(charText, "%d", amount);
                    std::string amountStr(charText);
                    
                    auto productUses = Label::createWithTTF("x" + amountStr, kFontPath + kFontNameStyle, 10);
                    productUses->setAnchorPoint(Vec2(1.0f, 0.0f));
                    productUses->enableShadow(Color4B(0,0,0,150),Size(.5f,-.5f),0);
                    productUses->setColor(Color3B{252, 249, 206});
                    productUses->setPosition(pos.x + 17.0f, pos.y - 18.0f);
                    node->addChild(productUses);
                }
            }
        }
        
        
        i++;
    }
    
    if(resetBtn)
    {
        Sprite* btn = Sprite::createWithSpriteFrameName("tactics-button-clear.png");
//        btn->setPosition(Vec2(visibleSize.width / 2 + 164, 22));
        btn->setPosition(Vec2(visibleSize.width / 2 + 166, 28));
        btn->setContentSize(Size(btn->getContentSize().width + 10, btn->getContentSize().height + 10));
        btn->setTag(kTAG_TACTICS);
        btn->setName("reset");
        node->addChild(btn);
    }
    else
    {
        Sprite* btn = Sprite::createWithSpriteFrameName("tactics-button-change.png");
//        btn->setPosition(Vec2(visibleSize.width / 2 + 164, 22));
        btn->setPosition(Vec2(visibleSize.width / 2 + 166, 28));
        btn->setContentSize(Size(btn->getContentSize().width + 10, btn->getContentSize().height + 10));
        btn->setTag(kTAG_TACTICS);
        btn->setName("1_outfits");
        node->addChild(btn);
    }
    
    
    
    Size size(visibleSize.width, 45);
    node->setContentSize(size);
   
    
    if(Manager::getInstance()->getDebugTools())
    {
        auto dNode = DrawNode::create();
        Vec2 rectangle[4];
        rectangle[0] = Vec2(0, 0);
        rectangle[1] = Vec2(size.width, 0);
        rectangle[2] = Vec2(size.width, size.height);
        rectangle[3] = Vec2(0, size.height);
        dNode->drawPolygon(rectangle, 4, Color4F(0.25f, 0.65f, 0.78f, 0.2f), 1, Color4F::RED);
        node->addChild(dNode);
    }
    return node;
}



/// RESET ///

void Manager::resetCoins()
{
    _coins = 0;
    UserDefault::getInstance()->setIntegerForKey("coins", 0);
    _diamonds = 0;
    UserDefault::getInstance()->setIntegerForKey("diamonds", 0);
    
    getStage("stage_a")->reset();
    getStage("stage_b")->reset();
    getStage("stage_c")->reset();
    getStage("stage_d")->reset();
}


void Manager::resetAll()
{
    getQuests()->reset();
    getUser()->reset();
    this->initStages(true);
    getStore()->updateLocks(getUser()->getUserLevel());
}

void Manager::resetAllStore()
{
    getStore()->reset();
    getSlotHandler()->reset();

}

void Manager::completeAll()
{
    getUser()->becomePuppyWhisperer();
    this->initStages(true);
    getStore()->updateLocks(getUser()->getUserLevel());
}




/////

void Manager::initDebugTools()
{
    int tool = UserDefault::getInstance()->getIntegerForKey("debugTools", 0);
    _debugTools = (tool == 0) ? false : true;
}

bool Manager::getDebugTools()
{
    return _debugTools;
}

void Manager::setDebugTools(bool tool)
{
    int val = ((tool) ? 1 : 0);
    UserDefault::getInstance()->setIntegerForKey("debugTools", val);
    _debugTools = val;
}




