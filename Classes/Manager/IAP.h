#ifndef PuppyMadness__IAP
#define PuppyMadness__IAP

#include "cocos2d.h"


typedef struct _iap_details{
    
    std::string key;
    std::string description;
    std::string sprite;
    int price;
    int coins;
    
} IAPDetails;



class IAP{
    
public:
    
    IAP();
    
    ~IAP();
    
    static IAP* create();
    
    void init(IAPDetails iap);
    
    std::string getDescription();
    
    std::string getSpriteName();
    
    std::string getPriceStr();
    
    int getPrice();
    
    std::string getKey();
    
    int getCoins();
    
    std::string getCoinsStr();
    
    
private:
    
    IAPDetails _iap;
    
};





#endif

