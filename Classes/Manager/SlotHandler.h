#ifndef PuppyMadness__SlotHandler
#define PuppyMadness__SlotHandler

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Slots.h"

#else
#include "Slots.h"

#endif



#include "cocos2d.h"



class SlotHandler{
    
    
public:
    
    SlotHandler();
    
    ~SlotHandler();
    
    static SlotHandler* create();
    
    void init();
    
    
    std::vector<Slot*> getAllSlots();
    
    Slot* getSlotByName(std::string name);
    
    std::vector<std::string> getAllProductsEquiped();
    
    std::vector<int> getAvailablePowerUps();
    
    bool anyFreeSlots(ProductGroup group, std::string key);
    
    void equipProduct(ProductGroup group, std::string key);
    
    bool isProductEquiped(std::string key);
    
    void useTalismans();
    
    int getPowerUpUses(int powerUp);
    
    bool hasCharges(int powerUp);
    
    void usePowerUp(int powerUp);
    
    void clearSlots();
    
    void reset();
    
    Slot* findSlotByProduct(std::string key);    

    
private:
    
    std::map<std::string, Slot*> _slots;
    
    std::string parsePowerUp(int powerUp);
    

    
};


#endif
