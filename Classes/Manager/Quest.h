#ifndef PuppyMadness__Quest
#define PuppyMadness__Quest

#include "cocos2d.h"


/*
 "title":"Quest Title"
 "tag":"001_quest",
 "stage":"any"/"stage_[a-d]"
 "distance":"overall"/"round",
 "event":"event_str",
 "param":[{"param":1}],
 "amount":999,
 "order":"any"/"inrow", @notimplemented
 "xp":999,
 "coins":999
 
 
 
 // EVENT
 // @harmful
 // catch, spears, ground, swim, snake, fence, bomb

 // @not harmful
 // teleport, challenge_off, powerup_use
 
 // PARAM
 // puppy: 1-mopsis 2-mailo 3-huskey
 // height: 0 - low, 1 - heigh
 // powerup: 1-5 for each powerup active
 // jump: 0 - JUMP, 1 - FALL
 // challenge: 1-5 for challenge active
 // @not_implemented: fenced, bombed, teleported, basket_type, talisman
 
 // once tactics is up, basket_type, talisam could be realy challenge full
 
 
 
*/

class Quest{
    
public:
    
    Quest();
    ~Quest();
    
    void init(std::string tag, std::string title, int amount, int coins, int xp, bool daily);
    
    void initGoal(std::string event, std::string stage, std::map<std::string, int> param, bool singleRound);
    
    bool eventMatch(std::string event, std::string levelPath);
    
    CC_SYNTHESIZE(int, _minLevel, MinLevel);
    
    void initParamCheck();
    
    void paramCheck(std::string string, int val);
    
    bool goalParamMatch();
    
    bool isCompleted();
    
    bool isClaimend();
    
    bool isSeen();
    
    void setClaim(bool claim);
    
    void setSeen(bool seen);
    
    std::string getTitle();
    
    std::string getTag();
    
    int getAmountGoal();
    
    int getAmountNow();
    
    std::string getAmountGoalStr();
    
    std::string getAmountNowStr();
    
    bool isDaily();
    
    int getRewardCoins();
    
    int getRewardXP();
    
    std::string getRewardCoinsStr();
    
    std::string getRewardXPStr();
    
    void reload();
    
    void reset();
    
    void resetVariables();
    
    
private:
    
    void setGoalMatch();
    
    std::string _tag;
    
    std::string _title;
    
    std::string _stage;
    
    bool _completed;
    
    bool _claimed;
    
    bool _seen;
    
    bool _singleRound;
    
    int _amountGoal;
    
    int _amountNow;
    
    int _minLvl;
    
    void setCompleted(bool completed);
    
    void saveAmount();
    
    std::string _event;
    
    std::map<std::string, int> _param;
    
    int _paramCount;
    
    int _paramCheck;
    
    int _rewardCoins;
    
    int _rewardXP;
    
    bool _daily;
    
};


#endif
