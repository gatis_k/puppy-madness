#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Slots.h"
#include "Manager/Manager.h"

#else
#include "Slots.h"
#include "Manager.h"

#endif
////////////////////////////


USING_NS_CC;


Slot::Slot()
{

}

Slot::~Slot()
{
    
    
}

Slot* Slot::create()
{
    Slot* pointer = new Slot;
    return pointer;
}

void Slot::init(ProductGroup group, std::string tag, std::string productKey)
{
    _group = group;
    _tag = tag;
    _productKey = productKey;
    
    _product = UserDefault::getInstance()->getStringForKey(_tag.c_str(), "empty");
}

bool Slot::isEmpty()
{
    if(_product == "empty")
        return true;
    else
        return false;
}

std::string Slot::getProductEquiped()
{
    return _product;
}

void Slot::setProductEquiped(std::string product)
{
    _product = product;
    UserDefault::getInstance()->setStringForKey(_tag.c_str(), _product);
    CCLOG("Equiped");
}

std::string Slot::getStoreKey()
{
    return _productKey;
}

ProductGroup Slot::getProductGroup()
{
    return _group;
}

void Slot::clear()
{
    _product = "empty";
    _uses = 0;
    UserDefault::getInstance()->setStringForKey(_tag.c_str(), "empty");
}

bool Slot::isBought()
{
    if(_productKey != "")
        return Manager::getInstance()->getStore()->getProductByKey(_productKey)->hasAmount();
    else
        return true;
}