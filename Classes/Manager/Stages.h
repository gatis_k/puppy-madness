#ifndef PuppyMadness__Stages
#define PuppyMadness__Stages


#include "cocos2d.h"

typedef enum _stage_name
{
    STAGE_A,
    STAGE_B,
    STAGE_C,
    STAGE_D
} StageName;


class Stage
{
    
public:
  
    Stage();
    ~Stage();
    
    static Stage* create();
    
    void initFromFile(std::string fileName);
    
    void validateUserLevel(int level);

    bool isLocked();

    std::string getTitle();
    
    std::string getTag();
    
    StageName getStageName();
    
    int getRequiredUserLevel();
    
    std::string getRequiredUserLevelStr();
    
    int getHighestScore();
    std::string getHighestScoreStr();
    void setHighestScore(int score);
    
    int getHighestSpeed();
    void setHighestSpeed(int speed);
    
    int getHighestCatch();
    std::string getHighestCatchStr();
    void setHighestCatch(int count);
    
    float getLongestTime();
    std::string getLongestTimeStr();
    void setLongestTime(float time);
    
    void clearLastGameData();
    
    bool hasLastGameRecord();
    
    void reset();
    
private:
    
    bool _locked;
    
    std::string _title;
    
    std::string _tag;
    
    StageName _name;
    
    int _requiredUserLevel;
    
    int _highestScore;
    
    int _highestCatch;
    
    int _highestSpeed;
    
    float _longestTime;

};
#endif

