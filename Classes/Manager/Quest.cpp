#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Quest.h"
#include "Utils/Jzon.h"

#else
#include "Quest.h"
#include "Jzon.h"

#endif
////////////////////////////


USING_NS_CC;


Quest::Quest():
_paramCount(0),
_amountNow(0),
_rewardCoins(0),
_rewardXP(0),
_minLvl(0),
_daily(false),
_completed(false),
_claimed(false),
_seen(false){
    
}

Quest::~Quest()
{
    
}

void Quest::init(std::string tag, std::string title, int amount, int coins, int xp, bool daily)
{
    _tag = tag;
    _title = title;
    _amountGoal = amount;
    _rewardCoins = coins;
    _rewardXP = xp;
    _daily = daily;
    
    std::string json_str = UserDefault::getInstance()->getStringForKey(_tag.c_str(), "{\"completed\":0,\"claimed\":0,\"amount\":0,\"seen\":0");
    
    Jzon::Object rootNode;
    Jzon::Parser parser(rootNode, json_str);
    
    if (parser.Parse())
    {
        _completed = (rootNode.Get("completed").ToInt() == 1) ? true : false;
        _claimed = (rootNode.Get("claimed").ToInt() == 1) ? true : false;
        _amountNow = rootNode.Get("amount").ToInt();
        _seen = (rootNode.Get("seen").ToInt() == 1) ? true : false;
    }
    else
    {
        CCLOG("JSON ERROR ON QUEST LOAD %s", _tag.c_str());
    }
}

void Quest::initGoal(std::string event, std::string stage, std::map<std::string, int> param, bool singleRound)
{
    _event = event;
    _stage = stage;
    _param = param;
    _singleRound = singleRound;
    _paramCount = 0;
    
    for (std::map<std::string, int>::iterator it = _param.begin(); it != _param.end(); ++it)
        _paramCount++;
    
    
}

bool Quest::eventMatch(std::string event, std::string levelPath)
{

    if(_stage != "any" && _stage != levelPath)
        return false;
    
    if(event == _event)
        return true;
    else
        return false;
}

void Quest::initParamCheck()
{
    _paramCheck = 0;
    
    /*
    std::string param = "";
    for (std::map<std::string, int>::iterator it = _param.begin(); it != _param.end(); ++it)
    {
        param +=  it->first + "; ";
    }
    if(_tag == "006_quest")
    {
    //    CCLOG("006 QUEST: %s val %d/%d param: %s", _tag.c_str(), _amountNow, _amountGoal, param.c_str());
        
    } */
}

void Quest::paramCheck(std::string string, int val)
{
    
    for (std::map<std::string, int>::iterator it = _param.begin(); it != _param.end(); ++it)
    {
        if(it->first == string && it->second == val)
        {
            _paramCheck++;
            break;
        }
    }
}

bool Quest::goalParamMatch()
{
    if(_paramCheck == _paramCount && !_completed && !_claimed)
    {
        this->setGoalMatch();
        return true;
    }
    else
    {
        return false;
    }
}

void Quest::setGoalMatch()
{
    _amountNow++;
    if(_amountNow >= _amountGoal)
        this->setCompleted(true);
    else
    {
        if(!_singleRound)
            this->saveAmount();
    }
}


void Quest::saveAmount()
{
    Jzon::Object root;
    root.Add("completed", 0);
    root.Add("claimed", 0);
    root.Add("amount", _amountNow);
    root.Add("seen", (_seen) ? 1 : 0);
    
    //CCLOG("amount saved %d", _amountNow);
    
    Jzon::Writer writer(root, Jzon::StandardFormat);
    writer.Write();
    std::string str = writer.GetResult();
    
    UserDefault::getInstance()->setStringForKey(_tag.c_str(), str);
}


void Quest::setCompleted(bool completed)
{
    _completed = completed;
    if(completed)
    {
        Jzon::Object root;
        root.Add("completed", 1);
        root.Add("claimed", 0);
        root.Add("amount", _amountNow);
        root.Add("seen", (_seen) ? 1 : 0);
        
        Jzon::Writer writer(root, Jzon::StandardFormat);
        writer.Write();
        std::string str = writer.GetResult();
        
        UserDefault::getInstance()->setStringForKey(_tag.c_str(), str);
    }
}

void Quest::setClaim(bool claim)
{
    _claimed = claim;
    if(claim)
    {
        Jzon::Object root;
        root.Add("completed", 1);
        root.Add("claimed", (_claimed) ? 1 : 0);
        root.Add("amount", _amountNow);
        root.Add("seen", (_seen) ? 1 : 0);
        
        Jzon::Writer writer(root, Jzon::StandardFormat);
        writer.Write();
        std::string str = writer.GetResult();
        
        UserDefault::getInstance()->setStringForKey(_tag.c_str(), str);
    }
}

bool Quest::isSeen()
{
    return _seen;
}

void Quest::setSeen(bool seen)
{
    _seen = seen;
    
    Jzon::Object root;
    root.Add("completed", (_completed) ? 1 : 0);
    root.Add("claimed", (_claimed) ? 1 : 0);
    root.Add("amount", _amountNow);
    root.Add("seen", (_seen) ? 1 : 0);
    
    Jzon::Writer writer(root, Jzon::StandardFormat);
    writer.Write();
    std::string str = writer.GetResult();
    
    UserDefault::getInstance()->setStringForKey(_tag.c_str(), str);
    
}

bool Quest::isDaily()
{
    return _daily;
}

int Quest::getRewardCoins()
{
    return _rewardCoins;
}

int Quest::getRewardXP()
{
    return _rewardXP;
}

std::string Quest::getRewardCoinsStr()
{
    char charText[100];
    sprintf(charText, "%d", _rewardCoins);
    std::string str(charText);
    return str;
}

std::string Quest::getRewardXPStr()
{
    char charText[100];
    sprintf(charText, "%d", _rewardXP);
    std::string str(charText);
    return str;
}

void Quest::reload()
{
    if(_singleRound && !_completed )
        _amountNow = 0;
}


void Quest::reset()
{
    Jzon::Object root;
    root.Add("completed", 0);
    root.Add("claimed", 0);
    root.Add("amount", 0);
    root.Add("seen", 0);
    
    Jzon::Writer writer(root, Jzon::StandardFormat);
    writer.Write();
    std::string str = writer.GetResult();
    
    UserDefault::getInstance()->setStringForKey(_tag.c_str(), str);
}

void Quest::resetVariables()
{
    _completed = false;
    _claimed =  false;
    _amountNow = 0;
    _seen = false;
}


bool Quest::isCompleted()
{
    return _completed;
}

bool Quest::isClaimend()
{
    return _claimed;
}

std::string Quest::getTitle()
{
    return _title;
}

std::string Quest::getTag()
{
    return _tag;
}

int Quest::getAmountGoal()
{
    return _amountGoal;
}

int Quest::getAmountNow()
{
    return _amountNow;
}

std::string Quest::getAmountGoalStr()
{
    char charText[100];
    sprintf(charText, "%d", _amountGoal);
    std::string str(charText);
    return str;
}

std::string Quest::getAmountNowStr()
{
    char charText[100];
    sprintf(charText, "%d", _amountNow);
    std::string str(charText);
    return str;
}