#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/IAP.h"

#else
#include "IAP.h"

#endif
////////////////////////////


USING_NS_CC;


IAP::IAP()
{
    
}

IAP::~IAP()
{
    
}

IAP* IAP::create()
{
    IAP* pointer = new IAP;
    return pointer;
}

void IAP::init(IAPDetails iap)
{
    _iap = iap;
}


std::string IAP::getDescription()
{
    return _iap.description;
}

std::string IAP::getSpriteName()
{
    return _iap.sprite;
}

std::string IAP::getPriceStr()
{
    
    char charText[100];
    sprintf(charText, "%d", _iap.price);
    std::string priceStr(charText);
    return priceStr;
}

int IAP::getPrice()
{
    return _iap.price;//_iap.price;
}

std::string IAP::getKey()
{
    return _iap.key;
}

int IAP::getCoins()
{
    return _iap.coins;
}

std::string IAP::getCoinsStr()
{
    
    char charText[100];
    sprintf(charText, "%d", _iap.coins);
    std::string priceStr(charText);
    return priceStr;
}

