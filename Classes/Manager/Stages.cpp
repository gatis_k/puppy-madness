#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Stages.h"
#include "Utils/Jzon.h"

#else
#include "Stages.h"
#include "Jzon.h"

#endif
////////////////////////////


USING_NS_CC;

Stage::Stage()
{
    
}

Stage::~Stage()
{
    
}

Stage* Stage::create()
{
    Stage* pointer = new Stage;
    return pointer;
}

void Stage::initFromFile(std::string fileName)
{
    std::string jsonStr = FileUtils::getInstance()->getStringFromFile(fileName);
    
    Jzon::Object rootNode;
    Jzon::Parser parser(rootNode, jsonStr);
    if (parser.Parse())
    {
        _title = rootNode.Get("title").ToString();
        _tag = rootNode.Get("tag").ToString();
        _requiredUserLevel = rootNode.Get("required_level").ToInt();
        
        _locked = UserDefault::getInstance()->getIntegerForKey(_tag.c_str(), 1) == 1 ? true : false;
        
        _highestScore = UserDefault::getInstance()->getIntegerForKey((_tag+"_score").c_str(), 0);
        _highestSpeed = UserDefault::getInstance()->getIntegerForKey((_tag+"_speed").c_str(), 0);
        _highestCatch = UserDefault::getInstance()->getIntegerForKey((_tag+"_catch").c_str(), 0);
        _longestTime = UserDefault::getInstance()->getFloatForKey((_tag+"_time").c_str(), 0);
        
        if(_tag == "stage_a")
            _name = STAGE_A;
        else if(_tag == "stage_b")
            _name = STAGE_A; // STAGE_B
        else if(_tag == "stage_c")
            _name = STAGE_C;
        else if(_tag == "stage_d")
            _name = STAGE_D;
        
    }else{
        
        CCLOG("%s DOESN'T INIT, CHECK JSON STRING QUALITY", fileName.c_str());
        
    }
}

void Stage::validateUserLevel(int level)
{
    if(level >= _requiredUserLevel)
        _locked = false;
    else
        _locked = true;
}


bool Stage::isLocked()
{
    return _locked;
}

std::string Stage::getTitle()
{
    return _title;
}

std::string Stage::getTag()
{
    return _tag;
}

StageName Stage::getStageName()
{
    return _name;
}

int Stage::getRequiredUserLevel()
{
    return _requiredUserLevel;
}

std::string Stage::getRequiredUserLevelStr()
{
    char charText[100];
    sprintf(charText, "%d", _requiredUserLevel);
    std::string priceStr(charText);
    
    return priceStr;
}

int Stage::getHighestScore()
{
    return _highestScore;
}

std::string Stage::getHighestScoreStr()
{
    char charText[100];
    sprintf(charText, "%d", _highestScore);
    std::string str(charText);
    return str;
}

void Stage::setHighestScore(int score)
{
    if(score > _highestScore)
    {
        _highestScore = score;
        UserDefault::getInstance()->setIntegerForKey((_tag+"_score").c_str(), score);
    }
}

int Stage::getHighestSpeed()
{
    return _highestSpeed;
}

void Stage::setHighestSpeed(int speed)
{
    if(speed > _highestSpeed)
    {
        _highestSpeed = speed;
        UserDefault::getInstance()->setIntegerForKey((_tag+"_speed").c_str(), speed);
    }
}

int Stage::getHighestCatch()
{
    return _highestCatch;
}

std::string Stage::getHighestCatchStr()
{
    char charText[100];
    sprintf(charText, "%d", _highestCatch);
    std::string str(charText);
    return str;
}
void Stage::setHighestCatch(int count)
{
    if(count > _highestCatch)
    {
        _highestCatch = count;
        UserDefault::getInstance()->setIntegerForKey((_tag+"_catch").c_str(), count);
    }
}


float Stage::getLongestTime()
{
    return _longestTime;
}



std::string Stage::getLongestTimeStr()
{
    int min = (int)_longestTime / 60;
    int sec = (int)_longestTime % 60;
    int milli = (_longestTime-(long)_longestTime) * 10;
    
    char charText[100];
    sprintf(charText, "%02i:%02i.%01i", min, sec, milli);
    std::string str(charText);
    return str;
}

void Stage::setLongestTime(float time)
{
    if(time > _longestTime)
    {
        _longestTime = time;
        UserDefault::getInstance()->setFloatForKey((_tag+"_time").c_str(), _longestTime);
    }
}


void Stage::reset()
{
    _highestScore = 0;
    UserDefault::getInstance()->setIntegerForKey((_tag+"_score").c_str(), _highestScore);
    
    _highestSpeed = 0;
    UserDefault::getInstance()->setIntegerForKey((_tag+"_speed").c_str(), _highestScore);
    
    _highestCatch = 0;
    UserDefault::getInstance()->setIntegerForKey((_tag+"_catch").c_str(), _highestCatch);
    
    _longestTime = 0.0f;
    UserDefault::getInstance()->setFloatForKey((_tag+"_time").c_str(), _longestTime);
}

