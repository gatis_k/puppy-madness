#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/Snake.h"

#else
#include "Snake.h"

#endif
////////////////////////////




USING_NS_CC;


Snake::Snake():
_active(false),
_inPosition(true),
_speedModifier(1)
{
    
    _timeTillAttack = 0.0f;
    _attackDelay = 2.0f;
    _state = SNAKE_RESET;
    
    auto tail = Sprite::createWithSpriteFrameName("snake-tail.png");
    tail->setAnchorPoint(Vec2(0.5f, 1.0f));
    tail->setPosition(21, 2);
    this->addChild(tail);
    
}

Snake::~Snake()
{
	CC_SAFE_RELEASE(_animationChew);
}

Snake* Snake::create()
{
	Snake * sprite = new Snake();
	cocos2d::SpriteFrame *frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("snake-head-0.png"); // snake-head-1.png
	if (sprite && sprite->initWithSpriteFrame(frame))
	{
		sprite->autorelease();
		sprite->initAnimations();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Snake::update(float dt, cocos2d::Vec2 targetPosition)
{
    float currentX = this->getPositionX();
    if(_active)
    {
        switch (_state) {
            case SNAKE_PURSUE:
                
                if (currentX > targetPosition.x + SNAKE_ATTACK_GAP)
                    this->setPositionX(currentX + (dt * SNAKE_SPEED * _speedModifier * -1));
                else if (currentX < targetPosition.x - SNAKE_ATTACK_GAP)
                    this->setPositionX(currentX + (dt * SNAKE_SPEED * _speedModifier));
                else
                {
                    _state = SNAKE_WAIT;
                    animate(SNAKE_WAIT);
                }
                break;
                
            case SNAKE_WAIT:
                
                this->setPositionX(targetPosition.x);
                
                _timeTillAttack += dt;
                if(_timeTillAttack > _attackDelay && _inPosition)
                {
                    _state = SNAKE_ATTACK;
                    animate(SNAKE_ATTACK);
                }
                
                break;
                
            case SNAKE_ATTACK:
                break;
            case SNAKE_RESET:
                
                
                
                break;
        }
    }
    else
    {
        // get bobo off screen
        float currentX = this->getPositionX();
        if(currentX > -100)
            this->setPositionX(currentX + (dt * SNAKE_SPEED_OFFLINE * _speedModifier * -1));
    }
}

void Snake::updatePostAttack(float dt)
{
    float currentY = this->getPositionY();
    
    if(!_inPosition)
    {
        if(currentY > _returnY)
            this->setPositionY(currentY - (dt * SNAKE_SPEED * 2 * _speedModifier));
        else
        {
            this->setPositionY(_returnY);
            _inPosition = true;
        }
    }
}

void Snake::setActive(bool active)
{
    _active = active;
}


bool Snake::canPursue(cocos2d::Vec2 position)
{
    if(_inPosition)
    {
        if(position.x > _basketX - SNAKE_AVOID_GAP && position.y < _basketX + SNAKE_AVOID_GAP)
            return false;
        else
            return true;
    }
    else
        return false;
    
}

bool Snake::canKill(cocos2d::Vec2 position)
{
    if(_state == SNAKE_ATTACK)
    {
        _returnY = this->getPositionY();
        this->setPosition(position);
        _inPosition = false;
        
        return true;
    }
    else
        return false;
}


void Snake::pursue()
{
    _state = SNAKE_PURSUE;
    animate(SNAKE_PURSUE);
}

void Snake::reset()
{
//     CCLOG("snake reset call");
    _state = SNAKE_RESET;
    animate(SNAKE_RESET);
    _timeTillAttack = 0.0f;
}


void Snake::avoidX(float x)
{
	_basketX = x;
}


void Snake::setSpeedModifier(float speed)
{
	_speedModifier = speed;
}


void Snake::animate(SnakeState state)
{
    switch (state) {
        case SNAKE_PURSUE:
			if (this->getActionManager()->getNumberOfRunningActionsInTarget(this) == 0)
				this->runAction(RepeatForever::create(Animate::create(_animationChew)));
            break;
        case SNAKE_WAIT:
			this->getActionManager()->removeAllActionsFromTarget(this);
			this->setSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("snake-head-0.png"));
            break;
        case SNAKE_ATTACK:
			this->getActionManager()->removeAllActionsFromTarget(this);
			this->setSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("snake-head-1.png"));
            break;
        case SNAKE_RESET:
			this->getActionManager()->removeAllActionsFromTarget(this);
			this->setSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("snake-head-1.png"));
			break;
    }
}

void Snake::initAnimations()
{
	Vector<SpriteFrame*> animationVector(2);
	SpriteFrame* animFrame;
	char str[100] = { 0 };
		for (int i = 0; i < 2; i++){
			sprintf(str, "snake-head-%i.png", i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animationVector.pushBack(animFrame);
		}
	_animationChew = Animation::createWithSpriteFrames(animationVector, 0.1f);
	_animationChew->retain();
}