#ifndef PuppyMadness_GameLayer_h
#define PuppyMadness_GameLayer_h

#include "cocos2d.h"
#include "Box2D/Box2D.h"

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/BackgroundLayer.h"
#include "Game/HUDLayer.h"
#include "Game/SimpleDPad.h"
#include "Game/WorldContract.h"

#else
#include "BackgroundLayer.h"
#include "HUDLayer.h"
#include "SimpleDPad.h"
#include "WorldContract.h"

#endif

#define kPlatformIndex 1
#define kPuppyIndex	2
#define kBasketIndex 3
#define kPuppyFallIndex 4
#define kFenceIndex 5
#define kBulletIndex 6
#define kSnakeIndex 6
#define kWaterIndex 7

#define boboRageMode 10

////////////////////////////



typedef enum _game_status{
	GAME_STATUS_READY = 1,
	GAME_STATUS_PLAYING,
	GAME_STATUS_PAUSE,
	GAME_STATUS_OVER
} GameStatus;

typedef enum _challenge_type{
    CHALLENGE_RAIN = 0,
    CHALLENGE_SPAWN,
    CHALLENGE_BOBO,
    CHALLENGE_WIND,
    CHALLENGE_SNAKE
//    CHALLENGE_FENCE
    
} ChallengeType;

const int kChallengeTypeCount = 5;


typedef struct _game_spawn{
	float Timer; 
	float bulletFrequency;
	float Timer2;
	float clusterFrequency;
} GameSpawn;

typedef struct _puppy_catch{
    cocos2d::Vec2 position;
    int type;
    int state;
    int direction;
    // int time;    
} PuppyCatch;


class Platform;
class PlatformTile;
class Basket;
class Puppy;
class Bobo;
class Snake;
class Bullet;
class Extra;
class PowerUp;
class Quest;

class GameLayer : public cocos2d::Layer, public SimpleDPad, public WorldContract
{

public:
	
	GameLayer();
	~GameLayer();

	static GameLayer* create(std::string level);
	virtual bool init(std::string level);


	virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);

	CC_SYNTHESIZE(HUDLayer*, _hud, HUD);
	CC_SYNTHESIZE(BackgroundLayer*, _background, Background);

	void gamePrepare(std::string level);
    virtual void gameStart();
	virtual void gameTooglePause();
	virtual void gameTooglePowerUp(int powerUp);
	
private:
	
    
	std::string _levelPath;

	cocos2d::Size _visibleSize;
	cocos2d::Point _originPoint;
	float _touchMax;
	float _touchMin;
    
    int _windSpeed;
    int _gravity;
    void initPhysics();
    
    void initStoreItems();
    
    void tick(float dt);
    int _tempGameSpeed;
    float32 _timeStep;
	float32 _physicsSpeed;

    

	//ASSETS & ASSET HANDLING
	Platform* _platform;
	
    Basket* _basket;
    
    cocos2d::Vector<Puppy* > _puppies;
    GameSpawn _puppySpawn;
    GameSpawn _puppyRainSpawn;
    Puppy* setPuppyInPlay(PlatformTile* tile);
    Puppy* setPuppyInPlay(cocos2d::Vec2 position, int direction);

	Bobo* _bobo;
    cocos2d::Vector<Bullet* >_bullets;
    GameSpawn _bulletSpawn;
    
    Snake* _snake;
    Puppy* _snakeTarget;
    
    cocos2d::Vector<Extra* > _extras;
    bool _extraLives;
    bool _extraSpeed;
	
	void spawnPuppy();
	Bullet* spawnBullet();
	void spawnCluster();
    void spawnExtra(int type);
    std::string _spawnPattern;
    
    int _spawnLast;
    int _spawnPointCount;
    int spawnControl();

	cocos2d::ParticleSystemQuad* _particleEffect;
	void initParticles();

	int _rageMode;

	//cocos2d::Vector<cocos2d::Node*>_pausedNodes;
    
    
    //PowerUps
    std::map<int, PowerUp*> _powerUps;
    void gamePowerUps(float dt);

    
    
	// GAME LOGIC & MECHANICS VARIABLES
	GameStatus _gameStatus;

	bool _gameWithBobo;
    bool _gameWithSnake;
    
	float _time;
    int _timeInt;
    float _lives;
    
    float _challengeTime;
    float _challengeCooldown;
    float _challengeDuration;
    int _challengeIntro;
    bool _challengeActive;
    
    ChallengeType challengeChoose();
    ChallengeType _challengeNow;
    
    void gameEvent(std::string event, bool harmful = false, Puppy* puppy = nullptr);
    void eventEvaluation();
    std::vector<Quest*> _quests;
    
    
    int _puppyValue;
    int _puppyCatchTotal;
    
    int _coins;
    int _coinsStack;
    int _coinsStackModifier;
    int _coinsStackBurst;
	
    cocos2d::Vec2 _basketStartPoint;
	float _fenceAccumulator;

	int _gameSpeed;
	int _gameSpeedMax;
	int _gameSpeedStart;
	int _gameSpeedStack;
    int _gameSpeedStackMax;
    void setGameSpeed(int speed);

	int _puppyCount;
    
    bool _gameChallengeMode;


	bool gameIsRunning();
    bool gameIsOver();
    void gameTimer(float dt);
	void gameAddCoins(cocos2d::Vec2 position);
	void gamePuppyStacking(bool stack);
    
    std::vector<PuppyCatch> _puppyCatch;
    int _puppyCatchPlay;
    void catchReplay(float dt);
    
	// Draw Debug Mask
	void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
	
	//Physics timestep
	float _timeStepAccumulator;
	float _timeStepAccumulatorRatio;

protected:

	// Debug Mask Properties
	cocos2d::Mat4 _modelViewMV;
	void onDraw(const cocos2d::Mat4 &transform, uint32_t flags);
	cocos2d::CustomCommand _customCommand;     
};


#endif
