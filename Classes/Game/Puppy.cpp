#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/Puppy.h"
#include "Manager/Manager.h"

#else
#include "Puppy.h"
#include "Manager.h"

#endif
////////////////////////////
USING_NS_CC;


Puppy::Puppy(WorldContract *game, cocos2d::Vec2 position) : b2Sprite(game)
{
	//CCLOG("Puppy Construct");
	_startPosition = position;
	_inPlay = true;
    _alive = true;
	_catched = false;
	_grounded = false;
	_watered = false;
	_vortexable = false;
	_magnetable = false;
	_wasgrounded = false;
	_speared = false;
	_helium = false;
	_bounced = true;
	_animationState = 0;
	_runSpeed = 0.0f;
}

Puppy::~Puppy()
{
	CC_SAFE_RELEASE(_animationRun);
	CC_SAFE_RELEASE(_animationRunDead);
	CC_SAFE_RELEASE(_animationJump);
	CC_SAFE_RELEASE(_animationDead);
	CC_SAFE_RELEASE(_animationFall);
	CC_SAFE_RELEASE(_animationDive);
	CC_SAFE_RELEASE(_animationSwim);
	CC_SAFE_RELEASE(_animationBounce);
	CC_SAFE_RELEASE(_animationHelium);
	CC_SAFE_RELEASE(_animationDie);
}

Puppy* Puppy::create(WorldContract *game, cocos2d::Vec2 position)
{
	//CCLOG("Puppy Create");
	Puppy * sprite = new Puppy(game, position);
	if (sprite) {
		sprite->initPuppy();
		sprite->autorelease();
        
        if(Manager::getInstance()->getDebugTools())
		sprite->setOpacity(50);
		
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Puppy::update(float dt, float ratio) {
	if (_body && isVisible()) {
		//setPositionX(_body->GetPosition().x * RATIO);
		//setPositionY(_body->GetPosition().y * RATIO);
		const float moveRatio = 1.f - ratio;
		getState().currentPosition = ratio * _body->GetPosition() + moveRatio * getState().previousPosition;
		setCurrentPosition(ratio * _body->GetPosition() + moveRatio * getState().previousPosition);
		setPositionX(getState().currentPosition.x * RATIO);
		setPositionY(getState().currentPosition.y * RATIO);
	}
}


void Puppy::initPuppy()
{
	_inPlay = true;

	b2BodyDef BodyDef;
	BodyDef.type = b2_dynamicBody;
	BodyDef.fixedRotation = true;
	_body = _game->getWorld()->CreateBody(&BodyDef);
	
	//_body->SetSleepingAllowed(true);	

	setRandomType();

	cocos2d::SpriteFrame* frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("mopsis-walk-1.png");
	this->initWithSpriteFrame(frame);

	initAnimations();
	initPhysics();
	setRandomRunSpeed();
	animate(PUPPY_ANIMSTATE_RUN);
	this->setSpritePosition(_startPosition);

	//Interpolation stuff
	this->createState(_body);
}

void Puppy::initAnimations()
{
	switch (_type)
	{
	case PUPPY_TYPE_MOPSIS:

		_animationRun = createAnimation("mopsis-walk-%i.png", 6, 0.1f);
		_animationRun->retain();

		_animationRunDead = createAnimation("mopsis-walk-dead-%i.png", 6, 0.1f);
		_animationRunDead->retain();

		_animationJump = createAnimation("mopsis-jump-%i.png", 9, 0.1f);
		_animationJump->retain();

		_animationBounce = createAnimation("mopsis-jump-%i.png", 9, 0.1f, true);
		_animationBounce->retain();

		_animationDead = createAnimation("mopsis-fall-dead-%i.png", 8, 0.1f);
		_animationDead->retain();

		_animationFall = createAnimation("mopsis-fall-%i.png", 8, 0.1f);
		_animationFall->retain();

		_animationDive = createAnimation("mopsis-walk-%i.png", 6, 0.1f);
		_animationDive->retain();

		_animationSwim = createAnimation("mopsis-walk-%i.png", 6, 0.1f);
		_animationSwim->retain();

		_animationHelium = createAnimation("mopsis-baloni-%i.png", 3, 0.1f, false, 0, 30);
		_animationHelium->retain();

		_animationDie = createAnimation("mopsis-crash-%i.png", 4, 0.1f);
		_animationDie->retain();
		break;

	case PUPPY_TYPE_MAILO:

		_animationRun = createAnimation("mailo-walk-%i.png", 7, 0.1f);
		_animationRun->retain();

		_animationRunDead = createAnimation("mailo-walk-dead-%i.png", 7, 0.1f);
		_animationRunDead->retain();

		_animationJump = createAnimation("mailo-jump-%i.png", 5, 0.1f);
		_animationJump->retain();

		_animationBounce = createAnimation("mailo-jump-%i.png", 5, 0.1f, true);
		_animationBounce->retain();

		_animationDead = createAnimation("mailo-fall-dead-%i.png", 3, 0.1f);
		_animationDead->retain();

		_animationFall = createAnimation("mailo-fall-%i.png", 3, 0.1f);
		_animationFall->retain();

		_animationDive = createAnimation("mailo-walk-%i.png", 7, 0.1f);
		_animationDive->retain();

		_animationSwim = createAnimation("mailo-walk-%i.png", 7, 0.1f);
		_animationSwim->retain();

		_animationHelium = createAnimation("mailo-baloni-%i.png", 7, 0.1f, false, 0, 30);
		_animationHelium->retain();

		_animationDie = createAnimation("mailo-crash-%i.png", 4, 0.1f);
		_animationDie->retain();
		break;

	case PUPPY_TYPE_HUSKY:

		_animationRun = createAnimation("haskijs-walk-%i.png", 7, 0.1f);
		_animationRun->retain();

		_animationRunDead = createAnimation("haskijs-walk-dead-%i.png", 7, 0.1f);
		_animationRunDead->retain();

		_animationJump = createAnimation("haskijs-jump-%i.png", 7, 0.1f);
		_animationJump->retain();

		_animationBounce = createAnimation("haskijs-jump-%i.png", 7, 0.1f, true);
		_animationBounce->retain();

		_animationDead = createAnimation("haskijs-fall-dead-%i.png", 5, 0.1f);
		_animationDead->retain();

		_animationFall = createAnimation("haskijs-fall-%i.png", 5, 0.1f);
		_animationFall->retain();

		_animationDive = createAnimation("mailo-walk-%i.png", 7, 0.1f);
		_animationDive->retain();

		_animationSwim = createAnimation("mailo-walk-%i.png", 7, 0.1f);
		_animationSwim->retain();

		_animationHelium = createAnimation("haskijs-baloni-%i.png", 18, 0.1f, false, 0, 30);
		_animationHelium->retain();

		_animationDie = createAnimation("haskijs-jump-%i.png", 7, 0.1f, true);
		_animationDie->retain();
		break;
	}


	/*
//Linear animations
	_animationRun = createAnimation("puppy-%i.png", 12, 0.05f);
	_animationRun->retain();

	//_animationRunDead = createAnimation("puppy-gray-%i.png", 8, 0.1f);
	_animationRunDead = createAnimation("puppy-%i.png", 12, 0.1f);
	_animationRunDead->retain();


	//_animationJump = createAnimation("puppy-jump-%i.png", 7, 0.1f);
	_animationJump = createAnimation("puppy-%i.png", 12, 0.15f);
	_animationJump->retain();


	//_animationBounce = createAnimation("puppy-jump-%i.png", 7, 0.1f, true);
	_animationBounce = createAnimation("puppy-%i.png", 12, 0.1f);
	_animationBounce->retain();


	//_animationDead = createAnimation("puppy-fall-gray-%i.png", 3, 0.1f);
	_animationDead = createAnimation("puppy-%i.png", 12, 0.1f);
	_animationDead->retain();


	//_animationFall = createAnimation("puppy-fall-%i.png", 3, 0.1f);
	_animationFall = createAnimation("puppy-%i.png", 12, 0.1f);
	_animationFall->retain();


	//_animationDive = createAnimation("puppy-dive-%i.png", 2, 0.1f);
	_animationDive = createAnimation("puppy-%i.png", 12, 0.1f);
	_animationDive->retain();


	//_animationSwim = createAnimation("puppy-swim-%i.png", 4, 0.1f);
	_animationSwim = createAnimation("puppy-%i.png", 12, 0.1f);
	_animationSwim->retain();


	_animationHelium = createAnimation("puppy-helium-%i.png", 2, 0.1f, false, 0, 30);
	_animationHelium->retain();
	
//Non-linear animations
	std::vector<int> frameSequence = { 0, 1, 2, 3, 2, 3, 2, 3 };
	_animationDie = createAnimation("puppy-grounded-%i.png", frameSequence, 0.1f);
	_animationDie->retain();
	*/
}

void Puppy::initPhysics()
{
	b2CircleShape circle;
	circle.m_radius = this->getContentSize().width / 2 / RATIO;
	b2FixtureDef FixtureDef;
	FixtureDef.shape = &circle;
	FixtureDef.density = 1.2f;
	FixtureDef.friction = 0.0f;
	FixtureDef.restitution = 0.0f;
	FixtureDef.filter.categoryBits = B2_CATEGORY_PUPPY;
	FixtureDef.filter.maskBits = B2_CATEGORY_BASKET | B2_CATEGORY_PLATFORM | B2_CATEGORY_GROUND | B2_CATEGORY_BORDER;
	FixtureDef.userData = (void*)B2_CONTACT_PUPPY;
	_body->CreateFixture(&FixtureDef);
	_body->SetUserData(this);
	_body->SetLinearDamping(AIR_FRICTION_RATIO);
}

void Puppy::reset()
{
	if (_body) {
		_body->SetLinearVelocity(b2Vec2_zero);
		_body->SetAngularVelocity(0);
		_body->SetGravityScale(1);
	}
	_alive = true;
	_inPlay = true;
	_catched = false;
	_grounded = false;
	_watered = false;
	_catchable = false;
	_speared = false;
	_helium = false;
	_bounced = true;
	_body->SetAwake(true);
	_body->SetActive(true);
	this->setVisible(true);
	this->animate(PUPPY_ANIMSTATE_RUN);
}

void Puppy::rest()
{
	_alive = false;
	_inPlay = false;
	_catched = false;
	_grounded = false;
	_watered = false;
	_catchable = false;
	_vortexable = false;
	_magnetable = false;
	_helium = false;
	_wasgrounded = false;
	_speared = false;
	_bounced = true;
	this->getBody()->SetGravityScale(0);
	this->hide();
	setOpacity(255);
	setRandomType();
	setRandomRunSpeed();
	initAnimations();
	this->animate(PUPPY_ANIMSTATE_RUN);
    
    if(!Manager::getInstance()->getDebugTools())
        this->setVisible(false);
}

void Puppy::animate(int state)
{
	if (_animationState != state){
		_animationState = state;

		auto swimCallback = CallFunc::create(this, callfunc_selector(Puppy::diveAnimationCallback));
		auto diveSwim = Sequence::createWithTwoActions(Animate::create(_animationDive), swimCallback);

		auto dieCallback = CallFunc::create(this, callfunc_selector(Puppy::dieAnimationCallback));
		auto dieRun = Sequence::createWithTwoActions(Animate::create(_animationDie), dieCallback);

		auto bounceCallback = CallFunc::create(this, callfunc_selector(Puppy::bounceAnimationCallback));
		auto bounceRun = Sequence::createWithTwoActions(Animate::create(_animationBounce), bounceCallback);
		

		auto rotate = RotateBy::create(2.0f, -360.f);

		this->stopAllActions();
		if (state == PUPPY_ANIMSTATE_RUN) this->runAction(RepeatForever::create(Animate::create(_animationRun)));
		if (state == PUPPY_ANIMSTATE_RUNDEAD) this->runAction(dieRun);
		if (state == PUPPY_ANIMSTATE_JUMP) this->runAction(Animate::create(_animationJump));
		if (state == PUPPY_ANIMSTATE_DEAD) this->runAction(RepeatForever::create(Animate::create(_animationDead)));
		if (state == PUPPY_ANIMSTATE_FALL) this->runAction(RepeatForever::create(Animate::create(_animationFall)));
		if (state == PUPPY_ANIMSTATE_SWIM) this->runAction(diveSwim);
		if (state == PUPPY_ANIMSTATE_BOUNCE) this->runAction(bounceRun);
		if (state == PUPPY_ANIMSTATE_HELIUM) this->runAction(RepeatForever::create(Animate::create(_animationHelium)));
		if (state == PUPPY_ANIMSTATE_VORTEX) this->runAction(RepeatForever::create(rotate));
	}
}

void Puppy::vortex(b2Vec2 basket)
{
	if (_alive){
		b2Vec2 puppyLocation = b2Vec2(this->getPositionX(), this->getPositionY());
		float32 distance = b2Distance(puppyLocation, basket);
		float32 maxDistance = PUPPY_VORTEX_DISTANCE;
		//float32 maxDistance = this->getParent()->getContentSize().height / 2;
		if (distance < maxDistance){
			b2Vec2 relativePosition = basket - puppyLocation;
			float32 radialForceMagnitude = PUPPY_VORTEX_POWER / 3;
			float32 tangForceMagnitude = PUPPY_VORTEX_POWER * maxDistance / distance;
			b2Vec2 radialVector = radialForceMagnitude * relativePosition;
			b2Vec2 tangVector = tangForceMagnitude * b2Vec2(radialVector.y, radialVector.x * -1.0f);
			b2Vec2 totalForce = radialVector + tangVector;
			totalForce = b2Vec2(totalForce.x / 60, totalForce.y / 60);
			_body->SetGravityScale(0);
			_body->SetLinearVelocity(totalForce);
			//_body->ApplyForceToCenter(totalForce, true);
			//_body->ApplyLinearImpulse(totalForce, _body->GetPosition(), true);
			if (_body->GetContactList()){
				_body->GetContactList()->contact->SetEnabled(false);
			}
			this->vortexable(true);
			this->catchable(true);
		}
		else {
			_body->SetGravityScale(1);
			this->vortexable(false);
		}
	}
}

void Puppy::magnet(b2Vec2 basket)
{
	if (_alive){
		b2Vec2 puppyLocation = b2Vec2(this->getPositionX(), this->getPositionY());
		float32 distance = b2Distance(puppyLocation, basket);
		float32 maxDistance = this->getContentSize().width * PUPPY_MAGNET_DISTANCE;
		if (distance < maxDistance){
			b2Vec2 relativePosition = basket - puppyLocation;
			float32 force = PUPPY_MAGNET_POWER / relativePosition.LengthSquared();
			b2Vec2 F = force * relativePosition;
			F = b2Vec2(F.x, F.y);
			_body->SetGravityScale(0);
			_body->ApplyForce(F, puppyLocation, true);
			if (!this->isMagnetable() && _body->GetContactList()){
				_body->GetContactList()->contact->SetEnabled(false);
			}
			this->magnetable(true);
			this->catchable(true);
		}
		else {
			_body->SetGravityScale(1);
			this->magnetable(false);
		}
	}
}

void Puppy::setDirection(int direction)
{
	_direction = direction;
	this->setScaleX(direction); // for mirroring animation
}

void Puppy::setRandomDirection()
{
	_direction = 0;
	while (_direction == 0) _direction = (rand() % 3) - 1;
}

void Puppy::changeDirection()
{
	_direction *= -1;
	this->setScaleX(_direction);
}

int Puppy::getDirection()
{
	return _direction;
}

int Puppy::getType()
{
    return _type;
}

bool Puppy::isAlive()
{
    return _alive;
}

void Puppy::setAlive(bool status)
{
	_alive = status;
}

void Puppy::catched(bool status)
{
	_catched = status;
}

bool Puppy::isCatched()
{
	if (_catched){
		_catched = false;
		return true;
	}
	else{
		return false;
	}
}

bool Puppy::isWatered()
{
	if (_watered){
		_watered = false;
		_waswatered = true;
		return true;
	}
	else
	return false;
}

void Puppy::setWatered(bool status)
{
	_watered = status;
}

void Puppy::grounded(bool status)
{
	_grounded = status;
	_wasgrounded = status;
}

bool Puppy::isGrounded()
{
	if (_grounded){
		_grounded = false;
        _wasgrounded = true;
		return true;
	}
	else{
		return false;
	}
}

bool Puppy::wasGrounded()
{
	return _wasgrounded;
}

void Puppy::jump()
{
	Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_PUPPY_JUMP);
}

void Puppy::catchable(bool status)
{
	_catchable = status;
}

bool Puppy::isCatchable()
{
	return _catchable;
}

void Puppy::speared(bool status)
{
	_speared = status;
}

bool Puppy::isSpeared()
{
    if (_speared){
        _speared = false;
        return true;
    }
    else
        return false;
}

void Puppy::vortexable(bool status)
{
	_vortexable = status;
}

bool Puppy::isVortexable()
{
	if (_vortexable) return true;
	else return false;
}

void Puppy::magnetable(bool status)
{
	_magnetable = status;
}

bool Puppy::isMagnetable()
{
	if (_magnetable) return true;
	else return false;
}

void Puppy::helium(bool status)
{
	_helium = status;
}

bool Puppy::isHelium()
{
	if (_helium) return true;
	else return false;
}

void Puppy::bounced(bool status)
{
	_bounced = status;
}

bool Puppy::isBounced()
{
    if (_bounced){
		_bounced = false;
		return true;
	}
    else{
        Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_PUPPY_BOUNCE);
		return false;
	}
}

bool Puppy::getBounced()
{
	return _bounced;
}

bool Puppy::bringToFront()
{
    if(_animationState == PUPPY_ANIMSTATE_FALL || _animationState == PUPPY_ANIMSTATE_JUMP || _animationState == PUPPY_ANIMSTATE_HELIUM)
        return true;
    else
        return false;
}

void Puppy::diveAnimationCallback()
{
	this->runAction(RepeatForever::create(Animate::create(_animationSwim)));
}

void Puppy::dieAnimationCallback()
{
	_body->SetLinearVelocity(b2Vec2(getRunSpeed() * _direction, 0));
	this->runAction(RepeatForever::create(Animate::create(_animationRunDead)));
}

void Puppy::bounceAnimationCallback()
{
	this->runAction(RepeatForever::create(Animate::create(_animationRun)));
}

void Puppy::interpolationReset()
{
	setPreviousPosition(_body->GetPosition());
}

int Puppy::getAnimationState()
{
	return _animationState;
}

void Puppy::setRandomType()
{
	//srand(static_cast<unsigned int>(time(0)));
	int type = rand() % 3;
	switch (type){
	case 0:
		_type = PUPPY_TYPE_MOPSIS;
		break;
	case 1:
		_type = PUPPY_TYPE_MAILO;
		break;
	case 2:
		_type = PUPPY_TYPE_HUSKY;
		break;
	}
}

void Puppy::setRandomRunSpeed()
{
	//int type = rand() % 3;
	switch (_type){
	case 1:
		_runSpeed = PUPPY_RUNSPEED_MOPSIS;
		_jumpHeight = PUPPY_JUMP_MOPSIS;
		break;
	case 2:
		_runSpeed = PUPPY_RUNSPEED_MAILO;
		_jumpHeight = PUPPY_JUMP_MAILO;
		break;
	case 3:
		_runSpeed = PUPPY_RUNSPEED_HASKIJS;
		_jumpHeight = PUPPY_JUMP_HASKIJS;
		break;
	}
}

float Puppy::getRunSpeed()
{
	return _runSpeed;
}

float Puppy::getJumpHeight()
{
	return _jumpHeight;
}

void Puppy::platformContactAdd(b2Contact* contact)
{
	_platformContacts.push_back(contact);
}

void Puppy::platformContactRemove()
{
	if (_platformContacts.size() > 0)	_platformContacts.pop_back();
}

int Puppy::getPlatformContacts()
{
	return _platformContacts.size();
}