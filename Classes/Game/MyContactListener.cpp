#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/MyContactListener.h"
#include "Game/WorldContract.h"
#include "Game/Puppy.h"
#include "Game/Bullet.h"
#include "Game/Basket.h"
#include "Game/Extras.h"

#else
#include "MyContactListener.h"
#include "WorldContract.h"
#include "Puppy.h"
#include "Bullet.h"
#include "Basket.h"
#include "Extras.h"

#endif
////////////////


// Why (int)(size_t) ? http://stackoverflow.com/questions/22419063/error-cast-from-pointer-to-smaller-type-int-loses-information-in-eaglview-mm

void MyContactListener::BeginContact(b2Contact* contact)
{
	b2Fixture *A = contact->GetFixtureA();
	b2Fixture *B = contact->GetFixtureB();

	float32 jumpHeight = PUPPY_MAXJUMPHEIGHT;

	b2Body *Ball = NULL;
	b2Body *CatBall = NULL;
	b2Body *OtherBody = NULL;
	b2Body *Fence = NULL;
	b2Body *Extras = NULL;
	
	int *userDataA = (int *)A->GetUserData();
	int *userDataB = (int *)B->GetUserData();
	int *userDataOther = NULL;

	//Puppy check
	if ((int)(size_t)userDataA == (int)B2_CONTACT_PUPPY){
		Ball = A->GetBody();
		OtherBody = B->GetBody();
		//OtherBody->SetUserData(B->GetUserData());
		userDataOther = (int*)B->GetUserData();
	}
	else if ((int)(size_t)userDataB == (int)B2_CONTACT_PUPPY){
		Ball = B->GetBody();
		OtherBody = A->GetBody();
		//OtherBody->SetUserData(A->GetUserData());
		userDataOther = (int*)A->GetUserData();
	}


	//Puppy related checks
	if (Ball){

		b2Vec2 Location = Ball->GetPosition();
		Puppy *bodyUserData = static_cast<Puppy*>(Ball->GetUserData());
		float32 speed = bodyUserData->getRunSpeed();

		//Jump check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_JUMP_RIGHT){
			auto velocityY = Ball->GetLinearVelocity().y;
			if (bodyUserData->isAlive() && !bodyUserData->isVortexable() && !bodyUserData->isHelium() && !bodyUserData->isMagnetable() && !bodyUserData->isCatchable()){
				int direction = bodyUserData->getDirection();
				//Ball->SetLinearVelocity(b2Vec2((speed), 0));
				bodyUserData->platformContactRemove();
				jumpHeight = bodyUserData->getJumpHeight();
				Ball->ApplyLinearImpulse(b2Vec2(0, jumpHeight), Location, 0);
				bodyUserData->setDirection(PUPPY_DIRECTION_RIGHT);
				bodyUserData->catchable(true);
				bodyUserData->jump();
				bodyUserData->animate(PUPPY_ANIMSTATE_JUMP);
			}
			else {
				contact->SetEnabled(false);
			}
		}
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_JUMP_LEFT){
			auto velocityY = Ball->GetLinearVelocity().y;
			if (bodyUserData->isAlive() && !bodyUserData->isVortexable() && !bodyUserData->isHelium() && !bodyUserData->isMagnetable() && !bodyUserData->isCatchable()){
				int direction = bodyUserData->getDirection();
				//Ball->SetLinearVelocity(b2Vec2((speed * -1.0f), 0));
				bodyUserData->platformContactRemove();
				jumpHeight = bodyUserData->getJumpHeight();
				Ball->ApplyLinearImpulse(b2Vec2(0, jumpHeight), Location, 0);
				bodyUserData->setDirection(PUPPY_DIRECTION_LEFT);
				bodyUserData->catchable(true);
				bodyUserData->jump();
				bodyUserData->animate(PUPPY_ANIMSTATE_JUMP);
			}
			else {
				contact->SetEnabled(false);
			}
		}

		//Platform run check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_RUN){
			if (bodyUserData->isAlive() && !bodyUserData->isVortexable() && !bodyUserData->isHelium() && !bodyUserData->isMagnetable()){
				bodyUserData->platformContactAdd(contact);
				if (bodyUserData->getPlatformContacts() > 1) bodyUserData->changeDirection();
				int direction = bodyUserData->getDirection();
				//Ball->SetLinearVelocity(b2Vec2(speed * direction, 0));
				bodyUserData->setScaleX(direction);
				
				if (!bodyUserData->isBounced() && !bodyUserData->isMagnetable() && bodyUserData->isCatchable()){
					//Ball->SetLinearVelocity(b2Vec2(speed * direction, 0));
//					CCLOG("Velocity before: %f", Ball->GetLinearVelocity().y);
					Ball->ApplyLinearImpulse(b2Vec2(0.2f * direction, Ball->GetLinearVelocity().y * -1.0f + 3.0f), Location, 0);
//					CCLOG("Velocity after: %f", Ball->GetLinearVelocity().y);
					bodyUserData->bounced(true);
					bodyUserData->animate(PUPPY_ANIMSTATE_BOUNCE);
					bodyUserData->catchable(false);
				}
				else{
					bodyUserData->animate(PUPPY_ANIMSTATE_RUN);
					bodyUserData->catchable(false);
				}
			}
			else {
				contact->SetEnabled(false);
			}
		}
		
		//Water check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_WATER){
			if (bodyUserData->isAlive()) bodyUserData->setWatered(true);
			int direction = bodyUserData->getDirection();
			bodyUserData->platformContactAdd(contact);
			//Ball->SetLinearVelocity(b2Vec2(speed * direction, 0));
			bodyUserData->setScaleX(direction);
			bodyUserData->catchable(false);
			bodyUserData->setAlive(false);
			bodyUserData->animate(PUPPY_ANIMSTATE_SWIM);
			bodyUserData->setOpacity(192);
		}

		//Ground check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_GROUND){
			if (bodyUserData->isAlive()) bodyUserData->grounded(true);
			int direction = bodyUserData->getDirection();
			bodyUserData->platformContactAdd(contact);
			bodyUserData->setScaleX(direction);
			bodyUserData->catchable(false);
			bodyUserData->animate(PUPPY_ANIMSTATE_RUNDEAD);
			bodyUserData->setAlive(false);
			bodyUserData->setOpacity(192);
		}

		//Spears check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_SPEARS){
			if (bodyUserData->isAlive() && !bodyUserData->isHelium()){
				bodyUserData->catchable(false);
				bodyUserData->speared(true);
				bodyUserData->setAlive(false);
				Ball->SetLinearVelocity(b2Vec2_zero);
				if (!bodyUserData->isVortexable()){
					jumpHeight = bodyUserData->getJumpHeight();
					Ball->ApplyLinearImpulse(b2Vec2(0, jumpHeight * 1.5), Location, 0);
				}
				bodyUserData->animate(PUPPY_ANIMSTATE_DEAD);
				bodyUserData->setOpacity(192);
			}
			else {
				contact->SetEnabled(false);
			}
		}

		//Basket - Ball check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_CATCH){
			if (bodyUserData->isCatchable() && bodyUserData->isAlive()){
				bodyUserData->catched(true);
				bodyUserData->setAlive(false);
			}
		}

		if ((int)(size_t)userDataOther == (int)B2_CONTACT_PUSH){
			if (bodyUserData->isAlive()){
				auto direction = bodyUserData->getDirection() * -1;
				bodyUserData->setDirection(direction);
				auto velocityX = Ball->GetLinearVelocity().x * -1;
				auto velocityY = Ball->GetLinearVelocity().y;
				Ball->SetLinearVelocity(b2Vec2(velocityX, velocityY));
			}
		}


		//Border - Ball check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_BORDER_LEFT){
			if (bodyUserData->isAlive()){
				bodyUserData->setDirection(PUPPY_DIRECTION_RIGHT);
				bodyUserData->setScaleX(PUPPY_DIRECTION_RIGHT);
			}
			else contact->SetEnabled(false);
		}

		if ((int)(size_t)userDataOther == (int)B2_CONTACT_BORDER_RIGHT){
			if (bodyUserData->isAlive()){
				bodyUserData->setDirection(PUPPY_DIRECTION_LEFT);
				bodyUserData->setScaleX(PUPPY_DIRECTION_LEFT);
			}
			else contact->SetEnabled(false);
		}

		if ((int)(size_t)userDataOther == (int)B2_CONTACT_BORDER_TOP){
			if (Ball->GetLinearVelocity().y < 0) contact->SetEnabled(false);
		}
	}


	//Bullet check
	if ((int)(size_t)userDataA == (int)B2_CONTACT_BULLET){
		CatBall = A->GetBody();
		OtherBody = B->GetBody();
		//OtherBody->SetUserData(B->GetUserData());
		userDataOther = (int*)B->GetUserData();
	}
	else if ((int)(size_t)userDataB == (int)B2_CONTACT_BULLET){
		CatBall = B->GetBody();
		OtherBody = A->GetBody();
		//OtherBody->SetUserData(A->GetUserData());
		userDataOther = (int*)A->GetUserData();
	}

	//Bullet related checks
	if (CatBall){
		Bullet *bodyUserData = static_cast<Bullet*>(CatBall->GetUserData());

		//Basket - Bullet check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_CATCH){
			bodyUserData->basketHit(true);
		}

		//Ground check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_GROUND){
			bodyUserData->grounded(true);
		}
		//Water check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_WATER){
			bodyUserData->grounded(true);
		}
	}

	
	//Fence check
	if ((int)(size_t)userDataA == (int)B2_CONTACT_FENCE){
		Fence = A->GetBody();
		OtherBody = B->GetBody();
		//OtherBody->SetUserData(B->GetUserData());
		userDataOther = (int*)B->GetUserData();
	}
	else if ((int)(size_t)userDataB == (int)B2_CONTACT_FENCE){
		Fence = B->GetBody();
		OtherBody = A->GetBody();
		//OtherBody->SetUserData(A->GetUserData());
		userDataOther = (int*)A->GetUserData();
	}
	
	if (Fence){
		
		Basket* bodyUserData = static_cast<Basket*>(OtherBody->GetUserData());

		if ((int)(size_t)userDataOther == (int)B2_CONTACT_FENCECHECK){
			bodyUserData->setFenced(true);
			bodyUserData->animate(BASKET_ANIMSTATE_FENCE);
			_fenceContacts.push_back(contact);
		}
	}


	//Extra check
	if ((int)(size_t)userDataA == (int)B2_CONTACT_EXTRA){
		Extras = A->GetBody();
		OtherBody = B->GetBody();
		//OtherBody->SetUserData(B->GetUserData());
		userDataOther = (int*)B->GetUserData();
	}
	else if ((int)(size_t)userDataB == (int)B2_CONTACT_EXTRA){
		Extras = B->GetBody();
		OtherBody = A->GetBody();
		//OtherBody->SetUserData(A->GetUserData());
		userDataOther = (int*)A->GetUserData();
	}

	//Extra related checks
	if (Extras){
		Extra *bodyUserData = static_cast<Extra*>(Extras->GetUserData());

		//Basket - Extra check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_CATCH){
			bodyUserData->basketHit(true);
		}

		//Ground check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_GROUND){
			bodyUserData->grounded(true);
		}

		//Water check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_WATER){
			bodyUserData->grounded(true);
		}
	}
	
	// */
	

}


void MyContactListener::PreSolve(b2Contact* contact)
{

}


void MyContactListener::EndContact(b2Contact* contact)
{
	b2Fixture *A = contact->GetFixtureA();
	b2Fixture *B = contact->GetFixtureB();

	b2Body *Ball = NULL;
	b2Body *CatBall = NULL;
	b2Body *OtherBody = NULL;
	b2Body *Fence = NULL;

	int *userDataA = (int *)A->GetUserData();
	int *userDataB = (int *)B->GetUserData();
	int *userDataOther = NULL;

	//Puppy check
	if ((int)(size_t)userDataA == (int)B2_CONTACT_PUPPY){
		Ball = A->GetBody();
		OtherBody = B->GetBody();
		OtherBody->SetUserData(B->GetUserData());
	}
	else if ((int)(size_t)userDataB == (int)B2_CONTACT_PUPPY){
		Ball = B->GetBody();
		OtherBody = A->GetBody();
		OtherBody->SetUserData(A->GetUserData());
	}

	if (Ball){

		b2Vec2 Location = Ball->GetPosition();
		Puppy *bodyUserData = static_cast<Puppy*>(Ball->GetUserData());

		userDataOther = (int*)OtherBody->GetUserData();

		//Platforms check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_RUN){
			bodyUserData->platformContactRemove();
		}
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_WATER){
			bodyUserData->platformContactRemove();
		}
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_GROUND){
			bodyUserData->platformContactRemove();
		}


		//Basket - Ball check
		if ((int)(size_t)userDataOther == (int)B2_CONTACT_CATCH){
			if (bodyUserData->isCatchable()){
				bodyUserData->catched(true);
			}
		}
	}

	//Fence check
	if ((int)(size_t)userDataA == (int)B2_CONTACT_FENCE){
		Fence = A->GetBody();
		OtherBody = B->GetBody();
		//OtherBody->SetUserData(B->GetUserData());
		userDataOther = (int*)B->GetUserData();
	}
	else if ((int)(size_t)userDataB == (int)B2_CONTACT_FENCE){
		Fence = B->GetBody();
		OtherBody = A->GetBody();
		//OtherBody->SetUserData(A->GetUserData());
		userDataOther = (int*)A->GetUserData();
	}
	
	if (Fence){
		
		Basket* bodyUserData = static_cast<Basket*>(OtherBody->GetUserData());

		if ((int)(size_t)userDataOther == (int)B2_CONTACT_FENCECHECK){
			_fenceContacts.pop_back();
			if (_fenceContacts.size() == 0){
				bodyUserData->setFenced(false);
				bodyUserData->idleAnimation();
			}
		}
	}
}