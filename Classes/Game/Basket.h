#ifndef PuppyMadness_Basket_h
#define PuppyMadness_Basket_h

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/b2Sprite.h"
#include "Game/WorldContract.h"

#else
#include "b2Sprite.h"
#include "WorldContract.h"

#endif
////////////////////////////


#define BASKET_GIRL_RIGHT 1
#define BASKET_GIRL_LEFT -1
#define BASKET_GIRL_FLIP_DELTA 15
#define kGirlIndex 3
#define kPuppyCatchedIndex 2
#define BASKET_ANIMSTATE_CATCH 1
#define BASKET_ANIMSTATE_FENCE 2
#define BASKET_ANIMSTATE_EXPLODE 3
#define BASKET_TRAIL_LENGTH 24
#define BASKET_TRAIL_FADETIME 0.1
#define BASKET_TRAIL_OPACITY 80
#define BASKET_TRAIL_MAX_LIMIT 350
#define BASKET_MIN_TELEPORT_DISTANCE 50

typedef enum _basket_type{
    BASKET_BASIC = 0,
    BASKET_MEDIC = 1
} BasketType;


class Basket : public b2Sprite
{
public:

	Basket(WorldContract *game, cocos2d::Vec2 position, int type);

	virtual ~Basket();

	static Basket* create(WorldContract *game, cocos2d::Vec2 position, int type);

	virtual void update(float dt);

	void show(bool isVisible);

	void explode();

	b2Vec2 getLocation();

	void flip(cocos2d::Vec2 Location);

	void setDirection(int direction);

	void animateCatch();
	void animateFence();
	void animateExplode();

	void setFenced(bool fenced);
	bool isFenced();

	bool isTeleported();
	void setTeleported(bool status);
    
    void powerUp(int powerUp, bool active);

	void idleAnimation();

	void animate(int state);

private:
	
	bool _fenced;
	bool _flipped;
	bool isFlipped();
	void setFlipped(bool flip);
	
	bool legs;
	bool _teleported;

	void initBasket();

	void initAnimations(std::string type);
	void initPhysics(std::string type);

    BasketType _basketType;

	//cocos2d::Sprite* _trailRight;
	//cocos2d::Sprite* _trailLeft;
	cocos2d::Sprite* _girl;
	cocos2d::SpriteFrame* _basket;
	cocos2d::Sprite* _basketEffect;
	//cocos2d::SpriteFrame* _blinkFrame;
	cocos2d::SpriteFrame* _animframe;
	cocos2d::Animation* _animationIdle;
	cocos2d::Animation* _catchA;
	cocos2d::Animation* _catchB;
	cocos2d::Animation* _catchC;
	cocos2d::Animation* _peak;
	cocos2d::Animation* _animationFenced;
	cocos2d::Animation* _animationBoom;
	cocos2d::Action* _idle;
	cocos2d::Vec2 _startPosition;
	cocos2d::Vector<cocos2d::Sprite*> _trailSprites;
	cocos2d::RepeatForever* _girlFloat;
	b2Body* _legs;
	b2Body* _fenceCheck;
	b2Body* _magnetCheck;
	cocos2d::Sprite* _legsSprite;
};


#endif