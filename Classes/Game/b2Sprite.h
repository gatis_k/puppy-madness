#ifndef PuppyMadness_b2Sprite_h
#define PuppyMadness_b2Sprite_h


#include "cocos2d.h"
#include "Box2D/Box2D.h"

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/WorldContract.h"

#else
#include "WorldContract.h"

#endif
////////////////////////////


class b2Sprite : public cocos2d::Sprite
{
public:

	b2Sprite(WorldContract * game);
	CC_SYNTHESIZE(bool, _inPlay, InPlay);
	CC_SYNTHESIZE(b2Body *, _body, Body);
	CC_SYNTHESIZE(WorldContract *, _game, Game);
	CC_SYNTHESIZE(int, _type, Type);

	virtual void setSpritePosition(cocos2d::Vec2 position);
	virtual void update(float dt);
	virtual void hide(void);
	virtual void reset(void);
	
	//Linear animation sequence
	virtual cocos2d::Animation* createAnimation(const char* name, int frameCount, float frameTime, bool reverse = false, int offsetX = 0, int offsetY = 0);

	//Non-linear animation sequence
	virtual cocos2d::Animation* createAnimation(const char* name, std::vector<int> frameVector, float frameTime);

	/*
	* get squared magnitude of sprite's vector
	*/
	virtual float mag();

	//Physics
	struct State
	{
		b2Vec2 previousPosition;
		b2Vec2 currentPosition;
		//float32 previousAngle;
		//float32 currentAngle;
	};
	CC_SYNTHESIZE(State, _state, State);
	virtual void createState(b2Body* body);
	virtual void setPreviousPosition(b2Vec2 position);
	virtual void setCurrentPosition(b2Vec2 position);
};

#endif 
