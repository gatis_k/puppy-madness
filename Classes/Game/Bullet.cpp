#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/Bullet.h"
#include "Manager/Manager.h"

#else
#include "Bullet.h"
#include "Manager.h"

#endif
////////////////////////////


USING_NS_CC;


Bullet::Bullet(WorldContract *game, cocos2d::Vec2 position) : b2Sprite(game)
{
	//CCLOG("Bullet Construct");
	_startPosition = position;
	_inPlay = true;
	_grounded = false;
	_basketHit = false;
}

Bullet::~Bullet()
{
}

Bullet* Bullet::create(WorldContract *game, cocos2d::Vec2 position)
{
	//CCLOG("Puppy Create");
	Bullet * sprite = new Bullet(game, position);
	if (sprite) {
		sprite->initBullet();
		sprite->autorelease();
        
        if(Manager::getInstance()->getDebugTools())
        sprite->setOpacity(50);
		
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Bullet::update(float dt, float ratio) {
	if (_body && isVisible()) {
		//setPositionX(_body->GetPosition().x * RATIO);
		//setPositionY(_body->GetPosition().y * RATIO);
		const float moveRatio = 1.f - ratio;
		getState().currentPosition = ratio * _body->GetPosition() + moveRatio * getState().previousPosition;
		setCurrentPosition(ratio * _body->GetPosition() + moveRatio * getState().previousPosition);
		setPositionX(getState().currentPosition.x * RATIO);
		setPositionY(getState().currentPosition.y * RATIO);
	}
}


void Bullet::initBullet()
{
	cocos2d::SpriteFrame *frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("bullet.png");
	frame->setOffsetInPixels(Vec2(-5.0f, 6.0f));
	this->initWithSpriteFrame(frame);
	initPhysics();
	this->setSpritePosition(_startPosition);
	//initAnimations();

	//Interpolation stuff
	this->createState(_body);
}

void Bullet::initPhysics()
{
	b2BodyDef BodyDef;
	BodyDef.type = b2_dynamicBody;
	BodyDef.fixedRotation = true;
	_body = _game->getWorld()->CreateBody(&BodyDef);

	b2CircleShape circle;
	circle.m_radius = (this->getContentSize().width / 2 - 4)/ RATIO;

	b2FixtureDef FixtureDef;
	FixtureDef.shape = &circle;
	FixtureDef.density = 1.0f;
	FixtureDef.friction = 0.0f;
	FixtureDef.isSensor = true;
	FixtureDef.filter.categoryBits = B2_CATEGORY_BULLET;
	FixtureDef.filter.maskBits = B2_CATEGORY_BASKET | B2_CATEGORY_GROUND;
	FixtureDef.userData = (void*)B2_CONTACT_BULLET;
	_body->CreateFixture(&FixtureDef);
	_body->SetUserData(this);
	_body->SetLinearDamping(AIR_FRICTION_RATIO);
}

void Bullet::initAnimations()
{

}

void Bullet::reset()
{
	if (_body) {
		_body->SetLinearVelocity(b2Vec2_zero);
		_body->SetAngularVelocity(0);
	}
	_inPlay = true;
	_body->SetAwake(true);
	_body->SetActive(true);
	this->setVisible(true);
}

void Bullet::rest()
{
	_inPlay = false;
	_grounded = false;
	_basketHit = false;
	this->hide();
	this->setSpritePosition(Vec2(0, 0));
	_body->SetAwake(false);
    
    if(!Manager::getInstance()->getDebugTools())
        this->setVisible(false);
}


void Bullet::basketHit(bool status)
{
	_basketHit = status;
	_explosionPosition = this->getPosition();
}

bool Bullet::basketIsHit()
{
	if (_basketHit){
		_basketHit = false;
		this->rest();
		return true;
	}
	else{
		return false;
	}
}

void Bullet::grounded(bool status)
{
	_grounded = status;
	_explosionPosition = this->getPosition();
}

bool Bullet::isGrounded()
{
	if (_grounded){
		_grounded = false;
		return true;
	}
	else{
		return false;
	}
}

cocos2d::Vec2 Bullet::getExplosionPosition()
{
	return _explosionPosition;
}

void Bullet::interpolationReset()
{
	setPreviousPosition(_body->GetPosition());
}