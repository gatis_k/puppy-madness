#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/b2Sprite.h"

#else
#include "b2Sprite.h"

#endif

////////////////////////////

USING_NS_CC;


b2Sprite::b2Sprite(WorldContract * game) {
	//CCLOG("b2sprite Construct");
	_game = game;
	_body = NULL;
	_inPlay = false;
	_state = { b2Vec2(0.0f, 0.0f), b2Vec2(0.0f, 0.0f)};
}

void b2Sprite::setSpritePosition(cocos2d::Vec2 position) {
	setPosition(position);
	//update box2d body if any
	if (_body) {
		_body->SetTransform(b2Vec2(position.x / RATIO, position.y / RATIO), _body->GetAngle());
	}
}

void b2Sprite::update(float dt) {
	if (_body && isVisible()) {
		setPositionX(_body->GetPosition().x * RATIO);
		setPositionY(_body->GetPosition().y * RATIO);
		setRotation(CC_RADIANS_TO_DEGREES(-1 * _body->GetAngle()));
	}
}

void b2Sprite::hide(void) {
    if (_body) {
        _body->SetLinearVelocity(b2Vec2_zero);
        _body->SetAngularVelocity(0);
        _body->SetTransform(b2Vec2_zero, 0.0);
        _body->SetAwake(false);
		_body->SetActive(false);
    }
}

void b2Sprite::reset() {}


//get squared magnitude of sprite's vector
float b2Sprite::mag() {
	if (_body) {
		return pow(_body->GetLinearVelocity().x, 2) +
			pow(_body->GetLinearVelocity().y, 2);
	}
	return 0.0;
}

void b2Sprite::createState(b2Body* body)
{
	_state.previousPosition = body->GetPosition();
	//_state.currentPosition = body->GetPosition();
	//_state.previousAngle = body->GetAngle();
	//_state.currentAngle = body->GetAngle();
}

void b2Sprite::setPreviousPosition(b2Vec2 position)
{
	_state.previousPosition = position;
}

void b2Sprite::setCurrentPosition(b2Vec2 position)
{
	_state.currentPosition = position;
}

cocos2d::Animation* b2Sprite::createAnimation(const char* name, int frameCount, float frameTime, bool reverse, int offsetX, int offsetY)
{
	Vector<SpriteFrame*> animationVector(frameCount);
	SpriteFrame* animFrame;
	Vec2 offset = Vec2(offsetX, offsetY);
	char str[100] = { 0 };
	if (!reverse){
		for (int i = 0; i < frameCount; i++){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	else
	{
		for (int i = frameCount - 1; i >= 0; i--){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	auto animation = Animation::createWithSpriteFrames(animationVector, frameTime);
	return animation;
}

cocos2d::Animation* b2Sprite::createAnimation(const char* name, std::vector<int> frameVector, float frameTime)
{
	Vector<SpriteFrame*> animationVector(frameVector.size());
	SpriteFrame* animFrame;
	char str[100] = { 0 };
	for (int i = 0; i < frameVector.size(); i++){
		sprintf(str, name, frameVector[i]);
		animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
		animationVector.pushBack(animFrame);
	}
	auto animation = Animation::createWithSpriteFrames(animationVector, frameTime);
	return animation;
}