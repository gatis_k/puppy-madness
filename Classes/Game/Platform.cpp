#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/Platform.h"
#include "Manager/Manager.h"
#include "Manager/Stages.h"

#else
#include "Platform.h"
#include "Manager.h"
#include "Stages.h"

#endif
////////////////////////////




USING_NS_CC;

Platform::Platform():
_spawnPoints(std::vector<PlatformTile*>())
{

}

Platform::~Platform()
{

}

Platform* Platform::create(WorldContract *game, std::vector<std::vector<int>> tilemap, cocos2d::Vec2 position, int stage)
{
	Platform * sprite = new Platform();
	if (sprite) {
		sprite->init(game, tilemap, position, stage);
		sprite->autorelease();

		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Platform::init(WorldContract *game, std::vector<std::vector<int>> tilemap, cocos2d::Vec2 position, int stage)
{
    _stage = stage;
    
	this->_tile_dimensions = 20;
    
	_position = position;

	this->setGame(game);
	this->setPosition(position);
	this->setAnchorPoint(Vec2(0.5f, 0.5f));

	int row = 0;
	int col = 0;
	bool flag = false;
	Vec2 drawFrom;
	Vec2 drawTo;
	bool jumpLeft = false;
	bool jumpRight = false;

	Size platformSize = Size(tilemap[0].size()*_tile_dimensions, tilemap.size()*_tile_dimensions);
	this->setContentSize(platformSize);

	//CCLOG("Size %.1f %.1f %d %d", platformSize.width, platformSize.height, tilemap[0].size(), tilemap.size());
	
	/*
	auto rectNode = DrawNode::create();
	Vec2 rectangle[4];
	rectangle[0] = Vec2(0, 0);
	rectangle[1] = Vec2(platformSize.width, 0);
	rectangle[2] = Vec2(platformSize.width, platformSize.height);
	rectangle[3] = Vec2(0, platformSize.height);

	Color4F white(1, 1, 1, 1);
	rectNode->drawPolygon(rectangle, 4, Color4F(0.25f, 0.65f, 0.78f, 0.2f), 1, Color4F::RED);
	this->addChild(rectNode);
	// */


	for (row = 0; row < (int)tilemap.size(); row++)
	{
		for (col = 0; col < (int)tilemap[row].size(); col++)
		{
			Vec2 position(col * _tile_dimensions + (_tile_dimensions / 2), ((platformSize.height - _tile_dimensions) - row * _tile_dimensions + (_tile_dimensions / 2)));
            
            int tile = tilemap[row][col];

			if (tile == 5) jumpLeft = true;
			if (tile == 6) jumpRight = true;
			if (tile == 7) drawSpear(position);

			if (tile == 1 || tile == 5 || tile == 6)
			{
				if (!flag){
					drawFrom = Vec2(col, row);
					flag = true;
                    if(tile == 1) tile = 5;
                }
            
                if(flag && tile == 1)
                {
                    if(col==23) tile = 6;
                    else if(col < 23 && tilemap[row][col + 1] == 0) tile = 6;
                }

				if (col == ((int)tilemap[row].size() - 1) && flag){ // close the line at the end
					drawTo = Vec2(col + 1, row);
					drawPlatform(drawFrom, drawTo, jumpLeft, jumpRight);
					flag = false;
					jumpLeft = false;
					jumpRight = false;
				}
                
			}
			else if (flag && (tile != 1 || tile != 5 || tile != 6)) // close the line
			{
				drawTo = Vec2(col, row);
				drawPlatform(drawFrom, drawTo, jumpLeft, jumpRight);
				flag = false;
				jumpLeft = false;
                jumpRight = false;
			}
            
            this->addTile(tile, position);
            
		}
	}

//	CCLOG("Capacity %i, size %i", _spawnPoints.capacity(), _spawnPoints.size());

}

cocos2d::Vec2 Platform::deltaPosition(cocos2d::Vec2 position)
{
	Vec2 truePosition;
	truePosition.x = _position.x - (this->getContentSize().width / 2) + position.x;
	truePosition.y = _position.y - (this->getContentSize().height / 2) + position.y;
	return truePosition;
}


void Platform::drawSpear(cocos2d::Vec2 position)
{
	position = deltaPosition(position);
	//CCLOG("position %.1f, %.1f", position.x, position.y);

	b2BodyDef BodyDef;
	BodyDef.type = b2_staticBody;
	BodyDef.position.Set(position.x / RATIO, position.y / RATIO);
	_body = _game->getWorld()->CreateBody(&BodyDef);

	b2PolygonShape platformShape;
	platformShape.SetAsBox((_tile_dimensions / 2 - TILE_JUMP_MARGIN * 2) / RATIO, _tile_dimensions / 4 / RATIO, b2Vec2(0, _tile_dimensions *0.24f / RATIO), 0 / RATIO);

	b2FixtureDef FixtureDef;
	FixtureDef.shape = &platformShape;
	FixtureDef.isSensor = true;
	FixtureDef.filter.categoryBits = B2_CATEGORY_PLATFORM;
	FixtureDef.filter.maskBits = B2_CATEGORY_PUPPY;
	FixtureDef.userData = (void*)B2_CONTACT_SPEARS;
	_body->CreateFixture(&FixtureDef);

}

void Platform::drawPlatform(cocos2d::Vec2 &drawFrom, cocos2d::Vec2 &drawTo, bool jumpLeft, bool jumpRight)
{
	Size platform((drawTo.x - drawFrom.x) * _tile_dimensions, _tile_dimensions);
	Vec2 position;
	position.x = drawFrom.x * _tile_dimensions + (drawTo.x - drawFrom.x) / 2 * _tile_dimensions;
	position.y = (this->getContentSize().height - _tile_dimensions) - drawFrom.y * _tile_dimensions + _tile_dimensions - (_tile_dimensions / 4);
	position = deltaPosition(position);

	b2BodyDef BodyDef;
	BodyDef.type = b2_staticBody;
	BodyDef.position.Set(position.x / RATIO, position.y / RATIO );
	//BodyDef.userData = "{empty}";
	_body = _game->getWorld()->CreateBody(&BodyDef);

	int jumpCount = ((jumpLeft) ? 1 : 0) + ((jumpRight) ? 1 : 0);	
	int jumpDelta = 0;
	if (jumpLeft && !jumpRight) jumpDelta = 1;
	if (!jumpLeft && jumpRight) jumpDelta = -1;

	b2PolygonShape platformShape;
	//platformShape.SetAsBox((platform.width / 2 - TILE_JUMP_PLATFORM * jumpCount - TILE_JUMP_MARGIN * jumpCount) / RATIO, _tile_dimensions / 4 / RATIO); // TILE_JUMP_MARGIN / RATIO, 0 / RATIO);
	platformShape.SetAsBox((platform.width / 2 - TILE_JUMP_PLATFORM * jumpCount - TILE_JUMP_MARGIN * jumpCount) / RATIO, (_tile_dimensions / 4 - 4) / RATIO, b2Vec2((TILE_JUMP_MARGIN + TILE_JUMP_PLATFORM) * jumpDelta / RATIO, 0), 0 / RATIO);

	b2FixtureDef FixtureDef;
	FixtureDef.shape = &platformShape;
	FixtureDef.filter.categoryBits = B2_CATEGORY_PLATFORM;
	FixtureDef.filter.maskBits = B2_CATEGORY_PUPPY;
	FixtureDef.userData = (void*)B2_CONTACT_RUN;
	_body->CreateFixture(&FixtureDef);

	if (jumpRight){

		platformShape.SetAsBox(TILE_JUMP_PLATFORM / 2 / RATIO, (_tile_dimensions / 4) / RATIO, b2Vec2((platform.width / 2 - TILE_JUMP_MARGIN) / RATIO, 0), 0 / RATIO);
		FixtureDef.shape = &platformShape;
		FixtureDef.userData = (void*)B2_CONTACT_JUMP_RIGHT;
		_body->CreateFixture(&FixtureDef);

	}

	if (jumpLeft){

		platformShape.SetAsBox(TILE_JUMP_PLATFORM / 2 / RATIO, (_tile_dimensions / 4) / RATIO, b2Vec2((platform.width / 2 - TILE_JUMP_MARGIN) * -1 / RATIO, 0), 0 / RATIO);
		FixtureDef.shape = &platformShape;
		FixtureDef.userData = (void*)B2_CONTACT_JUMP_LEFT;
		_body->CreateFixture(&FixtureDef);

	}
	// */
	//CCLOG(" Draw from: (%.f, %.f) to: (%.f, %.f)", drawFrom.x, drawFrom.y, drawTo.x, drawTo.y);
}

cocos2d::Node* Platform::drawFence(std::vector<int> fenceData)
{
    
    auto fenceNode = Node::create();
    
    Vec2 drawFrom(fenceData.at(0), fenceData.at(1));
    Vec2 drawTo(fenceData.at(2), fenceData.at(3));
    Size platform;
    Vec2 position;
    
    if(fenceData.at(0) - fenceData.at(2) == 0)
    {
        CCLOG("Vertical fense");
        platform = Size(_tile_dimensions, (drawTo.y - drawFrom.y + 1) *  _tile_dimensions);
        position.x = drawFrom.x * _tile_dimensions + _tile_dimensions / 2;
        position.y = this->getContentSize().height - (drawTo.y - drawFrom.y + 1) / 2 * _tile_dimensions - drawFrom.y * _tile_dimensions;
        
        for(int i=drawFrom.y; i<=drawTo.y; i++)
        {
            Vec2 tilePosition;
            tilePosition.x = position.x;
            tilePosition.y = this->getContentSize().height - i * _tile_dimensions - _tile_dimensions / 2;
            
            if(i == drawFrom.y)
                fenceNode->addChild(addFenceTiles(1, tilePosition));
            else if(i == drawTo.y)
                fenceNode->addChild(addFenceTiles(2, tilePosition));
            else
                fenceNode->addChild(addFenceTiles(0, tilePosition));
        }
    }
    else if(fenceData.at(1) - fenceData.at(3) == 0)
    {
        CCLOG("Horizontal fense");
        platform = Size((drawTo.x - drawFrom.x + 1) * _tile_dimensions, _tile_dimensions);
        position.x = drawFrom.x * _tile_dimensions + (drawTo.x - drawFrom.x + 1) / 2 * _tile_dimensions;
        position.y = (this->getContentSize().height - _tile_dimensions) - drawFrom.y * _tile_dimensions + _tile_dimensions - (_tile_dimensions / 2);
        
        for(int i=drawFrom.x; i<=drawTo.x; i++)
        {
            Vec2 tilePosition;
            tilePosition.x = _tile_dimensions + i * _tile_dimensions - _tile_dimensions / 2;
            tilePosition.y = position.y;
            
            if(i == drawFrom.x)
                fenceNode->addChild(addFenceTiles(4, tilePosition));
            else if(i == drawTo.x)
                fenceNode->addChild(addFenceTiles(5, tilePosition));
            else
                fenceNode->addChild(addFenceTiles(3, tilePosition));
        }
    }
    else{
        CCLOG("Mysterious fense: not supported");
    }
    position = deltaPosition(position);

    
    b2BodyDef BodyDef;
    BodyDef.type = b2_staticBody;
    BodyDef.position.Set(position.x / RATIO, position.y / RATIO );
    _body = _game->getWorld()->CreateBody(&BodyDef);
    

    
    b2PolygonShape platformShape;
    platformShape.SetAsBox(platform.width / 2 / RATIO, platform.height / 2 / RATIO);
    
    b2FixtureDef FixtureDef;
    FixtureDef.shape = &platformShape;
	FixtureDef.isSensor = true;
    FixtureDef.filter.categoryBits = B2_CATEGORY_FENCE;
    FixtureDef.filter.maskBits = B2_CATEGORY_FENCECHECK;
    FixtureDef.userData = (void*)B2_CONTACT_FENCE;
    _body->CreateFixture(&FixtureDef);
    
    return fenceNode;
   
}

void Platform::drawGround(cocos2d::Size visibleSize)
{
    auto userData = (void*)B2_CONTACT_GROUND;
    auto categoryBits = B2_CATEGORY_GROUND;
    float deltaPosition;
    
    switch (_stage) {
        default:
        case STAGE_A:
            userData = (void*)B2_CONTACT_GROUND;
            categoryBits = B2_CATEGORY_GROUND;
            deltaPosition = _tile_dimensions * 1.2;
            break;
            
        case STAGE_B:
            userData = (void*)B2_CONTACT_WATER;
            categoryBits = B2_CATEGORY_GROUND;
            deltaPosition = _tile_dimensions * 1.2;
            break;
    }
    
	Vec2 position;
	position.x = 0;
    position.y = visibleSize.height / 2 - this->getContentSize().height / 2 - deltaPosition;

	b2BodyDef BodyDef;
	BodyDef.position.Set(0, 0);
	_ground = _game->getWorld()->CreateBody(&BodyDef);

	b2FixtureDef FixtureDef;
	b2EdgeShape groundShape;
	FixtureDef.shape = &groundShape;

	groundShape.Set(b2Vec2(0, (position.y + _tile_dimensions / 2) / RATIO), b2Vec2(visibleSize.width / RATIO, (position.y + _tile_dimensions / 2) / RATIO));
	FixtureDef.userData = userData;
	FixtureDef.filter.categoryBits = categoryBits;
	FixtureDef.filter.maskBits = B2_CATEGORY_PUPPY | B2_CATEGORY_BULLET | B2_CATEGORY_EXTRA;
	_ground->CreateFixture(&FixtureDef);
}


void Platform::drawBorders(cocos2d::Size visibleSize)
{
	b2BodyDef BodyDef;
	b2Body *_borders;
	_borders = _game->getWorld()->CreateBody(&BodyDef);

	b2FixtureDef FixtureDef;
	b2EdgeShape borderShape;

	borderShape.Set(b2Vec2(0, visibleSize.height / RATIO), b2Vec2(0, 0));
	FixtureDef.shape = &borderShape;
	FixtureDef.restitution = 0.5f;
	FixtureDef.userData = (void*)B2_CONTACT_BORDER_LEFT;
	FixtureDef.filter.categoryBits = B2_CATEGORY_BORDER;
	FixtureDef.filter.maskBits = B2_CATEGORY_PUPPY;
	_borders->CreateFixture(&FixtureDef);

	borderShape.Set(b2Vec2(visibleSize.width / RATIO, visibleSize.height / RATIO), b2Vec2(visibleSize.width / RATIO, 0));
	FixtureDef.shape = &borderShape;
	FixtureDef.userData = (void*)B2_CONTACT_BORDER_RIGHT;
	FixtureDef.filter.categoryBits = B2_CATEGORY_BORDER;
	FixtureDef.filter.maskBits = B2_CATEGORY_PUPPY;
	FixtureDef.restitution = 0.5f;
	_borders->CreateFixture(&FixtureDef);

	borderShape.Set(b2Vec2(0, visibleSize.height / RATIO), b2Vec2(visibleSize.width / RATIO, visibleSize.height / RATIO));
	FixtureDef.shape = &borderShape;
	FixtureDef.userData = (void*)B2_CONTACT_BORDER_TOP;
	FixtureDef.filter.categoryBits = B2_CATEGORY_BORDER;
	FixtureDef.filter.maskBits = B2_CATEGORY_PUPPY;
	FixtureDef.restitution = 0.3f;
	_borders->CreateFixture(&FixtureDef);
}

void Platform::addTile(int tileID, cocos2d::Vec2 &position)
{
	std::string frameName;
	int direction(0);
	Vec2 anchorPoint = Vec2(0.5, 0.5);

	switch (tileID)
	{
	default:
		return; // no tile
	case 1:
		frameName = getTileName() + "-tile-platform";
		break;
	case 2:
		frameName = getTileName() + "-tile-spawn";
		direction = -1;
		break;
	case 3:
		frameName = getTileName() + "-tile-spawn";
		direction = 1;
		break;
	case 4:
		frameName = getTileName() + "-tile-spawn";
		direction = 2;
		break;
	case 5:
		frameName = getTileName() + "-tile-platform-left";
		break;
	case 6:
		frameName = getTileName() + "-tile-platform-right";
		break;
	case 7:
		frameName = getTileName() + "-tile-platform-spears";
        anchorPoint = Vec2(0.5f, 0.3f);
		break;
	}
    
    
   
    
    
    
    if(getTileAnimationFrameCount(tileID) > 0)
    {
        // aka tile has animation
        frameName += "-0.png";
    }
    else
        frameName += ".png";
    
     CCLOG("frame name: %s", frameName.c_str());

	PlatformTile *tile = PlatformTile::createWithFrameName(frameName);
    
    if (_stage == STAGE_A && (tileID == 2 || tileID == 3 || tileID == 4)){
        anchorPoint = Vec2(0.5f, 0.3f);
        _animationSpawn = createAnimation("stage-a-tile-spawn-%i.png", getTileAnimationFrameCount(tileID), 0.1f);
        tile->runAction(RepeatForever::create(Animate::create(_animationSpawn)));
    }

    
	tile->setPosition(position);
	tile->setAnchorPoint(anchorPoint);
	
    if(Manager::getInstance()->getDebugTools())
	tile->setOpacity(50);

	this->addChild(tile);
	if (direction != 0)
	{
		tile->setDirection(direction);
		_spawnPoints.push_back(tile);
	}
    

}

cocos2d::Sprite* Platform::addFenceTiles(int tileID, cocos2d::Vec2 &position)
{
    std::string frameName;
    float rotation = 0.0f;

    switch (tileID)
    {
        default:
            return nullptr; // no tile
        case 0:
            frameName = "fence-0.png"; // [GATIM] fence-0.png -> fence-1.png animācija
            break;
        case 1:
        case 2:
            frameName = "fence-point.png";
            break;
        case 3:
            frameName = "fence-0.png";
            rotation = 90;
            break;
        case 4:
        case 5:
            frameName = "fence-point.png";
            rotation = 90;
            break;
    }
    
    Sprite *tile = Sprite::createWithSpriteFrameName(frameName);
    tile->setPosition(position);
    tile->setRotation(rotation);

	_animationFence = createAnimation("fence-%i.png", 2, 0.1f);

	if (tileID == 0 || tileID == 3)	tile->runAction(RepeatForever::create(Animate::create(_animationFence)));
    
    if(Manager::getInstance()->getDebugTools())
    tile->setOpacity(50);
    
    return tile;
    //_fence->addChild(tile);
}



cocos2d::Vec2 Platform::pointToPosition(cocos2d::Point point)
{
	Vec2 position;    
	position.x = point.x * _tile_dimensions;
	position.y = point.y * _tile_dimensions;
	position = deltaPosition(position);
    
	return position;
}


bool Platform::hasSpawnPoints()
{
    if(_spawnPoints.size()>0)
        return true;
    else
        return false;
}

PlatformTile* Platform::getRandomSpawnPoint()
{
	int rng = rand() % _spawnPoints.size();
	return _spawnPoints.at(rng);
}

PlatformTile* Platform::getSpawnPoint(int nr)
{
	return _spawnPoints.at(nr);
}

std::vector<PlatformTile*> Platform::getAllSpawnPoints()
{
    return _spawnPoints;
}

int Platform::getSpawnPointCount()
{
	return (int)_spawnPoints.size();
}

std::string Platform::getTileName()
{
    /*
    if(_stage == STAGE_A)
        return "forest";
    else if(_tag == STAGE_B)
        return "snake";
    else if(_tag == STAGE_C)
        return "laser";
    else if(_tag == STAGE_D)
        return "hell";
    else*/
        return "stage-a";
}

int Platform::getTileAnimationFrameCount(int tileID)
{
    switch (_stage) {
        case STAGE_A:
            CCLOG("papal");
            if(tileID == 2 || tileID == 3 || tileID == 4) return 6;
            break;
            
        default:
            return 0;
            break;
    }
    return 0;
}

cocos2d::Animation* Platform::createAnimation(const char* name, int frameCount, float frameTime, bool reverse, int offsetX, int offsetY)
{
	Vector<SpriteFrame*> animationVector(frameCount);
	SpriteFrame* animFrame;
	Vec2 offset = Vec2(offsetX, offsetY);
	char str[100] = { 0 };
	if (!reverse){
		for (int i = 0; i < frameCount; i++){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	else
	{
		for (int i = frameCount - 1; i >= 0; i--){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	auto animation = Animation::createWithSpriteFrames(animationVector, frameTime);
	return animation;
}
