#ifndef PuppyMadness_SimpleDPad_h
#define PuppyMadness_SimpleDPad_h


#include "cocos2d.h"


class SimpleDPadDelegate
{
	/// someday maybe
};


class SimpleDPad
{

public:
	
	SimpleDPad();

	~SimpleDPad();
    
    virtual void gameStart(void) = 0;
    
	virtual void gameTooglePause(void) = 0;

	virtual void gameTooglePowerUp(int powerUp) = 0;
    

};


#endif