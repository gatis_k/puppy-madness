#ifndef PuppyMadness_Bobo_h
#define PuppyMadness_Bobo_h


#include "cocos2d.h"


#define BOBO_GAP 20
#define BOBO_SPEED 100
#define BOBO_SPEED_OFFLINE 250
#define zIndexWings 1
#define zIndexHead 1
#define zIndexMouth 1


class Bobo : public cocos2d::Sprite
{
public:

	Bobo(int type);

	virtual ~Bobo();

	static Bobo* create(int type);

	void update(float dt);

	void moveToX(float x);
    
    void setActive(bool active);
    
    bool isActive();

	void setSpeedModifier(float speed);

	bool inPosition();
	void RageMode(bool status);
	bool isRageModeOn();
	void animateBobo();


private:
    
    bool _active;
    
    
	int _type;
	void initBobo(int type);
	void initAnimations(int type);
	float _moveToX;

	float _speedModifier;

	bool _inPosition;

	int _rageStage;
	bool _rageMode;
	cocos2d::Size _visibleSize;
	cocos2d::Animation* _animationHead;
	cocos2d::Sprite* _mouth;
	cocos2d::Animation* createAnimation(const char* name, int frameCount, float frameTime, bool reverse = false, int offsetX = 0, int offsetY = 0);
};

#endif