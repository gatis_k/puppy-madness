#ifndef PuppyMadness_Platform_h
#define PuppyMadness_Platform_h

#include "cocos2d.h"
#include "Box2D/Box2D.h"

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)

#include "Game/PlatformTile.h"
#include "Game/WorldContract.h"

#else
#include "PlatformTile.h"
#include "WorldContract.h"

#endif
////////////////////////////


// tile global dimmension global

const float32 TILE_JUMP_PLATFORM = 2;

const float32 TILE_JUMP_MARGIN = 2;


class Platform: public cocos2d::Node
{
public:

	Platform();
	
	~Platform();

	static Platform* create(WorldContract *game, std::vector<std::vector<int>> tilemap, cocos2d::Vec2 position, int stage);

	void init(WorldContract *game, std::vector<std::vector<int>> tilemap, cocos2d::Vec2 position, int stage);
	
	void drawGround(cocos2d::Size visibleSize);
	void drawBorders(cocos2d::Size visibleSize);
    cocos2d::Node* drawFence(std::vector<int> fenceData);

	CC_SYNTHESIZE(WorldContract *, _game, Game);

	CC_SYNTHESIZE(b2Body *, _ground, Ground);

	CC_SYNTHESIZE(b2Body *, _body, Body);

	cocos2d::Vec2 deltaPosition(cocos2d::Vec2 position);
    
    bool hasSpawnPoints();

	PlatformTile* getRandomSpawnPoint();

	PlatformTile* getSpawnPoint(int nr);
    
    std::vector<PlatformTile*> getAllSpawnPoints();

	int getSpawnPointCount();

	cocos2d::Vec2 pointToPosition(cocos2d::Point point);

private:
    
    int _stage;

	unsigned int _platformHeight;

	cocos2d::Vec2 _position;

    std::vector<PlatformTile *> _spawnPoints;

	PlatformTile* _tutorialPoint;

	void addTile(int tileID, cocos2d::Vec2 &position);
    cocos2d::Sprite* addFenceTiles(int tileID, cocos2d::Vec2 &position);

	void drawPlatform(cocos2d::Vec2 &drawFrom, cocos2d::Vec2 &drawTo, bool jumpLeft, bool jumpRight);

	void drawSpear(cocos2d::Vec2 position);
    
    std::string getTileName();
    int getTileAnimationFrameCount(int tileID);

	cocos2d::Animation* _animationFence;
	cocos2d::Animation* _animationSpawn;
	cocos2d::Animation* _animationSpear;

	cocos2d::Animation* createAnimation(const char* name, int frameCount, float frameTime, bool reverse = false, int offsetX = 0, int offsetY = 0);

protected:

	float _tile_dimensions;

};

#endif