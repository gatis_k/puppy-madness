#ifndef PuppyMadness_BackgroundLayer_h
#define PuppyMadness_BackgroundLayer_h

#include "cocos2d.h"

#define kBackgroundIndex 1
#define kBackgroundElementsIndex 2
#define kGroundIndex 3
#define kBackIndex 4
#define kCloudsIndex 6
#define kWeather 10


class Cloud;

class BackgroundLayer : public cocos2d::Layer
{

public:
	
	BackgroundLayer();

	~BackgroundLayer();
    
    static BackgroundLayer* create(std::string level);

    virtual bool init(std::string level);

	void update(float dt);
    
    void shake(float stength = 1.0f);
    
    // acts like static, has no members from init() methong
    cocos2d::Node* getFrontLayer(int stage);

private:
    
    cocos2d::Node* _backgroundNode;

	cocos2d::Size _visibleSize;
    
    std::string _tag;
    
    int _stage;
    

    bool _hasClouds;
    std::vector<Cloud*> _clouds;
    
    
    void initGround(std::string tag, std::string element);
    
    
    std::map<std::string, cocos2d::Node> _background;
    
	int _speed;


	Cloud* createCloud(std::string fileName, int speed, float scale, cocos2d::Vec2 position, bool allowRotate = false);
    
    void drawGround(std::string tile);
    
    void drawWater(std::string tile, int frameCount);
    
    void addRain();

	cocos2d::Animation* createAnimation(const char* name, int frameCount, float frameTime, bool reverse = false, int offsetX = 0, int offsetY = 0);

	cocos2d::Animation* _animationBat;

};










class Cloud : public cocos2d::Sprite
{

public:
    static Cloud* create(std::string fileName);

	CC_SYNTHESIZE(int, _cloudSpeed, CloudSpeed);

	void move(float dt);

private:
	


};

#endif