#ifndef PuppyMadness_PlatformTile_h
#define PuppyMadness_PlatformTile_h

#include "cocos2d.h"

class PlatformTile : public cocos2d::Sprite
{
public:
	static PlatformTile* createWithFrameName(std::string frameName)
	{
		auto sprite = new PlatformTile;
		cocos2d::SpriteFrame *frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName);
		if (sprite && sprite->initWithSpriteFrame(frame))
		{
			sprite->autorelease();
			return sprite;
		}
		CC_SAFE_DELETE(sprite);
		return nullptr;
	}
	int _direction;
	void setDirection(int direction){ _direction = direction; }
	int getDirection(){ return ((_direction != 2) ? _direction : ((rand() % 2) == 1) ? 1 : -1); } // whether random or not
	PlatformTile() : _direction(0) {};
};

#endif