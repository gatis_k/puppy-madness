#ifndef PuppyMadness_Extras_h
#define PuppyMadness_Extras_h


#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/b2Sprite.h"
#include "Game/WorldContract.h"

#else
#include "b2Sprite.h"
#include "WorldContract.h"

#endif
////////////////////////////

typedef enum _extra_type
{
    EXTRA_SPEED,
    EXTRA_LIVES
//    EXTRA_COOLDOWN
}ExtraType;

class Extra: public b2Sprite
{
public:

	Extra(WorldContract *game, cocos2d::Vec2 position);

	virtual ~Extra();

	static Extra* create(WorldContract *game, cocos2d::Vec2 position);

	virtual void update(float dt, float ratio);
	void interpolationReset();

	virtual void reset();
    
    ExtraType getType();
    
    void setType(ExtraType type);
    
    //CC_SYNTHESIZE(ExtraType, _extraType, ExtraType);

	void basketHit(bool status);

	bool basketIsHit();

	void rest();

	void grounded(bool status);

	bool isGrounded();


private:
	
    ExtraType _type;
	
	void initExtra();

	void initAnimations();
	void initPhysics();

	cocos2d::Vec2 _startPosition;

	bool _basketHit;

	bool _grounded;

	cocos2d::SpriteFrame* _animframe;
	cocos2d::Animation* _extraLives;
	cocos2d::Animation* _extraSpeed;

};

#endif

