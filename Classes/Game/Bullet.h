#ifndef PuppyMadness_Bullet_h
#define PuppyMadness_Bullet_h


#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/b2Sprite.h"
#include "Game/WorldContract.h"

#else
#include "b2Sprite.h"
#include "WorldContract.h"

#endif
////////////////////////////


class Bullet: public b2Sprite
{
public:

	Bullet(WorldContract *game, cocos2d::Vec2 position);

	virtual ~Bullet();

	static Bullet* create(WorldContract *game, cocos2d::Vec2 position);

	virtual void update(float dt, float ratio);
	void interpolationReset();

	virtual void reset();

	void basketHit(bool status);

	bool basketIsHit();

	void rest();

	void grounded(bool status);

	bool isGrounded();

	void initPhysics();
	void initAnimations();

	cocos2d::Vec2 getExplosionPosition();


private:
	
	
	void initBullet();

	cocos2d::Vec2 _startPosition;
	cocos2d::Vec2 _explosionPosition;

	bool _basketHit;

	bool _grounded;

};

#endif

