#ifndef PuppyMadness_HUDLayer_h
#define PuppyMadness_HUDLayer_h


#include "cocos2d.h"

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/PowerUp.h"
#include "Game/Extras.h"
#else
#include "PowerUp.h"
#include "Extras.h"

#endif


////////////////////////////



const float kPadding = 8;

const int zIndexExplosions = 1;
const int zIndexHUD = 5;
const int zIndexShadow = 10;
const int zIndexFx = 11;
const int zIndexPopUpBack = 20;
const int zIndexPopUpElements = 21;
const int zIndexBlur = 9;

const int kBlurTag = 5551;
const int kHUDButtonTag = 6644;
const int kTAG_QUEST_PERCENTAGE = 8888;
const int kTAG_QUEST_COMPLETED = 8889;


//const cocos2d::Size kPopupSize(286, 176);
//const cocos2d::Size kQuestFrameSize(236, 28);
//const cocos2d::Size kQuestFrameSize(246, 30);


class SimpleDPad;
class Quest;


class HUDCoolDown : public cocos2d::Sprite
{
public:
	CC_SYNTHESIZE(int, _tag, Tag);
};



class HUDLayer : public cocos2d::Layer
{
public:

	HUDLayer();	
	~HUDLayer();

	virtual bool init();
	
	CREATE_FUNC(HUDLayer);
	CC_SYNTHESIZE(SimpleDPad*, _dPad, DPad);
    
    
    void showGameSetup(std::string level, int lives);
    
    
	void showGamePlay();
	void showGamePause();
	void showGamePauseResume();
	void showGameOver(int coins);

    void toogleGameStart();
	void tooglePause();
	void tooglePowerUp(int powerUp);
    void toogleSound();
    
    void updateSpeed(int speed);
    void updatePuppies(int puppies);
    void updateCoins(int coinsTotal, int coins, int burst, cocos2d::Vec2 position);
	void updateCoinLabel(float dt);
    void updateBones(cocos2d::Vec2 position);
    void updateLives(int lives);
    void updateTime(float time);
    void updateQuest(std::string tag, float perc, cocos2d::Vec2 position, bool completed, bool daily);
    

	void runGlorifyText(int nr);
    void runChallengeText(int nr);
    void runQuestText();

	void runPowerUpText(int powerUp, bool active);
    void updatePowerUpUses(int powerUp, int uses);
	void runCoolDown(int powerUp, float time, float timeTotal, bool active);
	void endCoolDown(int powerUp);

	void blinkFullScreen(int color);

	void scoreExtra(ExtraType type, cocos2d::Vec2 position, int amount = 0);

	void animateExplosion(cocos2d::Vec2 position);

private:
    
    cocos2d::Size _visibleSize;
    cocos2d::Point _originPoint;
    
    std::string _levelPath;
    std::string _goalMode;
    int _goalTarget;
    int _goalLives;
    int _goalLivesDelta;
    
    int _coins;
    int _puppies;
    int _speed;
    float _time;
    float _timePrev;
	float _score;
	float _alpha;
    bool _timeNewRecord;
    
    cocos2d::Menu* _powerUpMenu;
    cocos2d::Menu* _pauseMenu;
    cocos2d::Node* _shadow;
    
    
    
    cocos2d::Label* _gameText;
    int _gameStartCD = 3;
    void updateGameText(float dt);
    
    cocos2d::Node* _pause;
    cocos2d::Sprite* _pauseSoundRedline;
    
	cocos2d::RenderTexture* _screenShot;
	cocos2d::Sequence* _hudAnimation;
	cocos2d::Animation* _animationExplosion;
    
    void initGameStart();
    void initPauseAndQuests();
    void initHUDElements();
	void initAnimations();
    
    cocos2d::Node* createQuest(Quest* quest, cocos2d::Size size, bool daily = false, int nr = 0);
    cocos2d::Node* createQuestIcon(Quest* quest, bool daily = false, int nr = 0);
    std::map<std::string, cocos2d::Node* > _quests;
    std::map<std::string, cocos2d::Node* > _questsIcon;
    std::map<std::string, cocos2d::ProgressTimer* > _questsIconBar;
    
    
    void sceneReload();
    void sceneEnd(float dt);
    
    void scoreText(int score, int burst, cocos2d::Vec2 position);
    void boneSprite();
    void questIcon(bool daily, cocos2d::Vec2 from, cocos2d::Vec2 to, int nr = 0);
    
    std::vector<cocos2d::Label*> _scoreTexts;
    std::vector<cocos2d::Sprite*> _boneSprites;
    std::vector<cocos2d::Sprite*> _scoreSprites;
	std::vector<cocos2d::Sprite*> _effectSprites;
    std::vector<cocos2d::Sprite*> _questSpritesA;
    std::vector<cocos2d::Sprite*> _questSpritesB;
    std::vector<cocos2d::Sprite*> _questSpritesC;
    std::vector<cocos2d::Sprite*> _questDailySprites;

    cocos2d::Sprite* getScoreSprite();
    cocos2d::Sprite* getQuestSprite(bool daily, int i);
    cocos2d::Label* getScoreText(std::string text);
    float randFloat(int min, int max);

    
    void initGlorifyText(std::string text);
    std::vector<cocos2d::Label*> _glorifyTexts;
    
    void initChallengeText(int challenge, std::string text);
    std::map<int, cocos2d::Label*> _challengeTexts;
    
    cocos2d::Sprite* getBonesSprite();
    
    void initQuestText(std::string text);
    cocos2d::Label* _questText;
    
    
    // HUD Indicators
    
    cocos2d::Node* _coinsBack;
    
    cocos2d::Sprite* _livesIcon;
    cocos2d::Sprite* _timeIcon;
    cocos2d::Sprite* _timeIconNew;
    cocos2d::Sprite* _coinsIcon;
    cocos2d::Sprite* _speedIcon;
    cocos2d::Sprite* _questAIcon;
    cocos2d::Sprite* _questBIcon;
    cocos2d::Sprite* _questCIcon;
    cocos2d::Sprite* _questDIcon;
    
    cocos2d::Label* _livesLabel;
    cocos2d::Label* _timeLabel;
    cocos2d::Label* _timeLabelPrev;
    cocos2d::Label* _coinsLabel;
    cocos2d::Label* _speedLabel;
    cocos2d::Label* _questALabel;
    cocos2d::Label* _questBLabel;
    cocos2d::Label* _questCLabel;
    cocos2d::Label* _questDLabel;
    
    // Blink Sprites
    cocos2d::Sprite* _whiteBlinkSprite;
    cocos2d::Sprite* _redBlinkSprite;
    void blinkFullScreenWithSprite(cocos2d::Sprite* _blinkSprite);
    
    // Utils
    std::string intToString(const int nr);
    std::string intToTimeString(const int nr);
    std::string floatToTimeString(const float nr);
    
    
    // PowerUp texts
    cocos2d::Menu* _menuPowerUp;
    std::map<int, PowerUpSprite* > _powerUps;
    std::map<int, cocos2d::Label*> _powerUpsStart;
    std::map<int, cocos2d::Label*> _powerUpsEnd;
    void initPowerUpText(int powerUp, std::string text, bool final);
    
    cocos2d::MenuItemSprite* createPowerUpMenuItem(int powerUp);
    void initPowerUpButtons();
    void showPowerUpButtons();
    void hidePowerUpButtons();
    
	//Blur
	void blur();
    
};


#endif

