#ifndef PuppyMadness_PowerUp_h
#define PuppyMadness_PowerUp_h

#include "cocos2d.h"


////////////////////////////


typedef enum _powerup_tag {
	POWERUP_SLOWMO = 1,
	POWERUP_CLEAR,
	POWERUP_VORTEX,
    POWERUP_MAGNET,
    POWERUP_HELIUM
} PowerUpTag;



class PowerUp
{
public:

	PowerUp(PowerUpTag tag);

	~PowerUp();

	static PowerUp* create(PowerUpTag tag);
	
	void setParam(float duration, float coolDown);

	void unlock();
    
    void lock();

	void activate();

	void update(float dt);

	bool isActive();

	bool isReady();

	bool flag();

	float getCoolDownTime();

	float getActiveTime();

	bool onCoolDown();

	int getTag();
    
    float getDuration();
    
    float getCoolDown();
    
    bool showMessage();

private:
    

	enum _powerup_state {
		POWERUP_STATE_LOCKED,
		POWERUP_STATE_COOLDOWN,
		POWERUP_STATE_READY,
		POWERUP_STATE_ACTIVE,
		POWERUP_STATE_ENDED
	};
	
	_powerup_state _state;

	bool _unlocked;
    
    bool _msgShowed;

	PowerUpTag _tag;
	
	float _durationTime;
	
	float _duration;
	
	float _coolDownTime;
	
	float _coolDown;

};




class PowerUpSprite: public cocos2d::Node
{
    
public:
    
    static PowerUpSprite* create(int type);
    
    bool init(int type);
    
    PowerUpSprite();
    
    ~PowerUpSprite();
    
    void ready();
    
    void updateCoolDown(float time, float timeTotal);
    
    void updateDuration(float time, float timeTotal);
    
    void updateUses(int uses);
    
    
private:
    
    bool _wasOnReady;
    
    bool _wasOnActive;
    
    bool _lastUse;
    
    cocos2d::Sprite* _iconOffline;
    
    cocos2d::Sprite* _iconActive;
    
    cocos2d::Sprite* _backActive;
    
    cocos2d::Sprite* _circle;
    
    cocos2d::ProgressTimer* _pTimer;
    
    cocos2d::Label* _timeLabel;
    
    cocos2d::Sprite* _usesBack;
    
    cocos2d::Sprite* _usesBackNone;
    
    cocos2d::Label* _uses;
    
    std::string intToString(const int nr);
    
};


#endif
