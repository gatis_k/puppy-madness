#ifndef PuppyMadness_Puppy_h
#define PuppyMadness_Puppy_h

#include "Box2D/Box2D.h"
#include <ctime>

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/b2Sprite.h"
#include "Game/WorldContract.h"

#else
#include "b2Sprite.h"
#include "WorldContract.h"

#endif
////////////////////////////


#define PUPPY_MAXJUMPHEIGHT 3
#define PUPPY_RUNSPEED_MOPSIS 1.0
#define PUPPY_RUNSPEED_MAILO 1.2 //Baseline
#define PUPPY_RUNSPEED_HASKIJS 1.7
#define PUPPY_JUMP_MOPSIS 2
#define PUPPY_JUMP_MAILO 3.5
#define PUPPY_JUMP_HASKIJS 3.0
#define PUPPY_DIRECTION_RIGHT 1
#define PUPPY_DIRECTION_LEFT -1
#define PUPPY_VORTEX_DISTANCE 160 
#define PUPPY_VORTEX_POWER 2.5
#define PUPPY_MAGNET_DISTANCE 5
#define PUPPY_MAGNET_POWER 2000
#define PUPPY_ANIMSTATE_RUN 1
#define PUPPY_ANIMSTATE_JUMP 2
#define PUPPY_ANIMSTATE_DEAD 3
#define PUPPY_ANIMSTATE_FALL 4
#define PUPPY_ANIMSTATE_SWIM 5
#define PUPPY_ANIMSTATE_BOUNCE 6
#define PUPPY_ANIMSTATE_HELIUM 7
#define PUPPY_ANIMSTATE_VORTEX 8
#define PUPPY_ANIMSTATE_RUNDEAD 9
#define PUPPY_ANIMSTATE_DIE 10
#define PUPPY_TYPE_MOPSIS 1
#define PUPPY_TYPE_MAILO 2
#define PUPPY_TYPE_HUSKY 3


class Puppy: public b2Sprite
{
public:

	Puppy(WorldContract *game, cocos2d::Vec2 position);

	virtual ~Puppy();

	static Puppy* create(WorldContract *game, cocos2d::Vec2 position);

	virtual void update(float dt, float ratio);
	void interpolationReset();

	virtual void reset();

	void setDirection(int direction);
	void setRandomDirection();
	void changeDirection();
	int getDirection();
    
    int getType();

	void rest();
	void jump();

	void catched(bool status);
	bool isCatched();

	void grounded(bool status);
	bool isGrounded();
	bool wasGrounded();

	void catchable(bool status);
	bool isCatchable();

	void vortexable(bool status);
	bool isVortexable();

	void magnetable(bool status);
	bool isMagnetable();

	void speared(bool status);
	bool isSpeared();

	void vortex(b2Vec2 basket);
	void magnet(b2Vec2 basket);

	void animate(int state);
    
    bool bringToFront();

	int getAnimationState();
    
    bool isAlive();
	void setAlive(bool status);

	bool isWatered();
	void setWatered(bool status);

	bool isHelium();
	void helium(bool status);

	bool isBounced();
	void bounced(bool status);
	bool getBounced();

	void setRandomType();
	void setRandomRunSpeed();
	float getRunSpeed();
	float getJumpHeight();

	void platformContactAdd(b2Contact* contact);
	void platformContactRemove();
	int getPlatformContacts();


private:
	
	float _animationSpeedModifier = 1.0f; /// �O LIETOT PIE FRAME RATE LAI IEK� SLOW MO VAR KONTROL�T �TRUMU

	int _direction;
    
    int _animationState;

	float _runSpeed;
	float _jumpHeight;
	int _type;
	
	void initPuppy();

	void diveAnimationCallback();
	void dieAnimationCallback();
	void bounceAnimationCallback();

	std::vector<b2Contact*>_platformContacts;

	cocos2d::Vec2 _startPosition;
	cocos2d::SpriteFrame* _animframe;
	cocos2d::Animation* _animationRun;
	cocos2d::Animation* _animationRunDead;
	cocos2d::Animation* _animationJump;
	cocos2d::Animation* _animationDead;
	cocos2d::Animation* _animationFall;
	cocos2d::Animation* _animationDive;
	cocos2d::Animation* _animationSwim;
	cocos2d::Animation* _animationBounce;
	cocos2d::Animation* _animationHelium;
	cocos2d::Animation* _animationDie;

    bool _alive;
	bool _catched;
	bool _grounded;
	bool _watered;
	bool _catchable;
	bool _vortexable;
	bool _magnetable;
	bool _wasgrounded;
	bool _waswatered;
	bool _speared;
	bool _helium;
	bool _bounced;

	void initAnimations();
	void initPhysics();
};

#endif
