#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/Basket.h"
#include "Manager/Manager.h"

#else
#include "Basket.h"
#include "Manager.h"

#endif
////////////////////////////


USING_NS_CC;

Basket::Basket(WorldContract *game, cocos2d::Vec2 position, int type) : b2Sprite(game)
{
	//CCLOG("Basket Construct");
	_startPosition = position;
	_inPlay = true;
	_flipped = true;
	_fenced = false;
	_teleported = false;
	_legs = NULL;
    _basketType = (BasketType)type;
}

Basket::~Basket()
{
	CC_SAFE_RELEASE(_animationIdle);
	CC_SAFE_RELEASE(_girlFloat);
	CC_SAFE_RELEASE(_idle);
	CC_SAFE_RELEASE(_catchA);
	CC_SAFE_RELEASE(_catchB);
	CC_SAFE_RELEASE(_catchC);
	//CC_SAFE_RELEASE(_peak);
	CC_SAFE_RELEASE(_animationFenced);
	CC_SAFE_RELEASE(_animationBoom);
}

Basket* Basket::create(WorldContract *game, cocos2d::Vec2 position, int type)
{
	Basket * sprite = new Basket(game, position, type);
	if (sprite) {
		sprite->initBasket();
		sprite->autorelease();

        if(Manager::getInstance()->getDebugTools())
		sprite->setOpacity(50);

		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Basket::update(float dt) {
	if (_legs){
		
		if (isFlipped())
		_legsSprite->setRotation(CC_RADIANS_TO_DEGREES(-1 * _legs->GetAngle()));
		else
		_legsSprite->setRotation(CC_RADIANS_TO_DEGREES(_legs->GetAngle()));

		if (_legsSprite->getRotation() > 20) {
			_legsSprite->setRotation(15);
		}
		if (_legsSprite->getRotation() < -20) {
			_legsSprite->setRotation(-15);
		}
	}
}

void Basket::initBasket()
{
    std::string basketType = "";
    Point basketAnchor(0, 0);
    Point basketCatchPoint(0, 0);
    int basketAnimationCount = 0;
    float basketFloatTime = 0.0f;
    float basketFloatDelta = 0.0f;
    legs = false;
   
    switch (_basketType) {
        default:
        case BASKET_BASIC:
           
            basketType = "basic";
            basketAnchor = Vec2(0.340f, 0.10f);
            basketAnimationCount = 3;
            basketFloatTime = 2.0f;
            basketFloatDelta = 6;
            basketCatchPoint = Vec2(this->getContentSize().width  / 2 + 20, this->getContentSize().height / 2 + 40);
            legs = false; //temporary
            break;

		case BASKET_MEDIC:

			basketType = "medic";
			basketAnchor = Vec2(0.340f, 0.10f);
			basketAnimationCount = 3;
			basketFloatTime = 2.0f;
			basketFloatDelta = 6;
			basketCatchPoint = Vec2(this->getContentSize().width / 2 + 20, this->getContentSize().height / 2 + 40);
			legs = false; //temporary
			break;
    }

	_basket = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("elena-"+basketType+"-IDLE-0.png");

	for (int i = 0; i < BASKET_TRAIL_LENGTH; i++){
		auto trailSprite = Sprite::createWithSpriteFrameName("elena-" + basketType + "-IDLE-0.png");
		trailSprite->setOpacity(0);
		trailSprite->setAnchorPoint(basketAnchor);
		trailSprite->setPosition(_startPosition);
		_trailSprites.pushBack(trailSprite);
	}

    this->initWithSpriteFrame(_basket);
	initAnimations(basketType);
	initPhysics(basketType);
	this->setSpritePosition(_startPosition);
    
  	_basketEffect = Sprite::create();
    _basketEffect->setAnchorPoint(Vec2(0.5f, 0.0f));
	_basketEffect->setPosition(basketCatchPoint);
	this->addChild(_basketEffect);
    
	MoveBy* moveUp = MoveBy::create(basketFloatTime, Vec2(0, basketFloatDelta));
	MoveBy* moveDown = MoveBy::create(basketFloatTime, Vec2(0, -basketFloatDelta));
	Sequence* upAndDown = Sequence::createWithTwoActions(moveUp, moveDown);
	_girlFloat = RepeatForever::create(upAndDown);
	_girlFloat->retain();
	this->runAction(_girlFloat);
    this->setAnchorPoint(basketAnchor);
}

void Basket::show(bool isVisible)
{
	if (isVisible)
	{
		this->setVisible(true);
		for (int i = 0; i < BASKET_TRAIL_LENGTH; i++){
			this->getParent()->addChild(_trailSprites.at(i), 3);
		}
	}
	else{
		this->setVisible(false);
		this->hide();
	}
}

void Basket::explode()
{
	CCLOG("Basket explode animation");
}


b2Vec2 Basket::getLocation()
{
	return b2Vec2(this->getPositionX(), this->getPositionY() + this->getContentSize().height / 3);
}

void Basket::setDirection(int direction)
{
	this->setScaleX(direction);
}

void Basket::flip(cocos2d::Vec2 Location)
{
	float xmoveDifferenceX = Location.x - this->getPosition().x;
	float xmoveDiffreneceY = Location.y - this->getPosition().y;

	if (xmoveDifferenceX < 0 && abs(xmoveDifferenceX) > BASKET_GIRL_FLIP_DELTA) {
		this->setDirection(BASKET_GIRL_RIGHT);
		for (int i = 0; i < BASKET_TRAIL_LENGTH; i++){
			_trailSprites.at(i)->setScaleX(BASKET_GIRL_RIGHT);
		}
		setFlipped(true);
	}

	if (xmoveDifferenceX > 0 && abs(xmoveDifferenceX) > BASKET_GIRL_FLIP_DELTA) {
		this->setDirection(BASKET_GIRL_LEFT);
		for (int i = 0; i < BASKET_TRAIL_LENGTH; i++){
			_trailSprites.at(i)->setScaleX(BASKET_GIRL_LEFT);
		}
		setFlipped(false);
	}

	if ((xmoveDifferenceX != 0 && abs(xmoveDifferenceX) > BASKET_MIN_TELEPORT_DISTANCE) || (xmoveDiffreneceY != 0 && abs(xmoveDiffreneceY) > BASKET_MIN_TELEPORT_DISTANCE)){
		_teleported = true;
		auto movementVector = Vec2(xmoveDifferenceX, xmoveDiffreneceY);
		auto trailDelta = trunc(BASKET_TRAIL_LENGTH * movementVector.length() / BASKET_TRAIL_MAX_LIMIT);
		trailDelta = (trailDelta > BASKET_TRAIL_LENGTH) ? BASKET_TRAIL_LENGTH : trailDelta;
		for (int i = 0; i < trailDelta; i++){
			_trailSprites.at(i)->setPosition(Vec2(this->getPositionX() + xmoveDifferenceX * i / trailDelta, this->getPositionY() + xmoveDiffreneceY * i / trailDelta));
			_trailSprites.at(i)->setOpacity(BASKET_TRAIL_OPACITY * (i + 1) / trailDelta);
			_trailSprites.at(i)->runAction(FadeOut::create(BASKET_TRAIL_FADETIME));
		}
	}
	else{
		_teleported = false;
	}
}

void Basket::initPhysics(std::string type)
{
	b2BodyDef BodyDef;
	BodyDef.type = b2_kinematicBody;
	_body = _game->getWorld()->CreateBody(&BodyDef);

	
	b2Vec2 triangle[3];
	triangle[0].Set(0 - this->getContentSize().width * 0.55f / 2 / RATIO, 0 + this->getContentSize().height * 0.8f / 2 / RATIO);
	triangle[1].Set(0 + this->getContentSize().width * 0.55f / 2 / RATIO, 0 + this->getContentSize().height * 0.8f / 2 / RATIO);
	triangle[2].Set(0.0f, 0.5f / RATIO);
	int32 count = 3;
	b2PolygonShape basketShape;
	basketShape.Set(triangle, count);
	b2FixtureDef FixtureDef;

	if (type != "shield"){
		b2CircleShape basketCatchShape;
		basketCatchShape.m_p = b2Vec2(0 - this->getContentSize().width * 0.48f / 2 / RATIO, 0 + this->getContentSize().height * 0.8f / 2 / RATIO);
		basketCatchShape.m_radius = 3.0f / RATIO;
		FixtureDef.shape = &basketCatchShape;
		FixtureDef.isSensor = true;
		FixtureDef.density = 6.0f;
		FixtureDef.filter.categoryBits = B2_CATEGORY_BASKET;
		FixtureDef.filter.maskBits = B2_CATEGORY_PUPPY | B2_CATEGORY_BULLET | B2_CATEGORY_EXTRA;
		FixtureDef.userData = (void*)B2_CONTACT_CATCH;
		_body->CreateFixture(&FixtureDef);
		basketCatchShape.m_p = b2Vec2(0 + this->getContentSize().width * 0.48f / 2 / RATIO, 0 + this->getContentSize().height * 0.8f / 2 / RATIO);
		FixtureDef.shape = &basketCatchShape;
		_body->CreateFixture(&FixtureDef);
		basketCatchShape.m_p = b2Vec2(0.0f, 0.5f / RATIO);
		FixtureDef.shape = &basketCatchShape;
		_body->CreateFixture(&FixtureDef);
		basketCatchShape.m_p = b2Vec2(0.0f, 0 + this->getContentSize().height * 0.8f / 2 / RATIO);
		FixtureDef.shape = &basketCatchShape;
		_body->CreateFixture(&FixtureDef);
		_body->SetUserData(this);
	}
	else{
		FixtureDef.shape = &basketShape;
		FixtureDef.density = 1.0f;
		FixtureDef.isSensor = true;
		FixtureDef.filter.categoryBits = B2_CATEGORY_BASKET;
		FixtureDef.filter.maskBits = B2_CATEGORY_PUPPY | B2_CATEGORY_BULLET | B2_CATEGORY_EXTRA;
		FixtureDef.userData = (void*)B2_CONTACT_PUSH;
		_body->CreateFixture(&FixtureDef);
		_body->SetUserData(this);
	}
	BodyDef.type = b2_dynamicBody;
	BodyDef.position = b2Vec2(_body->GetPosition());
	_fenceCheck = _game->getWorld()->CreateBody(&BodyDef);
	FixtureDef.shape = &basketShape;
	FixtureDef.density = 1.0f;
	FixtureDef.filter.categoryBits = B2_CATEGORY_FENCECHECK;
	FixtureDef.filter.maskBits = B2_CATEGORY_FENCE;
	FixtureDef.userData = (void*)B2_CONTACT_FENCECHECK;
	_fenceCheck->CreateFixture(&FixtureDef);
	_fenceCheck->SetSleepingAllowed(false);
	_fenceCheck->SetUserData(this);

	b2WeldJointDef weldJointDef;
	weldJointDef.bodyA = _body;
	weldJointDef.bodyB = _fenceCheck;
	weldJointDef.localAnchorA = b2Vec2(0 / RATIO, 0 / RATIO);
	weldJointDef.localAnchorB = b2Vec2(0 / RATIO, 0 / RATIO);
	weldJointDef.collideConnected = false;
	weldJointDef.frequencyHz = 0;
	weldJointDef.dampingRatio = 1;
	b2WeldJoint* w_joint;
	w_joint = (b2WeldJoint*)_game->getWorld()->CreateJoint(&weldJointDef);

	if (legs)
	{
		_basket = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("basket-" + type + "-legs.png");
		_legsSprite = Sprite::createWithSpriteFrame(_basket);
		_legsSprite->setAnchorPoint(Vec2(0.5, 1.0));
		_legsSprite->setPosition(Vec2(60.0f, 29.0f));
		this->addChild(_legsSprite, -1);

		BodyDef.type = b2_dynamicBody;
		_legs = _game->getWorld()->CreateBody(&BodyDef);
		b2CircleShape legsShape;
		b2FixtureDef legsFixtureDef;
		legsShape.m_radius = (10 / RATIO);
		legsFixtureDef.shape = &legsShape;
		legsFixtureDef.density = 5.0f;
		legsFixtureDef.userData = _legsSprite;
		legsFixtureDef.isSensor = true;
		_legs->CreateFixture(&legsFixtureDef);
		_legs->SetUserData(_legsSprite);

		b2RevoluteJointDef revoluteJointDef;
		revoluteJointDef.bodyA = _body;
		revoluteJointDef.bodyB = _legs;
		revoluteJointDef.collideConnected = false;
		revoluteJointDef.localAnchorA = b2Vec2(0 / RATIO, 0 / RATIO);
		revoluteJointDef.localAnchorB = b2Vec2(0, 10 / RATIO);
		revoluteJointDef.enableLimit = true;
		revoluteJointDef.lowerAngle = -15 * DEGTORAD;
		revoluteJointDef.upperAngle = 15 * DEGTORAD;
		b2RevoluteJoint* r_joint;
		r_joint = (b2RevoluteJoint*)_game->getWorld()->CreateJoint(&revoluteJointDef);
	}
}

void Basket::initAnimations(std::string type)
{
//Basic
	if (type == "basic"){
		//Idle animation
		_animationIdle = createAnimation("elena-basic-IDLE-%i.png", 6, 0.1f);
		_animationIdle->retain();
		_idle = RepeatForever::create(Animate::create(_animationIdle));
		_idle->setTag(1001);
		_idle->retain();
		this->runAction(_idle);
		
		//Default catch animations
		_catchA = createAnimation("elena-basic-catch1-%i.png", 3, 0.1f);
		_catchA->retain();
		_catchB = createAnimation("elena-basic-catch2-%i.png", 3, 0.1f);
		_catchB->retain();
		_catchC = createAnimation("elena-basic-catch3-%i.png", 3, 0.1f);
		_catchC->retain();

		//Explosion
		_animationBoom = createAnimation("elena-basic-explosion-%i.png", 9, 0.1f);
		_animationBoom->retain();

		//Catch effect animation
		//_peak = createAnimation("basket-default-catch-%i.png", 3, 0.1f);
		//_peak->addSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("basket-catch-end.png"));
		//_peak->retain();
	}

//Medic
	if (type == "medic"){
		//Idle animation
		_animationIdle = createAnimation("elena-medic-IDLE-%i.png", 6, 0.1f);
		_animationIdle->retain();
		_idle = RepeatForever::create(Animate::create(_animationIdle));
		_idle->setTag(1001);
		_idle->retain();
		this->runAction(_idle);

		//Default catch animations
		_catchA = createAnimation("elena-medic-catch1-%i.png", 6, 0.1f);
		_catchA->retain();
		_catchB = createAnimation("elena-medic-catch2-%i.png", 6, 0.1f);
		_catchB->retain();
		_catchC = createAnimation("elena-medic-catch3-%i.png", 6, 0.1f);
		_catchC->retain();

		//Explosion
		_animationBoom = createAnimation("elena-medic-explosion-%i.png", 8, 0.1f);
		_animationBoom->retain();

		//Catch effect animation
		//_peak = createAnimation("basket-5000-catch-%i.png", 3, 0.1f);
		//_peak->addSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("basket-catch-end.png"));
		//_peak->retain();
	}

//FENCED animation
	_animationFenced = createAnimation("elena-basic-electrosock-%i.png", 3, 0.1f);
	_animationFenced->retain();
}

void Basket::animate(int state)
{
	if (state == BASKET_ANIMSTATE_CATCH) animateCatch();
	if (state == BASKET_ANIMSTATE_FENCE) animateFence();
	if (state == BASKET_ANIMSTATE_EXPLODE) animateExplode();
}

void Basket::animateCatch()
{
	this->getActionManager()->removeAllActionsByTag(1001, this);
	
	auto callback = CallFunc::create(this, callfunc_selector(Basket::idleAnimation));

	Sequence* seq;

	int i = rand() % 3 + 1;
	switch (i){
	case 1:
		seq = Sequence::createWithTwoActions(Animate::create(_catchA), callback);
		seq->setTag(1001);
		this->runAction(seq);
		break;
	case 2:
		seq = Sequence::createWithTwoActions(Animate::create(_catchB), callback);
		seq->setTag(1001);
		this->runAction(seq);
		break;
	case 3:
		seq = Sequence::createWithTwoActions(Animate::create(_catchC), callback);
		seq->setTag(1001);
		this->runAction(seq);
		break;
	}

	//_basketEffect->runAction(Animate::create(_peak));
}

void Basket::animateFence()
{
	this->getActionManager()->removeAllActionsByTag(1001, this);
	auto fenceanim = RepeatForever::create(Animate::create(_animationFenced));
	fenceanim->setTag(1001);
	this->runAction(fenceanim);
	if (legs) _legsSprite->setVisible(false);
}

void Basket::animateExplode()
{
	auto callback = CallFunc::create(this, callfunc_selector(Basket::idleAnimation));


	this->getActionManager()->removeAllActionsByTag(1001, this);
	auto explode = Animate::create(_animationBoom);
	explode->setTag(1001);
	auto seq = Sequence::createWithTwoActions(explode, callback);
	this->runAction(seq);
	if (legs) _legsSprite->setVisible(false);
}

void Basket::idleAnimation()
{
	this->getActionManager()->removeAllActionsByTag(1001, this);
	auto _newIdle = _idle->clone();
	_newIdle->setTag(1001);
	this->runAction(_newIdle);
	if (legs) _legsSprite->setVisible(true);
}

bool Basket::isFlipped()
{
	return _flipped;
}

void Basket::setFlipped(bool flip)
{
	_flipped = flip;
}

bool Basket::isFenced()
{
	return _fenced;
}

void Basket::setFenced(bool fenced)
{
	_fenced = fenced;
}

bool Basket::isTeleported()
{
	if (_teleported){
        Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_BASKET_TELEPORTED);
		_teleported = false;
		return true;
	}
	else
		return false;
}

void Basket::setTeleported(bool status)
{
	_teleported = status;
}

void Basket::powerUp(int powerUp, bool active)
{
    /// vieta kur pielikt vai atņemt nost power upu bļembukus/identifikatorus
    
    if(Manager::getInstance()->getDebugTools())
    {
        // šeit varētu ar drawPoint parādīt darbības rādiusu
    }
}