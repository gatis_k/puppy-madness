#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/PowerUp.h"
#include "Manager/Manager.h"

#else
#include "PowerUp.h"
#include "Manager.h"

#endif
////////////////////////////



USING_NS_CC;


PowerUp::PowerUp(PowerUpTag tag)
{
    _tag = tag;
}

PowerUp::~PowerUp()
{

}

PowerUp* PowerUp::create(PowerUpTag tag)
{
	PowerUp* pRet = new PowerUp(tag);
	if (pRet)
	{
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}


void PowerUp::setParam(float duration, float coolDown)
{
	_durationTime = 0.0f;
	_coolDownTime = 0.0f;
	_duration = duration;
	_coolDown = coolDown;
    _msgShowed = false;
	_state = POWERUP_STATE_LOCKED;
}

void PowerUp::unlock()
{
	_unlocked = true;
	_state = POWERUP_STATE_COOLDOWN;
}

void PowerUp::lock()
{
    _unlocked = false;
    _state = POWERUP_STATE_LOCKED;
}

void PowerUp::activate()
{
	if (_state == POWERUP_STATE_READY)
    {
		_state = POWERUP_STATE_ACTIVE;
        _msgShowed = false;
    }
}

void PowerUp::update(float dt)
{
	if (_unlocked)
	{
		if (_state == POWERUP_STATE_ACTIVE)
		{
			_durationTime += dt;
			if (_durationTime >= _duration)
			{
				_durationTime = 0.0f;
				_state = POWERUP_STATE_ENDED;
			}

		}

		if (_state == POWERUP_STATE_COOLDOWN)
		{
			_coolDownTime += dt;
			if (_coolDownTime >= _coolDown)
			{
				_coolDownTime = 0.0f;
				_state = POWERUP_STATE_READY;
			}
		}
	}
}


float PowerUp::getCoolDownTime()
{
	if (_state == POWERUP_STATE_COOLDOWN)
		return _coolDown - _coolDownTime;
	else
		return 0.0f;
}

float PowerUp::getDuration()
{
    return _duration;
}

float PowerUp::getCoolDown()
{
    return _coolDown;
}


float PowerUp::getActiveTime()
{
	if (_state == POWERUP_STATE_ACTIVE)
		return _duration - _durationTime;
	else
		return 0.0f;
}


bool PowerUp::isActive()
{
	return (_state == POWERUP_STATE_ACTIVE) ? true : false;
}

bool PowerUp::isReady()
{
	return (_state == POWERUP_STATE_READY) ? true : false;

}

bool PowerUp::onCoolDown()
{
	return (_state == POWERUP_STATE_COOLDOWN) ? true : false;
}

int PowerUp::getTag()
{
	return _tag;
}

bool PowerUp::flag()
{
	if (_state == POWERUP_STATE_ENDED)
	{
		_state = POWERUP_STATE_COOLDOWN;
		return true;
	}
	else
		return false;
}

bool PowerUp::showMessage()
{
    if(_msgShowed)
        return false;
    else
    {
        _msgShowed = true;
        return true;
    }
}

//// PowerUp Sprite

PowerUpSprite::PowerUpSprite()
{
    /*
    
    //*/
}

PowerUpSprite::~PowerUpSprite()
{
    
}



PowerUpSprite* PowerUpSprite::create(int type)
{
    PowerUpSprite * sprite = new PowerUpSprite();
    if (sprite) {
        sprite->init(type);
        sprite->autorelease();
        
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

bool PowerUpSprite::init(int type)
{
    _wasOnReady = false;
    _wasOnActive = false;
    _lastUse = false;
    std::string iconStr = "";
    switch (type)
    {
        case POWERUP_MAGNET:
            iconStr = "ui-hud-powerup-magnet";
            break;
        case POWERUP_VORTEX:
            iconStr = "ui-hud-powerup-vortex";
            break;
        case POWERUP_CLEAR:
            iconStr = "ui-hud-powerup-clear";
            break;
        case POWERUP_SLOWMO:
            iconStr = "ui-hud-powerup-slowmo";
            break;
        case POWERUP_HELIUM:
            iconStr = "ui-hud-powerup-helium";
            break;
            
    }
    

    Sprite* pTimerSprite = Sprite::createWithSpriteFrameName("ui-hud-powerup-offline-percentage.png");
    _pTimer = ProgressTimer::create(pTimerSprite);
    _pTimer->setType(ProgressTimer::Type::RADIAL); // kCCProgressTimerTypeRadial
    _pTimer->setPercentage(0.0f);
    _pTimer->setAnchorPoint(Vec2(0.0f, 0.0f));
    this->addChild(_pTimer);
    
    Vec2 middlePoint(pTimerSprite->getContentSize().width / 2, pTimerSprite->getContentSize().height / 2);
    
    _circle = Sprite::createWithSpriteFrameName("ui-hud-powerup-circle.png");
    _circle->setAnchorPoint(Vec2(0.5f, 0.5f));
    _circle->setPosition(middlePoint);
    _circle->setVisible(false);
    this->addChild(_circle);
    
    
    _backActive = Sprite::createWithSpriteFrameName("ui-hud-powerup-active.png");
    _backActive->setAnchorPoint(Vec2(0.0f, 0.0f));
    _backActive->setVisible(false);
    this->addChild(_backActive);
    
    _iconOffline = Sprite::createWithSpriteFrameName(iconStr + "-0.png");
    _iconOffline->setAnchorPoint(Vec2(0.5f, 0.5f));
    _iconOffline->setOpacity(120);
    _iconOffline->setPosition(middlePoint);
    this->addChild(_iconOffline);
    
    _iconActive = Sprite::createWithSpriteFrameName(iconStr + "-1.png");
    _iconActive->setAnchorPoint(Vec2(0.5f, 0.5f));
    _iconActive->setPosition(middlePoint);
    _iconActive->setVisible(false);
    this->addChild(_iconActive);
    
    
    _timeLabel = Label::createWithTTF(intToString(0), kFontPath + kFontNameStyle, 36);
    //_timeLabel->enableOutline({108, 86, 18, 255}, 1);
    _timeLabel->enableShadow(Color4B(0,0,0,105),Size(1,-1),0);
//    _timeLabel->setColor({236, 52, 130});
    _timeLabel->setColor(Color3B::WHITE);
    _timeLabel->setPosition(Vec2(middlePoint.x, middlePoint.y + 4));
    _timeLabel->setAnchorPoint(Vec2(0.5f, 0.65f));
    _timeLabel->setVisible(false);
    this->addChild(_timeLabel);
    
    _usesBack = Sprite::createWithSpriteFrameName("ui-hud-powerup-count.png");
    _usesBack->setAnchorPoint(Vec2(0.5f, 0.5f));
    _usesBack->setPosition(Vec2(middlePoint.x + 20, middlePoint.y - 20));
    _usesBack->setVisible(false);
    this->addChild(_usesBack);
    
    _usesBackNone = Sprite::createWithSpriteFrameName("ui-hud-powerup-count-none.png");
    _usesBackNone->setAnchorPoint(Vec2(0.5f, 0.5f));
    _usesBackNone->setPosition(Vec2(middlePoint.x + 20, middlePoint.y - 20));
    this->addChild(_usesBackNone);
    
    int uses = Manager::getInstance()->getSlotHandler()->getPowerUpUses(type);
    _uses = Label::createWithTTF(intToString(uses), kFontPath + kFontNameStyle, 16);
//    _uses->setColor({236, 52, 130});
    _uses->setColor(Color3B::WHITE);
    _uses->enableShadow(Color4B(0,0,0,155),Size(0.5f,-0.5f),0);
    _uses->setPosition(Vec2(middlePoint.x + 20, middlePoint.y - 20));
    _uses->setAnchorPoint(Vec2(.5f, .5f));
    this->addChild(_uses);
    
    
    this->setAnchorPoint(Vec2(0.0f, 0.0f));
    this->setContentSize(pTimerSprite->getContentSize());

    return true;
}

void PowerUpSprite::ready()
{
    if(!_wasOnReady)
    {
        _pTimer->setPercentage(100.0f);
        _iconOffline->setVisible(false);
        _iconActive->setVisible(true);
        _timeLabel->setVisible(false);
        _backActive->setVisible(true);
        _circle->setVisible(true);
        _usesBack->setVisible(true);
        _usesBackNone->setVisible(false);
        _timeLabel->setVisible(false);

        auto scale = ScaleBy::create(0.4, 1.1f);
        auto fade = FadeTo::create(0.3, 0.0f);
        auto scaleBack = ScaleTo::create(0.1, 1.0f);
        auto fadeBack = FadeTo::create(0.1, 255.0f);
        
        auto seq = Sequence::create(scale, fade, scaleBack, fadeBack, nullptr);
        _circle->runAction(RepeatForever::create(seq));
        

    }
    _wasOnReady = true;
}

void PowerUpSprite::updateCoolDown(float time, float timeTotal)
{
    if(_lastUse)
    {
        
        _usesBack->setVisible(false);
        _usesBackNone->setVisible(true);
        _iconOffline->setVisible(true);
        _iconActive->setVisible(false);
        _timeLabel->setVisible(false);
        _backActive->setVisible(false);
        _backActive->getActionManager()->removeAllActionsFromTarget(_backActive);
        
        _wasOnActive = true;
    }
    else
    {
        if(_wasOnReady){
            _wasOnReady = false;
            
            _usesBack->setVisible(false);
            _usesBackNone->setVisible(true);
            _iconOffline->setVisible(true);
            _iconActive->setVisible(false);
            _timeLabel->setVisible(true);
            _backActive->setVisible(false);
            _timeLabel->setVisible(true);
        }
        
        if(_wasOnActive)
        {
            _wasOnActive = false;
            _backActive->getActionManager()->removeAllActionsFromTarget(_backActive);
        }
        
        if(!_timeLabel->isVisible())
            _timeLabel->setVisible(true);
      
        float percent = (timeTotal - time) * 100 / timeTotal;
        _pTimer->setPercentage(percent);
        _timeLabel->setString(intToString((int)time));
    }
    
    

}

void PowerUpSprite::updateDuration(float time, float timeTotal)
{
    if(!_wasOnActive)
    {
        _wasOnActive = true;
        
        _circle->setVisible(false);
        _circle->getActionManager()->removeAllActionsFromTarget(_circle);
        
        auto blink = Blink::create(0.5, 1);
        _backActive->runAction(RepeatForever::create(blink));
    }
    /*
    if(!_timeLabel->isVisible())
        _timeLabel->setVisible(true);
    
    _timeLabel->setString(intToString((int)time));
     // */
}

void PowerUpSprite::updateUses(int uses)
{
    if(uses == 0)
    {
        _lastUse = true;
    }
    _uses->setString(this->intToString(uses));
}


std::string PowerUpSprite::intToString(const int nr)
{
    char charText[100];
    sprintf(charText, "%d", nr);
    std::string str(charText);
    return str;
}
