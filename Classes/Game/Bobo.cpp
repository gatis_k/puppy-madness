#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/Bobo.h"
#include "Manager/Manager.h"

#else
#include "Bobo.h"
#include "Manager.h"

#endif
////////////////////////////




USING_NS_CC;


Bobo::Bobo(int type) :
_active(false),
_inPosition(true),
_speedModifier(1),
_rageStage(0),
_rageMode(true),
_type(type)
{

	_visibleSize = Director::getInstance()->getVisibleSize();

    switch (type) {
        default:
//        case STAGE_FOREST:
			this->initWithSpriteFrameName("bobo-forest-head-0.png");
			_mouth = Sprite::createWithSpriteFrameName("bobo-forest-mouth.png");
			_mouth->setAnchorPoint(Vec2(0.5f, 1.0f));
			_mouth->setPosition(29, 8);
			this->addChild(_mouth, -1);
            break;
	/*
        case STAGE_LASER:
			this->initWithSpriteFrameName("bobo-laser-0.png");
			break;*/
    }    
}

Bobo::~Bobo()
{

}

Bobo* Bobo::create(int type)
{
	Bobo * sprite = new Bobo(type);
	if (sprite)
	{	
		sprite->initBobo(type);
		sprite->autorelease();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Bobo::initBobo(int type)
{
	initAnimations(type);
}

void Bobo::update(float dt)
{
    if(_active)
    {
        float currentX = this->getPositionX();

        if (_rageMode){
            //RageStage 0 - Go to nearest screen edge
            if (_rageStage == 0){
                if (currentX <= _visibleSize.width / 2){
                    if (currentX <= 0 + this->getContentSize().width / 2){
                        _rageStage = 1;
                        //CCLOG("rageStage Left: %i", _rageStage);
                    }
                    else {
                        this->setPositionX(currentX - (dt * BOBO_SPEED * _speedModifier * 2.0f));
                    }
                }
                else {
                    if (currentX < _visibleSize.width - this->getContentSize().width / 2){
                        this->setPositionX(currentX + (dt * BOBO_SPEED * _speedModifier * 2.0f));
                    }
                    else {
                        _rageStage = 1;
                        //CCLOG("rageStage Right: %i", _rageStage);
                    }
                }
            }

            //RageStage 1 - Target other screen edge
            if (_rageStage == 1){
                _inPosition = false;
                if (currentX < _visibleSize.width / 2){
                    _rageStage = 2;
                }
                else{
                    _rageStage = 3;
                }
                //CCLOG("Switch %i", _rageStage);
            }

            //RageStage 2 - Go Left to Right
            if (_rageStage == 2){
                if (currentX < _visibleSize.width - this->getContentSize().width / 2){
                    this->setPositionX(currentX + (dt * BOBO_SPEED * _speedModifier));
                    _inPosition = false;
                }
                else{
                    _rageMode = false;
                    _rageStage = 0;
                    //CCLOG("Done");
                    _moveToX = _visibleSize.width / 2;
                }
            }

            //RageStage 3 - Go Right to Left
            if (_rageStage == 3){
                if (currentX > 0 + this->getContentSize().width / 2){
                    this->setPositionX(currentX - (dt * BOBO_SPEED * _speedModifier));
                    _inPosition = false;
                }
                else{
                    _rageMode = false;
                    _rageStage = 0;
                    _moveToX = _visibleSize.width / 2;
                    //CCLOG("Done");
                }
            }
        }
        else
        {
            if (!_inPosition)
            {
                float currentX = this->getPositionX();

                if (currentX >= _moveToX - BOBO_GAP && currentX <= _moveToX + BOBO_GAP)
                {
                    _inPosition = true;
                }
                else{
                    if (currentX > _moveToX)
                    {
                        this->setPositionX(currentX + (dt * BOBO_SPEED * _speedModifier * -1));
                    }
                    else if (currentX < _moveToX)
                    {
                        this->setPositionX(currentX + (dt * BOBO_SPEED * _speedModifier));
                    }
                }
                //CCLOG("xpos %.1f", this->getPositionX());
            }
        }
            
    }
    else
    {
        // get bobo off screen
        float currentX = this->getPositionX();
        if(currentX > -100)
            this->setPositionX(currentX + (dt * BOBO_SPEED_OFFLINE * _speedModifier * -1));
		_rageMode = true;
        
    }
	

}

void Bobo::initAnimations(int type)
{
//	switch ((StageElement)type) {

//	case STAGE_FOREST:

		_animationHead = createAnimation("bobo-forest-head-%i.png", 3, 0.1f);
		this->runAction(RepeatForever::create(Animate::create(_animationHead)));
//		break;
  /*
    case STAGE_LASER:
            
        _animationHead = createAnimation("bobo-laser-%i.png", 3, 0.1f);
        this->runAction(RepeatForever::create(Animate::create(_animationHead)));

        break;
            
        break;

	} */

	auto floatDown = MoveBy::create(2.0f, Vec2(0.0f, -8.0f));
	auto floatUp = floatDown->reverse();
	auto floatAction = RepeatForever::create(Sequence::create(floatDown, floatUp, NULL));
	this->runAction(floatAction);
}

void Bobo::moveToX(float x)
{
	_moveToX = x;
	_inPosition = false;
}

void Bobo::setActive(bool active)
{
    _active = active;
}

bool Bobo::isActive()
{
    return _active;
}

void Bobo::setSpeedModifier(float speed)
{
	_speedModifier = speed;
}

bool Bobo::inPosition()
{
	return (_inPosition) ? true : false;
}

void Bobo::RageMode(bool status)
{
	_rageMode = true;
}

bool Bobo::isRageModeOn()
{
	return _rageMode;
}

cocos2d::Animation* Bobo::createAnimation(const char* name, int frameCount, float frameTime, bool reverse, int offsetX, int offsetY)
{
	Vector<SpriteFrame*> animationVector(frameCount);
	SpriteFrame* animFrame;
	Vec2 offset = Vec2(offsetX, offsetY);
	char str[100] = { 0 };
	if (!reverse){
		for (int i = 0; i < frameCount; i++){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	else
	{
		for (int i = frameCount - 1; i >= 0; i--){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	auto animation = Animation::createWithSpriteFrames(animationVector, frameTime);
	return animation;
}

void Bobo::animateBobo()
{
	if (getActionManager()->getNumberOfRunningActionsInTarget(_mouth) == 0){
//		switch ((StageElement)_type) {
//		default:
//		case STAGE_FOREST:
			cocos2d::MoveBy* mouthMoveDown;
			mouthMoveDown = MoveBy::create(0.2f, Vec2(0.0f, -5.0f));
			cocos2d::MoveBy* mouthMoveUp;
			mouthMoveUp = mouthMoveDown->reverse();
			cocos2d::Sequence* mouthAction;
			mouthAction = Sequence::createWithTwoActions(mouthMoveDown, mouthMoveUp);
			_mouth->runAction(mouthAction);
//			break;
//		case STAGE_LASER:

//			break;
//		}
	}
}