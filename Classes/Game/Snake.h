#ifndef PuppyMadness_Snake_h
#define PuppyMadness_Snake_h


#include "cocos2d.h"


#define SNAKE_ATTACK_GAP 5
#define SNAKE_AVOID_GAP 30
#define SNAKE_SPEED 100
#define SNAKE_SPEED_OFFLINE 300

typedef enum _snake_state{
    SNAKE_PURSUE,
    SNAKE_WAIT,
    SNAKE_ATTACK,
    SNAKE_RESET
}SnakeState;


class Snake : public cocos2d::Sprite
{
public:

	Snake();

	virtual ~Snake();

	static Snake* create();

    void update(float dt, cocos2d::Vec2 targetPosition);
    
    void updatePostAttack(float dt);
    
    void setActive(bool active);

	void avoidX(float x);

	void setSpeedModifier(float speed);
    
    bool canPursue(cocos2d::Vec2 position);
    
    bool canKill(cocos2d::Vec2 position);

    void pursue();
    
    void reset();

private:

	float _basketX;
    float _returnY;
    
    bool _active;
    
    bool _inPosition;
    
    cocos2d::Vec2 _targetPosition;
	cocos2d::Animation* _animationChew;
    SnakeState _state;
	    
    void animate(SnakeState state);

	float _speedModifier;
        
    float _timeTillAttack;
    float _attackDelay;

    bool inPosition();

	void initAnimations();
};

#endif