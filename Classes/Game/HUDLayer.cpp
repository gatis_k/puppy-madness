#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/HUDLayer.h"
#include "Game/SimpleDPad.h"
#include "Manager/Manager.h"

#else
#include "HUDLayer.h"
#include "SimpleDPad.h"
#include "Manager.h"

#endif

USING_NS_CC;


HUDLayer::HUDLayer() :
_dPad(NULL),
_coins(0),
_puppies(0),
_speed(0),
_time(0),
_timePrev(0),
_timeNewRecord(false),
_score(0.0f),
_alpha(0.0f),
_goalTarget(0),
_goalLives(0),
_goalLivesDelta(0)
{

}


HUDLayer::~HUDLayer()
{
    CC_SAFE_RELEASE(_hudAnimation);
	CC_SAFE_RELEASE(_animationExplosion);
}

bool HUDLayer::init() {
    
	if (!Layer::init())
	{
		return false;
	}
    
    auto director = Director::getInstance();
    
    _visibleSize = director->getVisibleSize();
    _originPoint = director->getVisibleOrigin();
    
    _shadow = Node::create();
    auto rectNodeShadow = DrawNode::create();
    Vec2 rectangleShadow[4];
    rectangleShadow[0] = Vec2(0, 0);
    rectangleShadow[1] = Vec2(_visibleSize.width, 0);
    rectangleShadow[2] = Vec2(_visibleSize.width, _visibleSize.height);
    rectangleShadow[3] = Vec2(0, _visibleSize.height);
    rectNodeShadow->drawPolygon(rectangleShadow, 4, Color4F(Color4B(0, 0, 0, 145)), 0, Color4F::RED);
    _shadow->setContentSize(_visibleSize);
    _shadow->addChild(rectNodeShadow);
    _shadow->setVisible(false);
    this->addChild(_shadow, zIndexShadow);
    
    
    
    // BLINK SCREENS ///////////////////////////////////////////
    
    /*
    auto blinkSprite = Sprite::createWithSpriteFrameName("fx-fade-red.png");
    blinkSprite->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2));
    this->addChild(blinkSprite, 1000); */
    
    _whiteBlinkSprite = Sprite::createWithSpriteFrameName("blinksprite-white.png");
    _whiteBlinkSprite->setScale(150);
    _whiteBlinkSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
    _whiteBlinkSprite->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2));
    _whiteBlinkSprite->setOpacity(0);
    this->addChild(_whiteBlinkSprite, 1000);
    
    _redBlinkSprite = Sprite::createWithSpriteFrameName("blinksprite-red.png");
    _redBlinkSprite->setScale(150);
    _redBlinkSprite->setOpacity(0);
    _redBlinkSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
    _redBlinkSprite->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2));
    this->addChild(_redBlinkSprite, 1000);
   

	//HUD Icons&labels animation
	auto scaleUp = ScaleBy::create(0.12f, 1.12f);
	auto scaleDown = scaleUp->reverse();
	_hudAnimation = Sequence::createWithTwoActions(scaleUp, scaleDown);
	_hudAnimation->retain();
    //end

    Sprite *pauseButton = Sprite::createWithSpriteFrameName("ui-hud-button-pause-0.png");
    Sprite *pauseButtonActive = Sprite::createWithSpriteFrameName("ui-hud-button-pause-1.png");
    auto pauseItem = MenuItemSprite::create(pauseButton, pauseButtonActive, NULL, CC_CALLBACK_0(HUDLayer::tooglePause, this));
    
    pauseItem->setAnchorPoint(Vec2(0.0f, 0.0f));
    pauseItem->setPosition(kPadding, kPadding);
    
    _pauseMenu = Menu::create(pauseItem, NULL);
    _pauseMenu->setPosition(_originPoint.x, _originPoint.y);
    _pauseMenu->setVisible(false);
    this->addChild(_pauseMenu, zIndexHUD);
    
	///*
    initGlorifyText("AWESOME");
    initGlorifyText("YOU ROCK");
    initGlorifyText("HOLY MACARONI");
    initGlorifyText("MADNESS");
    
    initChallengeText(0, "PUPPY RAIN");
    initChallengeText(1, "SPAWN SPAWN SPAWN");
    initChallengeText(2, "BABAI STRIKE");
    initChallengeText(3, "WIND");
    initChallengeText(4, "SNAKE");
    
    initQuestText("QUEST COMPLETED");
    
	//*/
    /*
	initGlorifyText("LOVE X2");
	initGlorifyText("LOVE X3");
	initGlorifyText("LOVE X4");
	initGlorifyText("LOVE X5");
     */
    _glorifyTexts.at(3)->setColor(Color3B::YELLOW);
    
    
    initPowerUpText(POWERUP_SLOWMO, "SNAIL MODE ON", false);
    initPowerUpText(POWERUP_SLOWMO, "SNAIL MODE OFF", true);
    
    initPowerUpText(POWERUP_MAGNET, "MAGNET ACTIVE", false);
    initPowerUpText(POWERUP_MAGNET, "MAGNET RUN OUT", true);
    
    initPowerUpText(POWERUP_VORTEX, "VORTEX ACTIVE", false);
    initPowerUpText(POWERUP_VORTEX, "VORTEX COLLAPSED", true);
    
    initPowerUpText(POWERUP_HELIUM, "LET'S GO UP", false);
    initPowerUpText(POWERUP_HELIUM, "RUN OUT OF HELIUM", true);
    
    initPowerUpText(POWERUP_CLEAR, "SWIPED!", false);
    

    initPowerUpButtons();
	initAnimations();
    

    
    return true;
}



void HUDLayer::showGameSetup(std::string level, int lives)
{
    _levelPath = level;
    _goalLives = _goalLivesDelta = lives;

    
    initGameStart();
    initPauseAndQuests();
    initHUDElements();
    
    showPowerUpButtons();
}

void HUDLayer::showGamePlay()
{
    showPowerUpButtons();
    _pauseMenu->setVisible(true);
    _shadow->setVisible(false);
    
	//this->removeChildByTag(kBlurTag, true);
}

void HUDLayer::showGamePause()
{
    hidePowerUpButtons();
    _pauseMenu->setVisible(false);
    _shadow->setVisible(true);
    _pause->setVisible(true);
    
    if(!Manager::getInstance()->getSoundsLibrary()->isSoundOn())
        _pauseSoundRedline->setVisible(true);
    
}

void HUDLayer::showGamePauseResume()
{
    showPowerUpButtons();
    _pauseMenu->setVisible(true);
    _shadow->setVisible(false);
    _pause->setVisible(false);
    _pauseSoundRedline->setVisible(false);
}

void HUDLayer::showGameOver(int coins)
{
    
	//getActionManager()->pauseAllRunningActions();
    _redBlinkSprite->setVisible(false);
    _whiteBlinkSprite->setVisible(false);
    
	_coinsLabel->setString(intToString((int)_coins));
    hidePowerUpButtons();
    _pauseMenu->setVisible(false);
    
    std::vector<std::string> overTxt;
    overTxt.push_back("ROUND OVER");
    overTxt.push_back("BUSTED!");
    overTxt.push_back("THAT'S IT?");
    overTxt.push_back("THANKS!");
    overTxt.push_back("SEE YOU!");
    int max_rand = overTxt.size() - 1;
    int r = random(0, max_rand);
    std::string txt = overTxt.at(r);
    
    //[GATIM] animation required;
    _gameText->setString(txt);
    _gameText->setVisible(true);
    
    this->schedule(schedule_selector(HUDLayer::sceneEnd),3.0f);
}

void HUDLayer::toogleGameStart()
{
    this->getDPad()->gameStart();
    //Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_FORWARD);
}

void HUDLayer::tooglePause()
{
    this->getDPad()->gameTooglePause();
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
}

void HUDLayer::tooglePowerUp(int powerUp)
{
	this->getDPad()->gameTooglePowerUp(powerUp);
}

void HUDLayer::toogleSound()
{
    Manager::getInstance()->getSoundsLibrary()->toogleSound();
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
    
    if(Manager::getInstance()->getSoundsLibrary()->isSoundOn())
        _pauseSoundRedline->setVisible(false);
    else
        _pauseSoundRedline->setVisible(true);
}

///////// HUD EFFECTS //////////////////////////////////

void HUDLayer::runGlorifyText(int nr)
{
    if(nr>0)
    {
        auto text = _glorifyTexts.at((nr-1));
        text->setScale(1.0f);
        text->setVisible(true);
        auto move = ScaleBy::create(1.5f, 1.6f);
        auto hide = Hide::create();
        
        auto glorify = Sequence::create(move, hide, NULL);
        
        text->stopAllActions();
        text->runAction(glorify);
    }
}


void HUDLayer::runChallengeText(int nr)
{
    auto text = _challengeTexts.at(nr);
    text->setScale(1.0f);
    text->setVisible(true);
    auto move = ScaleBy::create(1.2f, 1.4f);
    auto hide = Hide::create();
    
    auto glorify = Sequence::create(move, hide, NULL);
    
    text->stopAllActions();
    text->runAction(glorify);
}

void HUDLayer::runQuestText()
{
    auto text = _questText;
    text->setScale(1.0f);
    text->setVisible(true);
    auto move = ScaleBy::create(1.2f, 1.4f);
    auto hide = Hide::create();
    
    auto glorify = Sequence::create(move, hide, NULL);
    
    text->stopAllActions();
    text->runAction(glorify);
}



void HUDLayer::updateSpeed(int speed)
{
    _speed = speed;
    _speedLabel->setString(intToString(speed));
	if (_speedIcon->getActionManager()->getNumberOfRunningActionsInTarget(_speedIcon) == 0){
		//_speedLabel->runAction(_hudAnimation->clone());
		_speedIcon->runAction(_hudAnimation->clone());
	}
}

void HUDLayer::updatePuppies(int puppies)
{
    _puppies = puppies;
}


void HUDLayer::updateCoins(int coinsTotal, int coins, int burst, cocos2d::Vec2 position)
{
    _coins = coinsTotal;
	//_coinsLabel->setString(intToString(coinsTotal));
    this->scoreText(coins, burst, position);
	
	if (_coinsIcon->getActionManager()->getNumberOfRunningActionsInTarget(_coinsIcon) == 0){
		//_coinsLabel->runAction(_hudAnimation->clone());
		_coinsIcon->runAction(_hudAnimation->clone());
	}
}

void HUDLayer::updateCoinLabel(float dt)
{
	if (_score < _coins){
		float scoreDelta = (_coins - _alpha) / 0.5 * dt;
		_score = _score + scoreDelta;
		}
	else {
		_score = _coins;
		_alpha = _coins;
	}
	_coinsLabel->setString(intToString((int)_score));
    _coinsBack->setScaleX((_coinsLabel->getContentSize().width + 10) / 100);
}
void HUDLayer::updateBones(cocos2d::Vec2 position)
{
    auto bones = getBonesSprite();
    bones->setScale(0.8f);
    bones->setVisible(true);
    bones->setOpacity(150);
    bones->setPosition(position.x, position.y);
    

    auto moveOut = MoveTo::create(1.5f, Vec2(position.x, _visibleSize.height + 30));
    auto rotateOut = RotateBy::create(.5f, 180);
    auto rotateOut2 = RotateBy::create(1.0f, 180);
    auto fadeIcon = FadeOut::create(1.5f);
    auto hideIcon = Hide::create();
    auto iconAnimation = Sequence::create(rotateOut, fadeIcon, hideIcon, NULL);
    
    bones->stopAllActions();
    bones->runAction(rotateOut2);
    bones->runAction(moveOut);
    bones->runAction(iconAnimation);
}



void HUDLayer::updateLives(int lives)
{
    if (lives < _goalLivesDelta)
    {
        if (lives == 1)
            blinkFullScreen(1);
        else
            blinkFullScreen(0);
    }
    _livesLabel->setString(intToString(lives));
    _goalLivesDelta = lives;

	if (_livesIcon->getActionManager()->getNumberOfRunningActionsInTarget(_livesIcon) == 0){
		//_livesLabel->runAction(_hudAnimation->clone());
		_livesIcon->runAction(_hudAnimation->clone());
	}
}

void HUDLayer::updateTime(float time)
{
    
    if(time > _timePrev && !_timeNewRecord)
    {
        _timeNewRecord = true;
        _timeLabelPrev->setString("NEW");
        _timeLabelPrev->setScale(0.8f);
        _timeLabelPrev->setOpacity(255);
        _timeLabelPrev->setColor(Color3B::GREEN);
        _timeLabel->setColor(Color3B::GREEN);
    }
    _timeLabel->setString(floatToTimeString(time));
    _time = time;
}


void HUDLayer::updateQuest(std::string tag, float perc, cocos2d::Vec2 position, bool completed, bool daily)
{
    
    _quests.at(tag)->getChildByTag(kTAG_QUEST_PERCENTAGE)->setScaleX(perc / 100);
    _questsIconBar.at(tag)->setPercentage(perc);
    
    int nr = _quests.at(tag)->getTag();
    questIcon(daily, position, _questsIcon.at(tag)->getPosition(), nr);
    
    if(completed)
    {
        _quests.at(tag)->getChildByTag(kTAG_QUEST_COMPLETED)->setVisible(true);
        _questsIcon.at(tag)->getChildByTag(kTAG_QUEST_COMPLETED)->setVisible(true);
    }
    
    if(completed)
        Manager::getInstance()->getInstance()->getSoundsLibrary()->playSound(SOUND_QUEST_DONE);
    else
        Manager::getInstance()->getInstance()->getSoundsLibrary()->playSound(SOUND_QUEST_PROGRESS);
    
}

void HUDLayer::blinkFullScreen(int color)
{
    switch (color)
    {
        default:
        case 0:
            blinkFullScreenWithSprite(_whiteBlinkSprite);
            break;
        case 1:
            blinkFullScreenWithSprite(_redBlinkSprite);
            break;
    }
}



/// ASSET ///////////////////////////////////////////////////////////////////////////////////////


cocos2d::Sprite* HUDLayer::getBonesSprite()
{
    for (auto sprite : _boneSprites)
    {
        if (sprite->getOpacity() == 0)
            return sprite;
    }
    
    auto sprite = Sprite::createWithSpriteFrameName("ui-hud-bones-gray.png");
    this->addChild(sprite);
    _boneSprites.push_back(sprite);
    return sprite;
}


cocos2d::Sprite* HUDLayer::getQuestSprite(bool daily, int i)
{
    
    
    
    std::vector<cocos2d::Sprite*> questSpritesLibrary;
    std::string iconChar;
    
    if(daily)
        questSpritesLibrary = _questDailySprites;
    else
    {
        switch(i)
        {
            default:
            case 0:
                questSpritesLibrary = _questSpritesA;
                iconChar = "-a";
                break;
            case 1:
                questSpritesLibrary = _questSpritesB;
                iconChar = "-b";
                break;
            case 2:
                questSpritesLibrary = _questSpritesC;
                iconChar = "-c";
                break;
        }
    }
    
    
    for (auto questSprite : questSpritesLibrary)
    {
        if (questSprite->getOpacity() == 0)
            return questSprite;
    }
    
    auto sprite = Sprite::createWithSpriteFrameName((daily) ? "ui-hud-quest-daily.png" : "ui-hud-quest"+ iconChar +".png");
    this->addChild(sprite);

    
    if(daily)
        _questDailySprites.push_back(sprite);
    else
    {
        switch(i)
        {
            default:
            case 0:
                _questSpritesA.push_back(sprite);
                break;
            case 1:
                _questSpritesB.push_back(sprite);
                break;
            case 2:
                _questSpritesC.push_back(sprite);
                break;
        }
    }

    return sprite;
}



cocos2d::Sprite* HUDLayer::getScoreSprite()
{
	for (auto scoreSprite : _scoreSprites)
	{
		if (scoreSprite->getOpacity() == 0)
			return scoreSprite;
	}

	auto sprite = Sprite::createWithSpriteFrameName("ui-hud-puppylove.png");
	this->addChild(sprite);
	_scoreSprites.push_back(sprite);
	return sprite;
}


cocos2d::Label* HUDLayer::getScoreText(std::string text)
{
    for(auto scoreText: _scoreTexts)
    {
        if(!scoreText->isVisible())
        {
            scoreText->setString(text);
            return scoreText;
        }
    }
    
    auto scoreText = Label::createWithTTF(text, kFontPath + kFontNameStyle, 24);
//    scoreText->setColor(Color3B::WHITE);
    scoreText->setColor(Color3B{252, 249, 206});
    scoreText->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    this->addChild(scoreText);
    _scoreTexts.push_back(scoreText);
    
    return scoreText;
}

float HUDLayer::randFloat(int min, int max)
{
    int delta  = max - min;
    float rand = (std::rand() % delta + min) / 10.0f;
    return rand;
}

void HUDLayer::scoreText(int score, int burst, cocos2d::Vec2 position)
{
    
    burst = burst + 1;
    
    int randomDirection = ((rand() % 2) == 1) ? 1 : -1;
    
    // var ielikt arī random MoveBy, lai nav vienādi
	
	auto moveSpriteUp = JumpBy::create(1.0f, Vec2(randFloat(0, 50), randFloat(150, 200)), 8, 1);
	auto moveSpriteUpLeft = JumpBy::create(1.0f, Vec2(-randFloat(70, 120), randFloat(100, 150)), 8, 1);
	auto moveSpriteUpRight = JumpBy::create(1.0f, Vec2(randFloat(70, 120), randFloat(100, 150)), 8, 1);
    
	auto moveSpriteUpRandom = JumpBy::create(1.0f, Vec2((randomDirection * randFloat(70, 120)), randFloat(150, 200)), 8, 1);
	auto moveSpriteLeft = JumpBy::create(1.0f, Vec2(-randFloat(150, 250), 5.0f), 8, 1);
	auto moveSpriteRight = JumpBy::create(1.0f, Vec2(randFloat(150, 250), 5.0f), 8, 1);
	auto moveSpriteRandom = JumpBy::create(1.0f, Vec2((randomDirection * randFloat(150, 250)), 5.0f), 8, 1);
    
    
    auto movetoLabel = MoveTo::create(0.5f, Vec2( 0, _visibleSize.height));
    
	auto hideSprite = FadeOut::create(0.3f);

	/*
    auto moveSpriteUp = MoveBy::create(1.0f, Vec3(0.0f,20.0f,0.0f));
    auto moveSpriteUpLeft = MoveBy::create(.7f, Vec3(-10.0f,15.0f,0.0f));
    auto moveSpriteUpRight = MoveBy::create(.7f, Vec3(10.0f,15.0f,0.0f));
    auto moveSpriteUpRandom = MoveBy::create(1.0f, Vec3((randomDirection * 10.0f),20.0f,0.0f));
    auto moveSpriteLeft = MoveBy::create(.7f, Vec3(-20.0f,5.0f,0.0f));
    auto moveSpriteRight = MoveBy::create(.7f, Vec3(20.0f,5.0f,0.0f));
    auto moveSpriteRandom = MoveBy::create(.7f, Vec3((randomDirection * 20.0f),5.0f,0.0f));
    auto hideSprite = Hide::create();
	*/

    
    for (int i = 0; i < burst; i++) {
        
        auto sprite = getScoreSprite();
		sprite->setOpacity(255);
        
        switch(i)
        {
            default:
            case 0:
                sprite->stopAllActions();
                sprite->setPosition(Vec2(position.x, position.y + 15));
				sprite->runAction(Sequence::create(moveSpriteUp->clone(), hideSprite->clone(), NULL));
                break;
            case 1:
                sprite->stopAllActions();
                if(burst == 2 || burst == 4)
                {
                    sprite->setPosition(Vec2(position.x + (15 * randomDirection), position.y + 15));
					sprite->runAction(Sequence::create(moveSpriteUpRandom->clone(), hideSprite->clone(), NULL));
                }
                else
                {
                    sprite->setPosition(Vec2(position.x - 15, position.y + 10));
					sprite->runAction(Sequence::create(moveSpriteUpLeft->clone(), hideSprite->clone(), NULL));
                }
                
                break;
            case 2:
                sprite->stopAllActions();
                if(burst == 4)
                {
                    sprite->setPosition(Vec2(position.x + (15 * randomDirection), position.y + 10));
					sprite->runAction(Sequence::create(moveSpriteRandom->clone(), hideSprite->clone(), NULL));
                }
                else
                {
                    sprite->setPosition(Vec2(position.x + 15, position.y + 10));
					sprite->runAction(Sequence::create(moveSpriteUpRight->clone(), hideSprite->clone(), NULL));
                }
                break;
            case 3:
                sprite->stopAllActions();
                sprite->setPosition(Vec2(position.x - 20, position.y + 5));
				sprite->runAction(Sequence::create(moveSpriteLeft->clone(), hideSprite->clone(), NULL));
                break;
            case 4:
                sprite->stopAllActions();
                sprite->setPosition(Vec2(position.x + 20, position.y + 5));
				sprite->runAction(Sequence::create(moveSpriteRight->clone(), hideSprite->clone(), NULL));
                break;
        }
        
    }
    
    
    
    auto text = getScoreText(intToString(score));
    text->setVisible(true);
    text->setPosition(position);
    
    if(burst==5)
    {
//        text->setColor(Color3B{191, 77, 156});
        text->setColor(Color3B::YELLOW);
    }else{
        text->setColor(Color3B{252, 249, 206});
    }
    
    auto moveText = MoveBy::create(1.0f, Vec3(0.0f,10.0f,0.0f));
    auto hideText = Hide::create();
    auto scoreTextAnimation = Sequence::create(moveText, hideText, NULL);
    
    text->stopAllActions();
    text->runAction(scoreTextAnimation);
}

void HUDLayer::questIcon(bool daily, cocos2d::Vec2 from, cocos2d::Vec2 to, int nr)
{
    auto icon = getQuestSprite(daily, nr);
    icon->setVisible(true);
    icon->setOpacity(180);
    icon->setScale(1.0f);
    icon->setPosition(from.x, from.y);
    
    auto moveIcon = MoveTo::create(.3f, to);
    auto hideIcon = Hide::create();
    auto iconAnimation = Sequence::create(moveIcon, hideIcon, NULL);
    auto scaleAnimation = ScaleTo::create(.3f, 0.8f);

    
    icon->stopAllActions();
    icon->runAction(iconAnimation);
    icon->runAction(scaleAnimation);
}


void HUDLayer::sceneReload()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
    Manager::getInstance()->runScene(SCENE_GAME, _levelPath);
}

void HUDLayer::sceneEnd(float dt)
{
    Manager::getInstance()->setLastGame({_levelPath, _time, _coins, _puppies, _speed});
    
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
    Manager::getInstance()->runScene(SCENE_REWARDS, "results");
}


void HUDLayer::initGlorifyText(std::string text)
{
    auto glory = Label::createWithTTF(text, kFontPath + kFontNameStyle, 40);
    glory->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    glory->setColor(Color3B::WHITE);
    glory->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + 8);
    glory->setVisible(false);
    this->addChild(glory);
    _glorifyTexts.push_back(glory);
}

void HUDLayer::initChallengeText(int challenge, std::string text)
{
    auto challengeText = Label::createWithTTF(text, kFontPath + kFontNameStyle, 24);
    challengeText->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    challengeText->setColor(Color3B{96, 142, 202});
    challengeText->enableOutline(Color4B(0,0,0,105), 1);
    challengeText->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + 96);
    challengeText->setVisible(false);
    this->addChild(challengeText);
    
    
    _challengeTexts.insert(std::pair<int, cocos2d::Label*>(challenge, challengeText));
}

void HUDLayer::initQuestText(std::string text)
{
    _questText = Label::createWithTTF(text, kFontPath + kFontNameStyle, 24);
    _questText->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    //_questText->setColor(Color3B{96, 142, 202});
    _questText->setColor(Color3B::YELLOW);
    _questText->enableOutline(Color4B(0,0,0,105), 1);
    _questText->setAnchorPoint(Vec2(0.0f, 0.5f));
    _questText->setPosition(kPadding, _visibleSize.height / 2 - 45);
    _questText->setVisible(false);
    this->addChild(_questText);
}



void HUDLayer::initPowerUpText(int powerUp, std::string text, bool final)
{
    auto powerUpLabel = Label::createWithTTF(text, kFontPath + kFontNameStyle, 24);
    powerUpLabel->setAnchorPoint(Vec2(1.0f,0.5f));
    powerUpLabel->setColor(Color3B{217, 28, 92});
    powerUpLabel->enableOutline(Color4B(0,0,0,105), 1);
    powerUpLabel->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    powerUpLabel->setPosition(_visibleSize.width - kPadding, _visibleSize.height / 2 - 45);
    powerUpLabel->setVisible(false);
    this->addChild(powerUpLabel);
    
    if(final)
        _powerUpsEnd.insert(std::pair<int, cocos2d::Label*>(powerUp, powerUpLabel));
    else
        _powerUpsStart.insert(std::pair<int, cocos2d::Label*>(powerUp, powerUpLabel));
    
}

void HUDLayer::initHUDElements()
{
    
    Size rectSize{100,24};
    
    _coinsBack = Node::create();
    
    auto rectNode = DrawNode::create();
    Vec2 rectangle[4];
    rectangle[0] = Vec2(0, 0);
    rectangle[1] = Vec2(rectSize.width, 0);
    rectangle[2] = Vec2(rectSize.width, rectSize.height);
    rectangle[3] = Vec2(0, rectSize.height);
    rectNode->drawPolygon(rectangle, 4, Color4F(0,0,0,0.2f), 0, Color4F::RED);
    _coinsBack->addChild(rectNode);
    _coinsBack->setAnchorPoint(Vec2(0.0f, 0.5f));
    _coinsBack->setPosition(kPadding + 32, _visibleSize.height - kPadding - 28 );
    this->addChild(_coinsBack);
    
    
    
    auto coinsIconBack = Sprite::createWithSpriteFrameName("ui-hud-coins-back.png");
    coinsIconBack->setAnchorPoint(Vec2(1.0f, .5f));
    coinsIconBack->setPosition(kPadding + 32, _visibleSize.height - kPadding - 15.5);
    this->addChild(coinsIconBack, zIndexHUD);
    
    
    
    _coinsIcon = Sprite::createWithSpriteFrameName("ui-hud-coins.png");
    _coinsIcon->setAnchorPoint(Vec2(.5f, .5f));
    _coinsIcon->setPosition(kPadding + 15, _visibleSize.height - kPadding - 15.5);
    this->addChild(_coinsIcon, zIndexHUD);
    
    _coinsLabel = Label::createWithTTF("0", kFontPath + kFontNameStyle, 26);
    _coinsLabel->setAnchorPoint(Vec2(0.0f, .5f));
    _coinsLabel->setColor(Color3B{252, 249, 206});
    _coinsLabel->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _coinsLabel->setPosition(kPadding + 35, _visibleSize.height - kPadding - 17);
    this->addChild(_coinsLabel, zIndexHUD);
    
    _coinsBack->setScaleX((_coinsLabel->getContentSize().width + 10) / 100);
    
    
    auto timeIconBack = Sprite::createWithSpriteFrameName("ui-hud-time-back.png");
    timeIconBack->setAnchorPoint(Vec2(.5f, .5f));
    timeIconBack->setPosition(_visibleSize.width / 2, _visibleSize.height - kPadding - 15);
    this->addChild(timeIconBack, zIndexHUD);
    
    _timeIcon = Sprite::createWithSpriteFrameName("ui-hud-time.png");
    _timeIcon->setAnchorPoint(Vec2(.5f, .5f));
    _timeIcon->setPosition(_visibleSize.width / 2, _visibleSize.height - kPadding - 15);
    this->addChild(_timeIcon, zIndexHUD);
    
    
    _timePrev = Manager::getInstance()->getStage(_levelPath)->getLongestTime();
    _timeLabelPrev = Label::createWithTTF(floatToTimeString(_timePrev), kFontPath + kFontNameStyle, 24);
    _timeLabelPrev->setAnchorPoint(Vec2(1.0f, 0.5f));
    _timeLabelPrev->setColor(Color3B::WHITE);
    _timeLabelPrev->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _timeLabelPrev->setOpacity(150);
    _timeLabelPrev->setPosition(_visibleSize.width / 2 - 22, _visibleSize.height- kPadding - 16);
    this->addChild(_timeLabelPrev, zIndexHUD);
    

    _timeLabel = Label::createWithTTF("00:00.0", kFontPath + kFontNameStyle, 24);
    _timeLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
    _timeLabel->setColor(Color3B::WHITE);
    _timeLabel->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _timeLabel->setPosition(_visibleSize.width / 2 + 22, _visibleSize.height- kPadding - 16);
    this->addChild(_timeLabel, zIndexHUD);
    
    
    

     
     rectSize = Size{24,25};
     Rect rect = Rect{ _visibleSize.width - kPadding - 124, _visibleSize.height - kPadding - 3 - rectSize.height, rectSize.width, rectSize.height };
    
    
     auto rectNode3 = DrawNode::create();
     Vec2 rectangle3[4];
     rectangle3[0] = Vec2(rect.origin.x, rect.origin.y);
     rectangle3[1] = Vec2(rect.origin.x + rect.size.width, rect.origin.y);
     rectangle3[2] = Vec2(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height);
     rectangle3[3] = Vec2(rect.origin.x, rect.origin.y + rect.size.height);
     rectNode3->drawPolygon(rectangle3, 4, Color4F(0,0,0,0.2f), 0, Color4F::RED);
     this->addChild(rectNode3);
     
    
    auto speedIconBack = Sprite::createWithSpriteFrameName("ui-hud-speed-back.png");
    speedIconBack->setAnchorPoint(Vec2(.5f, .5f));
    speedIconBack->setPosition(_visibleSize.width - kPadding - 85, _visibleSize.height - kPadding - 16);
    this->addChild(speedIconBack, zIndexHUD);
    
    _speedIcon = Sprite::createWithSpriteFrameName("ui-hud-speed.png");
    _speedIcon->setAnchorPoint(Vec2(.5f, .5f));
    _speedIcon->setPosition(_visibleSize.width - kPadding - 85, _visibleSize.height - kPadding - 16);
    this->addChild(_speedIcon, zIndexHUD);
    
    _speedLabel = Label::createWithTTF(intToString(_speed), kFontPath + kFontNameStyle, 26);
    _speedLabel->setAnchorPoint(Vec2(.5f, .5f));
    _speedLabel->setColor(Color3B::WHITE);
    _speedLabel->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _speedLabel->setPosition(_visibleSize.width - 119, _visibleSize.height - kPadding - 17);
    this->addChild(_speedLabel, zIndexHUD);
    
    if(!Manager::getInstance()->getSlotHandler()->isProductEquiped("p_talisman_dropchance"))
    {
        _speedIcon->setVisible(false); // just an idea
        _speedLabel->setVisible(false);
        rectNode3->setVisible(false);
        speedIconBack->setVisible(false);
    }
    
    
    rectSize = Size{23,25};
    rect = Rect{ _visibleSize.width - kPadding - 63, _visibleSize.height - kPadding - 3 - rectSize.height, rectSize.width, rectSize.height };
    
    auto rectNode2 = DrawNode::create();
    Vec2 rectangle2[4];
    rectangle2[0] = Vec2(rect.origin.x, rect.origin.y);
    rectangle2[1] = Vec2(rect.origin.x + rect.size.width, rect.origin.y);
    rectangle2[2] = Vec2(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height);
    rectangle2[3] = Vec2(rect.origin.x, rect.origin.y + rect.size.height);
    rectNode2->drawPolygon(rectangle2, 4, Color4F(0,0,0,0.2f), 0, Color4F::RED);
    this->addChild(rectNode2);
    
    
    auto livesIconBack = Sprite::createWithSpriteFrameName("ui-hud-lives-back.png");
    livesIconBack->setAnchorPoint(Vec2(.0f, .5f));
    livesIconBack->setPosition(_visibleSize.width - kPadding - 40, _visibleSize.height - kPadding - 16);
    this->addChild(livesIconBack, zIndexHUD);
    
    _livesIcon = Sprite::createWithSpriteFrameName("ui-hud-lives.png");
    _livesIcon->setAnchorPoint(Vec2(.4f, .5f));
    _livesIcon->setPosition(_visibleSize.width - kPadding - 23, _visibleSize.height - kPadding - 16);
    this->addChild(_livesIcon, zIndexHUD);
    
    _livesLabel = Label::createWithTTF(intToString(_goalLives), kFontPath + kFontNameStyle, 26);
    _livesLabel->setAnchorPoint(Vec2(.5f, .5f));
    _livesLabel->setColor(Color3B::WHITE);
    _livesLabel->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _livesLabel->setPosition(_visibleSize.width - 58, _visibleSize.height - kPadding - 17);
    this->addChild(_livesLabel, zIndexHUD);
    
}



void HUDLayer::updateGameText(float dt)
{
    _gameStartCD--;
    std::string txt = "";
    if(_gameStartCD == 0)
    {
        std::vector<std::string> startTxt;
        startTxt.push_back("BE AWESOME!");
        startTxt.push_back("GO!");
        startTxt.push_back("DO YOUR BEST!");
        int max_rand = startTxt.size() - 1;
        int r = random(0, max_rand);
        txt = startTxt.at(r);
        
        this->toogleGameStart();
    }
    else if(_gameStartCD <0)
    {
        this->unschedule(schedule_selector(HUDLayer::updateGameText));
        _gameText->setVisible(false);
    }
    else{
        txt = intToString(_gameStartCD);
    }
    
    //[GATIM] animation required;
    _gameText->setString(txt);
}



void HUDLayer::initGameStart()
{
    
    _gameText = Label::createWithTTF(intToString(_gameStartCD), kFontPath + kFontNameStyle, 82);
    _gameText->setAnchorPoint(Vec2(0.5f, 0.5f));
    _gameText->setColor(Color3B{252, 208, 74});
    _gameText->enableOutline(Color4B(0,0,0,55), 3);
//    _gameText->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _gameText->setPosition(_visibleSize.width / 2, _visibleSize.height / 2);
    this->addChild(_gameText, zIndexHUD);
    
    this->schedule(schedule_selector(HUDLayer::updateGameText),.7f); // šo pārnest uz gameLayer
}

void HUDLayer::initPauseAndQuests()
{
    _pause = Node::create();
    
    int questCount = 0;
    auto quests = Manager::getInstance()->getQuests()->getActiveGeneral(true);
    questCount += (int)quests.size();
    
    std::vector<Quest* > dailyQuests;
    if(Manager::getInstance()->getQuests()->hasDailyQuest() && Manager::getInstance()->getUser()->getUserLevel() > 1)
        dailyQuests = Manager::getInstance()->getQuests()->getActiveDaily();
    
    questCount += (int)dailyQuests.size();
    
    float margin = 1.0f;
    float iconMargin = 1.0f;
    float wH = -10;

    Size questFrameSize(260, 26);
    Size backSize(questFrameSize.width + 4, ((questFrameSize.height + margin) * questCount + margin + 10));
    Size backLineSize(backSize.width + 4, 4);
    Size labelSize = Size(69, 12);
    
    /*

    Sprite* fxCircle = Sprite::createWithSpriteFrameName("fx-wheel.png");
    fxCircle->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - wH);
    fxCircle->setOpacity(150);
    _pause->addChild(fxCircle);
    
    auto rotateCircle = RotateBy::create(1.0f, 90.0f);
    fxCircle->runAction(RepeatForever::create(rotateCircle));
    */
    
    Label* title = Label::createWithTTF("PAUSE", kFontPath + kFontNameStyle, 60);
    title->setAnchorPoint(Vec2(0.5f, 0.5f));
    title->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    title->setColor(Color3B{252, 208, 74});
    title->setPosition(_visibleSize.width / 2 + 5, _visibleSize.height / 2 + backSize.height / 2 + 40);
    _pause->addChild(title);
    
    Sprite* bones_a = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_a->setPosition(title->getPositionX() - title->getContentSize().width / 2 - 20, title->getPositionY());
    _pause->addChild(bones_a);
    
    Sprite* bones_b = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_b->setPosition(title->getPositionX() + title->getContentSize().width / 2 + 20, title->getPositionY());
    _pause->addChild(bones_b);
    
    Vec2 pRBack[4];
    
    auto backFrame= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(backSize.width, 0);
    pRBack[2] = Vec2(backSize.width, backSize.height);
    pRBack[3] = Vec2(0, backSize.height);
    backFrame->drawPolygon(pRBack, 4, Color4F(kCOLOR_BACK), 0, Color4F::BLACK);
    backFrame->setContentSize(backSize);
    backFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
    backFrame->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - wH);
    _pause->addChild(backFrame);
    
    auto backLine= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(backLineSize.width, 0);
    pRBack[2] = Vec2(backLineSize.width, backLineSize.height);
    pRBack[3] = Vec2(0, backLineSize.height);
    backLine->drawPolygon(pRBack, 4, Color4F(kCOLOR_FRAME), 0, Color4F::BLACK);
    backLine->setContentSize(backLineSize);
    backLine->setAnchorPoint(Vec2(0.5f, 0.5f));
    backLine->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - wH - backSize.height / 2  + backLineSize.height / 2);
    _pause->addChild(backLine);
    
    auto backLine2= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(backLineSize.width, 0);
    pRBack[2] = Vec2(backLineSize.width, backLineSize.height);
    pRBack[3] = Vec2(0, backLineSize.height);
    backLine2->drawPolygon(pRBack, 4, Color4F(kCOLOR_FRAME), 0, Color4F::BLACK);
    backLine2->setContentSize(backLineSize);
    backLine2->setAnchorPoint(Vec2(0.5f, 0.5f));
    backLine2->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 - backLineSize.height / 2);
    _pause->addChild(backLine2);
    
    auto backLabel= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(labelSize.width, 0);
    pRBack[2] = Vec2(labelSize.width, labelSize.height);
    pRBack[3] = Vec2(0, labelSize.height);
    backLabel->drawPolygon(pRBack, 4, Color4F(kCOLOR_FRAME), 0, Color4F::BLACK);
    backLabel->setContentSize(labelSize);
    backLabel->setAnchorPoint(Vec2(0.5f, 0.5f));
    backLabel->setPosition(_visibleSize.width / 2 - backLineSize.width / 2 + labelSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 + labelSize.height / 2);
    _pause->addChild(backLabel);
    
    auto questsTxt = Label::createWithTTF("Active Quests", kFontPath + kFontNameStyle, 10);
    questsTxt->setColor(Color3B::WHITE);
    questsTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    questsTxt->setPosition(Vec2(backLabel->getPositionX(), backLabel->getPositionY() - 2));
    _pause->addChild(questsTxt);
    
    
    bool putInCenter = (int)_powerUps.size() > 1 ? false : true;
    
    int i = 0;
    Vec2 questPos, questIconPos;
    float delta;
    for(auto quest: quests)
    {
        questPos = Vec2(_visibleSize.width / 2 - questFrameSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 - questFrameSize.height - 1 - margin - backLineSize.height - (i * (questFrameSize.height + margin)));
        Node* questNode = createQuest(quest, questFrameSize, false, i);
        questNode->setPosition(questPos);
        _pause->addChild(questNode);
        
        
        Node* questIcon = createQuestIcon(quest, false, i);
        float questIconWidth = questIcon->getContentSize().width + iconMargin;
        if(putInCenter)
            delta = _visibleSize.width / 2 + (questIconWidth * i) - ((questCount - 1) * questIconWidth / 2) + iconMargin / 2;
        else
            delta = 93 + (questIconWidth * i);
        questIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
        questIconPos = Vec2(delta, 20);
        questIcon->setPosition(questIconPos);
        this->addChild(questIcon);
        
        
        if(i==0)
            _questText->setPosition(delta, 50);
        
        i++;
    }
    
    for(auto daily: dailyQuests)
    {
        questPos = Vec2(_visibleSize.width / 2 - questFrameSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 - questFrameSize.height - 1 - margin - backLineSize.height - (i * (questFrameSize.height + margin)));
        Node* questNode = createQuest(daily, questFrameSize, true, i);
        questNode->setPosition(questPos);
        _pause->addChild(questNode);
        
        Node* questIcon = createQuestIcon(daily, true, i);
        float questIconWidth = questIcon->getContentSize().width + iconMargin;
        if(putInCenter)
            delta = _visibleSize.width / 2 + (questIconWidth * i) - ((questCount - 1) * questIconWidth / 2) + iconMargin / 2;
        else
            delta = 93 + (questIconWidth * i);
        questIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
        questIconPos = Vec2(delta, 20);
        questIcon->setPosition(questIconPos);
        this->addChild(questIcon);
        
        i++;
    }
    
   /*
     auto txt = Label::createWithTTF("Daily quest available in 21:43", kFontPath + kFontNameStyle, 13);
     //        txt->setColor(Color3B::WHITE);
     txt->enableOutline(Color4B::BLACK, 1);
     //        txt->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
     txt->setColor(Color3B{252, 208, 74});
     txt->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + frameStart - (i * padding));
     _pause->addChild(txt);
     
     }
     */


    float buttonPos = 30;
    Sprite *buttonContinue = Sprite::createWithSpriteFrameName("ui-hud-button-play-0.png");
    Sprite *buttonContinueActive = Sprite::createWithSpriteFrameName("ui-hud-button-play-1.png");
    auto buttonContinueItem = MenuItemSprite::create(buttonContinue, buttonContinueActive, NULL, CC_CALLBACK_0(HUDLayer::tooglePause, this));
    buttonContinueItem->setPosition(_visibleSize.width / 2 - 82, _visibleSize.height / 2 - backSize.height / 2 - wH - buttonPos);
    
    Sprite *buttonSound = Sprite::createWithSpriteFrameName("ui-hud-button-sound-0.png");
    Sprite *buttonSoundActive = Sprite::createWithSpriteFrameName("ui-hud-button-sound-1.png");
    auto buttonSoundItem = MenuItemSprite::create(buttonSound, buttonSoundActive, NULL, CC_CALLBACK_0(HUDLayer::toogleSound, this));
    buttonSoundItem->setPosition(_visibleSize.width / 2 - 27, _visibleSize.height / 2 - backSize.height / 2 - wH - buttonPos);
    
    
    Sprite *buttonReplay = Sprite::createWithSpriteFrameName("ui-hud-button-replay-0.png");
    Sprite *buttonReplayActive = Sprite::createWithSpriteFrameName("ui-hud-button-replay-1.png");
    auto buttonReplayItem = MenuItemSprite::create(buttonReplay, buttonReplayActive, NULL, CC_CALLBACK_0(HUDLayer::sceneReload, this));
    buttonReplayItem->setPosition(_visibleSize.width / 2 + 27, _visibleSize.height / 2 - backSize.height / 2 - wH - buttonPos);
    
    
    Sprite *buttonBack = Sprite::createWithSpriteFrameName("ui-hud-button-exit-0.png");
    Sprite *buttonBackActive = Sprite::createWithSpriteFrameName("ui-hud-button-exit-1.png");
    auto buttonBackItem = MenuItemSprite::create(buttonBack, buttonBackActive, NULL, CC_CALLBACK_0(HUDLayer::sceneEnd, this, 0.0f));
    buttonBackItem->setPosition(_visibleSize.width / 2 + 82, _visibleSize.height / 2  - backSize.height / 2 - wH - buttonPos);
    
    
    auto menu = Menu::create(buttonContinueItem, buttonReplayItem, buttonBackItem, buttonSoundItem, NULL);
    menu->setPosition(_originPoint.x, _originPoint.y);
    _pause->addChild(menu);
    _pause->setVisible(false);
    
    this->addChild(_pause, zIndexPopUpElements);
    
    _pauseSoundRedline = Sprite::createWithSpriteFrameName("ui-hud-button-sound-redline.png");
    _pauseSoundRedline->setAnchorPoint(Vec2(.5f, .5f));
    _pauseSoundRedline->setPosition(_visibleSize.width / 2 - 31, _visibleSize.height / 2 - backSize.height / 2 - wH - buttonPos);
    _pauseSoundRedline->setVisible(false);
    this->addChild(_pauseSoundRedline, zIndexPopUpElements);
}

cocos2d::Node* HUDLayer::createQuestIcon(Quest* quest, bool daily, int nr)
{
    Node* node = Node::create();
    
    Sprite* questBack = Sprite::createWithSpriteFrameName("ui-hud-questicon-perc.png");
    questBack->setAnchorPoint(Vec2(.0f, .0f));
    questBack->setPosition(0, 0);
    node->addChild(questBack);
    
    Sprite* questPerc = Sprite::createWithSpriteFrameName("ui-hud-questicon-perc.png");
    questPerc->setAnchorPoint(Vec2(.5f, .5f));
    questPerc->setPosition(questBack->getContentSize().width / 2, questBack->getContentSize().height / 2);
    questPerc->setScale(0.9f);
    node->addChild(questPerc);
    
    
    
    
    std::string colorSpriteStr = (!daily) ? "ui-hud-questicon-general.png" : "ui-hud-questicon-daily.png";
    float percWidth = quest->getAmountNow() * 100 / quest->getAmountGoal();
    Sprite* questProgressBar = Sprite::createWithSpriteFrameName(colorSpriteStr);
    ProgressTimer* pXP = ProgressTimer::create(questProgressBar);
    pXP->setType(ProgressTimer::Type::RADIAL);
    pXP->setPercentage(percWidth);
    pXP->setTag(kTAG_QUEST_PERCENTAGE);
    pXP->setAnchorPoint(Vec2(.5f, .5f));
    pXP->setScale(0.9f);
    pXP->setPosition(questBack->getContentSize().width / 2, questBack->getContentSize().height / 2);
    node->addChild(pXP);
    
    Sprite* questCompleted = Sprite::createWithSpriteFrameName("ui-hud-questicon-completed.png");
    questCompleted->setAnchorPoint(Vec2(.5f, .5f));
    questCompleted->setPosition(questBack->getContentSize().width / 2, questBack->getContentSize().height / 2);
    questCompleted->setTag(kTAG_QUEST_COMPLETED);
    questCompleted->setScale(0.9f);
    questCompleted->setVisible(false);
    node->addChild(questCompleted);
    
    std::string iconChar;
    if(nr == 0)
        iconChar = "-a";
    else if(nr == 1)
        iconChar = "-b";
    else if(nr == 2)
        iconChar = "-c";
    
    
    Sprite* questIconSprite = Sprite::createWithSpriteFrameName((daily) ? "ui-hud-quest-daily.png" : "ui-hud-quest"+ iconChar +".png");
    questIconSprite->setAnchorPoint(Vec2(.5f, .5f));
    questIconSprite->setPosition(questBack->getContentSize().width / 2, questBack->getContentSize().height / 2);
    questIconSprite->setScale(0.9f);
    node->addChild(questIconSprite);
    
    
    
    
    node->setContentSize(questBack->getContentSize());
    _questsIcon.insert(std::pair<std::string, Node*>(quest->getTag(), node));
    _questsIconBar.insert(std::pair<std::string, ProgressTimer*>(quest->getTag(), pXP));
    return node;
}

cocos2d::Node* HUDLayer::createQuest(Quest* quest, cocos2d::Size frameSize, bool daily, int nr)
{
    Node* node = Node::create();
    
    float padding = 5.0f;
    float widthIcon = 20.0f + padding + 2;
    float widthReward = 45.0f + padding;
    
    auto drawNode = DrawNode::create();
    Vec2 rectangle[4];
    rectangle[0] = Vec2(0, 0);
    rectangle[1] = Vec2(frameSize.width, 0);
    rectangle[2] = Vec2(frameSize.width, frameSize.height);
    rectangle[3] = Vec2(0, frameSize.height);
    drawNode->drawPolygon(rectangle, 4, Color4F(Color4B(100,100,100,255)), 0, Color4F::BLACK);
    drawNode->setContentSize(frameSize);
    node->addChild(drawNode);
    
    auto color = Color4F(Color4B(96, 142, 202, 255));//Color4F{96, 142, 202, 100};
    if(daily)
        color = Color4F(Color4B{217, 28, 92, 255});
    
    float percWidth = quest->getAmountNow() * 100 / quest->getAmountGoal();
    
    
    auto percNode = Node::create();
    
    auto percDrawNode = DrawNode::create();
    Vec2 percRectangle[4];
    percRectangle[0] = Vec2(0, 0);
    percRectangle[1] = Vec2(frameSize.width, 0);
    percRectangle[2] = Vec2(frameSize.width, frameSize.height);
    percRectangle[3] = Vec2(0, frameSize.height);
    percDrawNode->drawPolygon(percRectangle, 4, color, 0, Color4F::BLACK);
    
    percNode->setScaleX(percWidth / 100);
    percNode->setTag(kTAG_QUEST_PERCENTAGE);
    percNode->addChild(percDrawNode);
    
    node->addChild(percNode);
    
    
    auto completeNode = Node::create();
    
    auto completeDrawNode = DrawNode::create();
    percRectangle[0] = Vec2(0, 0);
    percRectangle[1] = Vec2(frameSize.width, 0);
    percRectangle[2] = Vec2(frameSize.width, frameSize.height);
    percRectangle[3] = Vec2(0, frameSize.height);
    completeDrawNode->drawPolygon(percRectangle, 4, Color4F(Color4B(250, 162, 27, 255)), 0, Color4F::BLACK);
    
    completeNode->setTag(kTAG_QUEST_COMPLETED);
    completeNode->addChild(completeDrawNode);
    
    node->addChild(completeNode);
    
    if(!quest->isCompleted())
        completeNode->setVisible(false);
    
    
    
    std::string iconChar;
    if(nr == 0)
        iconChar = "-a";
    else if(nr == 1)
        iconChar = "-b";
    else if(nr == 2)
        iconChar = "-c";
    
    auto iconSprite = Sprite::createWithSpriteFrameName((daily) ? "ui-hud-quest-daily.png" : "ui-hud-quest"+ iconChar +".png");
    iconSprite->setAnchorPoint(Vec2(.5f, .5f));
    iconSprite->setScale(0.9f);
    iconSprite->setPosition( widthIcon / 2 , frameSize.height / 2);
    node->addChild(iconSprite);
    
    //    int strLimit = 24;
    float txtSize = frameSize.width - widthReward - widthIcon;
    std::string titleStr = quest->getTitle();
    //    CCLOG("STRING %d", (int)titleStr.length());
    Label* titleTxt = Label::createWithTTF(titleStr, kFontPath + kFontNameStyle, 12);
    titleTxt->setColor(Color3B::WHITE);
    titleTxt->enableShadow(Color4B(0,0,0,205),Size(0.4,-0.4),0);
    titleTxt->setAnchorPoint(Vec2(0.0f, .5f));
    // if((int)titleStr.length() > strLimit)
    {
        titleTxt->setDimensions(txtSize, frameSize.height);
        titleTxt->setLineHeight(20.0f);
        titleTxt->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        titleTxt->setContentSize(Size(txtSize, frameSize.height - padding));
        titleTxt->setPosition(widthIcon, frameSize.height / 2);
    }
    //else
    {
        //   titleTxt->setPosition(widthIcon, frameSize.height / 2);
    }
    node->addChild(titleTxt);
    
    
    if(quest->getRewardCoins() > 0)
    {
        auto coinsSprite = Sprite::createWithSpriteFrameName("ui-diamonds.png");
        coinsSprite->setAnchorPoint(Vec2(1.0f, .5f));
        coinsSprite->setPosition(frameSize.width - padding, frameSize.height / 2);
        node->addChild(coinsSprite);
        
        auto coinsTxt = Label::createWithTTF(quest->getRewardCoinsStr(), kFontPath + kFontNameStyle, 16);
        coinsTxt->setColor(Color3B::WHITE);
        coinsTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        coinsTxt->setAnchorPoint(Vec2(1.0f, .5f));
        coinsTxt->setPosition(coinsSprite->getPositionX() - coinsSprite->getContentSize().width - 3, frameSize.height / 2);
        node->addChild(coinsTxt);
    }
    else if(quest->getRewardXP() > 0)
    {
        auto xpSprite = Sprite::createWithSpriteFrameName("ui-hud-xp.png");
        xpSprite->setAnchorPoint(Vec2(1.0f, .5f));
        xpSprite->setPosition(frameSize.width - padding, frameSize.height / 2);
        node->addChild(xpSprite);
        
        auto xpTxt = Label::createWithTTF(quest->getRewardXPStr(), kFontPath + kFontNameStyle, 16);
        xpTxt->setColor(Color3B{252, 208, 74});
        xpTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        xpTxt->setAnchorPoint(Vec2(1.0f, .5f));
        xpTxt->setPosition(xpSprite->getPositionX() - xpSprite->getContentSize().width - 3, frameSize.height / 2);
        node->addChild(xpTxt);
    }
    
    
    
    
    
    
    node->setTag(nr);
    node->setContentSize(frameSize);
    _quests.insert(std::pair<std::string, Node*>(quest->getTag(), node));

    return node;
}

void HUDLayer::blinkFullScreenWithSprite(cocos2d::Sprite* _blinkSprite)
{
    CCLOG("blink");
    auto fadeOut = FadeOut::create(0.03f);
    auto fadeIn = FadeIn::create(0.03f);
    auto blinkAction = Sequence::create(fadeIn, fadeOut, NULL);
    
    _blinkSprite->stopAllActions();
    _blinkSprite->runAction(blinkAction);
}

std::string HUDLayer::intToString(const int nr)
{
    char charText[100];
    sprintf(charText, "%d", nr);
    std::string str(charText);
    return str;
}
std::string HUDLayer::intToTimeString(const int nr)
{
    int min = nr / 60;
    int sec = nr % 60;
    
    char charText[100];
    sprintf(charText, "%01i:%02i", min, sec);
    std::string str(charText);
    return str;
}


std::string HUDLayer::floatToTimeString(const float nr)
{
    int min = (int)nr / 60;
    int sec = (int)nr % 60;
    int milli = (nr-(long)nr) * 10;
    
    char charText[100];
    sprintf(charText, "%02i:%02i.%01i", min, sec, milli);
    std::string str(charText);
    return str;
}

void HUDLayer::blur()
{
   /*
    
	//Screen Capture
	_screenShot = RenderTexture::create(_visibleSize.width, _visibleSize.height, cocos2d::Texture2D::PixelFormat::RGBA8888);
	_screenShot->setPosition(_visibleSize.width / 2, _visibleSize.height / 2);
	_screenShot->retain();
	_screenShot->begin();
	this->getParent()->visit();
	_screenShot->end();

	//Screenshot to Sprite
	Sprite* _blur = Sprite::createWithTexture(_screenShot->getSprite()->getTexture());
	_blur->setFlipY(true);
	_blur->setPosition(_visibleSize.width / 2, _visibleSize.height / 2);

	//Sprite Bluring
	GLchar * fragSource = (GLchar*)String::createWithContentsOfFile(FileUtils::getInstance()->fullPathForFilename(kShaderPath + "Blur.fsh").c_str())->getCString();
	auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, fragSource);
	program->link();
	program->updateUniforms();
	auto glProgramState = GLProgramState::getOrCreateWithGLProgram(program);
	setGLProgramState(glProgramState);
	auto size = _blur->getTexture()->getContentSizeInPixels();
	getGLProgramState()->setUniformVec2("resolution", size);
	getGLProgramState()->setUniformFloat("blurRadius", 10.0f);
	getGLProgramState()->setUniformFloat("sampleNum", 10.0f);
	getGLProgramState()->applyUniforms();
	_blur->setShaderProgram(program);
	_blur->setTag(kBlurTag);

	this->addChild(_blur, zIndexBlur); //*/
}



cocos2d::MenuItemSprite* HUDLayer::createPowerUpMenuItem(int powerUp)
{
    Sprite *button = Sprite::createWithSpriteFrameName("ui-hud-powerup-offline.png");
    auto item = MenuItemSprite::create(button, button, NULL, CC_CALLBACK_0(HUDLayer::tooglePowerUp, this, powerUp));
    item->setAnchorPoint(Vec2(0.5f, 0.0f));
    return item;
}


void HUDLayer::initPowerUpButtons()
{
    auto powerUps = Manager::getInstance()->getSlotHandler()->getAvailablePowerUps();
    int count = powerUps.size();
    
    int i = 0;
    float w = 62;
    
    MenuItemSprite* powerUpItem1 = NULL;
    MenuItemSprite* powerUpItem2 = NULL;
    MenuItemSprite* powerUpItem3 = NULL;
    
    for (auto powerup : powerUps)
    {
        i++;
        
        switch(i)
        {
            case 1:
                powerUpItem1 = createPowerUpMenuItem(powerup);
                powerUpItem1->setPosition(Vec2(_visibleSize.width - kPadding - 30 - ((count - i) * w), kPadding- 2));
                break;
            case 2:
                powerUpItem2 = createPowerUpMenuItem(powerup);
                powerUpItem2->setPosition(Vec2(_visibleSize.width - kPadding - 30 - ((count - i) * w), kPadding- 2));
                break;
            case 3:
                powerUpItem3 = createPowerUpMenuItem(powerup);
                powerUpItem3->setPosition(Vec2(_visibleSize.width - kPadding - 30 - ((count - i) * w), kPadding- 2));
                break;
        }
        
        
        
        auto node = PowerUpSprite::create(powerup);
        node->setPosition(Vec2(_visibleSize.width - kPadding - 30 - ((count - i) * w), kPadding - 2));
        node->setAnchorPoint(Vec2(0.5f, 0.0f));
        node->setTag(kHUDButtonTag);
        node->setName(intToString(powerup));
        node->setVisible(false);
        this->addChild(node, 3);
        _powerUps.insert(std::pair<int, PowerUpSprite*>(powerup, node));
        
    }
    i++;

    _powerUpMenu = Menu::create(powerUpItem1,powerUpItem2, powerUpItem3, NULL);

    
    _powerUpMenu->setPosition(_originPoint.x, _originPoint.y);
    _powerUpMenu->setVisible(false);
    this->addChild(_powerUpMenu, 2);

}

void HUDLayer::showPowerUpButtons()
{
    for (std::map<int, PowerUpSprite*>::iterator it = _powerUps.begin(); it != _powerUps.end(); ++it)
    {
        it->second->setVisible(true);
    }
    _powerUpMenu->setVisible(true);
}

void HUDLayer::hidePowerUpButtons()
{
    for (std::map<int, PowerUpSprite*>::iterator it = _powerUps.begin(); it != _powerUps.end(); ++it)
    {
        it->second->setVisible(false);
    }
    _powerUpMenu->setVisible(false);

}


void HUDLayer::runCoolDown(int powerUp, float time, float timeTotal, bool active)
{    
    if(active)
        _powerUps.at(powerUp)->updateCoolDown(time, timeTotal);
    else
        _powerUps.at(powerUp)->updateDuration(time, timeTotal);
    
}

void HUDLayer::endCoolDown(int powerUp)
{
    _powerUps.at(powerUp)->ready();
}

void HUDLayer::runPowerUpText(int powerUp, bool active)
{
    Label* label = NULL;
    
    if(powerUp == POWERUP_CLEAR)
    {
        label = _powerUpsStart.at(POWERUP_CLEAR);
    }
    else{
        if(active)
            label = _powerUpsStart.at(powerUp);
        else
            label = _powerUpsEnd.at(powerUp);
    }

     label->setScale(1.0f);
     label->setVisible(true);
     auto move = ScaleBy::create(1.5f, 1.6f);
     auto hide = Hide::create();
     
     auto sequence = Sequence::create(move, hide, NULL);
     
     label->stopAllActions();
     label->runAction(sequence);
  
}

void HUDLayer::updatePowerUpUses(int powerUp, int uses)
{
    _powerUps.at(powerUp)->updateUses(uses);
}

void HUDLayer::scoreExtra(ExtraType type, cocos2d::Vec2 position, int amount)
{
	//auto text = (type == EXTRA_LIVES) ? "+1 Live" : "-5 Speed";
	std::string text;

	//if (type == EXTRA_SPEED) text = "-" + intToString(amount) + " Speed";
    if (type == EXTRA_SPEED) text = "Speed Down";
	if (type == EXTRA_LIVES) text = "Extra Live";

	auto moveText = MoveBy::create(1.0f, Vec3(0.0f, 10.0f, 0.0f));
	auto hideText = Hide::create();
	auto scoreTextAnimation = Sequence::create(moveText, hideText, NULL);

	for (auto scoreText : _scoreTexts)
	{
		if (!scoreText->isVisible())
		{
			scoreText->setString(text);
			scoreText->setColor(Color3B::YELLOW);
			scoreText->setPosition(position);
			scoreText->setVisible(true);
			scoreText->runAction(scoreTextAnimation);
			return;
		}
	}

	auto scoreText = Label::createWithTTF(text, kFontPath + kFontNameHUD, 18);
	scoreText->setColor(Color3B::YELLOW);
	scoreText->setPosition(position);
	this->addChild(scoreText);
	_scoreTexts.push_back(scoreText);

	scoreText->stopAllActions();
	scoreText->runAction(scoreTextAnimation);
}

void HUDLayer::animateExplosion(cocos2d::Vec2 position)
{
	auto hideEffect = Hide::create();
	auto explode = Sequence::create(Animate::create(_animationExplosion), hideEffect, NULL);
	int x = rand() % 360;

	for (auto explosion : _effectSprites)
	{
		if (!explosion->isVisible())
		{
			explosion->setPosition(Vec2(position.x, position.y - 12));
			explosion->setRotation(x);
			explosion->setVisible(true);
			explosion->runAction(explode);
			return;
		}
	}

	auto explosion = Sprite::createWithSpriteFrameName("explosion-0.png");
	explosion->setPosition(Vec2(position.x, position.y - 12));
	explosion->setRotation(x);
	this->addChild(explosion, zIndexExplosions);
	_effectSprites.push_back(explosion);

	explosion->stopAllActions();
	explosion->runAction(explode);
}

void HUDLayer::initAnimations()
{
	Vector<SpriteFrame*> animationVector(7);
	SpriteFrame* animFrame;
	Vec2 offset = Vec2(0, 0);
	char str[100] = { 0 };
		for (int i = 0; i < 7; i++){
			sprintf(str, "explosion-%i.png", i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
		_animationExplosion = Animation::createWithSpriteFrames(animationVector, 0.1f);
		_animationExplosion->retain();
}