#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/Extras.h"
#include "Manager/Manager.h"

#else
#include "Extras.h"
#include "Manager.h"

#endif
////////////////////////////


USING_NS_CC;


Extra::Extra(WorldContract *game, cocos2d::Vec2 position) : b2Sprite(game)
{
	_startPosition = position;
	_inPlay = true;
	_grounded = false;
	_basketHit = false;
}

Extra::~Extra()
{
	CC_SAFE_RELEASE(_extraLives);
	CC_SAFE_RELEASE(_extraSpeed);
}

Extra* Extra::create(WorldContract *game, cocos2d::Vec2 position)
{
	Extra * sprite = new Extra(game, position);
	if (sprite) {
		sprite->initExtra();
		sprite->autorelease();

        if(Manager::getInstance()->getDebugTools())
        sprite->setOpacity(50);
		
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Extra::update(float dt, float ratio) {
	if (_body && isVisible()) {
		//setPositionX(_body->GetPosition().x * RATIO);
		//setPositionY(_body->GetPosition().y * RATIO);
		const float moveRatio = 1.f - ratio;
		getState().currentPosition = ratio * _body->GetPosition() + moveRatio * getState().previousPosition;
		setCurrentPosition(ratio * _body->GetPosition() + moveRatio * getState().previousPosition);
		setPositionX(getState().currentPosition.x * RATIO);
		setPositionY(getState().currentPosition.y * RATIO);
	}
}

void Extra::initExtra()
{
	cocos2d::SpriteFrame *frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("extra-lives-0.png");
	this->initWithSpriteFrame(frame);

	initPhysics();
	initAnimations();	

	this->setSpritePosition(_startPosition);

	//Interpolation stuff
	this->createState(_body);
}

void Extra::initPhysics()
{
	b2BodyDef BodyDef;
	BodyDef.type = b2_dynamicBody;
	BodyDef.fixedRotation = true;
	_body = _game->getWorld()->CreateBody(&BodyDef);

	b2CircleShape circle;
	circle.m_radius = this->getContentSize().width / 2 / RATIO;

	b2FixtureDef FixtureDef;
	FixtureDef.shape = &circle;
	FixtureDef.density = 1.0f;
	FixtureDef.friction = 0.0f;
	FixtureDef.isSensor = true;
	FixtureDef.filter.categoryBits = B2_CATEGORY_EXTRA;
	FixtureDef.filter.maskBits = B2_CATEGORY_BASKET | B2_CATEGORY_GROUND;
	FixtureDef.userData = (void*)B2_CONTACT_EXTRA;
	_body->CreateFixture(&FixtureDef);
	_body->SetUserData(this);
	_body->SetLinearDamping(AIR_FRICTION_RATIO);
}

void Extra::initAnimations()
{
	_extraLives = createAnimation("drop-lives.png", 1, 0.1f);
	_extraLives->retain();

	_extraSpeed = createAnimation("drop-speed.png", 1, 0.1f); //Need new animation
	_extraSpeed->retain();
}

void Extra::setType(ExtraType type)
{
    _type = type;
	
	if (_type == EXTRA_LIVES){
		this->getActionManager()->removeAllActionsFromTarget(this);
		this->runAction(RepeatForever::create(Animate::create(_extraLives)));
	}
	if (_type == EXTRA_SPEED){
		this->getActionManager()->removeAllActionsFromTarget(this);
		this->runAction(RepeatForever::create(Animate::create(_extraSpeed)));
	}
}

ExtraType Extra::getType()
{
    return _type;
}

void Extra::reset()
{
	if (_body) {
		_body->SetLinearVelocity(b2Vec2_zero);
		_body->SetAngularVelocity(0);
	}
	_inPlay = true;
	_body->SetAwake(true);
	_body->SetActive(true);
	setVisible(true);
}

void Extra::rest()
{
	_inPlay = false;
	_grounded = false;
	_basketHit = false;
	this->hide();
	this->setSpritePosition(Vec2(0, 0));
	_body->SetAwake(false);
}


void Extra::basketHit(bool status)
{
	_basketHit = status;
}

bool Extra::basketIsHit()
{
	if (_basketHit){
		_basketHit = false;
		this->rest();
		return true;
	}
	else{
		return false;
	}
}

void Extra::grounded(bool status)
{
	_grounded = status;
}

bool Extra::isGrounded()
{
	if (_grounded){
		_grounded = false;
		return true;
	}
	else{
		return false;
	}
}

void Extra::interpolationReset()
{
	setPreviousPosition(_body->GetPosition());
}