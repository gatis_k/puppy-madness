#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/GameLayer.h"
#include "Game/Extras.h"
#include "Game/Platform.h"
#include "Game/Basket.h"
#include "Game/Puppy.h"
#include "Game/Bobo.h"
#include "Game/Bullet.h"
#include "Game/PowerUp.h"
#include "Game/Snake.h"
#include "Game/GLES-Render.h"
#include "Game/MyContactListener.h"
#include "Manager/Manager.h"
#include "Utils/Jzon.h"

#else
#include "GameLayer.h"
#include "Extras.h"
#include "Platform.h"
#include "Basket.h"
#include "Puppy.h"
#include "Bobo.h"
#include "Bullet.h"
#include "PowerUp.h"
#include "Snake.h"
#include "GLES-Render.h"
#include "MyContactListener.h"
#include "Manager.h"
#include "Jzon.h"

#endif
////////////////////////////



USING_NS_CC;




GameLayer::GameLayer() :
_hud(NULL),
_background(NULL),
_basket(NULL),
_platform(NULL),
_puppies(cocos2d::Vector<Puppy*>()),
_bobo(NULL),
_snake(NULL),
_snakeTarget(nullptr),
_bullets(cocos2d::Vector<Bullet*>()),
_gameWithBobo(false),
_gameWithSnake(false),


_coins(0),
_coinsStack(0),
_coinsStackModifier(1),
_coinsStackBurst(0),
_puppyValue(1),
_puppyCatchTotal(0),

_time(0.0f),
_timeInt(0),
_lives(10),
_challengeTime(0.0f),

_challengeCooldown(0.0f),
_challengeDuration(0.0f),
_challengeIntro(0),
_challengeActive(false),

_extraLives(false),
_extraSpeed(false),

_fenceAccumulator(0.0f),

_gameSpeed(1),
_gameSpeedMax(100),
_gameSpeedStart(1),
_gameSpeedStack(0),
_gameSpeedStackMax(8),
_spawnLast(0),
_spawnPointCount(0),

_puppyCatchPlay(0),

_puppyCount(0),

_rageMode(0)
{
    /* */
}

GameLayer::~GameLayer()
{
}

GameLayer* GameLayer::create(std::string level)
{
	GameLayer* pRet = new GameLayer();
	if (pRet && pRet->init(level))
	{
		pRet->autorelease();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

bool GameLayer::init(std::string level) {

	if (!Layer::init())
	{
		return false;
	}
    
	_levelPath = level;
	_timeStepAccumulator = 0;
	_timeStepAccumulatorRatio = 0;

	_visibleSize = Director::getInstance()->getVisibleSize();
	_originPoint = Director::getInstance()->getVisibleOrigin();
    
    Manager* manager = Manager::getInstance();
    if(manager->getSlotHandler()->isProductEquiped("p_elena_2_medic"))
        _lives += 10;
    
    int stage = (int)manager->getStage(level)->getStageName();
    
    std::string levelPath = kLevelPath + _levelPath + ".txt";
    std::string jsonStr = FileUtils::getInstance()->getStringFromFile(levelPath);
    
    std::vector<std::vector<int>> tilemap;
    std::vector<std::vector<int>> fences;
    Point start;
    
    Jzon::Object rootNode;
    Jzon::Parser parser(rootNode, jsonStr);
    
    CCLOG("levelPath %s", levelPath.c_str() );
    
    _gameSpeed = 1;
    _gameSpeedMax = 100;
    _gameWithBobo = true;
    _gameWithSnake = true;
    _windSpeed = 0;
    _gravity = -3;
    _spawnPattern = "random";

    if (parser.Parse())
    {
//        _gameWithBobo = rootNode.Get("bobo").ToBool();
//        _gameWithSnake = rootNode.Get("snake").ToBool();
//        _windSpeed = rootNode.Get("windspeed").ToInt();
//        _gravity = rootNode.Get("gravity").ToInt();
        
        if (rootNode.Get("fence").IsArray())
        {
            const Jzon::Array &allFences = rootNode.Get("fence").AsArray();
            for (Jzon::Array::const_iterator it = allFences.begin(); it != allFences.end(); ++it)
            {
                std::vector<int> vector;
                for (Jzon::Array::const_iterator jt = (*it).AsArray().begin(); jt != (*it).AsArray().end(); ++jt)
                    vector.push_back((*jt).ToInt());
                
                fences.push_back(vector);
            }
        }
        
        if (rootNode.Get("start").IsArray())
        {
            auto startArray = rootNode.Get("start").AsArray();
            start = Point(startArray.Get(0).ToInt(), (12 - startArray.Get(1).ToInt()));
        }
        else{
            start = Point(11,(12-6));
        }
        const Jzon::Array &tileMap = rootNode.Get("tileMap").AsArray();
        for (Jzon::Array::const_iterator it = tileMap.begin(); it != tileMap.end(); ++it)
        {
            std::vector<int> vector;
            for (Jzon::Array::const_iterator jt = (*it).AsArray().begin(); jt != (*it).AsArray().end(); ++jt)
                vector.push_back((*jt).ToInt());
            
            tilemap.push_back(vector);
        }
        
    }
    manager->getQuests()->reloadRoundVariables();
    
    _quests = manager->getQuests()->getActiveGeneral();
    
    if(manager->getQuests()->hasDailyQuest())
    {
        auto dailyQuests = manager->getQuests()->getActiveDaily();
        for(auto daily: dailyQuests)
            _quests.push_back(daily);
    }
    
    _challengeCooldown = 10.0f;
    _challengeDuration = 15.0f;
    
    //_puppyValue = _gameSpeed;
    
    this->initStoreItems();
	this->initPhysics();
	this->initParticles();
    
    float PU_duration = 1.0f;
    float PU_cooldown = 1.0f;
    
    if(Manager::getInstance()->getSlotHandler()->isProductEquiped("p_talisman_3_putimer"))
    {
        PU_duration = 0.6f;
        PU_cooldown = 1.6f;
    }
    
    PowerUp* slowMo = PowerUp::create(POWERUP_SLOWMO);
    slowMo->setParam(20.0f * PU_duration , 20.0f * PU_cooldown);
    _powerUps.insert(std::pair<int, PowerUp*>(POWERUP_SLOWMO, slowMo));
    
    PowerUp* clear = PowerUp::create(POWERUP_CLEAR);
    clear->setParam(.1f, 30.0f * PU_cooldown);
    _powerUps.insert(std::pair<int, PowerUp*>(POWERUP_CLEAR, clear));
    
    PowerUp* vortex = PowerUp::create(POWERUP_VORTEX);
    vortex->setParam(10.0f * PU_duration, 10.0f * PU_cooldown);
    _powerUps.insert(std::pair<int, PowerUp*>(POWERUP_VORTEX, vortex));
    
    PowerUp* magnet = PowerUp::create(POWERUP_MAGNET);
    magnet->setParam(2.0f * PU_duration, 2.0f * PU_cooldown);
    _powerUps.insert(std::pair<int, PowerUp*>(POWERUP_MAGNET, magnet));
    
    PowerUp* helium = PowerUp::create(POWERUP_HELIUM);
    helium->setParam(10.0f * PU_duration, 10.0f * PU_cooldown);
    _powerUps.insert(std::pair<int, PowerUp*>(POWERUP_HELIUM, helium));
    
    for (auto powerup : manager->getSlotHandler()->getAvailablePowerUps())
       _powerUps.at(powerup)->unlock();


	Vec2 platformPosition = Vec2(_visibleSize.width / 2 + _originPoint.x, _visibleSize.height / 2 + _originPoint.y - 10);
	_platform = Platform::create(this, tilemap, platformPosition, stage);
	_platform->drawGround(_visibleSize);
	//_platform->drawBorders(_visibleSize);
	_spawnPointCount = _platform->getSpawnPointCount();
    _touchMin = _platform->getPositionY() - _platform->getContentSize().height / 2 - 20;
	_touchMax = _visibleSize.height - 50;
    _basketStartPoint = _platform->pointToPosition(start);
    if (fences.size() > 0)
    {
        for(auto fence: fences)
        {
            auto fenceNode = _platform->drawFence(fence);
            fenceNode->setPosition(_platform->deltaPosition(fenceNode->getPosition()));
            this->addChild(fenceNode, kFenceIndex);
        }
    }
	this->addChild(_platform, kPlatformIndex);
    
    
    auto fronLayer = _background->getFrontLayer(stage);
    this->addChild(fronLayer, kWaterIndex);

	_basket = Basket::create(this, Vec2(_basketStartPoint), manager->getBasketType());
	this->addChild(_basket, kBasketIndex);
	_basket->show(true);


	if (_gameWithBobo)
	{
		_bobo = Bobo::create(stage);
        _bobo->setPosition(Vec2(-100, _visibleSize.height - 52));
		_bobo->setAnchorPoint(Vec2(0.5, 0));
		this->addChild(_bobo, kBasketIndex);
	}
    
    if (_gameWithSnake)
    {
        _snake = Snake::create();
        _snake->setPosition(Vec2(-100, _touchMin));
        _snake->setAnchorPoint(Vec2(0.5, 0));
        this->addChild(_snake, kSnakeIndex);
    }
    

	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	schedule(schedule_selector(GameLayer::tick));
        
    if(Manager::getInstance()->getDebugTools())
    {
//        waterFront->setOpacity(50);
//        waterBack->setOpacity(50);
    }
    


	return true;
}


void GameLayer::tick(float dt)
{
	if (gameIsRunning() || gameIsOver())
	{
        
		_timeStep = 1.f / 60.f;
		gameTimer(dt);
		gamePowerUps(dt);
		getBackground()->update(dt * _timeStep * _gameSpeed / 5);
		_hud->updateCoinLabel(dt);

		//Physics timestep
		const int max_steps = 5;
		_timeStepAccumulator += dt;
		const int nSteps = static_cast<int>(std::floor(_timeStepAccumulator / _timeStep));

		if (nSteps > 0){
			_timeStepAccumulator -= nSteps * _timeStep;
		}

		_timeStepAccumulatorRatio = _timeStepAccumulator / _timeStep;
		const int nStepsClamped = std::min(nSteps, max_steps);

		for (int i = 0; i < nStepsClamped; i++){
			for (auto puppy : this->_puppies){
				puppy->interpolationReset();
				
				if (puppy->getPlatformContacts() > 0 && abs(puppy->getBody()->GetLinearVelocity().x) < puppy->getRunSpeed()){
					puppy->getBody()->ApplyLinearImpulse(b2Vec2(0.1f * puppy->getDirection(), 0.0f), puppy->getBody()->GetPosition(), true);
				}

				if (_powerUps.at(POWERUP_VORTEX)->isActive()){
					if (puppy->getInPlay()){
						//puppy->vortex(_basket->getLocation(), dt);
						puppy->vortex(b2Vec2(_visibleSize.width / 2, _visibleSize.height / 2));
					}
				}

				if (_powerUps.at(POWERUP_MAGNET)->isActive()){
					if (puppy->getInPlay()){
						puppy->magnet(_basket->getLocation());
					}
				}

				if (_powerUps.at(POWERUP_HELIUM)->isActive()){
					for (auto puppy : _puppies){
						if (puppy->getInPlay() && puppy->isAlive()){
							auto velocity = puppy->getBody()->GetLinearVelocity();
							auto location = puppy->getBody()->GetPosition();
							if (puppy->getPositionY() <= _visibleSize.height - 30){
								if (velocity.y < _gravity * -0.4f)
									puppy->getBody()->ApplyLinearImpulse(b2Vec2(0.0f, 0.2f), location, true);
							}
						}
					}
				}
			}
			if (_gameWithBobo) for (auto bullet : this->_bullets) bullet->interpolationReset();
			if (_extraLives || _extraSpeed) for (auto extras : this->_extras) extras->interpolationReset();
			_world->Step(_physicsSpeed, 3, 2);
		}
		//End of physics timestep
		
		_basket->update(dt);

		if (_basket->isFenced())
        {
			//_basket->setFenced(false);
			if (_fenceAccumulator == 0){
				gameEvent("fence", true);
				_fenceAccumulator += dt;
			}
			else{
				_fenceAccumulator += dt;
				CCLOG("FENCE: %f", _fenceAccumulator);
				if (_fenceAccumulator >= 1.0f){
					gameEvent("fence", true);
					_fenceAccumulator = _fenceAccumulator - 1.0f;
				}
			}
        }
		else{
			_fenceAccumulator = 0.0f;
		}

		for (auto puppy : this->_puppies)
		{
			puppy->update(dt, _timeStepAccumulatorRatio);
			if ((puppy->getBody()->GetLinearVelocity().y < -1.5 && puppy->getBody()->GetLinearVelocity().y > -2.0) && puppy->isAlive() == true && !puppy->isVortexable()){
				puppy->animate(PUPPY_ANIMSTATE_FALL);
				puppy->bounced(false);
				//puppy->setRandomDirection();
				puppy->catchable(true);
			}

			if (puppy->isAlive() && (puppy->isVortexable() || puppy->isMagnetable())){
				if (!_powerUps.at(POWERUP_VORTEX)->isActive()){
					puppy->vortexable(false);
					puppy->getBody()->SetGravityScale(1);
				}
				if (!_powerUps.at(POWERUP_MAGNET)->isActive() && !_powerUps.at(POWERUP_VORTEX)->isActive()){
					puppy->magnetable(false);
					puppy->getBody()->SetGravityScale(1);
					if (puppy->getBody()->GetContactList()){
						if ((int)(size_t)(puppy->getBody()->GetContactList()->contact->GetFixtureA()->GetUserData()) == (int)B2_CONTACT_RUN ||
							(int)(size_t)(puppy->getBody()->GetContactList()->contact->GetFixtureB()->GetUserData()) == (int)B2_CONTACT_RUN){
							puppy->getBody()->SetLinearVelocity(b2Vec2(puppy->getRunSpeed() * puppy->getDirection(), 0));
							puppy->animate(PUPPY_ANIMSTATE_RUN);
						}
					}
				}
				if (_powerUps.at(POWERUP_VORTEX)->isActive()){
					puppy->animate(PUPPY_ANIMSTATE_VORTEX);
				}
			}
			else{
				puppy->setRotation(0.0f);
			}

			if (_powerUps.at(POWERUP_VORTEX)->isActive()){
				if (_particleEffect->getParticleCount() == 0){
					_particleEffect->resetSystem();
				}
				if (puppy->getInPlay()){
					//puppy->vortex(_basket->getLocation(), dt);
					puppy->vortex(b2Vec2(_visibleSize.width / 2, _visibleSize.height / 2));
				}
			}
			else {
				_particleEffect->stopSystem();
			}

			if (_powerUps.at(POWERUP_HELIUM)->isActive()){
				if (puppy->isAlive() && !puppy->isVortexable()){
					//puppy->vortexable(true);
					puppy->catchable(true);
					puppy->helium(true);
					puppy->animate(PUPPY_ANIMSTATE_HELIUM);
					if (puppy->getBody()->GetLinearVelocity().x == 0){
						puppy->getBody()->ApplyLinearImpulse(b2Vec2(puppy->getDirection() * 1, 0), b2Vec2(0, 0), true);
					}
				}
			}
			else
			{
				puppy->helium(false);
			}

			if (!puppy->isVortexable() || !puppy->isAlive()){
				puppy->getBody()->SetGravityScale(1);
			}
            
            if(puppy->bringToFront() && puppy->getZOrder() == kPuppyIndex)
                puppy->setZOrder(kPuppyFallIndex);
            else if(!puppy->bringToFront() && puppy->getZOrder() == kPuppyFallIndex)
                puppy->setZOrder(kPuppyIndex);
            
            
            // for wind mirroring
            /*
            if(puppy->isAlive() && puppy->getPositionX() < - 30)
                puppy->setPositionX(_visibleSize.width + 30);
            
            if(puppy->isAlive() && puppy->getPositionX() > _visibleSize.width + 30)
                puppy->setPositionX(-30);
             */                       
            

			if (puppy->isCatched())
			{
				if (!_basket->isFenced()){
					gameEvent("catch", true, puppy);
					_basket->animate(BASKET_ANIMSTATE_CATCH);
					puppy->rest();
					Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_PUPPY_CATCH);
					_puppyCount++;
//					CCLOG("Puppies catched: %i", _puppyCount);
				}
				else
				{
					puppy->setAlive(true);
				}
			}
            
			if (puppy->isSpeared())
            {
				gameEvent("spears", true, puppy);
            }
            
			if (puppy->isWatered())
            {
                gameEvent("swim", true, puppy);
            }

			if (puppy->isGrounded())
			{
                gameEvent("ground", true, puppy);
                Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_PUPPY_GROUNDED);
            }
            
            if(_gameWithSnake)
            {
                if(_snake->canPursue(puppy->getPosition()) && puppy->isAlive() && _snakeTarget == nullptr)
                {
                    _snake->pursue();
                    _snakeTarget = puppy;
                }
            }
            
		}
        
        if(_extraLives || _extraSpeed)
        {
            for (auto extra : this->_extras)
            {
                extra->update(dt, _timeStepAccumulatorRatio);
                
                // [GATIM] ja šo palaiž sākas diskotēka 
                if (extra->basketIsHit())
                {
					Vec2 extraPosition = _basket->getPosition();
					extraPosition.y += 50.0f;
                    switch(extra->getType())
                    {
                        case EXTRA_SPEED:
                            //_gameSpeed = (_gameSpeed - 1 >= _gameSpeedStart) ? _gameSpeed - 1 : _gameSpeed;
							int speedDelta;
							int speedText;
							speedText = _gameSpeed;
							if (_gameSpeed <= 19) speedDelta = 1;
							if (_gameSpeed > 19 && _gameSpeed <= 39) speedDelta = 2;
							if (_gameSpeed > 39 && _gameSpeed <= 59) speedDelta = 3;
							if (_gameSpeed > 59 && _gameSpeed <= 79) speedDelta = 4;
							if (_gameSpeed > 79 && _gameSpeed <= 100) speedDelta = 5;
							CCLOG("SpeedDelta: %i", speedDelta);
							_gameSpeed = (_gameSpeed - speedDelta >= _gameSpeedStart) ? _gameSpeed - speedDelta : _gameSpeedStart;
							speedText = speedText - _gameSpeed;
                            setGameSpeed(_gameSpeed);
							_hud->scoreExtra(EXTRA_SPEED, extraPosition, speedText);
                            //CCLOG("EXTRA Speed Down");
                            break;
                        case EXTRA_LIVES:
							_lives = _lives + 1;
							_hud->updateLives(_lives);
							_hud->scoreExtra(EXTRA_LIVES, extraPosition);
                            //CCLOG("EXTRA Lives + 1");
                            break;
                    }
                }
                if (extra->isGrounded() || extra->getPositionY() < 0)
                    extra->rest();
                 //*/
            }
        }
        

		if (_gameWithBobo)
		{
			if (_rageMode >= boboRageMode){
				_bobo->RageMode(true);
				_rageMode = 0;
			}
			_bobo->update(dt);
			for (auto bullet : this->_bullets)
			{
				bullet->update(dt, _timeStepAccumulatorRatio);

				if (bullet->basketIsHit())
				{
                    gameEvent("bomb", true);
					_hud->animateExplosion(bullet->getExplosionPosition());
					_basket->animate(BASKET_ANIMSTATE_EXPLODE);
                    _background->shake();
                    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_BOBO_BULLET);
				}

				if (bullet->isGrounded())
				{
					bullet->rest();
					_hud->animateExplosion(bullet->getExplosionPosition());
                    _background->shake();
                    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_BOBO_BULLET);
				}

				if (bullet->getPositionY() < 0) bullet->rest();
			}
		}
        
        if (_gameWithSnake)
        {
            if(_snakeTarget != nullptr)
            {
                if(!_snake->canPursue(_snakeTarget->getPosition()) || !_snakeTarget->isAlive())
                {
                    _snakeTarget = nullptr;
                    _snake->reset();
                }
                else if(_snake->canKill(_snakeTarget->getPosition()))
                {
                    gameEvent("snake", true, _snakeTarget);
                    _snakeTarget->rest();
                    _snakeTarget = nullptr;
                    _snake->reset();
                }
            }
            
            if(_snakeTarget != nullptr)
                _snake->update(dt, _snakeTarget->getPosition());
            
            _snake->updatePostAttack(dt);
        }
	} // game is running
    
    
    
}

bool GameLayer::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
	if (gameIsRunning())
    {
		if (touch->getLocation().y <= _touchMin){
			_basket->flip(Vec2(touch->getLocation().x, _touchMin));
			_basket->setSpritePosition(Vec2(touch->getLocation().x, _touchMin));
		}
		else if (touch->getLocation().y >= _touchMax){
			_basket->flip(Vec2(touch->getLocation().x, _touchMax));
			_basket->setSpritePosition(Vec2(touch->getLocation().x, _touchMax));
		}
		else{
			_basket->flip(touch->getLocation());
			_basket->setSpritePosition(touch->getLocation());
		}
	}

    if (gameIsRunning() && _gameWithBobo)
        _bobo->moveToX(touch->getLocation().x);
    
    if (gameIsRunning() && _gameWithSnake)
        _snake->avoidX(touch->getLocation().x);
    

    if(_basket->isTeleported())
        gameEvent("teleport", false);

	return true;
}

void GameLayer::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
	/*
	if (gameIsRunning() && touch->getLocation().y >= _touchMin && touch->getLocation().y <= (_touchMax - 10))
    {
        _basket->flip(touch->getLocation());
        _basket->setSpritePosition(touch->getLocation());
    }
	*/
	if (gameIsRunning())
	{
		if (touch->getLocation().y <= _touchMin){
			_basket->flip(Vec2(touch->getLocation().x, _touchMin));
			_basket->setSpritePosition(Vec2(touch->getLocation().x, _touchMin));
		}
		else if (touch->getLocation().y >= _touchMax){
			_basket->flip(Vec2(touch->getLocation().x, _touchMax));
			_basket->setSpritePosition(Vec2(touch->getLocation().x, _touchMax));
		}
		else{
			_basket->flip(touch->getLocation());
			_basket->setSpritePosition(touch->getLocation());
		}
	}

	if (gameIsRunning() && _gameWithBobo)
		_bobo->moveToX(touch->getLocation().x);
    
    if (gameIsRunning() && _gameWithSnake)
        _snake->avoidX(touch->getLocation().x);
   
}


void GameLayer::spawnPuppy()
{
    if(_platform->hasSpawnPoints())
    {
        if(_challengeActive && _challengeNow == CHALLENGE_SPAWN)
        {
            auto spawnPoints = _platform->getAllSpawnPoints();
            for(auto tile: spawnPoints)
            {
                setPuppyInPlay(tile);
            }
        }
        else
        {
            int nr = rand() % _spawnPointCount;
            if (_spawnPointCount > 2)
            {
                do
                {
                    nr = rand() % _spawnPointCount;
                } while (nr == _spawnLast);
            }
            _spawnLast = nr;
            setPuppyInPlay(_platform->getSpawnPoint(nr));
        }
    }
    
    /*
    if(_platform->hasSpawnPoints() && _spawnPattern != "rain")
    {
        if(_spawnPattern == "random")
        {
            int nr = rand() % _spawnPointCount;
            if (_spawnPointCount > 2)
            {
                do
                {
                    nr = rand() % _spawnPointCount;
                } while (nr == _spawnLast);
            }
            _spawnLast = nr;
            setPuppyInPlay(_platform->getSpawnPoint(nr));
            
        }
        else if(_spawnPattern == "linear")
        {
            setPuppyInPlay(_platform->getSpawnPoint(_spawnLast));
            if(_spawnLast < (_spawnPointCount - 1))
                _spawnLast++;
            else
                _spawnLast = 0;
        }
        else if(_spawnPattern == "all")
        {
            auto spawnPoints = _platform->getAllSpawnPoints();
            for(auto tile: spawnPoints)
            {
                setPuppyInPlay(tile);
            }
        }
    }
    else if(_spawnPattern == "rain")
    {
        int x = _visibleSize.width - 40 * 2;
        float randomX = (rand() % x) + 40;
        Vec2 point = Vec2(randomX, _visibleSize.height + 20);
        
        setPuppyInPlay(point, 1);
    }
     */
}


Puppy* GameLayer::setPuppyInPlay(PlatformTile* tile)
{
    Vec2 position = _platform->deltaPosition(tile->getPosition());
    position.y += 2;
    int direction = tile->getDirection();
    
    return setPuppyInPlay(position, direction);
}

Puppy* GameLayer::setPuppyInPlay(cocos2d::Vec2 position, int direction)
{
    for (auto puppy : _puppies)
    {
        if (!puppy->getInPlay())
        {
            puppy->reset();
            puppy->setSpritePosition(position);
            puppy->setDirection(direction);
			puppy->setRandomRunSpeed();
            return puppy;
        }
    }
    
    auto puppy = Puppy::create(this, position);
    puppy->setDirection(direction);
    _puppies.pushBack(puppy);
	puppy->setRandomRunSpeed();
    this->addChild(puppy, kPuppyIndex);
    return puppy;
}

Bullet* GameLayer::spawnBullet()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_BOBO_DROP);
    
    Vec2 spawnPoint = _bobo->getPosition();
    for (auto bullet : _bullets)
    {
        if (!bullet->getInPlay())
        {
            bullet->reset();
            bullet->setSpritePosition(spawnPoint);
			//CCLOG("Bullet old, velocity: %f", bullet->getBody()->GetLinearVelocity().x);
			if (!_bobo->isRageModeOn()) _rageMode++;
            return bullet;
        }
    }
    auto bullet = Bullet::create(this, spawnPoint);
	_bullets.pushBack(bullet);
	this->addChild(bullet, kBulletIndex);
 	//CCLOG("Bullet new, velocity: %f, x: %f, y: %f", bullet->getBody()->GetLinearVelocity().x, bullet->getPositionX(), bullet->getPositionY());
	return bullet;
}

void GameLayer::spawnCluster()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_BOBO_DROP);
    
	int clusterCount = 0;
	int clusterSpreat = 1.5;
	Vec2 spawnPoint = _bobo->getPosition();
	while (clusterCount < 3)
	{
		for (auto bullet : _bullets)
		{
			if (!bullet->getInPlay() && clusterCount < 3)
			{
				bullet->reset();
				bullet->setSpritePosition(spawnPoint);
				clusterCount++;
				_rageMode++;
				if (clusterCount == 1) bullet->getBody()->ApplyLinearImpulse(b2Vec2(clusterSpreat, 0), bullet->getBody()->GetPosition(), true);
				if (clusterCount == 3) bullet->getBody()->ApplyLinearImpulse(b2Vec2(-clusterSpreat, 0), bullet->getBody()->GetPosition(), true);
				//CCLOG("Cluster, count: %i, velocity: %f", clusterCount, bullet->getBody()->GetLinearVelocity().x);
			}
		}
		if (clusterCount < 3){
			auto bullet = Bullet::create(this, spawnPoint);
			_bullets.pushBack(bullet);
			this->addChild(bullet, kPuppyIndex);
			clusterCount++;
			_rageMode++;
			if (clusterCount == 1) bullet->getBody()->ApplyLinearImpulse(b2Vec2(clusterSpreat, 0), bullet->getBody()->GetPosition(), true);
			if (clusterCount == 3) bullet->getBody()->ApplyLinearImpulse(b2Vec2(-clusterSpreat, 0), bullet->getBody()->GetPosition(), true);
			//CCLOG("Cluster2, count: %i, velocity: %f, PositionX: %f, PositionY: %f", clusterCount, bullet->getBody()->GetLinearVelocity().x, bullet->getPositionX(), bullet->getPositionY());
		}
	}
}

void GameLayer::spawnExtra(int type)
{
    
    float randomWidth = _visibleSize.width -  (rand() % ((int)_visibleSize.width - 40)) + 20;
    Vec2 spawnPoint = Vec2(randomWidth, _visibleSize.height + 20);
    
    for (auto extra : _extras)
    {
        if (!extra->getInPlay())
        {
            extra->reset();
            extra->setSpritePosition(spawnPoint);
            extra->setType((ExtraType)type);
			//CCLOG("spawn extra old");
            return;
        }
    }
    auto extra = Extra::create(this, spawnPoint);
    extra->setSpritePosition(spawnPoint);
    extra->setType((ExtraType)type);
    this->addChild(extra, kPuppyIndex);
	//CCLOG("spawn extra new");
    _extras.pushBack(extra);    
}



void GameLayer::initPhysics()
{
	b2Vec2 gravity = b2Vec2(_windSpeed, _gravity);
    _world = new b2World(gravity);
	MyContactListener *contactListener = new MyContactListener;
	_world->SetContactListener(contactListener);

    if(Manager::getInstance()->getDebugTools())
    {    
        //*
        b2Draw *m_debugDraw = new GLESDebugDraw(RATIO);
        _world->SetDebugDraw(m_debugDraw);
        uint32 flags = 0;
        flags += b2Draw::e_shapeBit;
        flags += b2Draw::e_jointBit;
        //flags += b2Draw::e_aabbBit;
        //flags += b2Draw::e_pairBit;
        //flags += b2Draw::e_centerOfMassBit;
        m_debugDraw->SetFlags(flags);
        //*/
    }
}


void GameLayer::initStoreItems()
{
    if(Manager::getInstance()->getSlotHandler()->isProductEquiped("p_talisman_1_drop_lives"))
    {
        _extraLives = true;
    }
    
    if(Manager::getInstance()->getSlotHandler()->isProductEquiped("p_talisman_2_drop_speed"))
    {
        _extraSpeed = true;
    }
}

///////////////////////////////////////////////////////////////////
//////////////////////// GAME LOGIC ///////////////////////////////
///////////////////////////////////////////////////////////////////


void GameLayer::gamePrepare(std::string level)
{
    _hud->showGameSetup(level, _lives);
    //_background->shake(3.0);
//    _hud->runQuestText();
	
	_puppySpawn.Timer = 0.0f;
    _puppyRainSpawn.Timer = 0.0f;
	_bulletSpawn.Timer = 0.0f;
	_bulletSpawn.Timer2 = 0.0f;

	if (_gameWithBobo)
		_bobo->setSpeedModifier(1.0f);
    
    if (_gameWithSnake)
        _snake->setSpeedModifier(1.0f);
   
    setGameSpeed(_gameSpeed);
    _gameStatus = GAME_STATUS_READY;
    
   // _hud->showGamePause();
}

void GameLayer::gameStart()
{
    if (_gameStatus == GAME_STATUS_READY)
    {
        _gameStatus = GAME_STATUS_PLAYING;
        _hud->showGamePlay();
        Manager::getInstance()->getSlotHandler()->useTalismans();
    }
}

void GameLayer::gameTooglePause()
{
	if (_gameStatus == GAME_STATUS_PLAYING)
	{
		_gameStatus = GAME_STATUS_PAUSE;
		_hud->showGamePause();
		//_pausedNodes = getActionManager()->pauseAllRunningActions();
	}
	else if (_gameStatus == GAME_STATUS_PAUSE)
	{
		_gameStatus = GAME_STATUS_PLAYING;
		_hud->showGamePauseResume();
		//getActionManager()->resumeTargets(_pausedNodes);
	}
}

void GameLayer::gamePowerUps(float dt)
{
	int powerup_tag;
	for (std::map<int, PowerUp*>::iterator it = _powerUps.begin(); it != _powerUps.end(); ++it)
	{
		it->second->update(dt);

		powerup_tag = it->first;

		if (it->second->onCoolDown())
			_hud->runCoolDown(powerup_tag, _powerUps.at(powerup_tag)->getCoolDownTime(), _powerUps.at(powerup_tag)->getCoolDown(), true);

		else if (it->second->isReady())
			_hud->endCoolDown(powerup_tag);

		else if (it->second->isActive())
			_hud->runCoolDown(powerup_tag, _powerUps.at(powerup_tag)->getActiveTime(), _powerUps.at(powerup_tag)->getDuration(), false);

		else if (it->second->flag())
		{
            if(powerup_tag != POWERUP_SLOWMO)
                _hud->runPowerUpText(powerup_tag, false);
            
			switch (powerup_tag)
			{
			case POWERUP_SLOWMO:
				setGameSpeed(_gameSpeed); // return back to normal speed
				break;
			}
		}
	}
}


void GameLayer::gameTooglePowerUp(int powerUp)
{
	if (gameIsRunning())
	{
        if(!_powerUps.at(powerUp)->isActive() && _powerUps.at(powerUp)->isReady())
        {
            auto PUslot = Manager::getInstance()->getSlotHandler();
            if(PUslot->hasCharges(powerUp))
            {
//                CCLOG("Charge PU");
                PUslot->usePowerUp(powerUp);
                _powerUps.at(powerUp)->activate();
                _hud->updatePowerUpUses(powerUp, PUslot->getPowerUpUses(powerUp));
                gameEvent("powerup_use");
            }else{
                _powerUps.at(powerUp)->lock();
                CCLOG("Out of uses");
            }
        }
        

		if (_powerUps.at(powerUp)->isActive())
		{
            
            if(_powerUps.at(powerUp)->showMessage())
                _hud->runPowerUpText(powerUp, true);

			switch (powerUp)
			{
                case POWERUP_SLOWMO:
                    setGameSpeed(_gameSpeed);
                    break;
            
                case POWERUP_CLEAR:
                    for (auto puppy : this->_puppies)
                    {
                        if (puppy->getInPlay() && puppy->isAlive())
                        {
                            gameEvent("catch", true, puppy);
                            puppy->rest();
                        }
                    }
                    _hud->updateSpeed(_gameSpeed);
                    break;

                case POWERUP_VORTEX:
                    //setGameSpeed(_gameSpeed);
                    break;
                    
                case POWERUP_MAGNET:
                    CCLOG("MAGNET POWERUP");
                    break;
                    
                case POWERUP_HELIUM:
                    CCLOG("HELIUM POWERUP");
                    break;
			}
		}
	}
}

bool GameLayer::gameIsRunning()
{
    return (_gameStatus == GAME_STATUS_PLAYING) ? true : false;
}

bool GameLayer::gameIsOver()
{
    return (_gameStatus == GAME_STATUS_OVER) ? true : false;
}



void GameLayer::gameTimer(float dt)
{
    if(gameIsRunning())
    {
        _time += dt;
        
        _hud->updateTime(_time);
        
//        if(_timeInt < (int)_time)
        {
//            _timeInt = (int)_time;
            //_hud->updateTime(_timeInt);
        }
        
        _challengeTime += dt;
        if(_challengeActive)
        {
            if(_challengeTime > _challengeDuration)
            {
                _challengeTime = 0.0f;
                _challengeActive = false;
                switch(_challengeNow)
                {
                    case CHALLENGE_BOBO:
                        _bobo->setActive(false);
                        break;
                    case CHALLENGE_SNAKE:
                        _snake->setActive(false);
                        break;
                    case CHALLENGE_RAIN:
                        
                        break;
                    case CHALLENGE_WIND:
                        _windSpeed = 0;
                        _world->SetGravity(b2Vec2(_windSpeed, _gravity));
                        break;
                }
                CCLOG("Challenge %d OFF", (int)_challengeNow);
                gameEvent("challenge_off");

            }
        }
        else
        {
            if((int)(_challengeCooldown - _challengeTime) < 2)
            {
                if(_challengeIntro != (int)(_challengeCooldown - _challengeTime))
                {
                    _background->shake(2-(int)(_challengeCooldown - _challengeTime));
                    _challengeIntro = (int)(_challengeCooldown - _challengeTime);
                }
            }
            
            if(_challengeTime > _challengeCooldown)
            {
                _challengeTime = 0.0f;
                _challengeActive = true;
                _challengeNow = challengeChoose();
                _background->shake(3.0f);
                _hud->runChallengeText((int)_challengeNow);
                CCLOG("Challenge %d ON", (int)_challengeNow);
                switch(_challengeNow)
                {
                    case CHALLENGE_BOBO:
                        _bobo->setActive(true);
                        break;
                    case CHALLENGE_SNAKE:
                        _snake->setActive(true);
                        break;
                    case CHALLENGE_RAIN:
                        
                        break;
                    case CHALLENGE_WIND:
                        int r = random(0, 1);
                        _windSpeed = (r==1) ? 6 : -6;
                        _world->SetGravity(b2Vec2(_windSpeed, _gravity));
                        break;
                }
            }
        }
        
        int mod = Manager::getInstance()->getGameSpeedModifier();

        if (_gameSpeed < _gameSpeedMax)
        {
            if (_gameSpeedStack >= ( _gameSpeedStackMax * mod))
            {
                _gameSpeed++;
                _gameSpeedStack = 0;
                setGameSpeed(_gameSpeed);
            }
        }

        // bullet spawner
        if (_gameWithBobo && _bobo->isActive())
        {		
            if (!_bobo->inPosition())
            {
                //normal fire
                if (_bobo->isRageModeOn()) _bulletSpawn.Timer += 2 * dt;
                else _bulletSpawn.Timer += dt;
                if (_bulletSpawn.Timer >= _bulletSpawn.bulletFrequency)
                {
                    _bulletSpawn.Timer = _bulletSpawn.bulletFrequency - _bulletSpawn.Timer;
                    spawnBullet();
                    _bobo->animateBobo();
                }
            }
            else
            {
                // cluster fire
                _bulletSpawn.Timer2 += dt;
                if (_bulletSpawn.Timer2 >= _bulletSpawn.clusterFrequency)
                {
                    _bulletSpawn.Timer2 = _bulletSpawn.clusterFrequency - _bulletSpawn.Timer2;
                    spawnCluster();
                    _bobo->animateBobo();
                }

            }
        }

        // puppy spawner
        _puppySpawn.Timer += dt;
        
        float spawnLimiter = 1.0f;
        if(_challengeActive && _challengeNow == CHALLENGE_SPAWN)
            spawnLimiter = 1.8f;
        
        if (_puppySpawn.Timer > _puppySpawn.bulletFrequency * spawnLimiter)
        {
            _puppySpawn.Timer = 0.0f;
            spawnPuppy();
            
            if(_extraSpeed || _extraLives)
            {
                int type = (rand() % 50) - 1;
                
                if(type == 1 && _extraLives)
                    spawnExtra((int)EXTRA_LIVES);
                
                if(type == 2 && _extraSpeed)
                    spawnExtra((int)EXTRA_SPEED);
                
            }
        }
        if(_challengeNow == CHALLENGE_RAIN && _challengeActive)
        {
            _puppyRainSpawn.Timer += dt;
            if (_puppyRainSpawn.Timer > _puppyRainSpawn.bulletFrequency)
            {
                _puppyRainSpawn.Timer = 0.0f;
                
                int x = _visibleSize.width - 40 * 2;
                float randomX = (rand() % x) + 40;
                Vec2 point = Vec2(randomX, _visibleSize.height + 20);
                
                setPuppyInPlay(point, 1);
            }
        }
        
    }
}

void GameLayer::gameAddCoins(cocos2d::Vec2 position)
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_COINS);
    
	_gameSpeedStack++;
	_puppyCatchTotal++;
    
	_coins = _coins + (_puppyValue * _coinsStackModifier);
	_hud->updateCoins(_coins, (_puppyValue * _coinsStackModifier), _coinsStackBurst, position);
    _hud->updatePuppies(_puppyCatchTotal);
}

ChallengeType GameLayer::challengeChoose()
{
    int r = rand() % kChallengeTypeCount;
    /*
    if(_gameWithBobo && r == (int)CHALLENGE_BOBO)
        challengeChoose();
    else if(_gameWithBobo && r == (int)CHALLENGE_SNAKE)
        challengeChoose();
    else*/
        return (ChallengeType)r;
}

void GameLayer::gameEvent(std::string event, bool harmful, Puppy* puppy)
{
    if(gameIsRunning())
    {
    //    CCLOG("Event %s", event.c_str());
        
        if(harmful)
        {
            Vec2 coinsPosition = (puppy == nullptr) ? _basket->getPosition() : puppy->getPosition();
            if(event == "catch")
            {
                gameAddCoins(coinsPosition);
                gamePuppyStacking(true);
                
                _puppyCatch.push_back({coinsPosition, puppy->getType(), puppy->getAnimationState(), puppy->getDirection()}); // getPuppyType;
            }
            else
            {
                _lives--;
                _hud->updateLives((_lives < 0 ) ? 0 : _lives);
                
                if(puppy !=nullptr)
                    _hud->updateBones(coinsPosition);
                
                gamePuppyStacking(false);
            }
        }
        
        for(auto quest: _quests)
        {
            if(quest->eventMatch(event, _levelPath))
            {
                quest->initParamCheck();
                
                if(puppy != nullptr)
                {
                    if(puppy->getAnimationState() == PUPPY_ANIMSTATE_JUMP)
                        quest->paramCheck("jump", 1);
                    
                    if(puppy->getAnimationState() == PUPPY_ANIMSTATE_FALL)
                        quest->paramCheck("jump", 0);
                    
                    if(puppy->getPositionY() > 200)
                        quest->paramCheck("height", 1);
                    
                    if(puppy->getPositionY() < 100)
                        quest->paramCheck("height", 0);
                    
//                    CCLOG("PUPPY TYPE %d", puppy->getType());
                    quest->paramCheck("puppy", puppy->getType());
                }
                
                if(_powerUps.at(POWERUP_CLEAR)->isActive())
                    quest->paramCheck("powerup", POWERUP_CLEAR);
                
                if(_powerUps.at(POWERUP_VORTEX)->isActive())
                    quest->paramCheck("powerup", POWERUP_VORTEX);
                
                if(_powerUps.at(POWERUP_MAGNET)->isActive())
                    quest->paramCheck("powerup", POWERUP_MAGNET);
                
                if(_powerUps.at(POWERUP_SLOWMO)->isActive())
                    quest->paramCheck("powerup", POWERUP_SLOWMO);
                
                if(_powerUps.at(POWERUP_HELIUM)->isActive())
                    quest->paramCheck("powerup", POWERUP_HELIUM);
                
                if(_challengeActive || event == "challenge_off")
                    quest->paramCheck("challenge", _challengeNow);
                
                
                if(quest->goalParamMatch())
                {
                    float perc = (quest->getAmountNow() * 100) / quest->getAmountGoal();
                    bool completed = (quest->getAmountNow() == quest->getAmountGoal()) ? true : false;
                    _hud->updateQuest(quest->getTag(), perc, _basket->getPosition(), completed, quest->isDaily());
                    
                    if(completed)
                    {
                        _hud->runQuestText();
                    }
                    CCLOG("Quest Goal match (%s)!!", quest->getTag().c_str());
                }
                
            }
        }
        
        eventEvaluation();
    
    }
   
}

void GameLayer::eventEvaluation()
{
    if(_lives <= 0)
    {
        _gameStatus = GAME_STATUS_OVER;
        _hud->showGameOver(_coins);
        
        if(_gameStatus == GAME_STATUS_OVER)
        {
            CCLOG("ONLY ONCE");
            setGameSpeed(_gameSpeed); // for SlowMo Effect
            
            
            /*
            for(auto puppy: _puppies)
                puppy->rest();
            
            _bobo->setVisible(false);
            for(auto bullet: _bullets)
                bullet->setVisible(false);
            
            this->schedule(schedule_selector(GameLayer::catchReplay),.0f);
             */
        }
    }
    
    if(_gameStatus == GAME_STATUS_OVER)
        this->getActionManager()->removeAllActionsFromTarget(this);
}

void GameLayer::gamePuppyStacking(bool stack)
{
    /// extends on game speed % 10, so on high speeds is less increment
    
    
	if (stack)
		_coinsStack++;
	else
		_coinsStack = 0;
    
	int puppyStackModifier;
    int gloryText;

	if (_coinsStack >= 40){
		puppyStackModifier = 5;
        gloryText = 4;
	}
    else if (_coinsStack >= 20 && _coinsStack < 40)
    {
        puppyStackModifier = 4;
        gloryText = 3;
    }
	else if (_coinsStack >= 10 && _coinsStack < 20)
	{
		puppyStackModifier = 3;
        gloryText = 2;
	}
	else if (_coinsStack >= 5 && _coinsStack < 10)
	{
        puppyStackModifier = 2;
        gloryText = 1;
	}
	else
	{
        puppyStackModifier = 1;
        gloryText = 0;
	}
	if (_coinsStackModifier != puppyStackModifier)
	{
		_coinsStackModifier = puppyStackModifier;
        _coinsStackBurst = gloryText;
        
        //CCLOG("g burst %d", gloryText);

		if (_coinsStackModifier > 1)
		{
			_hud->runGlorifyText(gloryText);
		}
		//CCLOG("stack update %i", puppyStackModifier);
	}


}


void GameLayer::setGameSpeed(int speed)
{
	_gameSpeed = (speed > 100) ? 100 : speed;
	_gameSpeedStack = 0;
    
    
    //_puppyValue = _gameSpeed;

	_hud->updateSpeed(_gameSpeed);
	
	CCLOG("speed increment %i", speed);

	float puppySpawn, bulletSpawn, clusterSpawn, boboSpeedModifier, snakeSpeedModifier;

	//SPEED
	_physicsSpeed = 1.f / (float32)(60.f - (_gameSpeed - 1) * 0.6f);

	//PUPPYSPAWN
	if (_gameSpeed >= 1 && _gameSpeed <= 5) puppySpawn = 1.5f - (_gameSpeed * 0.06f);
	if (_gameSpeed > 5 && _gameSpeed <= 10) puppySpawn = 1.2f - (_gameSpeed * 0.04f);
	if (_gameSpeed > 10 && _gameSpeed <= 20) puppySpawn = 1.0f - (_gameSpeed * 0.025f);
	if (_gameSpeed > 20 && _gameSpeed <= 30) puppySpawn = 0.50f - (_gameSpeed * 0.0025f);
	if (_gameSpeed > 30 && _gameSpeed <= 40) puppySpawn = 0.40f - (_gameSpeed * 0.00083f);
	if (_gameSpeed > 40 && _gameSpeed <= 60) puppySpawn = 0.35f - (_gameSpeed * 0.000625f);
	if (_gameSpeed > 60 && _gameSpeed <= 100) puppySpawn = 0.30f;

	//BULLET SPAWN
	if (_gameSpeed >= 1 && _gameSpeed <= 5) bulletSpawn = 2.0f;
	if (_gameSpeed > 5 && _gameSpeed <= 10) bulletSpawn = 1.60f;
	if (_gameSpeed > 10 && _gameSpeed <= 20) bulletSpawn = 1.30f;
	if (_gameSpeed > 20 && _gameSpeed <= 30) bulletSpawn = 1.00f;
	if (_gameSpeed > 30 && _gameSpeed <= 40) bulletSpawn = 0.90f;
	if (_gameSpeed > 40 && _gameSpeed <= 60) bulletSpawn = 0.70f;
	if (_gameSpeed > 60 && _gameSpeed <= 100) bulletSpawn = 0.50f;

	//CLUSTER SPAWN
	if (_gameSpeed >= 1 && _gameSpeed <= 5) clusterSpawn = 1.6f;
	if (_gameSpeed > 5 && _gameSpeed <= 10) clusterSpawn = 1.30f;
	if (_gameSpeed > 10 && _gameSpeed <= 20) clusterSpawn = 1.00f;
	if (_gameSpeed > 20 && _gameSpeed <= 30) clusterSpawn = 0.80f;
	if (_gameSpeed > 30 && _gameSpeed <= 40) clusterSpawn = 0.60f;
	if (_gameSpeed > 40 && _gameSpeed <= 60) clusterSpawn = 0.50f;
	if (_gameSpeed > 60 && _gameSpeed <= 100) clusterSpawn = 0.40f;

    //BOBO SPEED
    boboSpeedModifier = 1.0f + (_gameSpeed - 1) * 0.01f;
    
    //SNAKE SPEED
    snakeSpeedModifier = 1.0f + (_gameSpeed - 1) * 0.02f;


	float divideConst = (_powerUps.at(POWERUP_SLOWMO)->isActive()) ? 2.0f : 1.0f;

    if(gameIsOver())
        divideConst = 2.4f;
    
	_physicsSpeed = _physicsSpeed / divideConst;
	_puppySpawn.bulletFrequency = puppySpawn * divideConst;
    _puppyRainSpawn.bulletFrequency = puppySpawn * divideConst / 1.2; // [GATIM] lietus spawn
	_bulletSpawn.bulletFrequency = bulletSpawn * divideConst;
	_bulletSpawn.clusterFrequency = clusterSpawn * divideConst;
	
	if (_gameWithBobo)
		_bobo->setSpeedModifier(boboSpeedModifier / divideConst);
    
    if(_gameWithSnake)
        _snake->setSpeedModifier(snakeSpeedModifier / divideConst);
}

void GameLayer::catchReplay(float dt)
{
    if(_puppyCatchPlay < _puppyCatch.size())
    {
        PuppyCatch catchData = _puppyCatch.at(_puppyCatchPlay);
    CCLOG("Catch pos %.1f %.1f, type %d, state %d, direction %d", catchData.position.x, catchData.position.y, catchData.type, catchData.state, catchData.direction);
        
        std::string frameName = "";
        
        switch (catchData.type) {
            case 1: frameName += "mailo-"; break;
            case 2: frameName += "mopsis-"; break;
            case 3: frameName += "haskijs-"; break;
        }
        
        switch (catchData.state) {
            default:
            case 2: frameName += "jump-"; break;
            case 4: frameName += "fall-"; break;
        }
        frameName += "0.png";
        
        auto sprite = Sprite::createWithSpriteFrameName(frameName);
        sprite->setPosition(catchData.position);
        sprite->setScaleX(catchData.direction);
        this->addChild(sprite);
        
        _puppyCatchPlay++;
    }
}

void GameLayer::initParticles()
{
	_particleEffect = ParticleSystemQuad::create();
	_particleEffect = ParticleFlower::createWithTotalParticles(1000);
	_particleEffect->stopSystem();
	_particleEffect->setEmitterMode(kCCParticleModeRadius);
	_particleEffect->setStartRadius(PUPPY_VORTEX_DISTANCE);
	_particleEffect->setEndRadius(25.0f);
	_particleEffect->setRotatePerSecond(PUPPY_VORTEX_POWER * 20.0f + (_gameSpeed - 1) * 0.6f);
	_particleEffect->setStartSize(5.0f);
	_particleEffect->setEndSize(0.1f);
	this->addChild(_particleEffect);
}

void GameLayer::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	Layer::draw(renderer, transform, flags);
	_customCommand.init(_globalZOrder);
	_customCommand.func = CC_CALLBACK_0(GameLayer::onDraw, this, transform, flags);
	renderer->addCommand(&_customCommand);
}

void GameLayer::onDraw(const Mat4 &transform, uint32_t flags)
{
	Director* director = Director::getInstance();
	CCASSERT(nullptr != director, "Director is null when seting matrix stack");
	director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
	director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, transform);
	GL::enableVertexAttribs(cocos2d::GL::VERTEX_ATTRIB_FLAG_POSITION);
	_world->DrawDebugData();
	CHECK_GL_ERROR_DEBUG();

	director->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
}