#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Game/BackgroundLayer.h"
#include "Manager/Manager.h"
#include "Manager/Stages.h"

#else
#include "BackgroundLayer.h"
#include "Manager.h"
#include "Stages.h"

#endif
////////////////////////////


USING_NS_CC;

BackgroundLayer::BackgroundLayer():
_hasClouds(false)
{
	//
}

BackgroundLayer::~BackgroundLayer()
{

}

BackgroundLayer* BackgroundLayer::create(std::string level)
{
    BackgroundLayer* pRet = new BackgroundLayer();
    if (pRet && pRet->init(level))
    {
        pRet->autorelease();
        return pRet;
    }
    else{
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}


bool BackgroundLayer::init(std::string level) {
	
	if (!Layer::init())
	{
		return false;
	}
    auto manager = Manager::getInstance();
    _stage = (int)manager->getStage(level)->getStageName();
    
    CCLOG("STAGE %d", _stage);
    
    
	_visibleSize = Director::getInstance()->getVisibleSize();

    
    std::string backgroundImage = "";
    
    
    _backgroundNode = Node::create();
    
    if(_stage == STAGE_A)
    {
        backgroundImage = "stage-a-background.png";
        
        /*
         _hasClouds = true;
        
        int w = 50;
        int count = _visibleSize.width / w;
        for(int i =0; i<count; i++)
        {
            _clouds.push_back(createCloud("stage-a-cloud-1.png", 150, 1.0f, Vec2(w * i, _visibleSize.height / 2)));
         
         _clouds.push_back(createCloud("stage-a-cloud-2.png", 150, 1.0f, Vec2(w * i, _visibleSize.height / 2)));
        }
        */
        
        
        Sprite* detailA = Sprite::createWithSpriteFrameName("stage-a-left.png");
        detailA->setAnchorPoint(Vec2(0.0f, 0.0f));
        detailA->setPosition(0 - 20,  _visibleSize.height / 2 - (12 / 2 * 20 + 26));
        _backgroundNode->addChild(detailA, kBackgroundElementsIndex);
        
        Sprite* detailB = Sprite::createWithSpriteFrameName("stage-a-right.png");
        detailB->setAnchorPoint(Vec2(1.0f, 0.0f));
        detailB->setPosition(_visibleSize.width + 20,  _visibleSize.height / 2 - (12 / 2 * 20 + 30));
        _backgroundNode->addChild(detailB, kBackgroundElementsIndex);
        
        
    }
        
    
    /*
    
    if(_stage == STAGE_A)
    {
        backgroundImage = "background-forest.png";
        
        _hasClouds = true;
        _clouds.push_back(createCloud("forest-cloud-1.png", 150, 1.0f, Vec2(50, _visibleSize.height / 2 + 100)));
        _clouds.push_back(createCloud("forest-cloud-2.png", 250, 1.0f, Vec2(220, _visibleSize.height / 2 + 50)));
        _clouds.push_back(createCloud("forest-cloud-3.png", 320, 1.0f, Vec2(380, _visibleSize.height / 2 + 10)));
        
        Sprite* detailA = Sprite::createWithSpriteFrameName("forest-detail-a.png");
        detailA->setAnchorPoint(Vec2(0.0f, 0.0f));
        detailA->setPosition(0 - 20,  _visibleSize.height / 2 - (12 / 2 * 20 + 10));
        _backgroundNode->addChild(detailA, kBackgroundElementsIndex);
        
        Sprite* detailB = Sprite::createWithSpriteFrameName("forest-detail-b.png");
        detailB->setAnchorPoint(Vec2(1.0f, 0.0f));
        detailB->setPosition(_visibleSize.width + 20,  _visibleSize.height / 2 - (12 / 2 * 20 + 10));
        _backgroundNode->addChild(detailB, kBackgroundElementsIndex);

    }
    else if(_stage == STAGE_B)
    {
        backgroundImage = "background-snake.png";
        
        _hasClouds = true;
        _clouds.push_back(createCloud("snake-cloud-1.png", 150, 1.0f, Vec2(40, _visibleSize.height / 2 + 100), true));
        _clouds.push_back(createCloud("snake-cloud-2.png", 400, 1.0f, Vec2(220, _visibleSize.height / 2 + 60), true));
        _clouds.push_back(createCloud("snake-cloud-1.png", 280, 1.0f, Vec2(340, _visibleSize.height / 2 + 20), true));
        _clouds.push_back(createCloud("snake-cloud-2.png", 400, 1.0f, Vec2(420, _visibleSize.height / 2 + 120), true));
        
        Sprite* detailA = Sprite::createWithSpriteFrameName("snake-detail-a.png");
        detailA->setAnchorPoint(Vec2(0.0f, 0.0f));
        detailA->setPosition(0,  _visibleSize.height / 2 - (12 / 2 * 20 - 22));
        _backgroundNode->addChild(detailA, kBackgroundElementsIndex);
        
        
		Sprite* lightTowerBeam = Sprite::createWithSpriteFrameName("snake-light-tower-beam.png");
		lightTowerBeam->setAnchorPoint(Vec2(0.0f, 0.5f));
		lightTowerBeam->setPosition(21, _visibleSize.height / 2 - (12 / 2 * 20 - 160));
		_backgroundNode->addChild(lightTowerBeam, kBackgroundElementsIndex);

		Sprite* lightTowerFlash = Sprite::createWithSpriteFrameName("snake-light-tower-circle.png");
		lightTowerFlash->setPosition(21, _visibleSize.height / 2 - (12 / 2 * 20 - 160));
		lightTowerFlash->setOpacity(0);
		lightTowerFlash->setScale(1.3f);
		_backgroundNode->addChild(lightTowerFlash, kBackgroundElementsIndex);
			
		auto shrinkTo = EaseIn::create(ScaleTo::create(4.0f, 0.0f, 1.3f), 2);
		auto deShrinkTo = EaseOut::create(ScaleTo::create(4.0f, -1.0f, 1.0f), 2);
		auto shrinkAway = EaseIn::create(ScaleTo::create(4.0f, 0.0f, 0.6f), 2);
		auto deShrinkAway = EaseOut::create(ScaleTo::create(4.0f, 1.0f, 1.0f), 2);
		auto beamAction = RepeatForever::create(Sequence::create(shrinkTo, deShrinkTo, shrinkAway, deShrinkAway, NULL));
		lightTowerBeam->runAction(beamAction);

		auto flashFadeIn = FadeIn::create(0.2f);
		auto flashFadeOut = FadeOut::create(0.2f);
		auto flashWait = DelayTime::create(3.6f);
		auto flashWait2 = DelayTime::create(12.0f);
		auto flashAction = RepeatForever::create(Sequence::create(flashWait, flashFadeIn, flashFadeOut, flashWait2, NULL));
		lightTowerFlash->runAction(flashAction);

        
        
        Sprite* detailB = Sprite::createWithSpriteFrameName("snake-detail-b.png");
        detailB->setAnchorPoint(Vec2(1.0f, 0.0f));
        detailB->setPosition(_visibleSize.width,  _visibleSize.height / 2 - (12 / 2 * 20 - 22));
        _backgroundNode->addChild(detailB, kBackgroundElementsIndex);
        
        
        Sprite* ship = Sprite::createWithSpriteFrameName("snake-ship.png");
        ship->setAnchorPoint(Vec2(0.5f, 0.0f));
        ship->setPosition(_visibleSize.width / 2 - 200,  _visibleSize.height / 2 - (12 / 2 * 20 - 22));
        _backgroundNode->addChild(ship, kBackgroundElementsIndex);

		auto shipMoveTo = MoveBy::create(20.0f, Vec2(400.0f, 0.0f));
		auto shipMoveBack = shipMoveTo->reverse();
		auto shipRotateTo = ScaleTo::create(0.0f, -1.0f, 1.0f);
		auto shipRotateBack = ScaleTo::create(0.0f, 1.0f, 1.0f);
		auto shipAction = RepeatForever::create(Sequence::create(shipMoveTo, shipRotateTo, shipMoveBack, shipRotateBack, NULL));
		ship->runAction(shipAction);
      

        
        Sprite* simpleWater1 = Sprite::createWithSpriteFrameName("element-water-simple.png");
        simpleWater1->setAnchorPoint(Vec2(0.5f, 0.0f));
        simpleWater1->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 - 22));
        _backgroundNode->addChild(simpleWater1, kBackgroundElementsIndex);
        
        Sprite* simpleWater2 = Sprite::createWithSpriteFrameName("element-water-simple.png");
        simpleWater2->setAnchorPoint(Vec2(0.5f, 0.0f));
        simpleWater2->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 - 18));
        _backgroundNode->addChild(simpleWater2, kBackgroundElementsIndex);
        
        Sprite* simpleWater3 = Sprite::createWithSpriteFrameName("element-water-simple.png");
        simpleWater3->setAnchorPoint(Vec2(0.5f, 0.0f));
        simpleWater3->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 - 13));
        _backgroundNode->addChild(simpleWater3, kBackgroundElementsIndex);
        
        Sprite* simpleWater4 = Sprite::createWithSpriteFrameName("element-water-simple.png");
        simpleWater4->setAnchorPoint(Vec2(0.5f, 0.0f));
        simpleWater4->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 - 7));
        _backgroundNode->addChild(simpleWater4, kBackgroundElementsIndex);

		auto waterMoveTo = EaseInOut::create(MoveBy::create(3.0f, Vec2(4.0f, 0.0f)), 2);
		auto waterMoveBack = EaseInOut::create(MoveBy::create(3.0f, Vec2(-4.0f, 0.0f)), 2);
		auto waterAction1 = RepeatForever::create(Sequence::createWithTwoActions(waterMoveTo, waterMoveBack));

		waterMoveTo = EaseInOut::create(MoveBy::create(2.5f, Vec2(4.0f, 0.0f)), 2);
		waterMoveBack = EaseInOut::create(MoveBy::create(2.5f, Vec2(-4.0f, 0.0f)), 2);
		auto waterAction2 = RepeatForever::create(Sequence::createWithTwoActions(waterMoveTo, waterMoveBack));

		waterMoveTo = EaseInOut::create(MoveBy::create(2.0f, Vec2(4.0f, 0.0f)), 2);
		waterMoveBack = EaseInOut::create(MoveBy::create(2.0f, Vec2(-4.0f, 0.0f)), 2);
		auto waterAction3 = RepeatForever::create(Sequence::createWithTwoActions(waterMoveTo, waterMoveBack));

		waterMoveTo = EaseInOut::create(MoveBy::create(1.5f, Vec2(4.0f, 0.0f)), 2);
		waterMoveBack = EaseInOut::create(MoveBy::create(1.5f, Vec2(-4.0f, 0.0f)), 2);
		auto waterAction4 = RepeatForever::create(Sequence::createWithTwoActions(waterMoveTo, waterMoveBack));

		simpleWater1->runAction(waterAction1);
		simpleWater2->runAction(waterAction2);
		simpleWater3->runAction(waterAction3);
		simpleWater4->runAction(waterAction4);
        
        drawWater("element-water-%i.png",4);
        
        Sprite* fish1 = Sprite::createWithSpriteFrameName("snake-fish-a.png");
        fish1->setAnchorPoint(Vec2(0.5f, 0.0f));
		float fish1scale = 0.5f;
		fish1->setScale(fish1scale);
        fish1->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 + 25));
        _backgroundNode->addChild(fish1, kBackgroundElementsIndex);
        
        Sprite* fish2 = Sprite::createWithSpriteFrameName("snake-fish-b.png");
        fish2->setAnchorPoint(Vec2(0.5f, 0.0f));
		float fish2scale = 1.0f;
		fish2->setScale(fish2scale);
        fish2->setPosition(_visibleSize.width / 2 - 100,  _visibleSize.height / 2 - (12 / 2 * 20 + 32));
        _backgroundNode->addChild(fish2, kBackgroundElementsIndex);
        
        Sprite* fish3 = Sprite::createWithSpriteFrameName("snake-fish-c.png");
        fish3->setAnchorPoint(Vec2(0.5f, 0.0f));
		float fish3scale = 0.7f;
		fish3->setScale(fish3scale);
		fish3->setScaleX(-fish3scale);
        fish3->setPosition(_visibleSize.width / 2 + 220,  _visibleSize.height / 2 - (12 / 2 * 20 + 35));
        _backgroundNode->addChild(fish3, kBackgroundElementsIndex);
        
        Sprite* fish4 = Sprite::createWithSpriteFrameName("snake-fish-b.png");
        fish4->setAnchorPoint(Vec2(0.5f, 0.0f));
		float fish4scale = 0.8f;
		fish4->setScale(fish4scale);
		fish4->setScaleX(-fish4scale);
        fish4->setPosition(_visibleSize.width / 2 + 50,  _visibleSize.height / 2 - (12 / 2 * 20 + 28));
        _backgroundNode->addChild(fish4, kBackgroundElementsIndex);
        
        Sprite* fish5 = Sprite::createWithSpriteFrameName("snake-fish-a.png");
        fish5->setAnchorPoint(Vec2(0.5f, 0.0f));
		float fish5scale = 1.0f;
		fish5->setScale(fish5scale);
        fish5->setPosition(_visibleSize.width / 2 - 180,  _visibleSize.height / 2 - (12 / 2 * 20 + 30));
        _backgroundNode->addChild(fish5, kBackgroundElementsIndex);

		auto fishMoveTo = EaseInOut::create(MoveBy::create(5.0f, Vec2(200.0f, 0.0f)), 2);
		auto fishMoveBack = EaseInOut::create(MoveBy::create(5.0f, Vec2(-200.0f, 0.0f)), 2);
		auto fishRotateTo = ScaleTo::create(0.0f, -fish1scale, fish1scale);
		auto fishRotateBack = ScaleTo::create(0.0f, fish1scale, fish1scale);
		auto fishAction1 = RepeatForever::create(Sequence::create(fishMoveTo, fishRotateTo, fishMoveBack, fishRotateBack, NULL));

		fishMoveTo = EaseInOut::create(MoveBy::create(8.0f, Vec2(300.0f, 0.0f)), 2);
		fishMoveBack = EaseInOut::create(MoveBy::create(8.0f, Vec2(-300.0f, 0.0f)), 2);
		fishRotateTo = ScaleTo::create(0.0f, -fish2scale, fish2scale);
		fishRotateBack = ScaleTo::create(0.0f, fish2scale, fish2scale);
		auto fishAction2 = RepeatForever::create(Sequence::create(fishMoveTo, fishRotateTo, fishMoveBack, fishRotateBack, NULL));

		fishMoveTo = EaseInOut::create(MoveBy::create(12.0f, Vec2(-440.0f, 0.0f)), 2);
		fishMoveBack = EaseInOut::create(MoveBy::create(12.0f, Vec2(440.0f, 0.0f)), 2);
		fishRotateTo = ScaleTo::create(0.0f, fish3scale, fish3scale);
		fishRotateBack = ScaleTo::create(0.0f, -fish3scale, fish3scale);
		auto fishAction3 = RepeatForever::create(Sequence::create(fishMoveTo, fishRotateTo, fishMoveBack, fishRotateBack, NULL));

		fishMoveTo = EaseInOut::create(MoveBy::create(9.0f, Vec2(-250.0f, 0.0f)), 2);
		fishMoveBack = EaseInOut::create(MoveBy::create(9.0f, Vec2(250.0f, 0.0f)), 2);
		fishRotateTo = ScaleTo::create(0.0f, fish3scale, fish3scale);
		fishRotateBack = ScaleTo::create(0.0f, -fish3scale, fish3scale);
		auto fishAction4 = RepeatForever::create(Sequence::create(fishMoveTo, fishRotateTo, fishMoveBack, fishRotateBack, NULL));

		fishMoveTo = EaseInOut::create(MoveBy::create(15.0f, Vec2(380.0f, 0.0f)), 2);
		fishMoveBack = EaseInOut::create(MoveBy::create(15.0f, Vec2(-380.0f, 0.0f)), 2);
		fishRotateTo = ScaleTo::create(0.0f, -fish3scale, fish3scale);
		fishRotateBack = ScaleTo::create(0.0f, fish3scale, fish3scale);
		auto fishAction5 = RepeatForever::create(Sequence::create(fishMoveTo, fishRotateTo, fishMoveBack, fishRotateBack, NULL));

		fish1->runAction(fishAction1);
		fish2->runAction(fishAction2);
		fish3->runAction(fishAction3);
		fish4->runAction(fishAction4);
		fish5->runAction(fishAction5);        
    }
    else if(_stage == STAGE_C)
    {
        backgroundImage = "background-laser.png";
        
        auto bolbEffect = Sprite::createWithSpriteFrameName("laser-detail-a-0.png"); // empty frame before -> laser-detail-a-0.png -> laser-detail-a-2.png
        bolbEffect->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 + 150));
        bolbEffect->setOpacity(80.0f);
        this->addChild(bolbEffect, kBackgroundElementsIndex);
        
        // [GATIM] šiem ir jākustas, vari pavairot skaitu, varbūt kā zivīm
        
        auto doctor1 =  Sprite::createWithSpriteFrameName("laser-detail-doctor.png");
        doctor1->setPosition(Vec2(_visibleSize.width / 2 - 238, _visibleSize.height / 2 - 6));
        this->addChild(doctor1, kBackgroundElementsIndex);
        
        auto doctor2 =  Sprite::createWithSpriteFrameName("laser-detail-doctor.png");
        doctor2->setPosition(Vec2(_visibleSize.width / 2 - 100, _visibleSize.height / 2 - 81));
        this->addChild(doctor2, kBackgroundElementsIndex);
        
        auto doctor3 =  Sprite::createWithSpriteFrameName("laser-detail-doctor.png");
        doctor3->setPosition(Vec2(_visibleSize.width / 2 - 164, _visibleSize.height / 2 - 31));
        this->addChild(doctor3, kBackgroundElementsIndex);
        
        auto doctor4 =  Sprite::createWithSpriteFrameName("laser-detail-doctor.png");
        doctor4->setPosition(Vec2(_visibleSize.width / 2 + 10, _visibleSize.height / 2 - 64));
        doctor4->setScaleX(-1.0f);
        this->addChild(doctor4, kBackgroundElementsIndex);
        
        auto doctor5 =  Sprite::createWithSpriteFrameName("laser-detail-doctor.png");
        doctor5->setPosition(Vec2(_visibleSize.width / 2 - 70, _visibleSize.height / 2 - 81));
        doctor5->setScaleX(-1.0f);
        this->addChild(doctor5, kBackgroundElementsIndex);
        
        auto doctor6 =  Sprite::createWithSpriteFrameName("laser-detail-doctor.png");
        doctor6->setPosition(Vec2(_visibleSize.width / 2 + 50, _visibleSize.height / 2 - 78));
        this->addChild(doctor6, kBackgroundElementsIndex);
        
        auto doctor7 =  Sprite::createWithSpriteFrameName("laser-detail-doctor.png");
        doctor7->setPosition(Vec2(_visibleSize.width / 2 + 70, _visibleSize.height / 2 - 80));
        doctor7->setScaleX(-1.0f);
        this->addChild(doctor7, kBackgroundElementsIndex);

		auto doctorMoveTo = EaseInOut::create(MoveBy::create(2.0f, Vec2(15.0f, 2.0f)), 2);
		auto doctorMoveBack = EaseInOut::create(MoveBy::create(2.0f, Vec2(-15.0f, -2.0f)), 2);
		auto doctorRotateTo = ScaleTo::create(0.0f, -1, 1);
		auto doctorRotateBack = ScaleTo::create(0.0f, 1, 1);
		auto doctorAction1 = RepeatForever::create(Sequence::create(doctorMoveTo, doctorRotateTo, doctorMoveBack, doctorRotateBack, NULL));

		doctorMoveTo = EaseInOut::create(MoveBy::create(5.0f, Vec2(70.0f, 6.0f)), 2);
		doctorMoveBack = EaseInOut::create(MoveBy::create(5.0f, Vec2(-70.0f, -6.0f)), 2);
		doctorRotateTo = ScaleTo::create(0.0f, -1, 1);
		doctorRotateBack = ScaleTo::create(0.0f, 1, 1);
		auto doctorAction2 = RepeatForever::create(Sequence::create(doctorMoveTo, doctorRotateTo, doctorMoveBack, doctorRotateBack, NULL));

		doctorMoveTo = EaseInOut::create(MoveBy::create(1.0f, Vec2(5.0f, -2.0f)), 2);
		doctorMoveBack = EaseInOut::create(MoveBy::create(1.0f, Vec2(-5.0f, 2.0f)), 2);
		auto doctorMoveDown = EaseInOut::create(MoveBy::create(3.0f, Vec2(0.0f, -30.0f)), 2);
		auto doctorMoveUp = EaseInOut::create(MoveBy::create(3.0f, Vec2(0.0f, 30.0f)), 2);
		auto delay = DelayTime::create(2.0f);
		doctorRotateTo = ScaleTo::create(0.0f, -1, 1);
		doctorRotateBack = ScaleTo::create(0.0f, 1, 1);
		auto doctorAction3 = RepeatForever::create(Sequence::create(doctorMoveTo, doctorRotateTo, doctorMoveDown, doctorRotateBack, doctorMoveTo, doctorRotateTo, doctorMoveBack, doctorMoveUp, doctorMoveBack, doctorRotateBack, delay, NULL));

		doctorMoveTo = EaseInOut::create(MoveBy::create(2.0f, Vec2(-20.0f, 0.0f)), 1);
		doctorMoveBack = EaseInOut::create(MoveBy::create(2.0f, Vec2(20.0f, 0.0f)), 1);
		doctorRotateTo = ScaleTo::create(0.0f, 1, 1);
		doctorRotateBack = ScaleTo::create(0.0f, -1, 1);
		auto doctorAction4 = RepeatForever::create(Sequence::create(doctorMoveTo, doctorRotateTo, delay, doctorMoveBack, doctorRotateBack, delay, NULL));

		doctorMoveTo = EaseInOut::create(MoveBy::create(8.0f, Vec2(-60.0f, -8.0f)), 1);
		doctorMoveBack = EaseInOut::create(MoveBy::create(8.0f, Vec2(60.0f, 8.0f)), 1);
		delay = DelayTime::create(1.0f);
		doctorRotateTo = ScaleTo::create(0.0f, 1, 1);
		doctorRotateBack = ScaleTo::create(0.0f, -1, 1);
		auto doctorAction5 = RepeatForever::create(Sequence::create(doctorMoveTo, doctorRotateTo, delay, doctorMoveBack, doctorRotateBack, delay, NULL));

		doctorMoveTo = EaseInOut::create(MoveBy::create(4.0f, Vec2(55.0f, 4.0f)), 2);
		doctorMoveBack = EaseInOut::create(MoveBy::create(4.0f, Vec2(-55.0f, -4.0f)), 2);
		doctorRotateTo = ScaleTo::create(0.0f, -1, 1);
		doctorRotateBack = ScaleTo::create(0.0f, 1, 1);
		auto doctorAction6 = RepeatForever::create(Sequence::create(doctorMoveTo, doctorRotateTo, doctorMoveBack, doctorRotateBack, NULL));

		doctorMoveTo = EaseInOut::create(MoveBy::create(5.0f, Vec2(-50.0f, -2.0f)), 2);
		doctorMoveBack = EaseInOut::create(MoveBy::create(5.0f, Vec2(50.0f, 2.0f)), 2);
		doctorRotateTo = ScaleTo::create(0.0f, 1, 1);
		doctorRotateBack = ScaleTo::create(0.0f, -1, 1);
		auto doctorAction7 = RepeatForever::create(Sequence::create(doctorMoveTo, doctorRotateTo, doctorMoveBack, doctorRotateBack, NULL));

		doctor1->runAction(doctorAction1);
		doctor2->runAction(doctorAction2);
		doctor3->runAction(doctorAction3);
		doctor4->runAction(doctorAction4);
		doctor5->runAction(doctorAction5);
		doctor6->runAction(doctorAction6);
		doctor7->runAction(doctorAction7);

        drawGround("laser-tile-platform.png");
    }
    else if(_stage == STAGE_D)
    {
        backgroundImage = "background-hell.png";
        
        Sprite* simpleFire1 = Sprite::createWithSpriteFrameName("element-fire-simple.png");
        simpleFire1->setAnchorPoint(Vec2(0.5f, 0.0f));
        simpleFire1->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 - 22));
        _backgroundNode->addChild(simpleFire1, kBackgroundElementsIndex);
        
        Sprite* simpleFire2 = Sprite::createWithSpriteFrameName("element-fire-simple.png");
        simpleFire2->setAnchorPoint(Vec2(0.5f, 0.0f));
        simpleFire2->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 - 16));
        _backgroundNode->addChild(simpleFire2, kBackgroundElementsIndex);
        
        Sprite* simpleFire3 = Sprite::createWithSpriteFrameName("element-fire-simple.png");
        simpleFire3->setAnchorPoint(Vec2(0.5f, 0.0f));
        simpleFire3->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 - 9));
        _backgroundNode->addChild(simpleFire3, kBackgroundElementsIndex);
        
        auto fireMoveTo = EaseInOut::create(MoveBy::create(3.0f, Vec2(4.0f, 0.0f)), 2);
        auto fireMoveBack = EaseInOut::create(MoveBy::create(3.0f, Vec2(-4.0f, 0.0f)), 2);
        auto fireAction1 = RepeatForever::create(Sequence::createWithTwoActions(fireMoveTo, fireMoveBack));
        
        fireMoveTo = EaseInOut::create(MoveBy::create(2.5f, Vec2(4.0f, 0.0f)), 2);
        fireMoveBack = EaseInOut::create(MoveBy::create(2.5f, Vec2(-4.0f, 0.0f)), 2);
        auto fireAction2 = RepeatForever::create(Sequence::createWithTwoActions(fireMoveTo, fireMoveBack));
        
        fireMoveTo = EaseInOut::create(MoveBy::create(2.0f, Vec2(4.0f, 0.0f)), 2);
        fireMoveBack = EaseInOut::create(MoveBy::create(2.0f, Vec2(-4.0f, 0.0f)), 2);
        auto fireAction3 = RepeatForever::create(Sequence::createWithTwoActions(fireMoveTo, fireMoveBack));
        
        
        simpleFire1->runAction(fireAction1);
        simpleFire2->runAction(fireAction2);
        simpleFire3->runAction(fireAction3);
        
        // Bats
		_animationBat = createAnimation("hell-bat-%i.png", 3, 0.1f);
        
        Sprite* bat1 = Sprite::createWithSpriteFrameName("hell-bat-0.png"); // hell-bat-0.png -> hell-bat-2.png
        bat1->setPosition(_visibleSize.width / 2 - 150,  _visibleSize.height / 2 + 50);
		bat1->runAction(RepeatForever::create(Animate::create(_animationBat)));
		float bat1scale = 0.4f;
		bat1->setScale(bat1scale);
		bat1->setScaleX(-bat1scale);
        _backgroundNode->addChild(bat1, kBackgroundElementsIndex);
        
        Sprite* bat2 = Sprite::createWithSpriteFrameName("hell-bat-0.png");
        bat2->setPosition(_visibleSize.width / 2 + 150,  _visibleSize.height / 2 + 10);
		bat2->runAction(RepeatForever::create(Animate::create(_animationBat->clone())));
		float bat2scale = 0.6f;
		bat2->setScale(bat2scale);
        _backgroundNode->addChild(bat2, kBackgroundElementsIndex);
        
        Sprite* bat3 = Sprite::createWithSpriteFrameName("hell-bat-0.png");
        bat3->setPosition(_visibleSize.width / 2 - 200,  _visibleSize.height / 2 - 60);
		bat3->runAction(RepeatForever::create(Animate::create(_animationBat->clone())));
		float bat3scale = 0.4f;
		bat3->setScale(bat3scale);
		bat3->setScaleX(-bat3scale);
        _backgroundNode->addChild(bat3, kBackgroundElementsIndex);

		auto batMoveTo = EaseInOut::create(MoveBy::create(1.0f, Vec2(0.0f, 20.0f)), 2);
		auto batMoveBack = EaseInOut::create(MoveBy::create(1.0f, Vec2(0.0f, -20.0f)), 2);
		auto batActionFloat1 = RepeatForever::create(Sequence::create(batMoveTo, batMoveBack, NULL));
		auto batActionFloat2 = RepeatForever::create(Sequence::create(batMoveBack, batMoveTo, NULL));
		batMoveTo = EaseInOut::create(MoveBy::create(10.0f, Vec2(300.0f, 0.0f)), 2);
		batMoveBack = EaseInOut::create(MoveBy::create(10.0f, Vec2(-300.0f, 0.0f)), 2);
		auto batRotateTo = ScaleTo::create(0.0f, bat1scale, bat1scale);
		auto batRotateBack = ScaleTo::create(0.0f, -bat1scale, bat1scale);
		auto batAction1 = RepeatForever::create(Sequence::create(batMoveTo, batRotateTo, batMoveBack, batRotateBack, NULL));

		batMoveTo = EaseInOut::create(MoveBy::create(10.0f, Vec2(-300.0f, 0.0f)), 2);
		batMoveBack = EaseInOut::create(MoveBy::create(10.0f, Vec2(300.0f, 0.0f)), 2);
		batRotateTo = ScaleTo::create(0.0f, -bat2scale, bat2scale);
		batRotateBack = ScaleTo::create(0.0f, bat2scale, bat2scale);
		auto batAction2 = RepeatForever::create(Sequence::create(batMoveTo, batRotateTo, batMoveBack, batRotateBack, NULL));

		batMoveTo = EaseInOut::create(MoveBy::create(15.0f, Vec2(300.0f, 0.0f)), 2);
		batMoveBack = EaseInOut::create(MoveBy::create(15.0f, Vec2(-300.0f, 0.0f)), 2);
		batRotateTo = ScaleTo::create(0.0f, bat3scale, bat3scale);
		batRotateBack = ScaleTo::create(0.0f, -bat3scale, bat3scale);
		auto batAction3 = RepeatForever::create(Sequence::create(batMoveTo, batRotateTo, batMoveBack, batRotateBack, NULL));

		batMoveTo = EaseInOut::create(MoveBy::create(4.0f, Vec2(0.0f, 120.0f)), 2);
		batMoveBack = EaseInOut::create(MoveBy::create(4.0f, Vec2(0.0f, -120.0f)), 2);
		auto batActionFloat3 = RepeatForever::create(Sequence::create(batMoveTo, batMoveBack, NULL));

		bat1->runAction(batActionFloat1);
		bat1->runAction(batAction1);
		bat2->runAction(batActionFloat2);
		bat2->runAction(batAction2);
		bat3->runAction(batActionFloat3);
		bat3->runAction(batAction3);


        
        Sprite* detailA = Sprite::createWithSpriteFrameName("hell-detail-a.png");
        detailA->setAnchorPoint(Vec2(0.0f, 0.0f));
        detailA->setPosition(0,  _visibleSize.height / 2 - (12 / 2 * 20 + 10));
        _backgroundNode->addChild(detailA, kBackgroundElementsIndex);
        
        Sprite* detailB = Sprite::createWithSpriteFrameName("hell-detail-b.png");
        detailB->setAnchorPoint(Vec2(1.0f, 0.0f));
        detailB->setPosition(_visibleSize.width,  _visibleSize.height / 2 - (12 / 2 * 20 + 10));
        _backgroundNode->addChild(detailB, kBackgroundElementsIndex);
        
        Sprite* detailC = Sprite::createWithSpriteFrameName("hell-detail-c.png");
        detailC->setAnchorPoint(Vec2(0.5f, 0.0f));
        detailC->setPosition(_visibleSize.width / 2,  _visibleSize.height / 2 - (12 / 2 * 20 + 10));
        _backgroundNode->addChild(detailC, kBackgroundElementsIndex);
        
		Sprite* puppyInPot = Sprite::createWithSpriteFrameName("hell-detail-e.png");
		puppyInPot->setPosition(_visibleSize.width / 2 - 20, _visibleSize.height / 2 - 30);
		_backgroundNode->addChild(puppyInPot, kBackgroundElementsIndex);

        Sprite* puppyDevil = Sprite::createWithSpriteFrameName("hell-detail-d.png");
        puppyDevil->setPosition(_visibleSize.width / 2 + 18,  _visibleSize.height / 2 - 26);
        _backgroundNode->addChild(puppyDevil, kBackgroundElementsIndex);

		auto devilMoveTo = EaseInOut::create(MoveBy::create(2.0f, Vec2(-15.0f, 0.0f)), 2);
		auto devilMoveBack = EaseInOut::create(MoveBy::create(2.0f, Vec2(15.0f, 0.0f)), 2);
		auto delay = DelayTime::create(2.0f);
		auto devilRotateTo = ScaleTo::create(0.0f, -1, 1);
		auto devilRotateBack = ScaleTo::create(0.0f, 1, 1);
		auto devilAction = RepeatForever::create(Sequence::create(devilMoveTo, delay, devilRotateTo, devilMoveBack, devilRotateBack, delay, NULL));
		puppyDevil->runAction(devilAction);


        //BELLS

		Sprite* bellDong1 = Sprite::createWithSpriteFrameName("hell-bell-dong.png");
		bellDong1->setAnchorPoint(Vec2(.5f, 1.2f));
		bellDong1->setRotation(-25.0f);
		bellDong1->setPosition(_visibleSize.width / 2 - 132, _visibleSize.height - 48);
		_backgroundNode->addChild(bellDong1, kBackgroundElementsIndex);
        
        Sprite* bell1 = Sprite::createWithSpriteFrameName("hell-bell.png");
        bell1->setAnchorPoint(Vec2(.5f, .8f));
	    bell1->setRotation(25.0f);
        bell1->setPosition(_visibleSize.width / 2 - 132,  _visibleSize.height - 5);
        _backgroundNode->addChild(bell1, kBackgroundElementsIndex);
        
		Sprite* bellDong2 = Sprite::createWithSpriteFrameName("hell-bell-dong.png");
		bellDong2->setAnchorPoint(Vec2(.5f, 1.2f));
		bellDong2->setRotation(-25.0f);
		bellDong2->setPosition(_visibleSize.width / 2, _visibleSize.height - 48);
		_backgroundNode->addChild(bellDong2, kBackgroundElementsIndex);

        Sprite* bell2 = Sprite::createWithSpriteFrameName("hell-bell.png");
        bell2->setAnchorPoint(Vec2(.5f, .8f));
		bell2->setRotation(25.0f);
        bell2->setPosition(_visibleSize.width / 2,  _visibleSize.height - 5);
        _backgroundNode->addChild(bell2, kBackgroundElementsIndex);
        
		Sprite* bellDong3 = Sprite::createWithSpriteFrameName("hell-bell-dong.png");
		bellDong3->setAnchorPoint(Vec2(.5f, 1.2f));
		bellDong3->setRotation(-25.0f);
		bellDong3->setPosition(_visibleSize.width / 2 + 132, _visibleSize.height - 48);
		_backgroundNode->addChild(bellDong3, kBackgroundElementsIndex);

        Sprite* bell3 = Sprite::createWithSpriteFrameName("hell-bell.png");
        bell3->setAnchorPoint(Vec2(.5f, .8f));
		bell3->setRotation(25.0f);
        bell3->setPosition(_visibleSize.width / 2 + 132,  _visibleSize.height - 5);
        _backgroundNode->addChild(bell3, kBackgroundElementsIndex);

		auto bellSwingRight = EaseInOut::create(RotateBy::create(2.0f, -50.0f), 2);
		auto bellSwingLeft = EaseInOut::create(RotateBy::create(2.0f, 50.0f), 2);
		delay = DelayTime::create(0.1f);
		auto bellDongSwingRight = RotateBy::create(2.0f, -50.0f);
		auto bellDongSwingLeft = bellDongSwingRight->reverse();
		auto bellAction = RepeatForever::create(Sequence::create(bellSwingRight, delay, bellSwingLeft, delay, NULL));
		auto bellDongAction = RepeatForever::create(Sequence::create(bellDongSwingLeft, delay, bellDongSwingRight, delay, NULL));
        
		bell1->runAction(bellAction);
		bell2->runAction(bellAction->clone());
		bell3->runAction(bellAction->clone());

		bellDong1->runAction(bellDongAction);
		bellDong2->runAction(bellDongAction->clone());
		bellDong3->runAction(bellDongAction->clone());

        drawWater("element-fire-%i.png",3);
    }
     
     */

	auto background = Sprite::createWithSpriteFrameName(backgroundImage);
    background->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2));
    background->setAnchorPoint(Vec2(0.5f, 0.5f));
    _backgroundNode->addChild(background, kBackgroundIndex);
    
    
    addChild(_backgroundNode);

    addRain();

	return true;
}

void BackgroundLayer::update(float dt)
{
    if(_hasClouds)
    {
        for (auto cloud : _clouds)
        {
            if (cloud->getPositionX() < cloud->getContentSize().width / 2  * -1)
                cloud->setPositionX(_visibleSize.width + cloud->getContentSize().width / 2);
            cloud->move(dt);
        }
    }
}

void BackgroundLayer::shake(float strength)
{
    strength = strength;
    
    CCLOG("Shake %.2f", strength);
    
    auto shake1 = MoveBy::create(0.05f, Vec2(strength, strength));
    auto shake2 = MoveBy::create(0.05f, Vec2(strength * -2, 0.0f));
    auto shake3 = MoveBy::create(0.05f, Vec2(strength, strength * -1));
    
    auto shakeItBaby = Sequence::create(shake1, shake2, shake3, NULL);
    
    _backgroundNode->stopAllActions();
    _backgroundNode->runAction(shakeItBaby);
}

Cloud* BackgroundLayer::createCloud(std::string fileName, int speed, float scale, cocos2d::Vec2 position, bool allowRotate)
{
	auto cloud = Cloud::create(fileName);
	cloud->setCloudSpeed(speed);
	cloud->setPosition(position);
	cloud->setScale(scale);
	cloud->setAnchorPoint(Vec2(0.5f, 0.5f));
	_backgroundNode->addChild(cloud, kCloudsIndex);
    
    if(allowRotate)
    {
		auto cloudRotate = RepeatForever::create(RotateBy::create(30.0f, -360.0f));
		cloud->runAction(cloudRotate);
    }
    
	return cloud;
}

void BackgroundLayer::drawWater(std::string tile, int frameCount)
{
    auto waterBack = Sprite::createWithSpriteFrameName("element-water-0.png");
    waterBack->setPosition(Vec2(_visibleSize.width / 2, (_visibleSize.height / 2 - (12 / 2 * 20 + 5))));
    waterBack->setAnchorPoint(Vec2(0.5f, 0.5f));
    _backgroundNode->addChild(waterBack, kGroundIndex);
    
   
    
    Vector<SpriteFrame*> waterBackAnimation(frameCount);
    char str[100] = { 0 };
    for (int i = 0; i < frameCount; i++){
        sprintf(str, tile.c_str(), i);
        auto _animframe = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
        waterBackAnimation.pushBack(_animframe);
    }
    auto _animationWaterBack = Animation::createWithSpriteFrames(waterBackAnimation, 0.2f);
    _animationWaterBack->retain();
    waterBack->runAction(RepeatForever::create(Animate::create(_animationWaterBack)));
    
    auto move = MoveBy::create(1.5f, Vec2(7, 0));
    auto moveBack = move->reverse();
    auto moveRight = EaseInOut::create(move, 1);
    auto moveLeft = EaseInOut::create(moveBack, 1);
    auto waterBackFloat = Sequence::createWithTwoActions(moveRight, moveLeft);
    waterBack->runAction(RepeatForever::create(waterBackFloat));
 
    if (Manager::getInstance()->getDebugTools())
    {
        waterBack->setOpacity(50);
    }
}

void BackgroundLayer::drawGround(std::string tile)
{
    int tileCount = (int)(_visibleSize.width / 20) + 1;
    
    for(int i = 0; i < tileCount; i++)
    {
        auto sprite = Sprite::createWithSpriteFrameName(tile);
        sprite->setAnchorPoint(Vec2(0.5f, 0.5f));
        sprite->setPosition((20 * i), _visibleSize.height / 2 - (12 / 2 * 20 + 20));
        
        if(Manager::getInstance()->getDebugTools())
            sprite->setOpacity(50);
        
        _backgroundNode->addChild(sprite, kGroundIndex);
    }
}




void BackgroundLayer::addRain()
{
//    ParticleExplosion* explosion = ParticleExplosion::create();
//    this->addChild(explosion, kWeather);
//    ParticleSmoke* smoke = ParticleSmoke::create();
//    this->addChild(smoke, kWeather);
    
//    auto test = ParticleFlower::create();
//    this->addChild(test);
    
//    auto test2 = ParticleGalaxy::create();
//    this->addChild(test2);
    
//    _particleEffect = ParticleSystemQuad::create();
//    _particleEffect = ParticleFlower::createWithTotalParticles(1000);
    
//    auto rainDrop = Sprite::createWithSpriteFrameName("fx-raindrop.png");
//    ParticleRain* rain = ParticleRain::createWithTotalParticles(1000);
//    rain->setTexture(rainDrop->getTexture());
//    rain->setGravity(Vec2(-1,-1));
    
    //rain->setTexture(Director::getInstance()->getTextureCache()->ad
//
//    rain->setAngle(180);
//    rain->setPosition(0, _visibleSize.height / 2);
//    rain->setAnchorPoint(Vec2(0.0f, 0.0f));
//    rain->setContentSize(_visibleSize);
//    this->addChild(rain, kWeather);
}



cocos2d::Node* BackgroundLayer::getFrontLayer(int stage)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto node = Node::create();
 
    if(stage == STAGE_B)
    {
        
            
            auto waterFront = Sprite::createWithSpriteFrameName("element-water-0.png");
            waterFront->setPosition(Vec2(visibleSize.width / 2, (visibleSize.height / 2 - (12 / 2 * 20 + 10))));
            waterFront->setAnchorPoint(Vec2(0.5f, 0.5f));
            node->addChild(waterFront, kBackIndex);
            
            char str[100] = { 0 };
            Vector<SpriteFrame*> waterFrontAnimation(4);
            for (int i = 0; i < 4; i++){
                sprintf(str, "element-water-%i.png", i);
                auto _animframe = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
                waterFrontAnimation.pushBack(_animframe);
            }
            auto _animationWaterFront = Animation::createWithSpriteFrames(waterFrontAnimation, 0.2f);
            _animationWaterFront->retain();
            waterFront->runAction(RepeatForever::create(Animate::create(_animationWaterFront)));
            
            
            auto move = MoveBy::create(1.5f, Vec2(-7, 0));
            auto moveBack = move->reverse();
            auto moveRight = EaseInOut::create(move, 1);
            auto moveLeft = EaseInOut::create(moveBack, 1);
            auto waterFrontFloat = Sequence::createWithTwoActions(moveRight, moveLeft);
            waterFront->runAction(RepeatForever::create(waterFrontFloat));
            
            move = MoveBy::create(4, Vec2(0, -2));
            moveBack = move->reverse();
            auto moveUp = EaseInOut::create(move, 2);
            auto moveDown = EaseInOut::create(moveBack, 2);
            auto waterFrontFloat2 = Sequence::createWithTwoActions(moveUp, moveDown);
            waterFront->runAction(RepeatForever::create(waterFrontFloat2));
            
            
            if (Manager::getInstance()->getDebugTools())
            {
                waterFront->setOpacity(50);
            }
            
    }
    else if(stage == STAGE_D)
    {
        
            
            auto fireFront = Sprite::createWithSpriteFrameName("element-fire-0.png");
            fireFront->setPosition(Vec2(visibleSize.width / 2, (visibleSize.height / 2 - (12 / 2 * 20 + 10))));
            fireFront->setAnchorPoint(Vec2(0.5f, 0.5f));
            node->addChild(fireFront, kBackIndex);
            
            char str[100] = { 0 };
            Vector<SpriteFrame*> fireFrontAnimation(3);
            for (int i = 0; i < 3; i++){
                sprintf(str, "element-fire-%i.png", i);
                auto _animframe = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
                fireFrontAnimation.pushBack(_animframe);
            }
            auto _animationWaterFront = Animation::createWithSpriteFrames(fireFrontAnimation, 0.2f);
            _animationWaterFront->retain();
            fireFront->runAction(RepeatForever::create(Animate::create(_animationWaterFront)));
            
            
            auto move = MoveBy::create(1.5f, Vec2(-7, 0));
            auto moveBack = move->reverse();
            auto moveRight = EaseInOut::create(move, 1);
            auto moveLeft = EaseInOut::create(moveBack, 1);
            auto waterFrontFloat = Sequence::createWithTwoActions(moveRight, moveLeft);
            fireFront->runAction(RepeatForever::create(waterFrontFloat));
            
            move = MoveBy::create(4, Vec2(0, -2));
            moveBack = move->reverse();
            auto moveUp = EaseInOut::create(move, 2);
            auto moveDown = EaseInOut::create(moveBack, 2);
            auto waterFrontFloat2 = Sequence::createWithTwoActions(moveUp, moveDown);
            fireFront->runAction(RepeatForever::create(waterFrontFloat2));
            
            
            if (Manager::getInstance()->getDebugTools())
            {
                fireFront->setOpacity(50);
            }
        
    }// */
    return node;
}



Cloud* Cloud::create(std::string fileName)
{
	auto sprite = new Cloud;
	if (sprite && sprite->initWithSpriteFrameName(fileName))
	{
		sprite->autorelease();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return nullptr;
}

void Cloud::move(float dt)
{
	this->setPositionX(this->getPositionX() - (this->getCloudSpeed() * dt));
}


cocos2d::Animation* BackgroundLayer::createAnimation(const char* name, int frameCount, float frameTime, bool reverse, int offsetX, int offsetY)
{
	Vector<SpriteFrame*> animationVector(frameCount);
	SpriteFrame* animFrame;
	Vec2 offset = Vec2(offsetX, offsetY);
	char str[100] = { 0 };
	if (!reverse){
		for (int i = 0; i < frameCount; i++){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	else
	{
		for (int i = frameCount - 1; i >= 0; i--){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	auto animation = Animation::createWithSpriteFrames(animationVector, frameTime);
	return animation;
}