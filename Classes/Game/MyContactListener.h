#ifndef PuppyMadness_MyContactListener_h
#define PuppyMadness_MyContactListener_h

#include "Box2D/Box2D.h"
#include <vector>


class MyContactListener : public b2ContactListener
{
public:

	MyContactListener(){ };
	~MyContactListener(){ };

	virtual void BeginContact(b2Contact* contact);

	virtual void PreSolve(b2Contact* contact);

	virtual void EndContact(b2Contact* contact);

private:

	std::vector<b2Contact*>_fenceContacts;
};

#endif