#ifndef PuppyMadness_MainScene_h
#define PuppyMadness_MainScene_h

#include "cocos2d.h"

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM || CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM)
#include "Manager/Manager.h"

#else
#include "Manager.h"


#endif


const int zIndexPopUp = 1000;


class MainScene : public cocos2d::LayerColor{
public:
    static cocos2d::Scene* createScene(std::string str);

    virtual bool init(std::string str);
    
    static MainScene* create(std::string str);
    
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
    
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);

private:
    
    cocos2d::Size _visibleSize;
    cocos2d::Point _originPoint;
    
    void goToStore(std::string str = "");
    
    void goToSettings();
    
    void goPlay(std::string level);

	void initAnimations();
    
    cocos2d::Node* _stageMap;
    cocos2d::Node* _store;

    cocos2d::Node* _userInfo;
    cocos2d::Node* _tactics;
    cocos2d::Node* _namePlates;
    cocos2d::Sprite* _settingsBtn;
    
    void drawTouchPoint(std::string tag, cocos2d::Rect rect);
    
    cocos2d::Node* createNamePlate(std::string stage);

	//Linear animation sequence
	cocos2d::Animation* createAnimation(const char* name, int frameCount, float frameTime, bool reverse = false, int offsetX = 0, int offsetY = 0);

	//Non-linear animation sequence
	cocos2d::Animation* createAnimation(const char* name, std::vector<int> frameVector, float frameTime);
    
    std::map<std::string, cocos2d::Rect> _touchPoints;
    
    void showStage(std::string stage);
    void hideStage();
    
    void createStage(std::string stage);
    cocos2d::Node* drawBox(cocos2d::Size size, cocos2d::Vec2 position, cocos2d::Color4B color);
    cocos2d::Node* createLeaderBoards(std::string stage);
    cocos2d::Node* createQuest(Quest* quest, cocos2d::Size size, bool daily = false, int nr = 0);
    std::map<std::string, cocos2d::Node*> _stages;
    cocos2d::Node* _shadow;

	cocos2d::Animation* _fly1Animation;
	cocos2d::Animation* _fly2Animation;
	cocos2d::Animation* _butterfly1Animation;
	cocos2d::Animation* _butterfly2Animation;

};

#endif
