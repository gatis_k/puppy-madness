#ifndef Preload_Scene_h
#define Preload_Scene_h

#include "cocos2d.h"

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM || CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM)
#include "Manager/Manager.h"
#else
#include "Manager.h"
#endif

const cocos2d::Size kLoadingBar(300, 20);

class PreloadScene : public cocos2d::LayerColor
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(PreloadScene);
    
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);

private:
	void AssetsCounter();
	void LoadAssets(float dt);
    void enableTouch(float dt);
	void runMainScene();

	int _assetsCount;
	bool _assetIsLoading;
    bool _loadingIsDone;

//	cocos2d::Label* _loadingProgress;
    
    cocos2d::Node* _loadingProgress;
    
    cocos2d::Label* _touchToStart;
    
    
	std::string sprite_resource;
    std::vector<std::string> _spriteSheets;
    std::vector<std::string> _tips;
    char _charText[100];
};

#endif