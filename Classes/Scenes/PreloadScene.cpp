#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM || CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM)
#include "Scenes/PreloadScene.h"
#include "Manager/Manager.h"

#else
#include "PreloadScene.h"
#include "Manager.h"
#endif

USING_NS_CC;

typedef struct tagResource
{
	cocos2d::Size size;
	char directory[100];
}Resource;

static Resource smallResource = { cocos2d::Size(512, 320), "-sd" }; // 3x2
static Resource mediumResource = { cocos2d::Size(1024, 640), "-hd" };
static Resource largeResource = { cocos2d::Size(2048, 1280), "-hdr" };

Scene* PreloadScene::createScene()
{
	auto scene = Scene::create();
	auto layer = PreloadScene::create();
	scene->addChild(layer);
	return scene;
}

bool PreloadScene::init()
{
	if (!LayerColor::initWithColor(Color4B(144,188,209,255)))
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	Size frameSize = Director::getInstance()->getOpenGLView()->getFrameSize();

	if (frameSize.height > mediumResource.size.height)
	{
		sprite_resource = largeResource.directory;
	}
	else if (frameSize.height > smallResource.size.height)
	{
		sprite_resource = mediumResource.directory;
	}
	else
	{
		sprite_resource = smallResource.directory;
	}
    
    Label* title = Label::createWithTTF("PUPPY MADNESS TITLE", kFontPath + kFontNameHUD, 24);
    title->setPosition(Point(visibleSize.width / 2, visibleSize.height / 2));
    title->setAnchorPoint(Vec2(0.5, 0.5));
    title->setColor(Color3B::WHITE);
    title->enableOutline(Color4B::BLACK, 1);
    this->addChild(title);
    
    
    //float percWidth = quest->getAmountNow() * 100 / quest->getAmountGoal();
    
    
    _loadingProgress = Node::create();
    
    auto percDrawNode = DrawNode::create();
    Vec2 percRectangle[4];
    percRectangle[0] = Vec2(0, 0);
    percRectangle[1] = Vec2(kLoadingBar.width, 0);
    percRectangle[2] = Vec2(kLoadingBar.width, kLoadingBar.height);
    percRectangle[3] = Vec2(0, kLoadingBar.height);
    percDrawNode->drawPolygon(percRectangle, 4, Color4F::RED, 0, Color4F::BLACK);
    percDrawNode->setContentSize(kLoadingBar);
    
    _loadingProgress->setContentSize(kLoadingBar);
    _loadingProgress->setAnchorPoint(Vec2(0.0f, 0.5f));
    _loadingProgress->setPosition(visibleSize.width / 2 - kLoadingBar.width / 2, visibleSize.height / 5 + 20);
    _loadingProgress->addChild(percDrawNode);
    
    this->addChild(_loadingProgress);
    
    _loadingProgress->setScaleX(0.0f);
    
    
    _touchToStart = Label::createWithTTF("Touch to Start", kFontPath + kFontNameHUD, 18);
    _touchToStart->setPosition(Point(visibleSize.width / 2, visibleSize.height / 5 + 20));
    _touchToStart->setAnchorPoint(Vec2(0.5, 0.5));
    _touchToStart->setColor(Color3B::RED);
    _touchToStart->enableOutline(Color4B::BLACK, 1);
    _touchToStart->setVisible(false);
    this->addChild(_touchToStart);

	_assetsCount = 0;
	_assetIsLoading = false;
    _loadingIsDone = false;
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(PreloadScene::onTouchBegan, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    
    Manager::getInstance()->getSoundsLibrary()->preloadSounds();
    
	schedule(schedule_selector(PreloadScene::LoadAssets));

    // new spritesheets
    _spriteSheets.push_back("ui-elements");
    _spriteSheets.push_back("stage");
    _spriteSheets.push_back("store");
    _spriteSheets.push_back("main");
    _spriteSheets.push_back("background-main");
    _spriteSheets.push_back("background-stage-a");
    _spriteSheets.push_back("elena-basic");
    _spriteSheets.push_back("elena-medic");
    _spriteSheets.push_back("puppies");
    //_spriteSheets.push_back("tactics");
    
    // old spritesheets
    //_spriteSheets.push_back("basket");
    _spriteSheets.push_back("environment");
    _spriteSheets.push_back("spritesheet");
    _spriteSheets.push_back("background");
    _spriteSheets.push_back("background-forest");
    
    
    _tips.push_back("Complete quest to level up Elene.");
    _tips.push_back("Use Talismans wisely as they have negative effect.");
    _tips.push_back("Talismans consumes per round.");
    _tips.push_back("Use Tactics to your advantage.");

    
    //int r = rand() % _tips.size();
	int max_rand = _tips.size() - 1;
	int r = random(0, max_rand);
    
    auto tipTxt = Label::createWithTTF("TIP: " + _tips.at(r), kFontPath + kFontNameHUD, 12);
    tipTxt->setPosition(Point(visibleSize.width / 2, visibleSize.height / 5 - 20));
    tipTxt->setAnchorPoint(Vec2(0.5, 0.5));
    tipTxt->setColor(Color3B::WHITE);
    tipTxt->enableOutline(Color4B::BLACK, 1);
    this->addChild(tipTxt);
    
	return true;
}

void PreloadScene::AssetsCounter()
{
	_assetsCount++;
	_assetIsLoading = false;
}

void PreloadScene::LoadAssets(float dt)
{
    if(_assetsCount >= _spriteSheets.size())
    {
       // _loadingProgress->setString("Loading: 100%");
        //_loadingProgress->setVisible(false);
        CCLOG("Loading Done!");
        this->unscheduleAllSelectors();
        scheduleOnce(schedule_selector(PreloadScene::enableTouch), 0.5f);
    }
    else
    {
        if(_assetIsLoading == false)
        {
            _assetIsLoading = true;
            CCLOG("PRELOAD SPRITESHEET %s", _spriteSheets.at(_assetsCount).c_str());
            
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(kImagesPath + _spriteSheets.at(_assetsCount) + sprite_resource + ".plist");
            AssetsCounter();

            float proc = (_assetsCount * 100 / _spriteSheets.size());
//            sprintf(_charText, "%d", proc);
//            std::string str(_charText);
            
            //CCLOG("proc %.2f", (proc / 100.0f));
            
            _loadingProgress->setScaleX((proc / 100.0f));
            
          //  _loadingProgress->setString("Loading: " + str + "%");
        }
    }
}

void PreloadScene::enableTouch(float dt)
{
    _loadingIsDone = true;
    _loadingProgress->setVisible(false);
    _touchToStart->setVisible(true);
    
    auto blink = RepeatForever::create(Blink::create(1.0f, 1));
    _touchToStart->runAction(blink);
}

bool PreloadScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(_loadingIsDone)
        runMainScene();

    return true;
}


void PreloadScene::runMainScene()
{
//    Manager::getInstance()->runScene(SCENE_GAME, "stage_a");
//    Manager::getInstance()->runScene(SCENE_STORE);
//    Manager::getInstance()->setLastGame({"stage_a", 326.32, 999, 99});
//    Manager::getInstance()->runScene(SCENE_REWARDS, "results");
//    Manager::getInstance()->runScene(SCENE_REWARDS, "levelup");
//    return;
    
    auto gameLoopList = Manager::getInstance()->getGameLoopList();
    if(gameLoopList.size() > 0)
        Manager::getInstance()->runScene(SCENE_REWARDS, gameLoopList.at(0));
    else
        Manager::getInstance()->runScene(SCENE_MAIN);
}

