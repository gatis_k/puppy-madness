#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Scenes/RewardsScene.h"

#else
#include "RewardsScene.h"

#endif


USING_NS_CC;



Scene* RewardsScene::createScene(std::string str)
{
    auto scene = Scene::create();
    
    auto layer = RewardsScene::create(str);
    scene->addChild(layer);
    
    return scene;
}

RewardsScene* RewardsScene::create(std::string str)
{
    RewardsScene* pRet = new RewardsScene();
    if (pRet && pRet->init(str))
    {
        pRet->autorelease();
        return pRet;
    }
    else{
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}


bool RewardsScene::init(std::string str)
{
    _currentLoop = str;
    
    Color4B backgroundColor(97,200,211,255);
    if(_currentLoop == "results")
        backgroundColor = Color4B(105, 78, 160, 255);
    else if(_currentLoop == "daily")
        backgroundColor = Color4B(217, 29, 93, 255);
    else if(_currentLoop == "quests")
        backgroundColor = Color4B(96, 142, 202, 255);
    else if(_currentLoop == "levelup")
        backgroundColor = Color4B(96, 142, 202, 255);

    
    if ( !LayerColor::initWithColor(backgroundColor))
    {
        return false;
    }
    _visibleSize = Director::getInstance()->getVisibleSize();
    
    Sprite* fxCircle = Sprite::createWithSpriteFrameName("fx-wheel.png");
    fxCircle->setPosition(_visibleSize.width / 2, _visibleSize.height / 2);
    fxCircle->setScale(1.6f);
    this->addChild(fxCircle);
    
    auto rotateCircle = RotateBy::create(1.0f, 90.0f);
    fxCircle->runAction(RepeatForever::create(rotateCircle));
    
    Sprite *button0 = Sprite::createWithSpriteFrameName("rewards-button-continue-0.png");
    Sprite *button0A = Sprite::createWithSpriteFrameName("rewards-button-continue-1.png");
    auto menuItem0 = MenuItemSprite::create(button0, button0A, NULL, CC_CALLBACK_0(RewardsScene::goContinue, this));
    
    menuItem0->setPosition(0, 0);
    
    _menuContinue = Menu::create(menuItem0, NULL);
    _menuContinue->setPosition(_visibleSize.width / 2, _visibleSize.height / 2);
    this->addChild(_menuContinue);
    
    
    if(_currentLoop == "results")
    {
        fxCircle->setOpacity(180);
        initResults();
    }
    else if(_currentLoop == "daily")
    {
        initDaily();
    }
    else if(_currentLoop == "quests")
    {
        initQuests();
    }
    else if(_currentLoop == "levelup")
    {
        initLevelUp();
    }

    
   
    
    
    /// LOOP LOGIC
    
    _nextLoop = "";
    auto gameLoopList = Manager::getInstance()->getGameLoopList();
    if(gameLoopList.size() > 0 && _nextLoop != str)
        _nextLoop = gameLoopList.at(0);
    
    CCLOG("NEXT LOOP %s - %s", str.c_str(), _nextLoop.c_str());
    
    
   
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(RewardsScene::onTouchBegan, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}


bool RewardsScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
      return true;
}

void RewardsScene::goContinue()
{
    if(_nextLoop == "")
        Manager::getInstance()->runScene(SCENE_MAIN);
    else
        Manager::getInstance()->runScene(SCENE_REWARDS, _nextLoop);
}

void RewardsScene::preSchedule(float dt)
{
    if(_currentLoop == "results" || _currentLoop == "daily" || _currentLoop == "levelup")
    {
        this->schedule(schedule_selector(RewardsScene::transferCoins),_transferSpeed);
    }
    else if(_currentLoop == "quests")
    {
        this->schedule(schedule_selector(RewardsScene::transferXP),_transferSpeed);
    }
}

void RewardsScene::initResults()
{
    GameData results = Manager::getInstance()->getLastGame();
    float recordsTime = Manager::getInstance()->getStage(results.stage)->getLongestTime();
    int recordsCoins = Manager::getInstance()->getStage(results.stage)->getHighestScore();
    int recordsPuppies = Manager::getInstance()->getStage(results.stage)->getHighestCatch();
    
    bool newTime = false;
    bool newRevenue = false;
    bool newCatch = false;
    float dtTime = 0.0f;
    
    if(results.time > recordsTime && recordsTime > 0)
    {
        newTime = true;
        dtTime = results.time - recordsTime;
    }
    
    if(results.coins > recordsCoins && recordsCoins > 0)// && results.coins  > 0)
    {
        newRevenue = true;
//        dtRevenue = results.coins - recordsCoins;
    }
    CCLOG("coins %d %d (%d)", results.coins, recordsCoins, newRevenue);
    
    if(results.puppies > recordsPuppies && recordsPuppies > 0)// && results.puppies > 0)
    {
        newCatch = true;
//        dtCatch = results.puppies - recordsPuppies;
    }
    CCLOG("puppies %d %d (%d)", results.puppies, recordsPuppies, newCatch);
    
    float wH = 0;
    Size backSize(280, 152);
    Size backDepositSize(backSize.width - 4, 36);
    Size backLineSize(backSize.width + 4, 4);
    Size backColorFadeSize(backDepositSize.width - 4, backDepositSize.height - 12);
    Size backColorSize(backSize.width - 4, backSize.height - backLineSize.height * 2 - 4 - backDepositSize.height - 2);
    
    _menuContinue->setPositionY(_visibleSize.height / 2 - backSize.height / 2 - 42);
    
    auto title = Label::createWithTTF("RESULTS", kFontPath + kFontNameStyle, 42);
    title->setAnchorPoint(Vec2(0.5f, 0.5f));
    title->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    title->setColor(Color3B{252, 208, 74});
    title->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + 106);
    this->addChild(title);
    
    Sprite* bones_a = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_a->setPosition(title->getPositionX() - title->getContentSize().width / 2 - 18, title->getPositionY());
    bones_a->setScale(0.8f);
    this->addChild(bones_a);
    
    Sprite* bones_b = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_b->setPosition(title->getPositionX() + title->getContentSize().width / 2 + 18, title->getPositionY());
    bones_b->setScale(0.8f);
    this->addChild(bones_b);
    
    
    auto backFrame = drawBox(backSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH), kCOLOR_BACK);
    this->addChild(backFrame);
    
    auto backLine = drawBox(backLineSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH - backSize.height / 2  + backLineSize.height / 2), kCOLOR_FRAME);
    this->addChild(backLine);
    
    auto backLine2 = drawBox(backLineSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 - backLineSize.height / 2), kCOLOR_FRAME);
    this->addChild(backLine2);
    
    auto backColor = drawBox(backColorSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH  + backDepositSize.height / 2 + 1), Color4B(143, 49, 137, 255));
    this->addChild(backColor);
    
    auto backColorDeposite = drawBox(backDepositSize, Vec2(0,0), Color4B(115, 51, 112, 255));
    backColorDeposite->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - 52));
    this->addChild(backColorDeposite);
    
    auto backColorFade = drawBox(backColorFadeSize, Vec2(0,0), Color4B(115, 51, 112, 255));
    backColorFade->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - wH - backSize.height / 2 + 58);
    this->addChild(backColorFade);
    
    Size labelSizeTransfer = Size(54, 12);
    Size labelSizePerformance = Size(70, 12);
    
    Vec2 posPerformance = Vec2(_visibleSize.width / 2 - backLineSize.width / 2 + labelSizePerformance.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 + labelSizePerformance.height / 2);
    this->addChild(drawBox(labelSizePerformance, posPerformance, kCOLOR_FRAME));
    
    
    Vec2 posTransfer = Vec2(_visibleSize.width / 2 - backLineSize.width / 2 + labelSizeTransfer.width / 2, _visibleSize.height / 2 - wH - backSize.height / 2 - labelSizeTransfer.height / 2);
    this->addChild(drawBox(labelSizeTransfer, posTransfer, kCOLOR_FRAME));
    
    auto performanceTxt = Label::createWithTTF("Performance", kFontPath + kFontNameStyle, 10);
    performanceTxt->setColor(Color3B::WHITE);
    performanceTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    performanceTxt->setPosition(Vec2(posPerformance.x, posPerformance.y - 2));
    this->addChild(performanceTxt);
    
    auto transferTxt = Label::createWithTTF("Transfer", kFontPath + kFontNameStyle, 10);
    transferTxt->setColor(Color3B::WHITE);
    transferTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    transferTxt->setPosition(Vec2(posTransfer.x, posTransfer.y + 1));
    this->addChild(transferTxt);
    
    float dif = 52;
    float midY = 8;
    
    std::string timeLabelStr = "TIME";
    Color3B timeLabelColor(252, 208, 74);
    Color3B timeColor(255, 255, 255);
    if(newTime)
    {
        timeLabelStr = "NEW RECORD";
        timeLabelColor = Color3B(123, 193, 67);
        timeColor = Color3B(252, 208, 74);
    }
    
    Label* bestTime = Label::createWithTTF(floatToStringTime(results.time), kFontPath + kFontNameStyle, 52);
    bestTime->setColor(timeColor);
    bestTime->setAnchorPoint(Vec2(0.5f, 0.5f));
    bestTime->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    bestTime->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH + midY + 28));
    this->addChild(bestTime);
    
    Label* bestTimeTxt = Label::createWithTTF(timeLabelStr, kFontPath + kFontNameStyle, 18);
    bestTimeTxt->setColor(timeLabelColor);
    bestTimeTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    bestTimeTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    bestTimeTxt->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH + midY));
    this->addChild(bestTimeTxt);
    
    if(newTime)
    {
        Label* bestTimeImprov = Label::createWithTTF("+" + floatToStringTime(dtTime, true) + "", kFontPath + kFontNameStyle, 18);
        bestTimeImprov->setColor(Color3B::WHITE);
        bestTimeImprov->setAnchorPoint(Vec2(0.0f, 0.5f));
        bestTimeImprov->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        bestTimeImprov->setPosition(bestTime->getPositionX() + bestTime->getContentSize().width / 2 + 4, bestTime->getPositionY());
        this->addChild(bestTimeImprov);
    }
    
    Color3B revenueLabelColor(204, 204, 204);
    Color3B revenueColor(255, 255, 255);
    if(newRevenue)
    {
        revenueColor = Color3B(252, 208, 74);
        revenueLabelColor = Color3B(123, 193, 67);
    }
    
    auto highestRevenueTxt = Label::createWithTTF("REVENUE:", kFontPath + kFontNameStyle, 14);
    highestRevenueTxt->setColor(revenueLabelColor);
    highestRevenueTxt->setAnchorPoint(Vec2(1.0f, .5f));
    highestRevenueTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    highestRevenueTxt->setPosition(_visibleSize.width / 2 - dif, backColorFade->getPositionY());
    this->addChild(highestRevenueTxt);
    
    auto highestRevenue = Label::createWithTTF(intToString(results.coins), kFontPath + kFontNameStyle, 18);
    highestRevenue->setColor(revenueColor);
    highestRevenue->setAnchorPoint(Vec2(0.0f, 0.5f));
    highestRevenue->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    highestRevenue->setPosition(_visibleSize.width / 2 - dif + 4, backColorFade->getPositionY());
    this->addChild(highestRevenue);
    
    Sprite *highestRevenueIcon = Sprite::createWithSpriteFrameName("main-stage-icon-coins.png");
    highestRevenueIcon->setAnchorPoint(Vec2(0.0f, 0.5f));
    highestRevenueIcon->setPosition(highestRevenue->getPositionX() + highestRevenue->getContentSize().width + 3, backColorFade->getPositionY());
    this->addChild(highestRevenueIcon);

    
    Color3B catchLabelColor(204, 204, 204);
    Color3B catchColor(255, 255, 255);
    if(newCatch)
    {
        catchColor = Color3B(252, 208, 74);
        catchLabelColor = Color3B(123, 193, 67);
    }
    
    auto highestCatchTxt = Label::createWithTTF("CATCH:", kFontPath + kFontNameStyle, 14);
    highestCatchTxt->setColor(catchLabelColor);
    highestCatchTxt->setAnchorPoint(Vec2(1.0f, .5f));
    highestCatchTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    highestCatchTxt->setPosition(_visibleSize.width / 2 + dif, backColorFade->getPositionY());
    this->addChild(highestCatchTxt);
    
    auto highestCatch = Label::createWithTTF(intToString(results.puppies), kFontPath + kFontNameStyle, 18);
    highestCatch->setColor(catchColor);
    highestCatch->setAnchorPoint(Vec2(0.0f, 0.5f));
    highestCatch->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    highestCatch->setPosition(_visibleSize.width / 2 + dif + 4, backColorFade->getPositionY());
    this->addChild(highestCatch);
    
    Sprite *bestCatchIcon = Sprite::createWithSpriteFrameName("main-stage-icon-puppies.png");
    bestCatchIcon->setAnchorPoint(Vec2(0.0f, 0.5f));
    bestCatchIcon->setPosition(highestCatch->getPositionX() + highestCatch->getContentSize().width + 3, highestCatch->getPositionY());
    this->addChild(bestCatchIcon);

    
    
    _transferNow = Manager::getInstance()->getCoins();
    _transferTill = _transferNow + results.coins;
    _transferStep = 9;
    _transferSpeed = 0.0f;
    
    Manager::getInstance()->getStage(results.stage)->setLongestTime(results.time);
    Manager::getInstance()->getStage(results.stage)->setHighestScore(results.coins);
    Manager::getInstance()->getStage(results.stage)->setHighestCatch(results.puppies);
    Manager::getInstance()->getStage(results.stage)->setHighestSpeed(results.speed);
    Manager::getInstance()->depositeCoins(results.coins);
    Manager::getInstance()->depositePuppies(results.puppies);
    
    initTransferBox(backColorDeposite->getPosition(), results.coins);
    
    
    
}


void RewardsScene::initDaily()
{
    auto dailyQuests = Manager::getInstance()->getQuests()->getDailyQuestToClaim();
    //    std::vector<Quest*> dailyQuests({Manager::getInstance()->getQuests()->getDaily("001_daily")});
    //    dailyQuests.push_back(Manager::getInstance()->getQuests()->getDaily("001_daily"));
    CCLOG("size %d", (int)dailyQuests.size() + 1);
    
    Size frameSize(276, 36);
    float margin = 1.0f;
    float wH = 0;
    Size backSize(frameSize.width + 4, ((int)dailyQuests.size() + 1) * (frameSize.height + margin) + 8 + 3);
    Size backDepositSize(backSize.width - 4, 36);
    Size backLineSize(backSize.width + 4, 4);
    
    _menuContinue->setPositionY(_visibleSize.height / 2 - backSize.height / 2 - 42);
    
    auto title = Label::createWithTTF("DAILY BONUSES", kFontPath + kFontNameStyle, 42);
    title->setAnchorPoint(Vec2(0.5f, 0.5f));
    title->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    title->setColor(Color3B{252, 208, 74});
    title->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + wH + backSize.height / 2 + 40);
    this->addChild(title);
    
    Sprite* bones_a = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_a->setPosition(title->getPositionX() - title->getContentSize().width / 2 - 18, title->getPositionY());
    bones_a->setScale(0.8f);
    this->addChild(bones_a);
    
    Sprite* bones_b = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_b->setPosition(title->getPositionX() + title->getContentSize().width / 2 + 18, title->getPositionY());
    bones_b->setScale(0.8f);
    this->addChild(bones_b);
    
    auto backFrame = drawBox(backSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH), kCOLOR_BACK);
    this->addChild(backFrame);
    
    auto backLine = drawBox(backLineSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH - backSize.height / 2  + backLineSize.height / 2), kCOLOR_FRAME);
    this->addChild(backLine);
    
    auto backLine2 = drawBox(backLineSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 - backLineSize.height / 2), kCOLOR_FRAME);
    this->addChild(backLine2);
    
    auto backColorDeposite = drawBox(backDepositSize, Vec2(0,0), Color4B(100, 100, 100, 255));
    backColorDeposite->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - backSize.height / 2 + backDepositSize.height / 2 + 6));
    this->addChild(backColorDeposite);
    
    Size labelSizeTransfer = Size(54, 12);
    Size labelSizePerformance = Size(88, 12);
    
    Vec2 posPerformance = Vec2(_visibleSize.width / 2 - backLineSize.width / 2 + labelSizePerformance.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 + labelSizePerformance.height / 2);
    this->addChild(drawBox(labelSizePerformance, posPerformance, kCOLOR_FRAME));
    
    
    Vec2 posTransfer = Vec2(_visibleSize.width / 2 - backLineSize.width / 2 + labelSizeTransfer.width / 2, _visibleSize.height / 2 - wH - backSize.height / 2 - labelSizeTransfer.height / 2);
    this->addChild(drawBox(labelSizeTransfer, posTransfer, kCOLOR_FRAME));
    
    auto performanceTxt = Label::createWithTTF("Quests Completed", kFontPath + kFontNameStyle, 10);
    performanceTxt->setColor(Color3B::WHITE);
    performanceTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    performanceTxt->setPosition(Vec2(posPerformance.x, posPerformance.y - 2));
    this->addChild(performanceTxt);
    
    auto transferTxt = Label::createWithTTF("Transfer", kFontPath + kFontNameStyle, 10);
    transferTxt->setColor(Color3B::WHITE);
    transferTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    transferTxt->setPosition(Vec2(posTransfer.x, posTransfer.y + 1));
    this->addChild(transferTxt);
    
    Vec2 questPos;
    int i = 0;
    int diamondsToTransfer = 0;
    for(auto daily: dailyQuests)
    {
        questPos = Vec2(_visibleSize.width / 2 - frameSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 - frameSize.height - 1 - margin - backLineSize.height - (i * (frameSize.height + margin)));
        
        Node* questNode = createQuest(daily, frameSize, true, i);
        questNode->setPosition(questPos);
        this->addChild(questNode);
        
        diamondsToTransfer += daily->getRewardCoins();
        
        if(daily->getRewardCoins() > 0)
        {
            daily->setClaim(true);
            Manager::getInstance()->getQuests()->setDailyQuestTime();
        }
        
        _quests.push_back(questNode);
        _questsAmount.push_back(daily->getRewardCoins());
        
        i++;
    }
    _questsAt = 0; //init start
    _questsAmountNow = 0;
    
    
    _transferNow = Manager::getInstance()->getDiamonds();
    _transferTill = _transferNow + diamondsToTransfer;
    _transferStep = 1;
    _transferSpeed = 0.025f;
    
    Manager::getInstance()->depositeDiamonds(diamondsToTransfer);
    
    initTransferBox(backColorDeposite->getPosition(), diamondsToTransfer, false);
    
}


void RewardsScene::initQuests()
{
    auto quests = Manager::getInstance()->getQuests()->getGeneralQuestToClaim();
//    quests.push_back(Manager::getInstance()->getQuests()->getGeneral("001_quest"));
//    quests.push_back(Manager::getInstance()->getQuests()->getGeneral("002_quest"));
//    quests.push_back(Manager::getInstance()->getQuests()->getGeneral("004_quest"));
    CCLOG("size %d", (int)quests.size());
    
    Size frameSize(276, 36);
    float margin = 1.0f;
    float wH = 0;
    Size backSize(frameSize.width + 4, ((int)quests.size()) * (frameSize.height + margin) + 8 + 2);
    Size backLineSize(backSize.width + 4, 4);
    
    _menuContinue->setPositionY(_visibleSize.height / 2 - backSize.height / 2 - 33);
    
    
    auto backFrame = drawBox(backSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH), kCOLOR_BACK);
    this->addChild(backFrame);
    
    auto backLine = drawBox(backLineSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH - backSize.height / 2  + backLineSize.height / 2), kCOLOR_FRAME);
    this->addChild(backLine);
    
    auto backLine2 = drawBox(backLineSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 - backLineSize.height / 2), kCOLOR_FRAME);
    this->addChild(backLine2);
    
    Size labelSizePerformance = Size(88, 12);
    
    Vec2 posPerformance = Vec2(_visibleSize.width / 2 - backLineSize.width / 2 + labelSizePerformance.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 + labelSizePerformance.height / 2);
    this->addChild(drawBox(labelSizePerformance, posPerformance, kCOLOR_FRAME));
    
    auto performanceTxt = Label::createWithTTF("Quests Completed", kFontPath + kFontNameStyle, 10);
    performanceTxt->setColor(Color3B::WHITE);
    performanceTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    performanceTxt->setPosition(Vec2(posPerformance.x, posPerformance.y - 2));
    this->addChild(performanceTxt);
    
    Vec2 questPos;
    int i = 0;
    int xpToTransfer = 0;
    for(auto quest: quests)
    {
        questPos = Vec2(_visibleSize.width / 2 - frameSize.width / 2, _visibleSize.height / 2 - wH + backSize.height / 2 - frameSize.height - 1 - margin - backLineSize.height - (i * (frameSize.height + margin)));
        
        Node* questNode = createQuest(quest, frameSize, false, i);
        questNode->setPosition(questPos);
        this->addChild(questNode);
        
        xpToTransfer += quest->getRewardXP();
        
        if(quest->getRewardXP() > 0)
        {
            quest->setClaim(true);
        }
        
        _quests.push_back(questNode);
        _questsAmount.push_back(quest->getRewardXP());
        
        i++;
    }
    _questsAt = 0; //init start
    _questsAmountNow = 0;
//

    _transferNow = Manager::getInstance()->getUser()->getUserXP(true);
    _transferTill = _transferNow + xpToTransfer;
    _transferStep = 4;
    _transferSpeed = 0.025f;
    _userLevelBreakPoint = Manager::getInstance()->getUser()->getUserXPNextLevel(true);
    _userLevelNow = Manager::getInstance()->getUser()->getUserLevel();
    _userLevelUp = false;
    
//    CCLOG("TRANSFER %d %d %d", _transferNow, _transferTill, _userLevelBreakPoint);
    
    CCLOG("TRANSFER %d %d (%d)", _transferNow, _userLevelBreakPoint, _transferTill);
    
    Sprite* pUserLevelBack = Sprite::createWithSpriteFrameName("rewards-user-level-back.png");
    pUserLevelBack->setAnchorPoint(Vec2(.5f, .5f));
    pUserLevelBack->setPosition(_visibleSize.width / 2 , _visibleSize.height / 2 + backSize.height / 2 + 33);
    this->addChild(pUserLevelBack);
    
    Sprite* bones_a = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_a->setPosition(pUserLevelBack->getPositionX() - pUserLevelBack->getContentSize().width / 2 - 22, pUserLevelBack->getPositionY());
    //    bones_a->setScale(0.8f);
    this->addChild(bones_a);
    
    Sprite* bones_b = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_b->setPosition(pUserLevelBack->getPositionX() + pUserLevelBack->getContentSize().width / 2 + 22, pUserLevelBack->getPositionY());
    //    bones_b->setScale(0.8f);
    this->addChild(bones_b);
    
    
    float percWidth = Manager::getInstance()->getUser()->getUserXP(true) * 100 /  Manager::getInstance()->getUser()->getUserXPNextLevel(true);
    
    Sprite* pUserLevelBar = Sprite::createWithSpriteFrameName("rewards-user-level.png");
    _userBar = ProgressTimer::create(pUserLevelBar);
    _userBar->setType(ProgressTimer::Type::RADIAL); // kCCProgressTimerTypeRadial
    _userBar->setPercentage(percWidth);
    _userBar->setAnchorPoint(Vec2(.5f, .5f));
    _userBar->setPosition(pUserLevelBack->getPosition());
    this->addChild(_userBar);
    
    _userLevel = Label::createWithTTF(Manager::getInstance()->getUser()->getUserLevelStr(), kFontPath + kFontNameStyle, 72);
    _userLevel->setAnchorPoint(Vec2(.5f, .5f));
    _userLevel->setColor(Color3B{252, 249, 206});
    _userLevel->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _userLevel->setPosition(pUserLevelBack->getPositionX(), pUserLevelBack->getPositionY() - 4);
    this->addChild(_userLevel);
    
    if(_transferNow < _transferTill)
        this->scheduleOnce(schedule_selector(RewardsScene::preSchedule), 1.0f);
    
     Manager::getInstance()->getUser()->depositeXP(xpToTransfer);
}



void RewardsScene::initLevelUp()
{
    User* user = Manager::getInstance()->getUser();
    if(user->isLevelUp())
    {
        user->levelUp();
        Manager::getInstance()->getStore()->updateLocks(user->getUserLevel());
        Manager::getInstance()->updateStageLocks();
    }
    
    float wH = 36;
    float wBack = - 55;
    
    Size backSize(280, 46);
    Size backDepositSize(backSize.width - 2, 36);
    Size backLineSize(backSize.width + 4, 4);
    
    auto backFrame = drawBox(backSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 + wH + wBack), kCOLOR_BACK);
    this->addChild(backFrame);
    
    auto backLine = drawBox(backLineSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 + wH + wBack - backSize.height / 2  + backLineSize.height / 2), kCOLOR_FRAME);
    this->addChild(backLine);
    
    auto backLine2 = drawBox(backLineSize, Vec2(_visibleSize.width / 2, _visibleSize.height / 2 + wH + wBack + backSize.height / 2 - backLineSize.height / 2), kCOLOR_FRAME);
    this->addChild(backLine2);
    
    auto backColorDeposite = drawBox(backDepositSize, Vec2(0,0), Color4B(143, 49, 137, 255));
    backColorDeposite->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 + wH + wBack - backSize.height / 2 + backDepositSize.height / 2 + 5));
    this->addChild(backColorDeposite);
    
    Size labelSizeTransfer = Size(54, 12);
    
    Vec2 posTransfer = Vec2(_visibleSize.width / 2 - backLineSize.width / 2 + labelSizeTransfer.width / 2, _visibleSize.height / 2 + wH + wBack - backSize.height / 2 - labelSizeTransfer.height / 2);
    this->addChild(drawBox(labelSizeTransfer, posTransfer, kCOLOR_FRAME));
    
    auto transferTxt = Label::createWithTTF("Transfer", kFontPath + kFontNameStyle, 10);
    transferTxt->setColor(Color3B::WHITE);
    transferTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    transferTxt->setPosition(Vec2(posTransfer.x, posTransfer.y + 1));
    this->addChild(transferTxt);
    
    Sprite* pUserLevelBack = Sprite::createWithSpriteFrameName("rewards-user-level-back.png");
    pUserLevelBack->setAnchorPoint(Vec2(.5f, .5f));
    pUserLevelBack->setPosition(_visibleSize.width / 2 , _visibleSize.height / 2 + wH);
    this->addChild(pUserLevelBack);
    
    float percWidth = Manager::getInstance()->getUser()->getUserXP(true) * 100 /  Manager::getInstance()->getUser()->getUserXPNextLevel(true);
    
    Sprite* pUserLevelBar = Sprite::createWithSpriteFrameName("rewards-user-level.png");
    _userBar = ProgressTimer::create(pUserLevelBar);
    _userBar->setType(ProgressTimer::Type::RADIAL); // kCCProgressTimerTypeRadial
    _userBar->setPercentage(percWidth);
    _userBar->setAnchorPoint(Vec2(.5f, .5f));
    _userBar->setPosition(pUserLevelBack->getPosition());
    this->addChild(_userBar);
    
    _userLevel = Label::createWithTTF(Manager::getInstance()->getUser()->getUserLevelStr(), kFontPath + kFontNameStyle, 72);
    _userLevel->setAnchorPoint(Vec2(.5f, .5f));
    _userLevel->setColor(Color3B{252, 249, 206});
    _userLevel->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _userLevel->setPosition(pUserLevelBack->getPositionX(), pUserLevelBack->getPositionY() - 4);
    this->addChild(_userLevel);
    
    auto title = Label::createWithTTF("LEVEL UP!", kFontPath + kFontNameStyle, 42);
    title->setAnchorPoint(Vec2(0.5f, 0.5f));
    title->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    title->setColor(Color3B(252, 208, 74));
    title->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + wH + pUserLevelBack->getContentSize().height / 2 + 24);
    this->addChild(title);
    
    Sprite* bones_a = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_a->setPosition(pUserLevelBack->getPositionX() - pUserLevelBack->getContentSize().width / 2 - 22, pUserLevelBack->getPositionY());
    //    bones_a->setScale(0.8f);
    this->addChild(bones_a);
    
    Sprite* bones_b = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_b->setPosition(pUserLevelBack->getPositionX() + pUserLevelBack->getContentSize().width / 2 + 22, pUserLevelBack->getPositionY());
    //    bones_b->setScale(0.8f);
    this->addChild(bones_b);
    
    
    
    auto newProducts = Manager::getInstance()->getStore()->getProductsByLevel(user->getUserLevel());
    
    
    _menuContinue->setPositionY(_visibleSize.height / 2 + wH - 112);
    
    if(newProducts.size()>0)
    {
        
        Size lineSize(_visibleSize.width, 40);
        
        auto line = drawBox(lineSize, Vec2(0,0), Color4B(96, 142, 202, 155));
        line->setPosition(_visibleSize.width / 2, lineSize.height / 2); //63
        this->addChild(line);
        
        
        std::string productList = "";
        int i = 0;
        for(auto product: newProducts)
        {
            productList += product->getTitle();
            productList += (++i == newProducts.size() ? "." : ", ");
        }
        Label* text = Label::createWithTTF("New items: " + productList, kFontPath + kFontNameStyle, 14);
        text->setAnchorPoint(Vec2(.5f, .5f));
        text->setColor(Color3B::WHITE);
        text->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        text->setPosition(line->getPosition());
        this->addChild(text);
    }
    
    int diamondsToTransfer = 50;
    _transferNow = Manager::getInstance()->getDiamonds();
    _transferTill = _transferNow + diamondsToTransfer;
    _transferStep = 1;
    _transferSpeed = 0.025f;
    
    Manager::getInstance()->depositeDiamonds(diamondsToTransfer);
    
    initTransferBox(backColorDeposite->getPosition(), diamondsToTransfer, false);
    if(_transferNow < _transferTill)
        this->scheduleOnce(schedule_selector(RewardsScene::preSchedule), 1.0f);
    
}


cocos2d::Node* RewardsScene::createQuest(Quest* quest, cocos2d::Size frameSize, bool daily, int nr)
{
    
    Node* node = Node::create();
    
    float padding = 5.0f;
    float widthIcon = 20.0f + padding + 2;
    float widthReward = 45.0f + padding;
    
    auto drawNode = DrawNode::create();
    Vec2 rectangle[4];
    rectangle[0] = Vec2(0, 0);
    rectangle[1] = Vec2(frameSize.width, 0);
    rectangle[2] = Vec2(frameSize.width, frameSize.height);
    rectangle[3] = Vec2(0, frameSize.height);
    drawNode->drawPolygon(rectangle, 4, Color4F(Color4B(100,100,100,255)), 0, Color4F::BLACK);
    drawNode->setContentSize(frameSize);
    node->addChild(drawNode);
    
    auto color = Color4F(Color4B(250, 162, 27, 255));
    
    auto percNode = Node::create();
    
    auto percDrawNode = DrawNode::create();
    Vec2 percRectangle[4];
    percRectangle[0] = Vec2(0, 0);
    percRectangle[1] = Vec2(frameSize.width, 0);
    percRectangle[2] = Vec2(frameSize.width, frameSize.height);
    percRectangle[3] = Vec2(0, frameSize.height);
    percDrawNode->drawPolygon(percRectangle, 4, color, 0, Color4F::BLACK);
    percNode->setContentSize(frameSize);
    percNode->setAnchorPoint(Vec2(1.0f, 0.0f));
    percNode->setPositionX(frameSize.width);
    //percNode->setScaleX(percWidth / 100);
    percNode->addChild(percDrawNode);
    percNode->setTag(kTAG_TRANSFER_BAR);
    node->addChild(percNode);
    
    std::string iconChar;
    if(nr == 0)
        iconChar = "-a";
    else if(nr == 1)
        iconChar = "-b";
    else if(nr == 2)
        iconChar = "-c";
    
    
    
    iconChar = "-a";
    
    auto iconSprite = Sprite::createWithSpriteFrameName((daily) ? "ui-hud-quest-daily.png" : "ui-hud-quest"+ iconChar +".png");
    iconSprite->setAnchorPoint(Vec2(.5f, .5f));
    iconSprite->setScale(0.9f);
    iconSprite->setPosition( widthIcon / 2 , frameSize.height / 2);
    node->addChild(iconSprite);
    
    //    int strLimit = 24;
    float txtSize = frameSize.width - widthReward - widthIcon;
    std::string titleStr = quest->getTitle();
    //    CCLOG("STRING %d", (int)titleStr.length());
    Label* titleTxt = Label::createWithTTF(titleStr, kFontPath + kFontNameStyle, 12);
    titleTxt->setColor(Color3B::WHITE);
    titleTxt->enableShadow(Color4B(0,0,0,205),Size(0.4,-0.4),0);
    titleTxt->setAnchorPoint(Vec2(0.0f, .5f));
    // if((int)titleStr.length() > strLimit)
    {
        titleTxt->setDimensions(txtSize, frameSize.height);
        titleTxt->setLineHeight(20.0f);
        titleTxt->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        titleTxt->setContentSize(Size(txtSize, frameSize.height - padding));
        titleTxt->setPosition(widthIcon, frameSize.height / 2);
    }
    //else
    {
        //   titleTxt->setPosition(widthIcon, frameSize.height / 2);
    }
    node->addChild(titleTxt);
    
    
    if(quest->getRewardCoins() > 0)
    {
        auto coinsSprite = Sprite::createWithSpriteFrameName("ui-diamonds.png");
        coinsSprite->setAnchorPoint(Vec2(1.0f, .5f));
        coinsSprite->setPosition(frameSize.width - padding, frameSize.height / 2);
        node->addChild(coinsSprite);
        
        auto coinsTxt = Label::createWithTTF(quest->getRewardCoinsStr(), kFontPath + kFontNameStyle, 16);
        coinsTxt->setTag(kTAG_TRANSFER_LABEL);
        coinsTxt->setColor(Color3B::WHITE);
        coinsTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        coinsTxt->setAnchorPoint(Vec2(1.0f, .5f));
        coinsTxt->setPosition(coinsSprite->getPositionX() - coinsSprite->getContentSize().width - 3, frameSize.height / 2);
        node->addChild(coinsTxt);
    }
    else if(quest->getRewardXP() > 0)
    {
        auto xpSprite = Sprite::createWithSpriteFrameName("ui-hud-xp.png");
        xpSprite->setAnchorPoint(Vec2(1.0f, .5f));
        xpSprite->setPosition(frameSize.width - padding, frameSize.height / 2);
        node->addChild(xpSprite);
        
        auto xpTxt = Label::createWithTTF(quest->getRewardXPStr(), kFontPath + kFontNameStyle, 16);
        xpTxt->setTag(kTAG_TRANSFER_LABEL);
        xpTxt->setColor(Color3B{252, 208, 74});
        xpTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        xpTxt->setAnchorPoint(Vec2(1.0f, .5f));
        xpTxt->setPosition(xpSprite->getPositionX() - xpSprite->getContentSize().width - 3, frameSize.height / 2);
        node->addChild(xpTxt);
    }
    
    node->setContentSize(frameSize);
    return node;
}

void RewardsScene::initTransferBox(cocos2d::Vec2 pos, int amount, bool coins)
{
    Sprite* transferIcon = Sprite::createWithSpriteFrameName("rewards-transfer.png");
    transferIcon->setAnchorPoint(Vec2(.5f, .5f));
    transferIcon->setScale(0.2f);
    transferIcon->setPosition(_visibleSize.width/2, pos.y);
    this->addChild(transferIcon);
    
    std::string transferStr = intToString(_transferNow);
    float transferPos = transferStr.length() *  7.2 + 6;
    
    _transferDeposite = Label::createWithTTF(transferStr, kFontPath + kFontNameStyle, 22);
    _transferDeposite->setAnchorPoint(Vec2(0.0f, .5f));
    _transferDeposite->setColor(Color3B::WHITE);
    _transferDeposite->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _transferDeposite->setPosition(transferIcon->getPositionX() - 6 - transferPos, pos.y);
    this->addChild(_transferDeposite);
    
    _transferIcon = Sprite::createWithSpriteFrameName((coins) ? "ui-hud-coins.png" : "ui-hud-diamonds.png");
    _transferIcon->setAnchorPoint(Vec2(1.0f, .5f));
    _transferIcon->setScale(0.8f);
    _transferIcon->setPosition(_transferDeposite->getPositionX() - 5, pos.y);
    this->addChild(_transferIcon);
    
    _transferReward = Label::createWithTTF(intToString(amount), kFontPath + kFontNameStyle, 22);
    _transferReward->setAnchorPoint(Vec2(.0f, .5f));
    _transferReward->setColor(Color3B{252, 249, 206});
    _transferReward->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _transferReward->setPosition(transferIcon->getPositionX() + 10, pos.y);
    this->addChild(_transferReward);
    
    
    if(_transferNow < _transferTill)
        this->scheduleOnce(schedule_selector(RewardsScene::preSchedule), 1.0f);
    else
    {
        _transferReward->setString("TRANSFER COMPLETED");
        _transferReward->setScale(0.6f);
        
        auto blink = RepeatForever::create(Blink::create(1.0f, 1));
        _transferReward->runAction(blink);
    }
}


void RewardsScene::transferCoins(float dt)
{
    if(_currentLoop == "daily")
    {
        if(_quests.size() > _questsAt)
        {
            
            Label* mLabel = (Label*)_quests.at(_questsAt)->getChildByTag(kTAG_TRANSFER_LABEL);
            Node* mBar = _quests.at(_questsAt)->getChildByTag(kTAG_TRANSFER_BAR);
            if(_questsAmountNow < _questsAmount.at(_questsAt))
            {
                _questsAmountNow += _transferStep;
                float perc = (100 - (_questsAmountNow * 100 / _questsAmount.at(_questsAt)))/ 100.0f;
                mLabel->setString(intToString(_questsAmount.at(_questsAt)-_questsAmountNow));
                mBar->setScaleX(perc);
//                CCLOG("i %d %d %.2f", _questsAt, _questsAmountNow, perc);
            }
            else
            {
                mBar->setVisible(false);
                mLabel->setString("0");
                _questsAt++;
                _questsAmountNow = 0;
            }
        }
    }
    
    
    if(_transferNow < _transferTill)
    {
        _transferNow += _transferStep;
        
        std::string transferStr = intToString(_transferNow);
        float transferPos = transferStr.length() *  7.2 + 6;
        _transferDeposite->setString(transferStr);
        _transferDeposite->setPositionX(_visibleSize.width/2 - 6 - transferPos);
        _transferIcon->setPositionX(_transferDeposite->getPositionX() - 5);
        _transferReward->setString(intToString(_transferTill - _transferNow));
    }
    else
    {
        _transferDeposite->setString(intToString(_transferTill));
        _transferReward->setString("TRANSFER COMPLETED");
        _transferReward->setScale(0.6f);
        
        auto blink = RepeatForever::create(Blink::create(1.0f, 1));
        _transferReward->runAction(blink);
        
        
        
        if(_currentLoop=="daily")
        {
            for(auto q:_quests)
            {
                Label* mLabel = (Label*)q->getChildByTag(kTAG_TRANSFER_LABEL);
                Node* mBar = q->getChildByTag(kTAG_TRANSFER_BAR);
                mLabel->setString("0");
                mBar->setVisible(false);
            }
        }
        CCLOG("------");
        this->unschedule(schedule_selector(RewardsScene::transferCoins));
        
//
    }
    
   
}


void RewardsScene::transferXP(float dt)
{
    if(_quests.size() > _questsAt)
    {
        
        Label* mLabel = (Label*)_quests.at(_questsAt)->getChildByTag(kTAG_TRANSFER_LABEL);
        Node* mBar = _quests.at(_questsAt)->getChildByTag(kTAG_TRANSFER_BAR);
        if(_questsAmountNow < _questsAmount.at(_questsAt))
        {
            _questsAmountNow += _transferStep;
            float perc = (100 - (_questsAmountNow * 100 / _questsAmount.at(_questsAt)))/ 100.0f;
            //                perc = (float)perc ;
            mLabel->setString(intToString(_questsAmount.at(_questsAt)-_questsAmountNow));
            mBar->setScaleX(perc);
            //                CCLOG("i %d %d %.2f", _questsAt, _questsAmountNow, perc);
        }
        else
        {
            mBar->setVisible(false);
            mLabel->setString("0");
            _questsAt++;
            _questsAmountNow = 0;
        }
    }
    
    
    if(_transferNow < _transferTill)
    {
        
        if(_transferNow >= _userLevelBreakPoint && !_userLevelUp)
        {
            _transferTill = _transferTill - _transferNow;
            _transferNow = 0;
            
            _userLevelUp = true;
            _userLevelNow++;
            _userLevel->setString(intToString(_userLevelNow));
            
            _userLevelBreakPoint = Manager::getInstance()->getUser()->getUserXPNextLevel(true, _userLevelNow);
            
//            CCLOG("TRANSFER %d %d (%d)", _transferNow, _userLevelBreakPoint, _transferTill);
        }
        _transferNow += _transferStep;
        
       // int deltaTransfer =  _transferNow; //(_transferNow >= _userLevelBreakPoint) ? _transferNow - _userLevelBreakPoint : _transferNow;
    
        
        
        float percWidth = _transferNow * 100.0f /  _userLevelBreakPoint;
        _userBar->setPercentage(percWidth);

    }
    else
    {
        if(_transferNow >= _userLevelBreakPoint && !_userLevelUp)
        {
            _userLevelUp = true;
            _userLevelNow++;
            _userLevel->setString(intToString(_userLevelNow));
            _userLevelBreakPoint = Manager::getInstance()->getUser()->getUserXPNextLevel(_userLevelNow);
        }
        int deltaTransfer = (_transferNow >= _userLevelBreakPoint) ? _transferNow - _userLevelBreakPoint : _transferNow;
        float percWidth = deltaTransfer * 100.0f / _userLevelBreakPoint;
        _userBar->setPercentage(percWidth);
        
        for(auto q:_quests)
        {
            Label* mLabel = (Label*)q->getChildByTag(kTAG_TRANSFER_LABEL);
            Node* mBar = q->getChildByTag(kTAG_TRANSFER_BAR);
            mLabel->setString("0");
            mBar->setVisible(false);
        }
        
//        CCLOG("TRANSFER %d %d (%d)", _transferNow, _userLevelBreakPoint, _transferTill);
        CCLOG("------");
        this->unschedule(schedule_selector(RewardsScene::transferXP));
    }
}




cocos2d::Node* RewardsScene::drawBox(cocos2d::Size size, cocos2d::Vec2 position, cocos2d::Color4B color)
{
    Node* node = Node::create();
    
    Vec2 pRBack[4];
    auto backFrame= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(size.width, 0);
    pRBack[2] = Vec2(size.width, size.height);
    pRBack[3] = Vec2(0, size.height);
    backFrame->drawPolygon(pRBack, 4, Color4F(color), 0, Color4F::BLACK);
    backFrame->setContentSize(size);
    backFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
    backFrame->setPosition(position);
    node->addChild(backFrame);
    return node;
}

std::string RewardsScene::floatToStringTime(float time, bool round)
{
    int min = (int)time / 60;
    int sec = (int)time % 60;
    int milli = (time-(long)time) * 10;
    
    char charText[100];
    if(round)
    {
        if(min>0)
            sprintf(charText, "%02i:%02i.%01i", min, sec, milli);
        else if(sec > 0)
            sprintf(charText, "%02i.%01i", sec, milli);
        else
            sprintf(charText, "0.%01i", milli);
    }
    else
    {
        sprintf(charText, "%02i:%02i.%01i", min, sec, milli);
    }
    std::string str(charText);
    return str;
}

std::string RewardsScene::intToString(int nr)
{
    char charText[100];
    sprintf(charText, "%d", nr);
    std::string str(charText);
    return str;
}
