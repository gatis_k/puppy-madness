#ifndef PuppyMadness_Rewards_h
#define PuppyMadness_Rewards_h

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Manager.h"

#else
#include "Manager.h"

#endif

#include "cocos2d.h"


const int kTAG_TRANSFER_BAR = 678001;
const int kTAG_TRANSFER_LABEL = 678002;


class RewardsScene : public cocos2d::LayerColor
{

public:

    static cocos2d::Scene* createScene(std::string str);
    
    virtual bool init(std::string str);
    
    static RewardsScene* create(std::string str);
    
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
    
    void goContinue();

private:
    
    cocos2d::Size _visibleSize;
    
    std::string _currentLoop;
    std::string _nextLoop;
    
    cocos2d::Menu* _menuContinue;
    
    void preSchedule(float dt);
    
    void initResults();
    
    void initTransferBox(cocos2d::Vec2 pos, int amount, bool coins = true);
    void transferCoins(float dt);
    void transferXP(float dt);
    cocos2d::Node* _transferIcon;
    cocos2d::Label* _transferDeposite;
    cocos2d::Label* _transferReward;
    int _transferNow, _transferTill, _transferStep;
    float _transferSpeed;
    int _userLevelBreakPoint;
    
    std::vector<cocos2d::Node*> _quests;
    std::vector<int> _questsAmount;
    int _questsAt;
    int _questsAmountNow;
    cocos2d::ProgressTimer* _userBar;
    cocos2d::Label* _userLevel;
    int _userLevelNow;
    bool _userLevelUp;
    
    void initDaily();
    void initQuests();
    
    cocos2d::Node* createQuest(Quest* quest, cocos2d::Size frameSize, bool daily = false, int nr = 0);
    
    void initLevelUp();
    
    
    cocos2d::Node* drawBox(cocos2d::Size size, cocos2d::Vec2 position, cocos2d::Color4B color);
    
    
    
    
    std::string floatToStringTime(float time, bool round = false);
    
    std::string intToString(int nr);
    
};

#endif