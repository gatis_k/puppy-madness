#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Scenes/GameScene.h"
#include "Manager/Manager.h"
#include "Game/BackgroundLayer.h"
#include "Game/GameLayer.h"
#include "Game/HUDLayer.h"

#else
#include "GameScene.h"
#include "Manager.h"
#include "BackgroundLayer.h"
#include "GameLayer.h"
#include "HUDLayer.h"

#endif

///////////////









USING_NS_CC;

GameScene::GameScene(void) :
_gameLayer(NULL),
_hudLayer(NULL),
_backgroundLayer(NULL)
{

}

GameScene::~GameScene(void)
{

}

GameScene* GameScene::create(std::string level)
{
	GameScene* pRet = new GameScene();
	if (pRet && pRet->init(level))
	{
		pRet->autorelease();
		return pRet;
	}
	else{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}


bool GameScene::init(std::string level)
{
	if (!Scene::init()) {
		return false;
	}
    
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_FORWARD);

    
	_backgroundLayer = BackgroundLayer::create(level);
	this->addChild(_backgroundLayer, 0);

	_hudLayer = HUDLayer::create();
    this->addChild(_hudLayer, 2);

	_gameLayer = GameLayer::create(level);
	this->addChild(_gameLayer, 1);

	// link delegates DOWN
	_hudLayer->setDPad(_gameLayer);
	// link delegates UP
	_gameLayer->setHUD(_hudLayer);
	_gameLayer->setBackground(_backgroundLayer);
	_gameLayer->gamePrepare(level);
	
	return true;
	
}
