#ifndef PuppyMadness_SettingsScene_h
#define PuppyMadness_SettingsScene_h


#include "cocos2d.h"

class SettingsScene : public cocos2d::LayerColor
{

public:

    static cocos2d::Scene* createScene();

    virtual bool init();
    
	CREATE_FUNC(SettingsScene);

private:
    
    cocos2d::Size _visibleSize;
    
	void goToMain();

	void reset();
    
    void resetStore();
    
    void resetCoins();

	void complete();
    
    void toogleDebug();
    
    void toogleBackgroundMusic();
    
    void toogleSound();

	void facebook(bool login);
};

#endif