#ifndef PuppyMadness_GameScene_h
#define PuppyMadness_GameScene_h

#include "cocos2d.h"


class BackgroundLayer;
class GameLayer;
class HUDLayer;


class GameScene : public cocos2d::Scene
{
public:
	
	GameScene();

	~GameScene();

	virtual bool init(std::string level);

	static GameScene* create(std::string level);

	CC_SYNTHESIZE(BackgroundLayer*, _backgroundLayer, DeligateBackgroundLayer);

	CC_SYNTHESIZE(GameLayer*, _gameLayer, DeligateGameLayer);

	CC_SYNTHESIZE(HUDLayer*, _hudLayer, DeligateHUDLayer);

};

#endif // __GAME_SCENE_H__
