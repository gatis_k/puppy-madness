#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Scenes/SplashScene.h"
#include "Manager/Manager.h"

#else
#include "SplashScene.h"
#include "Manager.h"

#endif
////////////////////////////


USING_NS_CC;

Scene* SplashScene::createScene()
{
    auto scene = Scene::create();
    auto layer = SplashScene::create();
    scene->addChild(layer);
    return scene;
}

bool SplashScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::createWithSpriteFrameName("logo.png");
    backgroundSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
    backgroundSprite->setAnchorPoint(Vec2(0.5, 0.5));
    this->addChild(backgroundSprite);
    
    scheduleOnce(schedule_selector(SplashScene::runLoadingScene), 1.0f);
    
    
    return true;
}

void SplashScene::runLoadingScene(float dt)
{
    Manager::getInstance()->runScene(SCENE_PRELOAD);
}