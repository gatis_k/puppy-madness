#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Scenes/MainScene.h"

#else
#include "MainScene.h"

#endif

#include <time.h>
////////////////////////////




USING_NS_CC;



Scene* MainScene::createScene(std::string str)
{
    auto scene = Scene::create();
    
    auto layer = MainScene::create(str);
    scene->addChild(layer);
    
    return scene;
}

MainScene* MainScene::create(std::string str)
{
    MainScene* pRet = new MainScene();
    if (pRet && pRet->init(str))
    {
        pRet->autorelease();
        return pRet;
    }
    else{
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}


bool MainScene::init(std::string str)
{
    if ( !LayerColor::initWithColor(Color4B(144,188,209,255)))
    {
        return false;
    }
    
//    if(Manager::getInstance()->getQuests()->hasDailyQuest())
//        CCLOG("HAS DAILY");
//    else
//        CCLOG("HASN'T DAILY");
    
    _visibleSize = Director::getInstance()->getVisibleSize();
    _originPoint = Director::getInstance()->getVisibleOrigin();
    
    _shadow = drawBox(_visibleSize, Vec2(_visibleSize.width / 2,_visibleSize.height / 2), Color4B(0, 0, 0, 145));
    _shadow->setAnchorPoint(Vec2(0.0f, 0.0f));
    _shadow->setVisible(false);
    this->addChild(_shadow, zIndexPopUp);

    
    Point delta(-30, -40);
    
    
    auto background = Sprite::createWithSpriteFrameName("background-main.png");
    background->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2));
    background->setAnchorPoint(Vec2(0.5f, 0.5f));
    this->addChild(background);
    
    
    _stageMap = Node::create();

    auto balloon_a1 = Sprite::createWithSpriteFrameName("main-balloon-a.png");
    balloon_a1->setAnchorPoint(Vec2(.5f, -0.3f));
    balloon_a1->setRotation(-15);
    balloon_a1->setPosition(Vec2(-15, 10));
    _stageMap->addChild(balloon_a1);
    
    auto balloon_a2 = Sprite::createWithSpriteFrameName("main-balloon-a.png");
    balloon_a2->setAnchorPoint(Vec2(.5f, -0.3f));
    balloon_a2->setRotation(-5);
    balloon_a2->setPosition(Vec2(-12, 15));
    _stageMap->addChild(balloon_a2);
    
    auto balloon_b = Sprite::createWithSpriteFrameName("main-balloon-b.png");
    balloon_b->setAnchorPoint(Vec2(.5f, -0.3f));
    balloon_b->setRotation(15);
    balloon_b->setPosition(Vec2(50 , 45));
    _stageMap->addChild(balloon_b);
    
    auto balloon_c = Sprite::createWithSpriteFrameName("main-balloon-c.png");
    balloon_c->setAnchorPoint(Vec2(.5f, -0.3f));
    balloon_c->setRotation(22);
    balloon_c->setPosition(Vec2(62 , 35));
    _stageMap->addChild(balloon_c);
    
    auto balloon_d = Sprite::createWithSpriteFrameName("main-balloon-d.png");
    balloon_d->setAnchorPoint(Vec2(.5f, -0.3f));
    balloon_d->setRotation(30);
    balloon_d->setPosition(Vec2(60 , 20));
    _stageMap->addChild(balloon_d);

	auto balloonRotationRight = EaseInOut::create(RotateBy::create(3.0f, 7.0f), 2);
	auto balloonRotationLeft = EaseInOut::create(RotateBy::create(3.0f, -7.0f), 2);
	auto balloonSwing1 = Sequence::create(balloonRotationRight, balloonRotationLeft, NULL);
	balloonRotationRight = EaseInOut::create(RotateBy::create(2.5f, 6.0f), 2);
	balloonRotationLeft = EaseInOut::create(RotateBy::create(2.5f, -6.0f), 2);
	auto balloonSwing2 = Sequence::create(balloonRotationLeft, balloonRotationRight, NULL);
	balloonRotationRight = EaseInOut::create(RotateBy::create(2.0f, 5.0f), 2);
	balloonRotationLeft = EaseInOut::create(RotateBy::create(2.0f, -5.0f), 2);
	auto balloonSwing3 = Sequence::create(balloonRotationLeft, balloonRotationRight, NULL);

	balloon_a1->runAction(RepeatForever::create(balloonSwing1));
	balloon_a2->runAction(RepeatForever::create(balloonSwing2));
	balloon_b->runAction(RepeatForever::create(balloonSwing1->clone()));
	balloon_c->runAction(RepeatForever::create(balloonSwing3));
	balloon_d->runAction(RepeatForever::create(balloonSwing2->clone()));
    
    auto shelter = Sprite::createWithSpriteFrameName("main-shelter.png");
    shelter->setAnchorPoint(Vec2(.5f, .5f));
    shelter->setPosition(Vec2(28, 66));
    _stageMap->addChild(shelter);
    
    auto candybox = Sprite::createWithSpriteFrameName("main-candybox.png");
    candybox->setAnchorPoint(Vec2(.5f, .5f));
    candybox->setPosition(Vec2(-97, 100));
    _stageMap->addChild(candybox);
    
    
    
    if(Manager::getInstance()->getStage("stage_b")->isLocked())
    {
        auto dreamgarden = Sprite::createWithSpriteFrameName("main-dreamgarden-offline.png");
        dreamgarden->setAnchorPoint(Vec2(.5f, .5f));
        dreamgarden->setPosition(Vec2(-88, -10));
        _stageMap->addChild(dreamgarden);
    }
    else{
    
        auto dreamgarden = Sprite::createWithSpriteFrameName("main-dreamgarden.png");
        dreamgarden->setAnchorPoint(Vec2(.5f, .5f));
        dreamgarden->setPosition(Vec2(-88, -10));
        _stageMap->addChild(dreamgarden);
        
        {
            auto fly_1 = Sprite::createWithSpriteFrameName("main-dreamgarden-detail-a-0.png"); // => "main-dreamgarden-detail-a-1.png"
            fly_1->setAnchorPoint(Vec2(.5f, .5f));
            fly_1->setPosition(Vec2(-120, -20));
            

			auto flyUp = EaseOut::create(MoveBy::create(2.0f, Vec2(0.0f, 10.0f)), 2);
			auto flyDown = EaseOut::create(MoveBy::create(2.0f, Vec2(0.0f, -10.0f)), 2);
			auto flyVertical = RepeatForever::create(Sequence::createWithTwoActions(flyUp, flyDown));
			fly_1->runAction(flyVertical);
			auto flyRight = EaseOut::create(MoveBy::create(1.8f, Vec2(40.0f, 0.0f)), 2);
			auto flyLeft = EaseOut::create(MoveBy::create(1.8f, Vec2(-40.0f, 0.0f)), 2);
			auto delay = DelayTime::create(2.4f);
			auto flip = FlipX::create(true);
			auto unflip = FlipX::create(false);
			auto flyHorizontal = RepeatForever::create(Sequence::create(delay, unflip, flyRight, flyRight, delay, flip, flyLeft, flyLeft, NULL));
			fly_1->runAction(flyHorizontal);

            auto fly_2 = Sprite::createWithSpriteFrameName("main-dreamgarden-detail-b-0.png"); // => "main-dreamgarden-detail-b-2.png"
            fly_2->setAnchorPoint(Vec2(.5f, .5f));
            fly_2->setPosition(Vec2(-140, -13));

			flyUp = EaseOut::create(MoveBy::create(1.0f, Vec2(0.0f, 15.0f)), 1);
			flyDown = EaseOut::create(MoveBy::create(1.0f, Vec2(0.0f, -15.0f)), 1);
			flyVertical = RepeatForever::create(Sequence::createWithTwoActions(flyUp, flyDown));
			fly_2->runAction(flyVertical);
			flyRight = EaseOut::create(MoveBy::create(0.9f, Vec2(50.0f, 0.0f)), 1);
			flyLeft = EaseOut::create(MoveBy::create(0.9f, Vec2(-50.0f, 0.0f)), 1);
			delay = DelayTime::create(1.2f);
			flyHorizontal = RepeatForever::create(Sequence::create(delay, unflip, flyRight, flyRight, delay, flip, flyLeft, flyLeft, NULL));
			fly_2->runAction(flyHorizontal);
            
            auto butterfly_1 = Sprite::createWithSpriteFrameName("main-dreamgarden-detail-c-0.png"); // => "main-dreamgarden-detail-c-1.png"
            butterfly_1->setAnchorPoint(Vec2(.5f, .5f));
            butterfly_1->setPosition(Vec2(-125, 25));
            _stageMap->addChild(butterfly_1);
            
            auto butterfly_2 = Sprite::createWithSpriteFrameName("main-dreamgarden-detail-d-0.png"); // => "main-dreamgarden-detail-d-4.png"
            butterfly_2->setAnchorPoint(Vec2(.5f, .5f));
            butterfly_2->setPosition(Vec2(-104, -10));
            _stageMap->addChild(butterfly_2);
            
            _stageMap->addChild(fly_1);
            _stageMap->addChild(fly_2);
            

			_fly1Animation = createAnimation("main-dreamgarden-detail-a-%i.png", 2, 0.03f);
			_fly2Animation = createAnimation("main-dreamgarden-detail-b-%i.png", 3, 0.01f);
			_butterfly1Animation = createAnimation("main-dreamgarden-detail-c-%i.png", 2, 0.1f, true);
			_butterfly2Animation = createAnimation("main-dreamgarden-detail-d-%i.png", 5, 0.1f, true);

			fly_1->runAction(RepeatForever::create(Animate::create(_fly1Animation)));
			fly_2->runAction(RepeatForever::create(Animate::create(_fly2Animation)));

			delay = DelayTime::create(4.0f);
			auto butterfly1Wings = Sequence::create(Animate::create(_butterfly1Animation), delay, NULL);
			delay = DelayTime::create(3.0f);
			auto butterfly2Wings = Sequence::create(Animate::create(_butterfly2Animation), delay, NULL);
			butterfly_1->runAction(RepeatForever::create(butterfly1Wings));
			butterfly_2->runAction(RepeatForever::create(butterfly2Wings));
			//butterfly_1->runAction(RepeatForever::create(Animate::create(_butterfly1Animation)));
			//butterfly_2->runAction(RepeatForever::create(Animate::create(_butterfly2Animation)));
        }
    }
    
    if(Manager::getInstance()->getStage("stage_c")->isLocked())
    {
        auto treasurechest = Sprite::createWithSpriteFrameName("main-treasurechest-offline.png");
        treasurechest->setAnchorPoint(Vec2(.5f, .5f));
        treasurechest->setPosition(Vec2(108, -21));
        _stageMap->addChild(treasurechest);
    }
    else{
        auto treasurechest = Sprite::createWithSpriteFrameName("main-treasurechest.png");
        treasurechest->setAnchorPoint(Vec2(.5f, .5f));
        treasurechest->setPosition(Vec2(108, -21));
        _stageMap->addChild(treasurechest);
    }
    
    
    if(Manager::getInstance()->getStage("stage_d")->isLocked())
    {
        auto treasurechest = Sprite::createWithSpriteFrameName("main-clocks-offline.png");
        treasurechest->setAnchorPoint(Vec2(.5f, .5f));
        treasurechest->setPosition(Vec2(200, 60));
        _stageMap->addChild(treasurechest);
    }
    else{
        auto treasurechest = Sprite::createWithSpriteFrameName("main-clocks.png");
        treasurechest->setAnchorPoint(Vec2(.5f, .5f));
        treasurechest->setPosition(Vec2(200, 60));
        _stageMap->addChild(treasurechest);
        
        {
			time_t rawTime;
			struct tm * timeInfo;
			time(&rawTime);
			timeInfo = localtime(&rawTime);
			auto seconds = timeInfo->tm_sec;
			auto minutes = timeInfo->tm_min;
			auto hours = timeInfo->tm_hour;

			auto clock_0 = Sprite::createWithSpriteFrameName("main-clocks-detail-a.png");
			clock_0->setAnchorPoint(Vec2(.5f, .0f));
			clock_0->setRotation(seconds * 6);
			clock_0->setPosition(Vec2(200, 108));
			_stageMap->addChild(clock_0);

            auto clock_1 = Sprite::createWithSpriteFrameName("main-clocks-detail-a.png");
            clock_1->setAnchorPoint(Vec2(.5f, .0f));
            clock_1->setRotation(minutes * 6);
            clock_1->setPosition(Vec2(200, 108));
            _stageMap->addChild(clock_1);
            
            auto clock_2 = Sprite::createWithSpriteFrameName("main-clocks-detail-b.png");
            clock_2->setAnchorPoint(Vec2(.5f, .0f));
            clock_2->setRotation(hours * 30 + minutes / 2);
            clock_2->setPosition(Vec2(200, 108));
            _stageMap->addChild(clock_2);
            
            auto clock_3 = Sprite::createWithSpriteFrameName("main-clocks-detail-c.png");
            clock_3->setAnchorPoint(Vec2(.5f, .5f));
            clock_3->setPosition(Vec2(200, 108));
            _stageMap->addChild(clock_3);
            
            auto clock_4 = Sprite::createWithSpriteFrameName("main-clocks-detail-d.png");
            clock_4->setAnchorPoint(Vec2(.5f, 1.4f));
            clock_4->setPosition(Vec2(200, 108));
			clock_4->setRotation(10.0f);
            _stageMap->addChild(clock_4);

			auto Rotate0 = RotateBy::create(0.3f, 6.0f);
			auto delay = DelayTime::create(0.7f);
			auto Rotate1 = RotateBy::create(60.0f, 6.0f);
			auto Rotate2 = RotateBy::create(3600.0f, 6.0f);
			auto clockRotate1 = Sequence::create(delay, Rotate0, NULL);
			auto swingRight = EaseInOut::create(RotateBy::create(1.0f, -20.0f), 2);
			auto swingLeft = EaseInOut::create(RotateBy::create(1.0f, 20.0f), 2);
			auto clockSwing = Sequence::create(swingRight, swingLeft, NULL);

			clock_0->runAction(RepeatForever::create(clockRotate1));
			clock_1->runAction(RepeatForever::create(Rotate1));
			clock_2->runAction(RepeatForever::create(Rotate2));
			clock_4->runAction(RepeatForever::create(clockSwing));
			
        }
    }
    
    
    _namePlates = Node::create();
    
    auto nameplate_a = Sprite::createWithSpriteFrameName("main-title-candybox.png");
    nameplate_a->setAnchorPoint(Vec2(.5f, .5f));
    nameplate_a->setPosition(Vec2(-105, 64));
    _namePlates->addChild(nameplate_a);
    
    auto nameplate_b = createNamePlate("stage_b");
    nameplate_b->setPosition(Vec2(-80, -40));
    _namePlates->addChild(nameplate_b);

    auto nameplate_c = createNamePlate("stage_c");
    nameplate_c->setPosition(Vec2(150, -36));
    _namePlates->addChild(nameplate_c);
    
    auto nameplate_d = createNamePlate("stage_d");
    nameplate_d->setPosition(Vec2(165, 70));
    _namePlates->addChild(nameplate_d);
    
    _stageMap->addChild(_namePlates);
    
    
    _stageMap->setAnchorPoint(Vec2(.5f, .5f));
    _stageMap->setPosition(_visibleSize.width / 2 + delta.x, _visibleSize.height / 2 + delta.y);
    this->addChild(_stageMap);
    
    
    _settingsBtn = Sprite::createWithSpriteFrameName("main-settings-button.png");
    _settingsBtn->setAnchorPoint(Vec2(1.0f, 1.0f));
    _settingsBtn->setPosition(Vec2(_visibleSize.width - 10, _visibleSize.height - 10));
    this->addChild(_settingsBtn);
 
    
    //// USER & COINS
    
    _userInfo = Manager::getInstance()->getUserXPnWaller();
    _userInfo->setAnchorPoint(Vec2(0.0f, 1.0f));
    _userInfo->setScale(0.9f);
    _userInfo->setPosition(Vec2(10, _visibleSize.height - 10));
    this->addChild(_userInfo);
    
    //// INIT TACTICS
    _tactics = Manager::getInstance()->getTactics();
    _tactics->setPosition(0,0);
    this->addChild(_tactics, zIndexPopUp);
    
    
    if(str=="first_run")
    {
        CCLOG("First run");
    }
    
    
    Rect stage_a { _visibleSize.width / 2 - 190, _visibleSize.height / 2, 110, 80 };
    Rect stage_b { _visibleSize.width / 2 - 180, _visibleSize.height / 2 - 105, 140, 60 };
    Rect stage_c { _visibleSize.width / 2 + 48, _visibleSize.height / 2 - 105, 150, 60 };
    Rect stage_d { _visibleSize.width / 2 + 95, _visibleSize.height / 2 + 0, 80, 60 };
    
    Rect rectStore{ _visibleSize.width / 2 - 65, _visibleSize.height / 2 - 35, 120, 70 };
    Rect rectSettings{ _visibleSize.width - 90, _visibleSize.height - 50, 80, 40 };
    
    
    drawTouchPoint("store", rectStore);
    drawTouchPoint("settings", rectSettings);
    
    drawTouchPoint("stage_a", stage_a);
    createStage("stage_a");
    
    if(!Manager::getInstance()->getStage("stage_b")->isLocked())
    {
        createStage("stage_b");
        drawTouchPoint("stage_b", stage_b);
    }
    if(!Manager::getInstance()->getStage("stage_c")->isLocked())
    {
        createStage("stage_c");
        drawTouchPoint("stage_c", stage_c);
    }
    if(!Manager::getInstance()->getStage("stage_d")->isLocked())
    {
        createStage("stage_d");
        drawTouchPoint("stage_d", stage_d);
    }
    
    
    if(Manager::getInstance()->getActivePopUp()!="")
        this->showStage(Manager::getInstance()->getActivePopUp());
//    else
//        showStage("stage_a");

    
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(MainScene::onTouchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(MainScene::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

bool MainScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    return true;
}

void MainScene::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    Vec2 point = touch->getLocation();
    auto tooltip = _userInfo->getChildByTag(kTAG_USER_INFO_TOOLTIP);
    
    
    if(_tactics->getBoundingBox().containsPoint(point))
    {
        Vec2 deltaPoint = Vec2(point.x - _tactics->getPositionX() , point.y - _tactics->getPositionY());
        
        for(auto child: _tactics->getChildren())
        {
            if(child->getTag() == kTAG_TACTICS && child->getBoundingBox().containsPoint(deltaPoint))
                goToStore(child->getName());
        }
    }
    if(!_shadow->isVisible()){
        
        
        if(_userInfo->getBoundingBox().containsPoint(point))
        {
            Vec2 deltaPoint = Vec2(point.x - _userInfo->getPositionX() , point.y - _userInfo->getPositionY() + _userInfo->getContentSize().height);
            
            if(_userInfo->getChildByTag(kTAG_USER_COINS)->getBoundingBox().containsPoint(deltaPoint) ||
               _userInfo->getChildByTag(kTAG_USER_COINS_BTN)->getBoundingBox().containsPoint(deltaPoint) ||
               _userInfo->getChildByTag(kTAG_USER_COINS_BTN2)->getBoundingBox().containsPoint(deltaPoint))
            {
                goToStore("5_exchange");
            }
            else if(_userInfo->getChildByTag(kTAG_USER_DIAMONDS)->getBoundingBox().containsPoint(deltaPoint) ||
                    _userInfo->getChildByTag(kTAG_USER_DIAMONDS_BTN)->getBoundingBox().containsPoint(deltaPoint) ||
                    _userInfo->getChildByTag(kTAG_USER_DIAMONDS_BTN2)->getBoundingBox().containsPoint(deltaPoint))
            {
               goToStore("6_iap");
            }
            else if(_userInfo->getChildByTag(kTAG_USER_INFO)->getBoundingBox().containsPoint(deltaPoint))
            {
                
                if(tooltip->isVisible())
                    tooltip->setVisible(false);
                else
                    tooltip->setVisible(true);
            }
            
        }
        else{
            if(tooltip->isVisible())
                tooltip->setVisible(false);
        
            for (std::map<std::string, cocos2d::Rect>::iterator it = _touchPoints.begin(); it != _touchPoints.end(); ++it)
            {
                if(it->second.containsPoint(point))
                {
                    if(it->first == "store")
                        goToStore();
                    else if(it->first == "storeIAP")
                        goToStore("6_iap");
                    else if(it->first == "settings")
                        goToSettings();
                    else
                        showStage(it->first);
                        //openPopUp(it->first);
                }
            }
        }
        
    }
}

void MainScene::goToStore(std::string str)
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_FORWARD);
    Manager::getInstance()->runScene(SCENE_STORE, str);
}

void MainScene::goToSettings()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_FORWARD);
    Manager::getInstance()->runScene(SCENE_SETTINGS);
}


void MainScene::drawTouchPoint(std::string tag, cocos2d::Rect rect)
{
    if(Manager::getInstance()->getDebugTools())
    {
        auto rectNode = DrawNode::create();
        Vec2 rectangle[4];
        rectangle[0] = Vec2(rect.origin.x, rect.origin.y);
        rectangle[1] = Vec2(rect.origin.x + rect.size.width, rect.origin.y);
        rectangle[2] = Vec2(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height);
        rectangle[3] = Vec2(rect.origin.x, rect.origin.y + rect.size.height);
        rectNode->drawPolygon(rectangle, 4, Color4F(0.25f, 0.65f, 0.78f, 0.2f), 1, Color4F::RED);
        this->addChild(rectNode);
    }
    
    _touchPoints.insert(std::pair<std::string, cocos2d::Rect>(tag, rect));
    
}

cocos2d::Node* MainScene::createNamePlate(std::string stage)
{
    Node* node = Node::create();
    
    bool locked = Manager::getInstance()->getStage(stage)->isLocked();
    
    float y = 0;
    std::string str = "";
    if(stage=="stage_b")
        str = "main-title-dreamgarden";
    else if(stage=="stage_c")
        str = "main-title-treasurechest";
    else if(stage=="stage_d")
        str = "main-title-clocks";
    
    if(locked)
    {
        str += "-offline.png";
        y = 20.0f;
    }
    else
    {
        str += ".png";
        y = 0.0f;
    }
    
    
    auto namePlate = Sprite::createWithSpriteFrameName(str);
    namePlate->setAnchorPoint(Vec2(0.0f, 0.0f));
    namePlate->setPosition(Vec2(0, y));
    node->addChild(namePlate);
    
    node->setContentSize(namePlate->getContentSize());
    node->setAnchorPoint(Vec2(0.5f, 0.5f));
    
    if(locked)
    {
        auto requiredTxt = Label::createWithTTF("Level "+ Manager::getInstance()->getStage(stage)->getRequiredUserLevelStr() +" required", kFontPath + kFontNameBold, 7);
        
        Size consSize(requiredTxt->getContentSize().width + 8, 12);
        
        Vec2 rectangle[4];
        auto productRequired = DrawNode::create();
        rectangle[0] = Vec2(0, 0);
        rectangle[1] = Vec2(consSize.width, 0);
        rectangle[2] = Vec2(consSize.width, consSize.height);
        rectangle[3] = Vec2(0, consSize.height);
        productRequired->drawPolygon(rectangle, 4, Color4F(Color4B(0,0,0,100)), 0, Color4F::RED);
        productRequired->setContentSize(consSize);
        productRequired->setAnchorPoint(Vec2(0.5f, 0.5f));
        productRequired->setPosition(Vec2(namePlate->getContentSize().width / 2, 13.5));
        node->addChild(productRequired);
        
        requiredTxt->setColor(Color3B::WHITE);
        requiredTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        requiredTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
        requiredTxt->setPosition(Vec2(namePlate->getContentSize().width / 2, 13.5));
        node->addChild(requiredTxt);
    }
    
    return node;
}

void MainScene::showStage(std::string stage)
{
    Manager::getInstance()->setActivePopUp(stage);
    /*
    for (std::map<std::string, cocos2d::Node*>::iterator it = _stages.begin(); it != _stages.end(); ++it)
    {
        it->second->setVisible(false);
    } */
    _stages.at(stage)->setVisible(true);
    _shadow->setVisible(true);
    _userInfo->setVisible(false);
    _settingsBtn->setVisible(false);
    _namePlates->setVisible(false);
    
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_FORWARD);
}
void MainScene::hideStage()
{
    std::string stage = Manager::getInstance()->getActivePopUp();
    _stages.at(stage)->setVisible(false);
    _shadow->setVisible(false);
    _userInfo->setVisible(true);
    _settingsBtn->setVisible(true);
    _namePlates->setVisible(true);
    
    Manager::getInstance()->setActivePopUp("");
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
}

void MainScene::goPlay(std::string level)
{
//    CCLOG("level %s", level.c_str());
    Manager::getInstance()->runScene(SCENE_GAME, level);
}



void MainScene::createStage(std::string stageStr)
{
    Node *node = Node::create();
    
    Stage* stage = Manager::getInstance()->getStage(stageStr);
    
    auto title = Label::createWithTTF(stage->getTitle(), kFontPath + kFontNameStyle, 36);
    title->setAnchorPoint(Vec2(0.5f, 0.5f));
    title->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    title->setColor(Color3B{252, 208, 74});
    title->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + 118);
    node->addChild(title);
    
    
    Sprite* bones_a = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_a->setPosition(title->getPositionX() - title->getContentSize().width / 2 - 18, title->getPositionY());
    bones_a->setScale(0.8f);
    node->addChild(bones_a);
    
    Sprite* bones_b = Sprite::createWithSpriteFrameName("ui-hud-bones.png");
    bones_b->setPosition(title->getPositionX() + title->getContentSize().width / 2 + 18, title->getPositionY());
    bones_b->setScale(0.8f);
    node->addChild(bones_b);
    
    
    
    Size backSize(460, 140);
    Size backLineSize(backSize.width + 4, 4);
    
    float wh = -22;
    Vec2 pos = Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wh);
    node->addChild(drawBox(backSize, pos, kCOLOR_BACK));
    
    pos = Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wh + backSize.height / 2 );
    node->addChild(drawBox(backLineSize, pos, kCOLOR_FRAME));
    
    pos = Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wh - backSize.height / 2 );
    node->addChild(drawBox(backLineSize, pos, kCOLOR_FRAME));
    
    pos = Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wh - 10 );
    node->addChild(drawBox(backLineSize, pos, kCOLOR_FRAME));
    
    Size labelSizePerformance, labelSizeLeaderboard, labelSizeQuests;
    Vec2 posPerformance, posLeaderboard, posQuests;
    
    labelSizePerformance = Size(90, 12);
    labelSizeLeaderboard = Size(74, 12);
    labelSizeQuests = Size(69, 12);
    
    posPerformance = Vec2(_visibleSize.width / 2 - backLineSize.width / 2 + labelSizePerformance.width / 2, _visibleSize.height / 2 - wh + backSize.height / 2 + labelSizePerformance.height / 2);
    node->addChild(drawBox(labelSizePerformance, posPerformance, kCOLOR_FRAME));
    
    posLeaderboard = Vec2(_visibleSize.width / 2 + backLineSize.width / 2 - labelSizeLeaderboard.width / 2, _visibleSize.height / 2 - wh + backSize.height / 2 + labelSizeLeaderboard.height / 2);
    node->addChild(drawBox(labelSizeLeaderboard, posLeaderboard, kCOLOR_FRAME));
    
    posQuests = Vec2(_visibleSize.width / 2 - backLineSize.width / 2 + labelSizeQuests.width / 2, _visibleSize.height / 2 - wh - backSize.height / 2 - labelSizeQuests.height / 2);
    node->addChild(drawBox(labelSizeQuests, posQuests, kCOLOR_FRAME));
    
    Size wrapSize(307, 73);
    pos = Vec2(_visibleSize.width / 2 - 75, _visibleSize.height / 2 - wh + 30);
    node->addChild(drawBox(wrapSize, pos, Color4B(143, 49, 137, 255)));
    
    wrapSize = Size(148, 73);
    pos = Vec2(_visibleSize.width / 2 + 154, _visibleSize.height / 2 - wh + 30);
    node->addChild(drawBox(wrapSize, pos, Color4B(143, 49, 137, 255)));
    
    wrapSize = Size(151, 69);
    pos = Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wh + 30);
    node->addChild(drawBox(wrapSize, pos, Color4B(0, 0, 0, 50)));
    
    
    auto performanceTxt = Label::createWithTTF("Your Performance", kFontPath + kFontNameStyle, 10);
    performanceTxt->setColor(Color3B::WHITE);
    performanceTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    performanceTxt->setPosition(posPerformance);
    node->addChild(performanceTxt);

    auto leaderboardTxt = Label::createWithTTF("Leaderboards", kFontPath + kFontNameStyle, 10);
    leaderboardTxt->setColor(Color3B::WHITE);
    leaderboardTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    leaderboardTxt->setPosition(posLeaderboard);
    node->addChild(leaderboardTxt);
    
    auto questsTxt = Label::createWithTTF("Active Quests", kFontPath + kFontNameStyle, 10);
    questsTxt->setColor(Color3B::WHITE);
    questsTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    questsTxt->setPosition(posQuests);
    node->addChild(questsTxt);
    
    auto bestTime = Label::createWithTTF(stage->getLongestTimeStr(), kFontPath + kFontNameStyle, 38);
    bestTime->setColor(Color3B::WHITE);
    bestTime->setAnchorPoint(Vec2(0.5f, 0.5f));
    bestTime->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    bestTime->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wh + 36));
    node->addChild(bestTime);
    
    auto bestTimeTxt = Label::createWithTTF("BEST TIME", kFontPath + kFontNameStyle, 18);
    bestTimeTxt->setColor(Color3B{252, 208, 74});
    bestTimeTxt->setAnchorPoint(Vec2(0.5f, 0.5f));
    bestTimeTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    bestTimeTxt->setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 - wh + 8));
    node->addChild(bestTimeTxt);
    
    
    auto highestRevenueTxt = Label::createWithTTF("Highest Revenue:", kFontPath + kFontNameStyle, 12);
    highestRevenueTxt->setColor(Color3B{204, 204, 204});
    highestRevenueTxt->setAnchorPoint(Vec2(1.0f, 0.5f));
    highestRevenueTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    highestRevenueTxt->setPosition(Vec2(_visibleSize.width / 2 - 123, _visibleSize.height / 2 - wh + 40));
    node->addChild(highestRevenueTxt);
    
    
    auto highestRevenue = Label::createWithTTF(stage->getHighestScoreStr(), kFontPath + kFontNameStyle, 14);
    highestRevenue->setColor(Color3B::WHITE);
    highestRevenue->setAnchorPoint(Vec2(0.0f, 0.5f));
    highestRevenue->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    highestRevenue->setPosition(Vec2(_visibleSize.width / 2 - 120, _visibleSize.height / 2 - wh + 40));
    node->addChild(highestRevenue);
    
    Sprite *highestRevenueIcon = Sprite::createWithSpriteFrameName("main-stage-icon-coins.png");
    highestRevenueIcon->setAnchorPoint(Vec2(0.0f, 0.5f));
    highestRevenueIcon->setPosition(highestRevenue->getPositionX() + highestRevenue->getContentSize().width + 3, highestRevenue->getPositionY());
    node->addChild(highestRevenueIcon);
    
    
    auto bestCatchTxt = Label::createWithTTF("Best catch:", kFontPath + kFontNameStyle, 12);
    bestCatchTxt->setColor(Color3B{204, 204, 204});
    bestCatchTxt->setAnchorPoint(Vec2(1.0f, 0.5f));
    bestCatchTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    bestCatchTxt->setPosition(Vec2(_visibleSize.width / 2 - 123, _visibleSize.height / 2 - wh + 22));
    node->addChild(bestCatchTxt);
    
    auto bestCatch = Label::createWithTTF(stage->getHighestCatchStr(), kFontPath + kFontNameStyle, 14);
    bestCatch->setColor(Color3B::WHITE);
    bestCatch->setAnchorPoint(Vec2(0.0f, 0.5f));
    bestCatch->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
    bestCatch->setPosition(Vec2(_visibleSize.width / 2 - 120, _visibleSize.height / 2 - wh + 22));
    node->addChild(bestCatch);
    
    Sprite *bestCatchIcon = Sprite::createWithSpriteFrameName("main-stage-icon-puppies.png");
    bestCatchIcon->setAnchorPoint(Vec2(0.0f, 0.5f));
    bestCatchIcon->setPosition(bestCatch->getPositionX() + bestCatch->getContentSize().width + 3, bestCatch->getPositionY());
    node->addChild(bestCatchIcon);

    Vec2 leaderNodePos(_visibleSize.width / 2 + 154, _visibleSize.height / 2 - wh + 30);
    auto leaderBoard = createLeaderBoards(stageStr);
    leaderBoard->setPosition(leaderNodePos);
    node->addChild(leaderBoard);
    
    auto quests = Manager::getInstance()->getQuests()->getActiveGeneral(true);
    int i = 0;
    int j = 0;
    int nr = 0;
    float questMargin = 1.0f;
    Vec2 questPos;
    Size frameSize(228, 26);
    for(auto quest: quests)
    {
        questPos = Vec2(_visibleSize.width / 2 - frameSize.width - questMargin + ((frameSize.width + questMargin) * i) + questMargin, _visibleSize.height / 2 - (j * (frameSize.height + questMargin)) - wh - 39.5);
        Node* questNode = createQuest(quest, Size(frameSize.width - (1*i), frameSize.height), false, nr);
        questNode->setPosition(questPos);
        node->addChild(questNode);
        
        i++;
        nr++;
        if(i==2)
        {
            i = 0;
            j++;
        }
    }
    
    
    
    if(Manager::getInstance()->getQuests()->hasDailyQuest())
    {
        auto dailyQuests = Manager::getInstance()->getQuests()->getActiveDaily();
        for(auto daily: dailyQuests)
        {
            questPos = Vec2(_visibleSize.width / 2 - frameSize.width - questMargin + ((frameSize.width + questMargin) * i) + questMargin, _visibleSize.height / 2 - (j * (frameSize.height + questMargin)) - wh - 39.5);
            
            Node* questNode = createQuest(daily, Size(frameSize.width - (1*i), frameSize.height), true);
            questNode->setPosition(questPos);
            node->addChild(questNode);
            
            /*
            auto backLine = drawBox(Size(32, 14), Vec2(questPos.x + 16, questPos.y + frameSize.height / 2 + 6), Color4B(0,0,0,105));
            node->addChild(backLine);
            
            auto timer = Label::createWithTTF("23:24:01", kFontPath + kFontNameBold, 6);
            timer->setColor(Color3B::WHITE);
            timer->setAnchorPoint(Vec2(.0f, 1.0f));
            timer->enableShadow(Color4B(0,0,0,155),Size(0.5f,-0.5f),0);
            timer->setPosition(questPos.x + 5, questPos.y + frameSize.height - 1);
            node->addChild(timer);
            */
            
            i++;
            if(i==2)
            {
                i = 0;
                j++;
            }
        }
    }
    else
    {
        questPos = Vec2(_visibleSize.width / 2 - frameSize.width - questMargin + ((frameSize.width + questMargin) * i) + questMargin, _visibleSize.height / 2 - (j * (frameSize.height + questMargin)) - wh - 39.5);
        
        auto noQuest = drawBox(Size(frameSize.width - (1*i), frameSize.height), Vec2(questPos.x + (frameSize.width - (1*i)) / 2, questPos.y + frameSize.height / 2), Color4B(100,100,100,255));
        noQuest->setAnchorPoint(Vec2(0.0f, 0.0f));
        node->addChild(noQuest);
        
        if(Manager::getInstance()->getUser()->getUserLevel() > 1)
        {
            auto txt = Label::createWithTTF("Todays Daily Quest Has been Completed", kFontPath + kFontNameStyle, 11);
            txt->setColor(Color3B::WHITE);
            txt->enableShadow(Color4B(0,0,0,55),Size(.5f,-.5f),0);
            txt->setPosition(questPos.x + frameSize.width / 2, questPos.y + frameSize.height / 2);
            node->addChild(txt);
        }
    }
    
    int buttonMargin = 12;
   
    
    Sprite *closeSprite = Sprite::createWithSpriteFrameName("store-button-close-0.png");
    Sprite *closeSpriteActive = Sprite::createWithSpriteFrameName("store-button-close-1.png");
    auto closeMenu = MenuItemSprite::create(closeSprite, closeSpriteActive, NULL, CC_CALLBACK_0(MainScene::hideStage, this));
    closeMenu->setAnchorPoint(Vec2(1.0f, 1.0f));
    closeMenu->setPosition(_visibleSize.width - buttonMargin, _visibleSize.height - buttonMargin);
    
    Sprite *playSprite = Sprite::createWithSpriteFrameName("main-stage-button-play-0.png");
    Sprite *playSpriteActive = Sprite::createWithSpriteFrameName("main-stage-button-play-1.png");
    auto playMenu = MenuItemSprite::create(playSprite, playSpriteActive, NULL, CC_CALLBACK_0(MainScene::goPlay, this, stageStr));
    playMenu->setAnchorPoint(Vec2(.5f, .5f));
    playMenu->setScale(0.9f);
    playMenu->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - 80);

    auto menu = Menu::create(closeMenu, playMenu, NULL);
    menu->setPosition(0, 0);
    node->addChild(menu);
    
    node->setContentSize(_visibleSize);
    node->setVisible(false);
    this->addChild(node, zIndexPopUp);
    
    _stages.insert(std::pair<std::string, Node*>(stageStr, node));
}

cocos2d::Node* MainScene::createLeaderBoards(std::string stage)
{
    bool inTop5 = true;
    int you = 4;
    
    Size leaderNodeSize = Size(144, 69);
    Node* node = Node::create();
    
//    auto back = drawBox(leaderNodeSize, Vec2(0,0), Color4B::WHITE);
//    back->setPosition(leaderNodeSize.width / 2, leaderNodeSize.height / 2);
//    node->addChild(back);
    
    std::map<std::string, std::string> leaderboards;
    leaderboards.insert(std::pair<std::string, std::string>("1. AAA", "02:24:212"));
    leaderboards.insert(std::pair<std::string, std::string>("2. AAA", "02:24:212"));
    leaderboards.insert(std::pair<std::string, std::string>("3. AAA", "02:24:212"));
    leaderboards.insert(std::pair<std::string, std::string>("4. AAA", "02:24:212"));
    leaderboards.insert(std::pair<std::string, std::string>("5. YOU", "02:24:212"));
    
    float posY;
    float margin = 1;
    float lineHeight = 13;
    if(!inTop5)
    {
        lineHeight = 12.5;
    }
    
    float delta = 0;
    int i = 0;
    Color3B color = Color3B::WHITE;
    
    for (std::map<std::string, std::string>::iterator it = leaderboards.begin(); it != leaderboards.end(); ++it)
    {
        if(i== you)
            color = Color3B{252, 208, 74};
        
        posY = leaderNodeSize.height - ((i * (lineHeight + margin) + lineHeight / 2) - delta);
        auto backLine = drawBox(Size(leaderNodeSize.width, lineHeight), Vec2(leaderNodeSize.width / 2, posY), Color4B(0,0,0,55));
        node->addChild(backLine);
        
        auto nameLabel = Label::createWithTTF(it->first, kFontPath + kFontNameStyle, 11.5);
        nameLabel->setColor(color);
        nameLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
        nameLabel->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        nameLabel->setPosition(Vec2(4, posY - 1));
        node->addChild(nameLabel);
        
        auto scoreLabel = Label::createWithTTF(it->second, kFontPath + kFontNameStyle, 11.5);
        scoreLabel->setColor(color);
        scoreLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
        scoreLabel->enableShadow(Color4B(0,0,0,155),Size(0.7,-0.7),0);
        scoreLabel->setPosition(Vec2(leaderNodeSize.width - 4, posY - .5f));
        node->addChild(scoreLabel);
        
        if(!inTop5 && i == 2)
        {
            delta = -3.5f;
            auto sep = drawBox(Size(leaderNodeSize.width, 2), Vec2(leaderNodeSize.width / 2, posY - lineHeight - delta), Color4B(255, 255, 255, 125));
            node->addChild(sep);
            
        }
        
        i++;
        
        
        
    }
    
    node->setAnchorPoint(Vec2(0.5f, 0.5f));
    node->setContentSize(leaderNodeSize);
    return node;
}

cocos2d::Node* MainScene::createQuest(Quest* quest, cocos2d::Size frameSize, bool daily, int nr)
{
    
    Node* node = Node::create();
    
    float padding = 5.0f;
    float widthIcon = 20.0f + padding + 2;
    float widthReward = 45.0f + padding;
    
    auto drawNode = DrawNode::create();
    Vec2 rectangle[4];
    rectangle[0] = Vec2(0, 0);
    rectangle[1] = Vec2(frameSize.width, 0);
    rectangle[2] = Vec2(frameSize.width, frameSize.height);
    rectangle[3] = Vec2(0, frameSize.height);
    drawNode->drawPolygon(rectangle, 4, Color4F(Color4B(100,100,100,255)), 0, Color4F::BLACK);
    drawNode->setContentSize(frameSize);
    node->addChild(drawNode);
    
    auto color = Color4F(Color4B(96, 142, 202, 255));//Color4F{96, 142, 202, 100};
    if(daily)
        color = Color4F(Color4B{217, 28, 92, 255});
    
    float percWidth = quest->getAmountNow() * 100 / quest->getAmountGoal();
    
    
    auto percNode = Node::create();
    
    auto percDrawNode = DrawNode::create();
    Vec2 percRectangle[4];
    percRectangle[0] = Vec2(0, 0);
    percRectangle[1] = Vec2(frameSize.width, 0);
    percRectangle[2] = Vec2(frameSize.width, frameSize.height);
    percRectangle[3] = Vec2(0, frameSize.height);
    percDrawNode->drawPolygon(percRectangle, 4, color, 0, Color4F::BLACK);
    
    percNode->setScaleX(percWidth / 100);
    percNode->addChild(percDrawNode);
    
    node->addChild(percNode);
    
    
//    auto tt = drawBox(Size(widthIcon, frameSize.height), Vec2(widthIcon / 2,frameSize.height / 2), Color4B::RED);
//    node->addChild(tt);
//    
//    auto tt2 = drawBox(Size(widthReward, frameSize.height), Vec2(frameSize.width - widthReward / 2,frameSize.height / 2), Color4B::RED);
//    node->addChild(tt2);
    
    std::string iconChar;
    if(nr == 0)
        iconChar = "-a";
    else if(nr == 1)
        iconChar = "-b";
    else if(nr == 2)
        iconChar = "-c";
    
    auto iconSprite = Sprite::createWithSpriteFrameName((daily) ? "ui-hud-quest-daily.png" : "ui-hud-quest"+ iconChar +".png");
    iconSprite->setAnchorPoint(Vec2(.5f, .5f));
    iconSprite->setScale(0.9f);
    iconSprite->setPosition( widthIcon / 2 , frameSize.height / 2);
    node->addChild(iconSprite);
    
//    int strLimit = 24;
    float txtSize = frameSize.width - widthReward - widthIcon;
    std::string titleStr = quest->getTitle();
//    CCLOG("STRING %d", (int)titleStr.length());
    Label* titleTxt = Label::createWithTTF(titleStr, kFontPath + kFontNameStyle, 12);
    titleTxt->setColor(Color3B::WHITE);
    titleTxt->enableShadow(Color4B(0,0,0,205),Size(0.4,-0.4),0);
    titleTxt->setAnchorPoint(Vec2(0.0f, .5f));
   // if((int)titleStr.length() > strLimit)
    {
        titleTxt->setDimensions(txtSize, frameSize.height);
        titleTxt->setLineHeight(20.0f);
        titleTxt->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        titleTxt->setContentSize(Size(txtSize, frameSize.height - padding));
        titleTxt->setPosition(widthIcon, frameSize.height / 2);
    }
    //else
    {
     //   titleTxt->setPosition(widthIcon, frameSize.height / 2);
    }
    node->addChild(titleTxt);
    
    
    if(quest->getRewardCoins() > 0)
    {
        auto coinsSprite = Sprite::createWithSpriteFrameName("ui-diamonds.png");
        coinsSprite->setAnchorPoint(Vec2(1.0f, .5f));
        coinsSprite->setPosition(frameSize.width - padding, frameSize.height / 2);
        node->addChild(coinsSprite);
        
        auto coinsTxt = Label::createWithTTF(quest->getRewardCoinsStr(), kFontPath + kFontNameStyle, 16);
        coinsTxt->setColor(Color3B::WHITE);
        coinsTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        coinsTxt->setAnchorPoint(Vec2(1.0f, .5f));
        coinsTxt->setPosition(coinsSprite->getPositionX() - coinsSprite->getContentSize().width - 3, frameSize.height / 2);
        node->addChild(coinsTxt);
    }
    else if(quest->getRewardXP() > 0)
    {
        auto xpSprite = Sprite::createWithSpriteFrameName("ui-hud-xp.png");
        xpSprite->setAnchorPoint(Vec2(1.0f, .5f));
        xpSprite->setPosition(frameSize.width - padding, frameSize.height / 2);
        node->addChild(xpSprite);
        
        auto xpTxt = Label::createWithTTF(quest->getRewardXPStr(), kFontPath + kFontNameStyle, 16);
        xpTxt->setColor(Color3B{252, 208, 74});
        xpTxt->enableShadow(Color4B(0,0,0,75),Size(0.7,-0.7),0);
        xpTxt->setAnchorPoint(Vec2(1.0f, .5f));
        xpTxt->setPosition(xpSprite->getPositionX() - xpSprite->getContentSize().width - 3, frameSize.height / 2);
        node->addChild(xpTxt);
    }
    
    /*
    auto completed = Sprite::createWithSpriteFrameName("ui-hud-quest-completed.png");
    completed->setAnchorPoint(Vec2(.5f, .5f));
    completed->setPosition(frameSize.width, frameSize.height - 5);
    completed->setRotation(-10.0f);
    node->addChild(completed);
    
    if(!quest->isCompleted())
        completed->setVisible(false); */
    
    node->setContentSize(frameSize);
    return node;
}


cocos2d::Node* MainScene::drawBox(cocos2d::Size size, cocos2d::Vec2 position, cocos2d::Color4B color)
{
    Node* node = Node::create();
    
    Vec2 pRBack[4];
    
    auto backFrame= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(size.width, 0);
    pRBack[2] = Vec2(size.width, size.height);
    pRBack[3] = Vec2(0, size.height);
    backFrame->drawPolygon(pRBack, 4, Color4F(color), 0, Color4F::BLACK);
    backFrame->setContentSize(size);
    backFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
    backFrame->setPosition(position);
    node->addChild(backFrame);
    
    return node;
}




void MainScene::initAnimations()
{
    /*
	std::vector<int> frameSequence = { 0, 1, 2, 1 };
	_mountainsAnimation = createAnimation("main-stage-mountains-%i.png", frameSequence, 0.1f);
    */
    
}

cocos2d::Animation* MainScene::createAnimation(const char* name, int frameCount, float frameTime, bool reverse, int offsetX, int offsetY)
{
	Vector<SpriteFrame*> animationVector(frameCount);
	SpriteFrame* animFrame;
	Vec2 offset = Vec2(offsetX, offsetY);
	char str[100] = { 0 };
	if (!reverse){
		for (int i = 0; i < frameCount; i++){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	else
	{
		for (int i = frameCount - 1; i >= 0; i--){
			sprintf(str, name, i);
			animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			animFrame->setOffsetInPixels(offset);
			animationVector.pushBack(animFrame);
		}
	}
	auto animation = Animation::createWithSpriteFrames(animationVector, frameTime);
	return animation;
}

cocos2d::Animation* MainScene::createAnimation(const char* name, std::vector<int> frameVector, float frameTime)
{
	Vector<SpriteFrame*> animationVector(frameVector.size());
	SpriteFrame* animFrame;
	char str[100] = { 0 };
	for (int i = 0; i < frameVector.size(); i++){
		sprintf(str, name, frameVector[i]);
		animFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
		animationVector.pushBack(animFrame);
	}
	auto animation = Animation::createWithSpriteFrames(animationVector, frameTime);
	return animation;
}