#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Scenes/StoreScene.h"

#else
#include "StoreScene.h"

#endif


USING_NS_CC;



Scene* StoreScene::createScene(std::string str)
{
    auto scene = Scene::create();
    
    auto layer = StoreScene::create(str);
    scene->addChild(layer);
    
    return scene;
}

StoreScene* StoreScene::create(std::string str)
{
    StoreScene* pRet = new StoreScene();
    if (pRet && pRet->init(str))
    {
        pRet->autorelease();
        return pRet;
    }
    else{
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}


bool StoreScene::init(std::string str)
{
    if ( !LayerColor::initWithColor(Color4B(30,75,150,255)))
    {
        return false;
    }
    
    _activeTab = (str == "") ? "1_outfits" : str;
    _tabs.push_back("1_outfits");
    _tabs.push_back("2_talismans");
    _tabs.push_back("3_powerups");
    _tabs.push_back("4_unlocks");
    _tabs.push_back("5_exchange");
    _tabs.push_back("6_iap");
    
    _visibleSize = Director::getInstance()->getVisibleSize();
    
    Size backSize(466, 168);
    Size backLineSize(backSize.width + 4, 4);
    
    Vec2 pRBack[4];
    
    auto backFrame= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(backSize.width, 0);
    pRBack[2] = Vec2(backSize.width, backSize.height);
    pRBack[3] = Vec2(0, backSize.height);
    backFrame->drawPolygon(pRBack, 4, Color4F(.0f,.0f,.0f,.5f), 0, Color4F::BLACK);
    backFrame->setContentSize(backSize);
    backFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
    backFrame->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - 25);
    this->addChild(backFrame);
    
    initTabButtons();
    
    auto backLine= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(backLineSize.width, 0);
    pRBack[2] = Vec2(backLineSize.width, backLineSize.height);
    pRBack[3] = Vec2(0, backLineSize.height);
    backLine->drawPolygon(pRBack, 4, Color4F(.0f,.0f,.0f,1.0f), 0, Color4F::BLACK);
    backLine->setContentSize(backLineSize);
    backLine->setAnchorPoint(Vec2(0.5f, 0.5f));
    backLine->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + 61);
    this->addChild(backLine);
    
    auto backLine2= DrawNode::create();
    pRBack[0] = Vec2(0, 0);
    pRBack[1] = Vec2(backLineSize.width, 0);
    pRBack[2] = Vec2(backLineSize.width, backLineSize.height);
    pRBack[3] = Vec2(0, backLineSize.height);
    backLine2->drawPolygon(pRBack, 4, Color4F(.0f,.0f,.0f,1.0f), 0, Color4F::BLACK);
    backLine2->setContentSize(backLineSize);
    backLine2->setAnchorPoint(Vec2(0.5f, 0.5f));
    backLine2->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - 25 - backSize.height / 2);
    this->addChild(backLine2);
    
    int buttonMargin = 12;
    
    Sprite *button0 = Sprite::createWithSpriteFrameName("store-button-close-0.png");
    Sprite *button0Active = Sprite::createWithSpriteFrameName("store-button-close-1.png");
    
    auto menuItem0 = MenuItemSprite::create(button0, button0Active, NULL, CC_CALLBACK_0(StoreScene::goToMain, this));
    menuItem0->setAnchorPoint(Vec2(1.0f, 1.0f));
    menuItem0->setPosition(_visibleSize.width - buttonMargin, _visibleSize.height - buttonMargin);
    auto menu = Menu::create(menuItem0, NULL);
    menu->setPosition(0, 0);
    this->addChild(menu);
    
    
    _userInfo = Manager::getInstance()->getUserXPnWaller();
    _userInfo->setAnchorPoint(Vec2(0.0f, 1.0f));
    _userInfo->setPosition(Vec2(10, _visibleSize.height - 10));
    _userInfo->setScale(0.9f);
    this->addChild(_userInfo);
    
    _tactics = Manager::getInstance()->getTactics(true);
    _tactics->setPosition(0,0);
    this->addChild(_tactics);
    
    
    float gap = _visibleSize.width - _userInfo->getContentSize().width - button0->getContentSize().width - buttonMargin;
    
    gap = 62.0f;
    
    Label* title = Label::createWithTTF("SHELTER", kFontPath + kFontNameStyle, 36);
    title->setAnchorPoint(Vec2(1.0f, 1.0f));
    title->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    title->setColor(Color3B{252, 208, 74});
    title->setPosition(_visibleSize.width - gap, _visibleSize.height - 14);
    this->addChild(title);
    
    
    initPopUp();
    
   
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(StoreScene::onTouchBegan, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}


bool StoreScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    Vec2 point = touch->getLocation();
    auto tooltip = _userInfo->getChildByTag(kTAG_USER_INFO_TOOLTIP);
    
    if(_shadow->isVisible())
    {
        if(tooltip->isVisible())
            tooltip->setVisible(false);
        
        if(_shadow->getChildByTag(kTagPopUpButton)->getBoundingBox().containsPoint(point))
            storeAction(_shadow->getName(), _temp_key);
        else
            _shadow->setVisible(false);
    }
    else if(_userInfo->getBoundingBox().containsPoint(point))
    {
        Vec2 deltaPoint = Vec2(point.x - _userInfo->getPositionX() , point.y - _userInfo->getPositionY() + _userInfo->getContentSize().height);
        {
            if(_userInfo->getChildByTag(kTAG_USER_COINS)->getBoundingBox().containsPoint(deltaPoint) ||
               _userInfo->getChildByTag(kTAG_USER_COINS_BTN)->getBoundingBox().containsPoint(deltaPoint) ||
               _userInfo->getChildByTag(kTAG_USER_COINS_BTN2)->getBoundingBox().containsPoint(deltaPoint))
            {
                showTab("5_exchange");
            }
            else if(_userInfo->getChildByTag(kTAG_USER_DIAMONDS)->getBoundingBox().containsPoint(deltaPoint) ||
                    _userInfo->getChildByTag(kTAG_USER_DIAMONDS_BTN)->getBoundingBox().containsPoint(deltaPoint) ||
                    _userInfo->getChildByTag(kTAG_USER_DIAMONDS_BTN2)->getBoundingBox().containsPoint(deltaPoint))
            {
                showTab("6_iap");
            }
            else if(_userInfo->getChildByTag(kTAG_USER_INFO)->getBoundingBox().containsPoint(deltaPoint))
            {
                
                if(tooltip->isVisible())
                    tooltip->setVisible(false);
                else
                    tooltip->setVisible(true);
            }
        }
        
        
    }
    else if(_tactics->getBoundingBox().containsPoint(point))
    {
        Vec2 deltaPoint = Vec2(point.x - _tactics->getPositionX() , point.y - _tactics->getPositionY());
        
        for(auto child: _tactics->getChildren())
        {
            if(child->getTag() == kTAG_TACTICS && child->getBoundingBox().containsPoint(deltaPoint))
            {
                if(child->getName()=="reset")
                    slotsClear();
                else
                    showTab(child->getName());
            }
            
        }
    }
    else
    {
        if(tooltip->isVisible())
            tooltip->setVisible(false);
        
        for (std::map<std::string, cocos2d::Node*>::iterator it = _tabButtons.begin(); it != _tabButtons.end(); ++it)
        {
            if(it->second->getTag() == kTabTag && it->second->getBoundingBox().containsPoint(point))
                showTab(it->second->getName());
        }
        
        for(auto child: this->getChildren())
        {
            if(child->getTag() == kTagProduct)
            {
                if(child->getBoundingBox().containsPoint(point))
                {
                    Vec2 deltaPoint = Vec2(point.x - child->getPositionX() , point.y - child->getPositionY());
                    
                    if(child->getChildByTag(kTagProductButton) != nullptr)
                    {
                        if(child->getChildByTag(kTagProductButton)->getBoundingBox().containsPoint(deltaPoint))
                        {
                            storeAction(child->getChildByTag(kTagProductButton)->getName(), child->getName());
                        }
                    }
                    
                    if(child->getChildByTag(kTagProductEquip) != nullptr)
                    {
                        if(child->getChildByTag(kTagProductEquip)->getBoundingBox().containsPoint(deltaPoint))
                            slotAction("equip", child->getName());
                    }
                    
                    if(child->getChildByTag(kTagProductUnequip) != nullptr)
                    {
                        if(child->getChildByTag(kTagProductUnequip)->getBoundingBox().containsPoint(deltaPoint))
                            slotAction("unequip", child->getName());
                    }
                    
                    if(child->getChildByTag(kTagProductInfoBtn) != nullptr)
                    {
                        if(child->getChildByTag(kTagProductInfoBtn)->getBoundingBox().containsPoint(deltaPoint) || child->getChildByTag(kTagProductInfoBtn2)->getBoundingBox().containsPoint(deltaPoint))
                        {
                            if(!child->getChildByTag(kTagProductInfo)->isVisible())
                                child->getChildByTag(kTagProductInfo)->setVisible(true);
                            else
                                child->getChildByTag(kTagProductInfo)->setVisible(false);
                        }
                    }
                }
            }
        }
    }
    return true;
}

void StoreScene::storeAction(std::string action, std::string key)
{
    CCLOG("Point %s %s", action.c_str(), key.c_str());
    
    auto manager = Manager::getInstance();
    auto store = manager->getStore();
    
    if(action == "coins")
    {
        int coins = store->getProductByKey(key)->getPrice();
        if(manager->getCoins() - coins < 0)
        {
            showPopUp(key);
        }
        else
        {
            store->getProductByKey(key)->addAmount(1);
            manager->removeCoins(coins);
            manager->getSoundsLibrary()->playSound(SOUND_STORE_BUY_COINS);
            manager->runScene(SCENE_STORE, _activeTab);
        }
    }
    else if(action == "diamonds")
    {
        int diamonds = store->getProductByKey(key)->getPrice();
        if(manager->getDiamonds() - diamonds < 0)
        {
            showPopUp(key, false);
        }
        else
        {
            store->getProductByKey(key)->addAmount(1);
            manager->removeDiamonds(diamonds);
            manager->getSoundsLibrary()->playSound(SOUND_STORE_BUY_DIAMONDS);
            manager->runScene(SCENE_STORE, _activeTab);
        }
    }
    else if(action == "exchange")
    {
        int coins = store->getExchangeByKey(key)->getCoins();
        int diamonds = store->getExchangeByKey(key)->getPrice();
        
        if(manager->getDiamonds() - diamonds < 0)
        {
            showPopUp(key, false);
        }
        else
        {
            manager->removeDiamonds(diamonds);
            manager->depositeCoins(coins);
            manager->getSoundsLibrary()->playSound(SOUND_STORE_EXCHANGE);
            manager->runScene(SCENE_STORE, _activeTab);
        }
        
    }
    else if(action == "iap")
    {
        int diamonds = store->getIAPByKey(key)->getCoins();
        manager->depositeDiamonds(diamonds);
        manager->getSoundsLibrary()->playSound(SOUND_STORE_BUY_IAP);
        manager->runScene(SCENE_STORE, _activeTab);
    }
    else if(action == "spend_diamonds")
    {
        manager->removeDiamonds(_temp_diamonds);
        store->getProductByKey(key)->addAmount(1);
        manager->getSoundsLibrary()->playSound(SOUND_STORE_BUY_DIAMONDS);
        manager->runScene(SCENE_STORE, _activeTab);
    }
    else if(action == "go_iap")
    {
        manager->getSoundsLibrary()->playSound(SOUND_TAP_TAB);
        manager->runScene(SCENE_STORE, "6_iap");
    }
    
}

void StoreScene::slotAction(std::string action, std::string key)
{
    CCLOG("Slot action %s %s", action.c_str(), key.c_str());
    
    auto manager = Manager::getInstance();
    auto slotHandler = manager->getSlotHandler();
    
    if(action == "equip")
    {
        ProductGroup group = manager->getStore()->getProductByKey(key)->getProductGroup();
        
        slotHandler->equipProduct(group, key);
        manager->getSoundsLibrary()->playSound(SOUND_TACTICS_EQUIP);
        manager->runScene(SCENE_STORE, _activeTab);
    }
    else if(action == "unequip")
    {
        auto slot = slotHandler->findSlotByProduct(key);
        slot->clear();
        
        manager->getSoundsLibrary()->playSound(SOUND_TACTICS_UNEQUIP);
        manager->runScene(SCENE_STORE, _activeTab);

    }
    
}
void StoreScene::slotsClear()
{
    Manager::getInstance()->getSlotHandler()->clearSlots();
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TACTICS_CLEAR);
    Manager::getInstance()->runScene(SCENE_STORE, "1_outfits");
}

void StoreScene::showTab(std::string tab)
{
    CCLOG("tab %s", tab.c_str());
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_TAB);
    Manager::getInstance()->runScene(SCENE_STORE, tab);
}

void StoreScene::goToMain()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
	Manager::getInstance()->runScene(SCENE_MAIN);
}

void StoreScene::initTabButtons()
{
    std::string title, icon;
    float margin = 3.0f;
    int i = 0;
    float delta = 0.0f;
    Color3B color = Color3B::WHITE;
    
    for(auto tabs : _tabs)
    {
        Node* node =  Node::create();
        
        icon = "store-tab-gold.png";
        if(tabs == _activeTab)
            icon = "store-tab-silver.png";
        
        Sprite *tabBack = Sprite::createWithSpriteFrameName(icon);
        tabBack->setAnchorPoint(Vec2(0.0f, 0.0f));
        Vec2 tabPosition(_visibleSize.width / 2 - ((tabBack->getContentSize().width + margin) * 3.0f) + margin / 2 + (i * (tabBack->getContentSize().width + margin)), _visibleSize.height / 2 + 62);
        node->addChild(tabBack);
        
        
        if(tabs == "3_powerups")
        {
            title = "PowerUps";
            icon = "store-tab-powerup";
            delta = 1.0f;
            if(tabs == _activeTab)
                initProducts(Manager::getInstance()->getStore()->getProductsByGroup(GROUP_POWERUP));
        }
        else if(tabs == "2_talismans")
        {
            title = "Talismans";
            icon = "store-tab-talisman";
            if(tabs == _activeTab)
                initProducts(Manager::getInstance()->getStore()->getProductsByGroup(GROUP_TALISMAN));
        }
        else if(tabs == "1_outfits")
        {
            title = "Outfits";
            icon = "store-tab-outfit";
            if(tabs == _activeTab)
                initProducts(Manager::getInstance()->getStore()->getProductsByGroup(GROUP_OUTFIT), true);
        }
        else if(tabs == "4_unlocks")
        {
            title = "Unlocks";
            icon = "store-tab-unlock";
            if(tabs == _activeTab)
                initProducts(Manager::getInstance()->getStore()->getProductsByGroup(GROUP_STATIC));
        }
        else if(tabs == "5_exchange")
        {
            title = "Exchange";
            icon = "store-tab-exchange";
//            delta = 3.0f;
            if(tabs == _activeTab)
                initIAP(Manager::getInstance()->getStore()->getAllExchange(), true);
            
        }
        else if(tabs == "6_iap")
        {
            title = "Diamonds";
            icon = "store-tab-diamonds";
            delta = 2.0f;
            if(tabs == _activeTab)
                initIAP(Manager::getInstance()->getStore()->getAllIAP());
        }
        
        if(tabs == _activeTab)
        {
            icon += "-1.png";
            color = Color3B{252, 208, 74};
        }
        else
        {
            icon += "-0.png";
            color = Color3B::WHITE;
        }
        
        
        
        Sprite *iconSprite = Sprite::createWithSpriteFrameName(icon);
        iconSprite->setPosition(tabBack->getContentSize().width / 2,tabBack->getContentSize().height / 2 + delta);
        node->addChild(iconSprite);
        
        auto tabTitle = Label::createWithTTF(title.c_str(), kFontPath + kFontNameStyle, 12);
        tabTitle->setColor(color);
        tabTitle->enableShadow(Color4B(0,0,0,155),Size(0.5,-0.5),0);
        tabTitle->setPosition(tabBack->getContentSize().width / 2, tabBack->getContentSize().height / 2 - 6);
        node->addChild(tabTitle);
       
        node->setContentSize(tabBack->getContentSize());
        node->setPosition(tabPosition);
        node->setTag(kTabTag);
        node->setName(tabs);
        
        this->addChild(node);
        
        _tabButtons.insert(std::pair<std::string, cocos2d::Node*>(tabs, node));
        
        i++;
    }
    
}

void StoreScene::initProducts(std::vector<Product*> products, bool animated)
{
    Size productBack(89, 162);
    Color4F productBackColor;
    float div = 2.5f;
    float margin = 3.0f;
    Vec2 rectangle[4];
    
    if(products.size() == 4)
    {
        productBack.width = 112.0f;
        div = 2.0f;
    }
    
    int i = 0;
    for(auto p: products)
    {
        Vec2 productPosition(_visibleSize.width / 2 - ((productBack.width + margin) * div) + margin / 2 + (i * (productBack.width + margin)), _visibleSize.height / 2 - 105);
        
        Node* node = Node::create();
        ProductGroup group = p->getProductGroup();
        
        if(p->isLocked())
        {
            productBackColor = Color4F(Color4B(137, 137, 137, 255));
        }
        else
        {
            if(group == GROUP_OUTFIT)
                productBackColor = Color4F(Color4B(143, 49, 137, 255));
            else if(group == GROUP_POWERUP)
                productBackColor = Color4F(Color4B(162, 24, 91, 255));
            else if(group == GROUP_TALISMAN)
                productBackColor = Color4F(Color4B(18, 161, 154, 255));
            else if(group == GROUP_STATIC)
                productBackColor = Color4F(Color4B(233, 78, 27, 255));
        }
        
        auto productNode = DrawNode::create();
        rectangle[0] = Vec2(0, 0);
        rectangle[1] = Vec2(productBack.width, 0);
        rectangle[2] = Vec2(productBack.width, productBack.height);
        rectangle[3] = Vec2(0, productBack.height);
        productNode->drawPolygon(rectangle, 4, productBackColor, 0, Color4F::RED);
        productNode->setContentSize(productBack);
        node->addChild(productNode);
        
        float titleBackY = 87;
        float spriteY = 123;
        if(group == GROUP_STATIC || group == GROUP_OUTFIT)
        {
            titleBackY = 60;
        }
        
        Size titleBackSize(productBack.width, 23);
        
        if(p->isLocked())
        {
            titleBackSize.height = 43;
            titleBackY = 80;
            spriteY = 120;
        }else{
            if(group == GROUP_STATIC || group == GROUP_OUTFIT)
            {
                spriteY = 110;
            }
        }
        
        auto productTitleBack = DrawNode::create();
        rectangle[0] = Vec2(0, 0);
        rectangle[1] = Vec2(titleBackSize.width, 0);
        rectangle[2] = Vec2(titleBackSize.width, titleBackSize.height);
        rectangle[3] = Vec2(0, titleBackSize.height);
        productTitleBack->drawPolygon(rectangle, 4, Color4F(.0f,.0f,.0f,.3f), 0, Color4F::RED);
        productTitleBack->setContentSize(titleBackSize);
        productTitleBack->setAnchorPoint(Vec2(0.5f, 1.0f));
        productTitleBack->setPosition(productBack.width / 2, titleBackY);
        node->addChild(productTitleBack);

        auto productTitle = Label::createWithTTF(p->getTitle().c_str(), kFontPath + kFontNameStyle, 16);
        productTitle->setColor(Color3B::WHITE);
        productTitle->enableShadow(Color4B(0,0,0,75),Size(1,-1),0);
        productTitle->setPosition(productBack.width / 2, titleBackY - 12);
        node->addChild(productTitle);
        
        if(animated)
        {
            Sprite *productSprite = Sprite::createWithSpriteFrameName("elena-"+p->getSpriteName()+"-IDLE-0.png");
            productSprite->setPosition(productBack.width / 2, spriteY);
            node->addChild(productSprite);
            
            if(!p->isLocked())
                productSprite->setTag(kTagProductInfoBtn);
            
            if(!p->hasAmount())
            {
                productSprite->setScale(0.8f);
                productSprite->setPositionY(spriteY - 10);
                
                Sprite *productLocked = Sprite::createWithSpriteFrameName("store-product-cage.png");
                productLocked->setPosition(productBack.width / 2, spriteY);
                node->addChild(productLocked);
            }
        }
        else
        {
            
            // image
            Sprite *productSprite = Sprite::createWithSpriteFrameName(p->getSpriteName()+".png");
            if(p->isLocked())
                productSprite->setOpacity(155);
            productSprite->setPosition(productBack.width / 2, spriteY);
            node->addChild(productSprite);
            
            if(!p->isLocked())
                productSprite->setTag(kTagProductInfoBtn);
            
            
            if(p->isConsumbale() && !p->isLocked() && p->getAmount() > 0)
            {
                auto productUses = Label::createWithTTF("x" + p->getAmountString(), kFontPath + kFontNameStyle, 16);
                
                Size consSize(productUses->getContentSize().width + 8, 18);
                
                auto productConsumable = DrawNode::create();
                rectangle[0] = Vec2(0, 0);
                rectangle[1] = Vec2(consSize.width, 0);
                rectangle[2] = Vec2(consSize.width, consSize.height);
                rectangle[3] = Vec2(0, consSize.height);
                productConsumable->drawPolygon(rectangle, 4, Color4F(.0f,.0f,.0f,.4f), 0, Color4F::RED);
                productConsumable->setContentSize(consSize);
                productConsumable->setAnchorPoint(Vec2(1.0f, 0.5f));
                productConsumable->setPosition(Vec2(productBack.width - 2, 98));
                node->addChild(productConsumable);
                
                productUses->setAnchorPoint(Vec2(1.0f, 0.5f));
                productUses->setColor(Color3B(109, 174, 255));
                productUses->enableShadow(Color4B(0,0,0,75),Size(1,-1),0);
                productUses->setContentSize(consSize);
                productUses->setPosition(Vec2(productBack.width + 2, 98));
                node->addChild(productUses);
            }
        }
        
        if(Manager::getInstance()->getSlotHandler()->isProductEquiped(p->getKey()))
        {
            auto productEquiped = Label::createWithTTF("Equiped", kFontPath + kFontNameStyle, 8);
            
            Size consSize(productEquiped->getContentSize().width + 8, 14);
            
            auto productConsumable = DrawNode::create();
            rectangle[0] = Vec2(0, 0);
            rectangle[1] = Vec2(consSize.width, 0);
            rectangle[2] = Vec2(consSize.width, consSize.height);
            rectangle[3] = Vec2(0, consSize.height);
            productConsumable->drawPolygon(rectangle, 4, Color4F::BLACK, 0, Color4F::RED);
            productConsumable->setContentSize(consSize);
            productConsumable->setAnchorPoint(Vec2(.0f, 0.5f));
            productConsumable->setPosition(Vec2(2, productBack.height - 2));
            node->addChild(productConsumable);
            
            productEquiped->setAnchorPoint(Vec2(.0f, 0.5f));
            productEquiped->setColor(Color3B::WHITE);
            //                productEquiped->enableShadow(Color4B(0,0,0,75),Size(1,-1),0);
            productEquiped->setPosition(Vec2(6,  productBack.height - 3));
            node->addChild(productEquiped);
        }
        
        if(!p->isSeen() && group != GROUP_OUTFIT && !p->isLocked())
        {
            p->setSeen(true);
            
            Sprite *seenSprite = Sprite::createWithSpriteFrameName("store-product-new.png");
            seenSprite->setAnchorPoint(Vec2(.0f, 1.0f));
            seenSprite->setPosition(0, productBack.height);
            node->addChild(seenSprite);
        }
        
        
        
        
        
        if(!p->isLocked())
        {
            Sprite *infoBtn = Sprite::createWithSpriteFrameName("store-button-info.png");
            infoBtn->setAnchorPoint(Vec2(1.0f, 1.0f));
            infoBtn->setPosition(productBack.width - 2, productBack.height - 2);
            infoBtn->setTag(kTagProductInfoBtn2);
            node->addChild(infoBtn);
        }
        
        {
            Node* nodeDesc = Node::create();
            
            Sprite *backDesc = Sprite::createWithSpriteFrameName("store-info.png");
            backDesc->setAnchorPoint(Vec2(.0f, .0f));
            nodeDesc->addChild(backDesc);
            
            nodeDesc->setContentSize(backDesc->getContentSize());
            nodeDesc->setAnchorPoint(Vec2(1.0f, 1.0f));
            nodeDesc->setPosition(productBack.width - 1, productBack.height - 2);
            
            
            nodeDesc->setVisible(false);
            nodeDesc->setTag(kTagProductInfo);
            node->addChild(nodeDesc);
            
        }
        
        
        if(p->isLocked())
        {
            productTitle->setPositionY(64);
            
            auto productRequiredTxt = Label::createWithTTF("Level "+ p->getLevelRequiredStr() +" required", kFontPath + kFontNameBold, 8);
            productRequiredTxt->setColor(Color3B::WHITE);
            productRequiredTxt->enableShadow(Color4B(0,0,0,75),Size(0.5f,-0.5f),0);
            productRequiredTxt->setAnchorPoint(Vec2(0.5f, 1.0f));
            productRequiredTxt->setContentSize(Size(titleBackSize.width, titleBackSize.height - 24));
            productRequiredTxt->setPosition(Vec2(titleBackSize.width / 2, 56));
            productRequiredTxt->setDimensions(titleBackSize.width, titleBackSize.height - 24);
            productRequiredTxt->setAlignment(TextHAlignment::CENTER);
            node->addChild(productRequiredTxt);
            
            
            auto productPrice = Label::createWithTTF("LOCKED!", kFontPath + kFontNameStyle, 16);
            productPrice->setColor(Color3B::WHITE);
            productPrice->enableShadow(Color4B(0,0,0,75),Size(1,-1),0);
            productPrice->setPosition(productBack.width / 2, 18);
            productPrice->setOpacity(220);
            node->addChild(productPrice);
            
        }
        else if(p->hasAmount() && !p->isConsumbale())
        {
            if(group != GROUP_OUTFIT)
            {
                auto productPrice = Label::createWithTTF("UNLOCKED!", kFontPath + kFontNameStyle, 16);
                productPrice->setColor(Color3B{252, 208, 74});
                productPrice->enableShadow(Color4B(0,0,0,75),Size(1,-1),0);
                productPrice->setPosition(productBack.width / 2, 18);
                node->addChild(productPrice);
            }
        }
        else
        {
            float buttonY = 18;
            if(p->isConsumbale())
            {
                buttonY = 46;
            }
            
            
            std::string sprite = "store-button-green.png";
            if(p->isPremium())
                sprite = "store-button-blue.png";
            
            Sprite *productBtn = Sprite::createWithSpriteFrameName(sprite);
            productBtn->setPosition(productBack.width / 2, buttonY);
            productBtn->setTag(kTagProductButton);
            if(p->isPremium())
                productBtn->setName("diamonds");
            else
                productBtn->setName("coins");
            
            node->addChild(productBtn);
            
            char charText[100];
            sprintf(charText, "%d", p->getPrice());
            std::string priceStr(charText);
            
            if(p->getPrice()==0)
                priceStr = "Unlock";
            
            auto productPrice = Label::createWithTTF(priceStr, kFontPath + kFontNameStyle, 20);
            auto color = Color3B(255, 67, 67);
            if(((Manager::getInstance()->getCoins() - p->getPrice()) >= 0 && !p->isPremium() )
               || ((Manager::getInstance()->getDiamonds() - p->getPrice()) >= 0 && p->isPremium() ))
            {
                color = Color3B::WHITE;
            }
            productPrice->enableShadow(Color4B(0,0,0,100),Size(1,-1),0);
            productPrice->setColor(color);
            productPrice->setPosition(productBack.width / 2, buttonY - 1);
            node->addChild(productPrice);
            
            if(p->getPrice() > 0)
            {
                productPrice->setPositionX(productBack.width / 2 + 5);
            
                std::string str = "ui-hud-puppylove.png";
                if(p->isPremium())
                    str = "ui-diamonds.png";
                
                float dif = productPrice->getContentSize().width / 2 + 5;
                
                Sprite *productCurrency = Sprite::createWithSpriteFrameName(str);
                productCurrency->setPosition(productBack.width / 2 - dif, buttonY - 1);
                node->addChild(productCurrency);
            
            
            }
            node->setTag(kTagProduct);
        }
        
    
        if(!p->isLocked() && group != GROUP_STATIC)
        {
            if(group == GROUP_OUTFIT)
            {
                if(p->hasAmount())
                {
                    int tag = kTagProductEquip;
                    std::string btnTxt = "Equip";
                    std::string str = "store-button-blue.png";
                    if(Manager::getInstance()->getSlotHandler()->isProductEquiped(p->getKey()))
                    {
                        btnTxt = "Equiped";
                        str = "store-button-gray.png";
                        tag = 0;
                        
                    }
                    
                    Sprite *productEquip = Sprite::createWithSpriteFrameName(str);
                    productEquip->setPosition(productBack.width / 2, 18);
                    if(tag != 0)
                    {
                        node->setTag(kTagProduct);
                        productEquip->setTag(tag);
                    }
                    node->addChild(productEquip);
                    
                    auto productEquipTitle = Label::createWithTTF(btnTxt, kFontPath + kFontNameStyle, 16.0f);
                    productEquipTitle->setColor(Color3B::WHITE);
                    productEquipTitle->enableShadow(Color4B(0,0,0,105),Size(0.5f,-0.5f),0);
                    productEquipTitle->setPosition(productBack.width / 2, 16);
                    node->addChild(productEquipTitle);
                }
            }
            else
            {
                int tag = kTagProductEquip;
                float fSize = 16.0f;
                Color3B color = Color3B::WHITE;
                std::string btnTxt = "Equip";
                std::string str = "store-button-blue-small.png";
                
                if(Manager::getInstance()->getSlotHandler()->isProductEquiped(p->getKey()))
                {
                    tag = kTagProductUnequip;
                    btnTxt = "Unequip";
                    str = "store-button-yellow-small.png";
                }
                else
                {
                    if(!Manager::getInstance()->getSlotHandler()->anyFreeSlots(group, p->getKey()))
                    {
                        tag = 0;
                        fSize = 11;
//                        color = Color3B(255, 67, 67);
                        btnTxt = "---";
                        str = "store-button-gray-small.png";
                    }
                    else if(!p->hasAmount())
                    {
                        tag = 0;
                        btnTxt = "EMPTY";
                        str = "store-button-gray-small.png";
                    }
                }
                
                Sprite *productEquip = Sprite::createWithSpriteFrameName(str);
                productEquip->setPosition(productBack.width / 2, 16);
                if(tag != 0)
                    productEquip->setTag(tag);
                node->addChild(productEquip);
                
                auto productEquipTitle = Label::createWithTTF(btnTxt, kFontPath + kFontNameStyle, fSize);
                productEquipTitle->setColor(color);
                productEquipTitle->enableShadow(Color4B(0,0,0,105),Size(0.5f,-0.5f),0);
                productEquipTitle->setPosition(productBack.width / 2, 16);
                node->addChild(productEquipTitle);
            }
        }
        
        
        
        // description
        /*
        auto productDesc = Label::createWithTTF(p->getDescription(), kFontPath + kFontNameBold, 9);
        productDesc->setAnchorPoint(Vec2(0.5f, 1.0f));
        productDesc->setColor(Color3B::WHITE);
        productDesc->setContentSize(Size(productBack.width - 10, 60));
        if(!p->isLocked())
            productDesc->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
        productDesc->setDimensions(productBack.width - 10, 70);
        productDesc->setPosition(productBack.width / 2, 65);
        productDesc->setAlignment(TextHAlignment::CENTER);
        productDesc->setColor(Color3B::WHITE);
        if(p->isLocked())
            productDesc->setOpacity(200);
        node->addChild(productDesc);
        
        */
        
       
        
        node->setContentSize(productBack);
        node->setName(p->getKey());
        node->setPosition(productPosition);
        this->addChild(node);
        i++;
    }
}


void StoreScene::initIAP(std::vector<IAP*> allIAP, bool exchange)
{
    
    
    
    Color4F productBackColorDefault = Color4F(Color4B(157, 26, 110, 255));
    Color4F productBackColor = productBackColorDefault;
    
    Size productBack(89, 162);
    float margin = 3.0f;
    float div = 2.5f;
    if(allIAP.size() == 4)
    {
        productBack.width = 112.0f;
        div = 2.0f;
    }
    
    
    
    int i = 0;
    
    for(auto iap: allIAP)
    {
        Node* node = Node::create();
        
        Vec2 productPosition(_visibleSize.width / 2 - ((productBack.width + margin) * div) + margin / 2 + (i * (productBack.width + margin)), _visibleSize.height / 2 - 105);
        
        auto productNode = DrawNode::create();
        Vec2 rectangle[4];
        rectangle[0] = Vec2(0, 0);
        rectangle[1] = Vec2(productBack.width, 0);
        rectangle[2] = Vec2(productBack.width, productBack.height);
        rectangle[3] = Vec2(0, productBack.height);
        productNode->drawPolygon(rectangle, 4, productBackColor, 0, Color4F::RED);
        productNode->setContentSize(productBack);
        node->addChild(productNode);
        
        
        
        
        
        Sprite *productSprite = Sprite::createWithSpriteFrameName(iap->getSpriteName()+".png");
        productSprite->setPosition(productBack.width / 2, 120);
        node->addChild(productSprite);

        Size titleBackSize(productBack.width, 43);
        auto productTitleBack = DrawNode::create();
        rectangle[0] = Vec2(0, 0);
        rectangle[1] = Vec2(titleBackSize.width, 0);
        rectangle[2] = Vec2(titleBackSize.width, titleBackSize.height);
        rectangle[3] = Vec2(0, titleBackSize.height);
        productTitleBack->drawPolygon(rectangle, 4, Color4F(.0f,.0f,.0f,.3f), 0, Color4F::RED);
        productTitleBack->setContentSize(titleBackSize);
        productTitleBack->setAnchorPoint(Vec2(0.5f, 1.0f));
        productTitleBack->setPosition(productBack.width / 2, 80);
        node->addChild(productTitleBack);
        
        
        auto productTitle = Label::createWithTTF(iap->getDescription(), kFontPath + kFontNameStyle, 16);
        productTitle->setColor(Color3B::WHITE);
        productTitle->enableShadow(Color4B(0,0,0,75),Size(1,-1),0);
        productTitle->setPosition(productBack.width / 2, 67);
        node->addChild(productTitle);
        
        // amount you get
        auto productCoins = Label::createWithTTF(iap->getCoinsStr().c_str(), kFontPath + kFontNameStyle, 20);
        productCoins->setColor(Color3B{252, 249, 206});
        productCoins->enableShadow(Color4B(0,0,0,75),Size(1,-1),0);
        productCoins->setPosition(productBack.width / 2 + 5, 50);
        node->addChild(productCoins);
        
        
        
        float dif = productCoins->getContentSize().width / 2 + 5;
        
        std::string currencyStr = "ui-diamonds.png";
        if(exchange)
            currencyStr = "ui-hud-puppylove.png";
        
        Sprite *productCurrency = Sprite::createWithSpriteFrameName(currencyStr);
        productCurrency->setPosition(productBack.width / 2 - dif, 50);
        node->addChild(productCurrency);
        
        std::string str = "store-button-pink.png";
        if(exchange)
            str = "store-button-blue.png";
        
        float buttonY = 18;
        
        Sprite *productBtn = Sprite::createWithSpriteFrameName(str);
        productBtn->setPosition(productBack.width / 2, buttonY);
        productBtn->setTag(kTagProductButton);
        if(exchange)
            productBtn->setName("exchange");
        else
            productBtn->setName("iap");
        node->addChild(productBtn);
        
        std::string priceStr = iap->getPriceStr();
        if(!exchange)
        {
            char charText[100];
            sprintf(charText, "%.2f", ((float)iap->getPrice() / 100));
            std::string priceStr2(charText);
            priceStr = priceStr2 + " EU";
        }
        
        auto productPrice = Label::createWithTTF(priceStr, kFontPath + kFontNameStyle, 20);
        auto color = Color3B(255, 67, 67);
        if((Manager::getInstance()->getDiamonds() - iap->getPrice()) >= 0 || !exchange)
        {
            color = Color3B::WHITE;
        }
        productPrice->enableShadow(Color4B(0,0,0,100),Size(1,-1),0);
        productPrice->setColor(color);
        node->addChild(productPrice);
        
        
        if(exchange)
        {
            productPrice->setPosition(productBack.width / 2 + 5, buttonY - 1);
            Sprite *productCurrency = Sprite::createWithSpriteFrameName("ui-diamonds.png");
            productCurrency->setPosition(productBack.width / 2 - 15, buttonY - 1);
            node->addChild(productCurrency);
        }
        else
        {
            productPrice->setPosition(productBack.width / 2, buttonY - 1);
        }
        node->setContentSize(productBack);
        node->setPosition(productPosition);
        node->setName(iap->getKey());
        node->setTag(kTagProduct);
        this->addChild(node);
        i++;
    }
}

void StoreScene::initPopUp()
{
    _shadow = Node::create();
    
    auto rectNodeShadow = DrawNode::create();
    Vec2 rectangleShadow[4];
    rectangleShadow[0] = Vec2(0, 0);
    rectangleShadow[1] = Vec2(_visibleSize.width, 0);
    rectangleShadow[2] = Vec2(_visibleSize.width, _visibleSize.height);
    rectangleShadow[3] = Vec2(0, _visibleSize.height);
    rectNodeShadow->drawPolygon(rectangleShadow, 4, Color4F(0, 0, 0, 0.7f), 0, Color4F::RED);
    _shadow->setContentSize(_visibleSize);
    _shadow->addChild(rectNodeShadow);
    
    Sprite *backPopup = Sprite::createWithSpriteFrameName("store-back-popup.png");
    backPopup->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - 10);
    _shadow->addChild(backPopup);
    
    _popUpTitle = Label::createWithTTF("Not Enough Coins!", kFontPath + kFontNameStyle, 32);
    _popUpTitle->setAnchorPoint(Vec2(0.5f, 0.0f));
    _popUpTitle->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _popUpTitle->setColor(Color3B{252, 208, 74});
    _popUpTitle->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + 33);
    _shadow->addChild(_popUpTitle);
    
    _popUpMsg = Label::createWithTTF("Buy missing diamonds?", kFontPath + kFontNameStyle, 18);
    _popUpMsg->setAnchorPoint(Vec2(0.5f, 0.0f));
    _popUpMsg->enableShadow(Color4B(0,0,0,55),Size(1,-1),0);
    _popUpMsg->setColor(Color3B::WHITE);
    _popUpMsg->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - 2);
    _shadow->addChild(_popUpMsg);
    
    
    _popUpBtnBlue = Sprite::createWithSpriteFrameName("store-button-blue.png");
    _popUpBtnBlue->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - 25);
    _popUpBtnBlue->setTag(kTagPopUpButton);
    _shadow->addChild(_popUpBtnBlue);
    
    _popUpBtnPink = Sprite::createWithSpriteFrameName("store-button-pink.png");
    _popUpBtnPink->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 - 25);
    _popUpBtnPink->setTag(kTagPopUpButton);
    _shadow->addChild(_popUpBtnPink);
    
    
    _popUpBtnPrice = Label::createWithTTF("99", kFontPath + kFontNameStyle, 20);
    _popUpBtnPrice->setColor(Color3B::WHITE);
    _popUpBtnPrice->enableShadow(Color4B(0,0,0,100),Size(1,-1),0);
    _popUpBtnPrice->setPosition(_visibleSize.width / 2 + 5, _visibleSize.height / 2 - 25);
    _shadow->addChild(_popUpBtnPrice);
    
    
    auto popUpBtnCurrency = Sprite::createWithSpriteFrameName("ui-diamonds.png");
    popUpBtnCurrency->setPosition(_visibleSize.width / 2 - 18, _visibleSize.height / 2 - 25);
    _shadow->addChild(popUpBtnCurrency);
    
    _shadow->setVisible(false);
    this->addChild(_shadow);
}



void StoreScene::showPopUp(std::string key, bool diamonds)
{
    
    _temp_key = key;
    _shadow->setVisible(true);
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_STORE_POPUP);
    
    if(diamonds)
    {
        int tax = 10;
        int one_diamond = 80;
        int price = Manager::getInstance()->getStore()->getProductByKey(key)->getPrice();
        price = price / one_diamond + ((price / one_diamond) * tax / 100 );
        
        
        _popUpTitle->setString("Not enough coins!");
        
        if((Manager::getInstance()->getDiamonds() - price) >= 0)
        {
            _popUpBtnPrice->setColor(Color3B::WHITE);
            _popUpMsg->setString("Buy missing coins?");
//            _popUpBtnPrice->enableShadow(Color4B(0,0,0,100),Size(1,-1),0);
            
            _temp_diamonds = price;
            _shadow->setName("spend_diamonds");
        }
        else
        {
            _popUpBtnPrice->setColor(Color3B(255, 67, 67));
            _popUpMsg->setString("Buy missing diamonds?");
//            _popUpBtnPrice->disableEffect();
            price = price - Manager::getInstance()->getDiamonds();
            
            _shadow->setName("go_iap");
        }
        
        _popUpBtnPrice->enableShadow(Color4B(0,0,0,100),Size(1,-1),0);
        _popUpBtnBlue->setVisible(true);
        _popUpBtnPink->setVisible(false);
        
        char charText[100];
        sprintf(charText, "%d", price);
        std::string priceStr(charText);
        
        _popUpBtnPrice->setString(priceStr);
    }
    else{
        _popUpTitle->setString("Not enough diamonds!");
        _popUpMsg->setString("Buy missing diamonds?");
        _popUpBtnPrice->setString("BUY");
        _popUpBtnBlue->setVisible(false);
        _popUpBtnPink->setVisible(true);
        
        _shadow->setName("go_iap");
    }
}
