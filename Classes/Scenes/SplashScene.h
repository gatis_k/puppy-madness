#ifndef PuppyMadness_SplashScene_h
#define PuppyMadness_SplashScene_h


#include "cocos2d.h"

class SplashScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    CREATE_FUNC(SplashScene);
    
private:
    
    void runLoadingScene(float dt);
};


#endif
