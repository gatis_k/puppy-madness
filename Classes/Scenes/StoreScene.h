#ifndef PuppyMadness_StoreScene_h
#define PuppyMadness_StoreScene_h

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Manager/Manager.h"

#else
#include "Manager.h"

#endif

#include "cocos2d.h"



const int kTabTag = 100000;
const int kTagProduct = 100001;
const int kTagProductButton = 100002;
const int kTagProductEquip = 100003;
const int kTagProductUnequip = 100004;
const int kTagProductInfo = 100005;
const int kTagProductInfoBtn = 100006;
const int kTagProductInfoBtn2 = 100007;

const int kTagPopUpButton = 100008;




class StoreScene : public cocos2d::LayerColor
{

public:

    static cocos2d::Scene* createScene(std::string str);
    
    virtual bool init(std::string str);
    
    static StoreScene* create(std::string str);
    
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);

private:
    
    std::string _activeTab;
    
    cocos2d::Size _visibleSize;
    
    
    void storeAction(std::string action, std::string key);
    
    void slotAction(std::string action, std::string key);
    
    void slotsClear();
    
    void showTab(std::string tab);
    
	void goToMain();
    
    std::vector<std::string> _tabs;
    
    std::map<std::string, cocos2d::Node*> _tabButtons;
    
    void initTabButtons();
    
    void initProducts(std::vector<Product*>, bool animated = false);
    
    void initIAP(std::vector<IAP*>, bool exchange = false);
    
    void initPopUp();
    
    void showPopUp(std::string key, bool diamonds = true);
    
    cocos2d::Node* _userInfo;
    cocos2d::Node* _tactics;
    
    cocos2d::Node* _shadow;
    cocos2d::Label* _popUpTitle;
    cocos2d::Label* _popUpMsg;
    cocos2d::Label* _popUpBtnPrice;
    cocos2d::Sprite* _popUpBtnBlue;
    cocos2d::Sprite* _popUpBtnPink;
    std::string _temp_key;
    int _temp_diamonds = 0;
};

#endif