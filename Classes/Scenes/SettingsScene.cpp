#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
#include "Scenes/SettingsScene.h"
#include "Manager/Manager.h"

#else
#include "SettingsScene.h"
#include "Manager.h"

#endif
////////////////////////////

USING_NS_CC;

Scene* SettingsScene::createScene()
{
    auto scene = Scene::create();
    
	auto layer = SettingsScene::create();

    scene->addChild(layer);

    return scene;
}

bool SettingsScene::init()
{
	if ( !LayerColor::initWithColor(Color4B(144,18,209,255)))
	{
		return false;
	}

	_visibleSize = Director::getInstance()->getVisibleSize();
    
    auto title = Label::createWithTTF("SETTINGS", kFontPath + kFontName, 16);
    title->setAnchorPoint(Vec2(0.5f, 0.5f));
    title->setColor(Color3B::WHITE);
    title->setPosition(_visibleSize.width / 2, _visibleSize.height / 2 + 120);
    this->addChild(title);
    
    
    Sprite *button0 = Sprite::createWithSpriteFrameName("store-button-close-0.png");
    Sprite *button0A = Sprite::createWithSpriteFrameName("store-button-close-1.png");
    auto menuItem0 = MenuItemSprite::create(button0, button0A, NULL, CC_CALLBACK_0(SettingsScene::goToMain, this));
    
    menuItem0->setPosition(_visibleSize.width / 2 + 100, _visibleSize.height / 2 + 120);
    
    auto menu = Menu::create(menuItem0, NULL);
    menu->setPosition(0, 0);
    this->addChild(menu);
    
    
    Label* completeAll = Label::createWithTTF("COMPLETE: Puppy Whisperer Level 10", kFontPath + kFontName, 16);
    auto menuComplete = MenuItemLabel::create(completeAll, CC_CALLBACK_0(SettingsScene::complete, this));
    menuComplete->setPosition(_visibleSize.width/ 2, _visibleSize.height / 2 + 80 );
    
    Label* clearAll = Label::createWithTTF("RESET: All Quests and User Level", kFontPath + kFontName, 16);
    auto menuClear = MenuItemLabel::create(clearAll, CC_CALLBACK_0(SettingsScene::reset, this));
    menuClear->setPosition(_visibleSize.width/ 2, _visibleSize.height / 2 + 50 );
    
    Label* clearProducts = Label::createWithTTF("RESET: All Products from Store and Slots", kFontPath + kFontName, 16);
    auto menuClearProducts = MenuItemLabel::create(clearProducts, CC_CALLBACK_0(SettingsScene::resetStore, this));
    menuClearProducts->setPosition(_visibleSize.width/ 2, _visibleSize.height / 2 + 20 );
    
    Label* clearCoins = Label::createWithTTF("RESET: All Puppy Love and Records", kFontPath + kFontName, 16);
    auto menuclearCoins = MenuItemLabel::create(clearCoins, CC_CALLBACK_0(SettingsScene::resetCoins, this));
    menuclearCoins->setPosition(_visibleSize.width/ 2, _visibleSize.height / 2 - 10 );
    
    std::string debugStatus = (Manager::getInstance()->getDebugTools()) ? "ON" : "OFF";
    
    Label* toogleDebugTools = Label::createWithTTF("DEBUG TOOLS: " + debugStatus, kFontPath + kFontName, 16);
    auto menutoogleDebugTools = MenuItemLabel::create(toogleDebugTools, CC_CALLBACK_0(SettingsScene::toogleDebug, this));
    menutoogleDebugTools->setPosition(_visibleSize.width/ 2, _visibleSize.height / 2 - 40 );
    
    
    std::string musicStatus = (Manager::getInstance()->getSoundsLibrary()->isMusicOn()) ? "ON" : "OFF";
    
    Label* toogleMusic = Label::createWithTTF("BACKGROUND MUSIC: " + musicStatus, kFontPath + kFontName, 16);
    auto menutoogleMusic = MenuItemLabel::create(toogleMusic, CC_CALLBACK_0(SettingsScene::toogleBackgroundMusic, this));
    menutoogleMusic->setPosition(_visibleSize.width/ 2 - 90, _visibleSize.height / 2 - 70 );
    
    
    std::string soundStatus = (Manager::getInstance()->getSoundsLibrary()->isSoundOn()) ? "ON" : "OFF";
    
    Label* toogleSound = Label::createWithTTF("SOUND (MASTER): " + soundStatus, kFontPath + kFontName, 16);
    auto menutoogleSound = MenuItemLabel::create(toogleSound, CC_CALLBACK_0(SettingsScene::toogleSound, this));
    menutoogleSound->setPosition(_visibleSize.width/ 2 + 90, _visibleSize.height / 2 - 70 );
    
    auto settings = Menu::create(menuComplete, menuClear, menuClearProducts, menuclearCoins, menutoogleDebugTools, menutoogleMusic, menutoogleSound, NULL);
    settings->setPosition(0,0);
    this->addChild(settings);
    
    
    
/*
	
	if (GameManager::getInstance()->facebookLoggedIn())
	{
		auto title3 = SpriteChars::getInstance()->convertCharacter("character_character", "facebook logout");
		title3->setPosition(Vec2(200 + 30, 50));
		title3->setAnchorPoint(Vec2(0, 0.5f));
		this->addChild(title3);

		Sprite *buttonFacebook = Sprite::createWithSpriteFrameName("test-button.png");
		Sprite *buttonFacebookActive = Sprite::createWithSpriteFrameName("path-1.png");
		auto menuItemFacebook = MenuItemSprite::create(buttonFacebook, buttonFacebookActive, NULL, CC_CALLBACK_0(SettingsScene::facebook, this, false));

		menuItemFacebook->setPosition(200, 50);

		auto menu = Menu::create(menuItemFacebook, NULL);
		menu->setPosition(Vec2(originPoint.x, originPoint.y));
		this->addChild(menu);
	}
	else
	{
		auto title3 = SpriteChars::getInstance()->convertCharacter("character_character", "facebook login");
		title3->setPosition(Vec2(200 + 30, 50));
		title3->setAnchorPoint(Vec2(0, 0.5f));
		this->addChild(title3);

		Sprite *buttonFacebook = Sprite::createWithSpriteFrameName("test-button.png");
		Sprite *buttonFacebookActive = Sprite::createWithSpriteFrameName("path-1.png");
		auto menuItemFacebook = MenuItemSprite::create(buttonFacebook, buttonFacebookActive, NULL, CC_CALLBACK_0(SettingsScene::facebook, this, true));

		menuItemFacebook->setPosition(200, 50);

		auto menu = Menu::create(menuItemFacebook, NULL);
		menu->setPosition(Vec2(originPoint.x, originPoint.y));
		this->addChild(menu);

	}
	*/

    return true;
}


void SettingsScene::goToMain()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
	Manager::getInstance()->runScene(SCENE_MAIN);
}

void SettingsScene::reset()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
    Manager::getInstance()->resetAll();
}

void SettingsScene::resetStore()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
    Manager::getInstance()->resetAllStore();
}

void SettingsScene::resetCoins()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
    Manager::getInstance()->resetCoins();
}

void SettingsScene::complete()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
	Manager::getInstance()->completeAll();
}

void SettingsScene::toogleDebug()
{
    Manager::getInstance()->getSoundsLibrary()->playSound(SOUND_TAP_BACK);
    bool debug = Manager::getInstance()->getDebugTools();
    bool newDebug = debug ? false : true;
    Manager::getInstance()->setDebugTools(newDebug);
    
    Director::getInstance()->setDisplayStats(newDebug);
    
    Manager::getInstance()->runScene(SCENE_SETTINGS);    
    
}


void SettingsScene::toogleBackgroundMusic()
{
    Manager::getInstance()->getSoundsLibrary()->toogleMusic();
    Manager::getInstance()->runScene(SCENE_SETTINGS);
}

void SettingsScene::toogleSound()
{
    Manager::getInstance()->getSoundsLibrary()->toogleSound();
    Manager::getInstance()->runScene(SCENE_SETTINGS);
}


void SettingsScene::facebook(bool login)
{
    /*
     if (login)
     GameManager::getInstance()->facebookLogin();
     else
     GameManager::getInstance()->facebookLogout();
     */
    //GameManager::getInstance()->runScene(SCENE_SETTINGS);
}
