#include "AppDelegate.h"

#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM || CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM)
#include "Manager/Manager.h"

#else
#include "Manager.h"

#endif
////////////////// KU8L74QKMJ




USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

typedef struct tagResource
{
	cocos2d::Size size;
	char directory[100];
}Resource;

static Resource smallResource = { cocos2d::Size(512, 320), "-sd" }; // 3x2
static Resource mediumResource = { cocos2d::Size(1024, 640), "-hd" };
static Resource largeResource = { cocos2d::Size(2048, 1280), "-hdr" };

static cocos2d::Size designResolutionSize = cocos2d::Size(480, 320); // logical size


bool AppDelegate::applicationDidFinishLaunching() {
	// initialize director
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();

	std::string sprite_resource;
	std::string background_resource;
    
	if (!glview) {
		glview = GLViewImpl::create("Puppy Madness");
		//glview = GLViewImpl::createWithRect("Puppy Madness", Rect(200, 200, 960, 640));	// 3x2
		//glview = GLViewImpl::createWithRect("Puppy Madness", Rect(200, 200, 1024, 768));	// 4x3
		//glview = GLViewImpl::createWithRect("Puppy Madness", Rect(200, 200, 480, 360));	// 4x3
		//glview = GLViewImpl::createWithRect("Puppy Madness", Rect(200, 200, 960, 540));	// 16x9	
		//glview = GLViewImpl::createWithRect("Puppy Madness", Rect(200, 200, 1024, 640));	// 16x10	
		//glview = GLViewImpl::createWithRect("Puppy Madness", Rect(200, 200, 1136, 640));	// iPhone 5 (Edge)
		//glview = GLViewImpl::createWithRect("Puppy Madness", Rect(200, 200, 854, 480));	// Sony Xperia (Dex)

		director->setOpenGLView(glview);
	}

	std::vector<std::string> paths;
    paths.push_back("fonts");
    paths.push_back("sounds");
    paths.push_back("music");
    paths.push_back("stages");
    paths.push_back("images");
    paths.push_back("quests");
	FileUtils::getInstance()->setSearchResolutionsOrder(paths);
	FileUtils::getInstance()->setSearchPaths(paths);

	Size frameSize = glview->getFrameSize();

	ResolutionPolicy resolution_policy = ResolutionPolicy::FIXED_HEIGHT;

	if (frameSize.width / frameSize.height < 1.5f)
		resolution_policy = ResolutionPolicy::FIXED_WIDTH;

	glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, resolution_policy);


	if (frameSize.height > mediumResource.size.height)
	{
		sprite_resource = largeResource.directory;
		director->setContentScaleFactor(largeResource.size.height / designResolutionSize.height);
	}
	else if (frameSize.height > smallResource.size.height)
	{
		sprite_resource = mediumResource.directory;
		director->setContentScaleFactor(mediumResource.size.height / designResolutionSize.height);	
	}
	else
	{
		sprite_resource = smallResource.directory;
		director->setContentScaleFactor(smallResource.size.height / designResolutionSize.height);
	}

	float designOrigin = 0;
	if (frameSize.width / frameSize.height < 1.5f)
		designOrigin = (frameSize.height - designResolutionSize.height *  director->getContentScaleFactor() / 2) / (director->getContentScaleFactor() / 2);

    director->setDisplayStats(Manager::getInstance()->getDebugTools());
	director->setAnimationInterval(1.0 / 60);
	register_all_packages();

    CCLOG("Resources from: %s", sprite_resource.c_str());
    
    
    // just for logo
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(kImagesPath + "logo" + sprite_resource + ".plist");
    
	/*SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    
    // new spritesheets
    cache->addSpriteFramesWithFile(kImagesPath + "ui-elements" + sprite_resource + ".plist");
    
    // old spritesheets (needs to be cleared)
    cache->addSpriteFramesWithFile(kImagesPath + "titlescreen" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "titlescreen-background" + sprite_resource + ".plist");
    
    cache->addSpriteFramesWithFile(kImagesPath + "map" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "map-animations-a" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "map-animations-b" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "map-animations-c" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "map-background" + sprite_resource + ".plist");
    
    cache->addSpriteFramesWithFile(kImagesPath + "basket" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "environment" + sprite_resource + ".plist");
    
    cache->addSpriteFramesWithFile(kImagesPath + "spritesheet" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "background" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "background-forest" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "background-snake" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "background-laser" + sprite_resource + ".plist");
    cache->addSpriteFramesWithFile(kImagesPath + "background-hell" + sprite_resource + ".plist");
	*/
    Manager::getInstance()->runScene(SCENE_SPLASH);
	//Manager::getInstance()->runScene(SCENE_PRELOAD);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
