#ifndef PuppyMadness__GGLibrary
#define PuppyMadness__GGLibrary



namespace GGLibrary
{

        std::string intToString(const int nr)
        {
            char charText[100];
            sprintf(charText, "%d", nr);
            std::string str(charText);
            return str;
        }
        std::string intToTimeString(const int nr)
        {
            int min = nr / 60;
            int sec = nr % 60;
            
            char charText[100];
            sprintf(charText, "%01i:%02i", min, sec);
            std::string str(charText);
            return str;
        }

}

#endif
