LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/Game/b2Sprite.cpp \
                   ../../Classes/Game/BackgroundLayer.cpp \
                   ../../Classes/Game/Basket.cpp \
                   ../../Classes/Game/Bobo.cpp \
                   ../../Classes/Game/Bullet.cpp \
                   ../../Classes/Game/Extras.cpp \
                   ../../Classes/Game/GameLayer.cpp \
                   ../../Classes/Game/GLES-Render.cpp \
                   ../../Classes/Game/HUDLayer.cpp \
                   ../../Classes/Game/MyContactListener.cpp \
                   ../../Classes/Game/Platform.cpp \
                   ../../Classes/Game/PowerUp.cpp \
                   ../../Classes/Game/Puppy.cpp \
                   ../../Classes/Game/SimpleDPad.cpp \
                   ../../Classes/Game/Snake.cpp \
                   ../../Classes/Manager/IAP.cpp \
                   ../../Classes/Manager/Manager.cpp \
                   ../../Classes/Manager/SlotHandler.cpp \
                   ../../Classes/Manager/Slots.cpp \
                   ../../Classes/Manager/Quest.cpp \
                   ../../Classes/Manager/QuestHandler.cpp \
                   ../../Classes/Manager/Sounds.cpp \
                   ../../Classes/Manager/Stages.cpp \
                   ../../Classes/Manager/Store.cpp \
                   ../../Classes/Manager/Product.cpp \
                   ../../Classes/Manager/User.cpp \
                   ../../Classes/Scenes/GameScene.cpp \
                   ../../Classes/Scenes/MainScene.cpp \
                   ../../Classes/Scenes/MainScenePopUp.cpp \
                   ../../Classes/Scenes/MainSceneQuests.cpp \
                   ../../Classes/Scenes/MainSceneStage.cpp \
                   ../../Classes/Scenes/MainSceneTactics.cpp \
                   ../../Classes/Scenes/MainSceneUser.cpp \
                   ../../Classes/Scenes/PreloadScene.cpp \
                   ../../Classes/Scenes/SettingsScene.cpp \
                   ../../Classes/Scenes/SplashScene.cpp \
                   ../../Classes/Scenes/StoreScene.cpp \
                   ../../Classes/Utils/Jzon.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Box2D

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END

LOCAL_STATIC_LIBRARIES := cocos2dx_static
LOCAL_STATIC_LIBRARIES += box2d_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
